<?php

return [
    'store_id' => 'Your store id',
    'signature_key' => 'Your signature key',
    'sandbox' => true,
    'redirect_url' => [
        'success' => [
            'route' => 'payment.success'
        ],
        'cancel' => [
            'route' => 'payment.cancel' 
        ]
    ]
];
