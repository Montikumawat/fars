@extends('layout.master')
@section('title', 'Packages')
@section('description', 'Packages of FARS')
@section('keywords', 'admin panel, fars')
@section('author', 'FARS')

@section('css')
    {{-- <!-- BEGIN: Page CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/pickers/form-flat-pickr.css') }}">
<!-- END: Page CSS--> --}}

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/tables/datatable/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/tables/datatable/rowGroup.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">
    <!-- END: Vendor CSS-->
    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
    {{-- <link rel="stylesheet" href="{{asset('plugins/summernote/summernote-bs4.css') }}"> --}}
        <!-- BEGIN: Page CSS-->
        {{-- <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/form-quill-editor.css') }}"> --}}
        <!-- END: Page CSS-->
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">   
    <!-- END: Page CSS-->
    <style>
        @media screen and (max-width: 768px) {
        #package_image {
            width: 242px;
            height: 150px;
          }
        }
        @media screen and (min-width: 768px) {
        #package_image {
            width: 745px;
            height: 560px;
          }
        }
      </style>
@endsection
@section('breadcrumb_title', 'Packages')
@section('breadcrumb_2', 'Packages')
@section('content')

    <section id="multiple-column-form">
        <form class="row" id="users" action="{{ route('superadmin.package.update',$package['id']) }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Edit Package</h4>
                        <div class="col-xl-3 col-md-6 col-12 mb-1">
                            <div class="custom-control custom-control-primary custom-switch">
                                <p class="mb-50"> <b>Package Default</b> </p>
                                <input type="checkbox" name="is_default" @if(isset($package['is_default']) && $package['is_default'] == '1') checked @endif value="{{ $package['is_default'] ?? '' }}" class="custom-control-input"
                                    id="customSwitch3">
                                    
                                    
                                <label class="custom-control-label" for="customSwitch3"></label>
                            </div>
                        </div>

                        <div class="col-xl-3 col-md-6 col-12 mb-1">
                            <div class="custom-control custom-control-primary custom-switch">
                                <p class="mb-50"> <b>Free Trial</b> </p>

                                <input type="checkbox" name="is_free_trial"  @if(isset($package['is_free_trial']) && $package['is_free_trial'] == '1') checked @endif value="{{ $package['is_free_trial'] ?? '' }}" class="custom-control-input"
                                    id="customSwitch4">
                                <label class="custom-control-label" for="customSwitch4"></label>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-4 col-md-6 col-12 mb-1">
                                <div class="form-group">
                                    <label for="inputName"> <b>Package Name</b> </label>
                                    <input type="text" id="inputName" name="name" value="{{ $package['name'] ?? '' }}" class="form-control"
                                        placeholder="Package Name">
                                </div>
                            </div>

                            <div class="col-xl-4 col-md-6 col-12 mb-1">
                                <div class="form-group">
                                    <label for="inputEstimatedBudget"> <b>Package Image (Dimension 745px*560px is best)</b> </label>
                                    <input type="file" name="image"  id="inputEstimatedBudget" class="form-control">
                                </div>
                            </div>
                            <div>
                            {{-- @if(isset($package['image']))
                            <img src="{{asset( $package['image'] )}}" alt="" style="width:90px; border-radius: 50%;" >
                            @else
                            later on add some default image to this 
                           <img src="{{asset( $package['image'] )}}" alt="" style="width:90px; border-radius: 50%;" >
                            @endif --}}
                            </div>

                            <div class="col-xl-4 col-md-6 col-12 mb-1">
                                <div class="form-group">
                                    <label for="inputStatus"> <b>Status</b> </label>
                                    <select id="inputStatus" name="status"  value="{{ $package['status'] ?? '' }}"class="form-control custom-select">
                                      
                                        <option value="0"@if(isset($package['status'])&& $package['status'] == 0) selected @endif>InActive</option>
                                        <option value="1"@if(isset($package['status'])&& $package['status'] == 1) selected @endif>Active</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-xl-12 col-md-12 col-12 mb-1">
                                <label for="inputDescription"> <b>Package Description</b> </label>

                                <textarea id="inputDescription" name="description"  value="{{ $package['description'] }}" class="form-control textarea"
                                    placeholder="Place some text here">{!! $package['description'] ?? '' !!}</textarea>

                            </div>

                            @if(isset($package['image']))
                            @php
                            $image = config('helper.image_url').$package['image']
                            @endphp

                            <div class="col-xl-12 col-md-12 col-12 mb-1">
                                <label for="inputName"> <b>Previous Package Image</b> </label>
                                <br>
                                <img src="{{ $image }}" alt="" id="package_image" onerror="javascript:this.src='{{asset('default/no_image_available.png')}}'">

                            </div>

                            @else
                                <img src="{{asset('default/no_image_available.png')}}" class="" alt="" width="200px;" height="130px;">
                            @endif



                            {{-- <div class="col-xl-12 col-md-6 col-12 mb-1">
                                      <label for="inputSupport">Package Support</label>
                                      <textarea id="inputSupport" name="support" value="{{ ($package->support) }}" class="form-control" rows="4"></textarea>
                                    </div> --}}

                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>


                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Package Detail</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-4 col-md-6 col-12 mb-1">
                                <label for="inputBillingType"> <b>Billing Type</b> </label>
                                <select id="inputBillingType" name="billing_type" value="{{ $package['billing_type'] }}"  class="form-control custom-select">
                                    <option disabled>Select Billing Type</option>
                                    <option value="0" @if(isset($package['billing_type']) && $package['billing_type'] == 0) selected @endif>Monthly</option>
                                    <option value="1" @if(isset($package['billing_type']) && $package['billing_type'] == 1 ) selected @endif>Yearly</option>

                                </select>
                            </div>
                            <div class="col-xl-4 col-md-6 col-12 mb-1">
                                <label for="inputPackageType"> <b>Package Type</b> </label>
                                <select id="inputPackageType" name="package_type" value="{{ $package['package_type'] }}" class="form-control custom-select">
                                    <option selected disabled>Select Package Type</option>
                                    <option value="0" @if(isset($package['package_type'])&& $package['package_type'] == 0) selected @endif>Corporate</option>
                                    <option value="1" @if(isset($package['package_type'])&& $package['package_type'] == 1) Retail @endif >Retail</option>

                                </select>
                            </div>
                            <div class="col-xl-4 col-md-6 col-12 mb-1">
                                <label for="inputCurrency"> <b>Currency</b> </label>
                                <select id="inputCurrency" name="currency" value="{{ $package['currency'] }}" class="form-control custom-select">
                                    <option value="INR">INR</option>
                                    <option value="DOLLAR">DOLLAR</option>
                                </select>
                            </div>

                            <div class="col-xl-4 col-md-6 col-12 mb-1">
                                <label for="inputEstimatedBudget"> <b>Amount</b> </label>
                                <input type="number" name="amount"   value="{{ $package['amount'] ?? '' }}"id="inputEstimatedBudget" class="form-control">
                            </div>
                            <div class="col-xl-4 col-md-6 col-12 mb-1" id="divUsers">
                                <label for="inputUsers"> <b>Number of Users for Facial Recognition (in K)</b> </label>
                                <select id="inputUsers" name="users_for_facial_recognition" value="{{ $package['users_for_facial_recognition'] }}" class="form-control custom-select">
                                    <option  disabled>Select Number of Users</option>
                                    <option value="10" @if(isset($package['users_for_facial_recognition']) && $package['users_for_facial_recognition'] == 10) selected @endif>10k</option>
                                    <option value="100" @if(isset($package['users_for_facial_recognition']) && $package['users_for_facial_recognition'] == 100) selected @endif>100K</option>
                                    <option value="0" @if(isset($package['users_for_facial_recognition']) && $package['users_for_facial_recognition'] == 0) selected @endif>Unlimited</option>
                                    <option value="" @if(isset($package['users_for_facial_recognition']) && $package['users_for_facial_recognition'] != 0 && $package['users_for_facial_recognition'] != 10 && $package['users_for_facial_recognition'] != 100)  selected @endif>Custom</option>
                                </select>
                            </div>

                            <div class="col-xl-4 col-md-6 col-12 mb-1" id="divCustomUsers" @if(isset($package['users_for_facial_recognition']) && $package['users_for_facial_recognition'] != 0 && $package['users_for_facial_recognition'] != 10 && $package['users_for_facial_recognition'] != 100)  style="display: block;" @else style="display: none;" @endif>
                                @if(isset($package['users_for_facial_recognition']) && $package['users_for_facial_recognition'] != '0' && $package['users_for_facial_recognition'] != '10' && $package['users_for_facial_recognition'] != '100')
                                <label for="inputUsers"> <b>Number of Users for Facial Recognition (in K)</b> </label>
                                <input type="number" name="users_for_facial_recognition" value="{{ $package['users_for_facial_recognition'] ?? '' }}" id="inputUsers" class="form-control">
                                @endif
                            </div>

                            <div class="col-xl-4 col-md-6 col-12 mb-1">
                                <label for="inputCheckIns"> <b>Number of CheckIns</b> </label>
                                <input type="number" name="check_ins" value="{{ $package['check_ins'] ?? '' }}" id="inputCheckIns" class="form-control">
                            </div>

                            <div class="col-xl-4 col-md-6 col-12 mb-1" id="divTouchPoint">
                                <label for="inputTouchPoint"> <b>TouchPoint</b> </label>
                                <select id="inputTouchPoint" name="touch_point" value="{{ $package['touch_point'] ?? '' }}" class="form-control custom-select">
                                    <option selected disabled>Select TouchPoint </option>
                                    <option value="0">Front Desk</option>
                                    <option value="1">Admin Portal</option>
                                    <option value="2">Edge Touch Points</option>
                                </select>
                            </div>
                            
                            <div class="col-xl-4 col-md-6 col-12 mb-1" id="divEdgeTouchPoint" @if(isset($package['edge_touch_point']) && $package['edge_touch_point'] != '0' && $package['edge_touch_point'] != 'NULL') style="display: block;" @else style="display: none;" @endif>
                                @if(isset($package['edge_touch_point']) && $package['edge_touch_point'] != '0' && $package['edge_touch_point'] != 'NULL')
                                <label for="inputEdgeTouchPoint"><b>Number of Edge TouchPoint</b></label><input type="number" value="{{ $package['edge_touch_point'] ?? '' }}" name="edge_touch_point" id="inputedgeTouchPoint" class="form-control">
                                @endif
                            </div>

                            <div class="col-xl-4 col-md-6 col-12 mb-1" id="divFrontDesk" @if(isset($package['front_touch_point']) && $package['front_touch_point'] != '0' && $package['front_touch_point'] != 'NULL') style="display: block;" @else style="display: none;" @endif>
                                @if(isset($package['front_touch_point']) && $package['front_touch_point'] != '0' && $package['front_touch_point'] != 'NULL')
                                <label for="inputFrontDesk"> <b>Number of Front Desk TouchPoint</b> </label><input type="number" value="{{ $package['front_touch_point'] ?? '' }}" name="front_touch_point" id="inputFrontDesk" class="form-control">
                                @endif
                            </div>

                            <div class="col-xl-4 col-md-6 col-12 mb-1" id="divAdminPortal" @if(isset($package['admin_touch_point']) && $package['admin_touch_point'] != '0' && $package['admin_touch_point'] != 'NULL') style="display: block;" @else style="display: none;" @endif>
                                @if(isset($package['admin_touch_point']) && $package['admin_touch_point'] != '0' && $package['admin_touch_point'] != 'NULL')
                                <label for="inputAdminPortal"> <b>Number of Admin Portal TouchPoint</b> </label><input type="number" value="{{ $package['admin_touch_point'] ?? '' }}" name="admin_touch_point" id="inputAdminPortal" class="form-control">                               
                                @endif
                            </div>

                            <div class="col-xl-4 col-md-6 col-12 mb-1">
                                <label for="inputBackEndIntegration"> <b>Number of BackEnd Integration</b> </label>
                                <input type="number" name="backend_integration" value="{{ $package['backend_integration'] }}" id="inputBackEndIntegration"
                                    class="form-control">
                            </div>

                            <div class="col-xl-4 col-md-6 col-12 mb-1">
                                <label for="inputSites"> <b>Number of Sites</b> </label>
                                <input type="number" name="sites" value="{{ $package['sites'] }}" id="inputSites" class="form-control">
                            </div>

                            <div class="col-xl-4 col-md-6 col-12 mb-1">
                                <label for="inputRateLimit"> <b>Rate-Limit (Number of Hits Per Second)</b> </label>
                                <input type="number" name="rate_limit" value="{{ $package['rate_limit'] }}" id="inputRateLimit" class="form-control">
                            </div>

                            <div class="col-xl-4 col-md-6 col-12 mb-1">
                                <label for="inputAdminUsers"> <b>Number of Admin Users</b> </label>
                                <input type="number" name="admin_users"  value="{{ $package['admin_users'] }}"id="inputAdminUsers" class="form-control">
                            </div>
                            <div class="col-xl-4 col-md-6 col-12 mb-1">
                                <label for="inputEstimatedDuration"> <b>Package Time(In Months)</b> </label>
                                <input type="number" name="package_time"  value="{{ $package['package_time'] }}" id="inputEstimatedDuration"
                                    class="form-control">
                            </div>

                            <div class="col-xl-4 col-md-6 col-12 mb-1" id="divYesFreeTrial">
                                <label for="inputFreeTrialDays"> <b>Number of days data should be maintained after
                                        completion of Package</b> </label><input type="number"
                                    name="data_maintained_days" value="{{ $package['data_maintained_days'] }}" id="inputFreeTrialDays" class="form-control">
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->


                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Custom Attributes</h4>
                    </div>
                    <div class="card-body">
                        <div class="col-xl-6 col-md-6 col-12 mb-1">
                            <label for="inputName">Press the below button to Add Custom Attributes</label>
                            {{-- <input type="text" id="inputName" name="name" class="form-control"> --}}
                        </div>
                        <div class="col-xl-2 col-md-6 col-12 mb-1">
                            <button type="button" class="btn btn-primary btn-block add" id="add">
                                <i class="fas fa-plus"></i>
                            </button>
                        </div>
                        @php
                        $increase = 0
                        @endphp
                        <div id="custom">
                            {{-- <div class="form-group"><label for="inputName">Name</label><input type="text" name="image" class="form-control"></div><div class="form-group"><label for="inputDescription">Editor</label><textarea id="inputDescription" class="form-control textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea></div><div class="form-group"><label for="inputCategory" >Category</label><input type="text" name="image" class="form-control"></div> --}}
                            <div class="table-responsive" style="width: 100%; margin-top: 36px;">
                                <label for="name"> <b>Custom Fields</b> </label>
                                <table class="table table-bordered" id="dynamic_field">
                                    <tr class="dynamic-added" style="display: flex;flex-direction: column;">
                                        {{-- <td class="name" data-row_id="{{$increase}}">
                                        <label for="">Name</label>
                                        <input type="text" id="name-{{$increase}}" custom1="{{$increase}}"
                                            custom2="{{$increase}}" class="form-control " name="custom['name']"
                                            placeholder="Name" required>
                                        </td>
                                        <td class="value" data-row_id="{{$increase}}">
                                            <label for="">Value</label>
                                            <textarea id="value-{{$increase}}" custom1="{{$increase}}"
                                                custom2="{{$increase}}" class="form-control textarea" name="custom['value']"
                                                placeholder="Value" required cols="30" rows="10"></textarea>

                                        </td>
                                        <td id="{{$increase}}" class="category" data-row_id="{{$increase}}">
                                            <label for="">Category</label>
                                            <input type="text" id="category-{{$increase}}" name="custom['category']"
                                                class="form-control" placeholder="Category" required>
                                        </td>
                                        <td><button type="button" name="minus" id="{{$increase}}"
                                                title="Remove Custom Field" class="btn btn-danger btn-m btn_remove"><i
                                                    class="fa fa-minus"></i></button></td>

                                        @php $increase++ @endphp --}}
                                    </tr>
                                    @if($customs != '' && count($customs) > 0)
                                    @foreach($customs as $custom)
                                    <tr id="row{{$increase}}" class="dynamic-added" style="display: flex;flex-direction: column; margin-bottom: 20px;" >
                                        <td><button type="button" name="minus" id="{{$increase}}" title="Remove Custom Field" class="btn btn-danger btn-m btn_remove" style="float: right;"><i class="fa fa-minus"></i></button></td>
                                                    <td class="name" data-row_id={{$increase}}>
                                                      <div class="row">
                                                        <div class="col-lg-6">
                                                          <label for="">Name</label>                     
                                                          <input type="text" name="custom[{{$increase}}][name]" id="name-{{$increase}}" value="{{ $custom->name ?? '' }}" custom1="{{$increase}}"  custom2="{{$increase}}" class="form-control " name="name[]" placeholder="Name" required>
                                                        </div>
                            
                                                        <div class="col-lg-6">
                                                          <label for="">Category</label>
                                                          <input type="text" id="category-{{$increase}}" value="{{ $custom->category ?? '' }}" name="custom[{{$increase}}][category]" class="form-control" placeholder="Category" required>
                                                        </div>
                            
                                                        <div class="col-lg-12">
                                                        <label for="">Value</label>
                                                        <textarea id="value-{{$increase}}" custom1="{{$increase}}" value="{{ $custom->value ?? '' }}" name="custom[{{$increase}}][value]" custom2="{{$increase}}" class="form-control textarea" name="value[]" placeholder="Value" required cols="30" rows="10">
                                                        {!!  $custom->value ?? '' !!}
                                                        </textarea>
                                                        </div>
                                                      </div>
                                                    </td>
                                                    
                                                    @php $increase++ @endphp      
                                    </tr>
                                    @endforeach
                                    @endif
                                </table>


                            </div>
                        </div>
                    </div>
                    <!-- /.card -->

                </div>

            </div>
        </div>
        <div></div>
        <div> <button type="submit" class="btn btn-primary" style="float:left;">Update</button>                
        </form> 
    </section>
@endsection



@section('script')

<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>

{{-- <script src="{{asset('plugins/summernote/summernote-bs4.min.js') }}"></script>--}}
<script>
  $(function () {
    // Summernote
    $('.textarea').summernote()
  })
</script> 

<script>
    $(document).ready(function(){
      var postURL = "<?php echo url('addmore'); ?>";
      var i=0;
      var table_row =  {{$increase}};
// console.log(table_row);
      $('#add').click(function(){
             i++;
             var html ;
          html = `<tr id="row` + i + `" class="dynamic-added" style="display: flex;flex-direction: column; margin-bottom: 20px;" >
            <td>
            <button type="button" name="minus" id="` + table_row + `" title="Remove Custom Field" class="btn btn-danger btn-m btn_remove" style="float: right;"><i class="fa fa-minus"></i></button>
            
            </td>
                        <td class="name" data-row_id=` + table_row + `>
                          <div class="row">
                            <div class="col-lg-6">
                              <label for="">Name</label>                     
                              <input type="text" id="name-` + table_row + `" custom1="` + table_row + `"  custom2="` +
            table_row + `" class="form-control " name="custom[` + table_row + `][name]" placeholder="Name" required>
                            </div>

                            <div class="col-lg-6">
                              <label for="">Category</label>
                              <input type="text" id="category-` + table_row + `" name="custom[` + table_row + `][category]" class="form-control" placeholder="Category" required>
                            </div>

                            <div class="col-lg-12">
                            <label for="">Value</label>
                            <textarea id="value-` + table_row + `" custom1="` + table_row + `"  custom2="` +
            table_row + `" class="form-control textarea" name="custom[` + table_row + `][value]" placeholder="Value" required cols="30" rows="10"></textarea>
                            </div>
                          </div>
                        </td>
                        <td>
                            <button type="button" class="btn btn-primary btn-m "  onclick="myFunction()" style="float: right;">
                             <i class="fas fa-plus"></i>
                            </button>
                        </td>
                        
                    </tr> <br> <br> <br>
                    @php $increase++ @endphp `   
           $('#dynamic_field').append(html);
           $("#custom").find('.textarea').summernote()
           r=$('#dynamic_field .dynamic-added').length;

            if(r==5){
                $('#add').prop('disabled', true);
            }
            table_row++;
      });
      $(document).on('click', '.btn_remove', function(){
           var button_id = $(this).attr("id");
            // console.log(button_id);
           $('#row'+button_id+'').remove();
           r=$('#dynamic_field .dynamic-added').length;
            if(r<5){
                $('#add').prop('disabled', false);
            }
      });
    });    
</script>
<!-- END: Page Vendor JS-->

<script>
  $(document).ready(function(){
    $('#inputUsers').on('change', function() {
      console.log(this.value);
      if(isset($package['users_for_facial_recognition']) && $package['users_for_facial_recognition'] != '0' && $package['users_for_facial_recognition'] != '10' && $package['users_for_facial_recognition'] != '100'){
      if ( this.value == '')
      //.....................^.......
      {
        $("#divCustomUsers").html('<label for="inputUsers"> <b>Number of Users for Facial Recognition (in K)</b> </label><input type="number" name="users_for_facial_recognition" value="{{ $package['users_for_facial_recognition'] ?? `` }}" id="inputUsers" class="form-control">');
        $("#divCustomUsers").css('display','block')
      }
    else {
        // $("#divUsers").html('<label for="inputUsers">Number of Users for Facial Recognition</label><select id="inputUsers"  name="users" class="form-control custom-select"><option selected disabled>Select Number of Users</option><option value="10">10k</option><option value="100">100K</option><option value="custom">Custom</option></select>');
          $("#divCustomUsers").html('')
    }
      }
    });
    $('#inputFreeTrial').on('change', function() {
      // console.log(this.value);
      if ( this.value == '1')
      //.....................^.......
      {
        // $("#divYesFreeTrial").html('<label for="inputFreeTrialDays">Number of days data should be maintained after completion of Free Trial</label><input type="number" name="freeTrialDays" id="inputFreeTrialDays" class="form-control">');
      }
    else {
        // $("#divUsers").html('<label for="inputUsers">Number of Users for Facial Recognition</label><select id="inputUsers"  name="users" class="form-control custom-select"><option selected disabled>Select Number of Users</option><option value="10">10k</option><option value="100">100K</option><option value="custom">Custom</option></select>');
          $("#divYesFreeTrial").html('')
    }
    });
  
    $('#inputTouchPoint').on('change', function() {
      //  console.log(this.value);
      if ( this.value == '2')
      //.....................^.......
      {
        $("#divEdgeTouchPoint").html('<label for="inputEdgeTouchPoint"><b>Number of Edge TouchPoint</b></label><input type="number" name="edge_touch_point" id="inputedgeTouchPoint" class="form-control">');
        $("#divEdgeTouchPoint").css('display','block')  
        // $("#divFrontDesk").html('')
          // $("#divAdminPortal").html('')
      }
      else if ( this.value == '0')
      //.....................^.......
      {
        $("#divFrontDesk").html('<label for="inputFrontDesk"> <b>Number of Front Desk TouchPoint</b> </label><input type="number" name="front_touch_point" id="inputFrontDesk" class="form-control">');
        $("#divFrontDesk").css('display','block')
        // $("#divEdgeTouchPoint").html('')
        //   $("#divAdminPortal").html('')
      }
      else if ( this.value == '1')
      //.....................^.......
      {
        $("#divAdminPortal").html('<label for="inputAdminPortal"> <b>Number of Admin Portal TouchPoint</b> </label><input type="number" name="admin_touch_point" id="inputAdminPortal" class="form-control">');
        $("#divAdminPortal").css('display','block')
        // $("#divEdgeTouchPoint").html('')
        //   $("#divFrontDesk").html('')
      }
    else {
        // $("#divUsers").html('<label for="inputUsers">Number of Users for Facial Recognition</label><select id="inputUsers"  name="users" class="form-control custom-select"><option selected disabled>Select Number of Users</option><option value="10">10k</option><option value="100">100K</option><option value="custom">Custom</option></select>');
          $("#divEdgeTouchPoint").html('')
          $("#divFrontDesk").html('')
          $("#divAdminPortal").html('')
    }
    });
   
  });
  </script>
  {{-- <script src="https://cdn.quilljs.com/1.3.6/quill.min.js" type="text/javascript"></script>
  <script>
    var quill = new Quill('#editor', {
      modules: {
          toolbar: '#toolbar'
      },
      theme: 'bubble'
  });
  </script> --}}
  <script>
 
    function myFunction() {
   //   console.log("Hello World");
     $(".add").trigger("click")
   }
   </script>
@endsection
