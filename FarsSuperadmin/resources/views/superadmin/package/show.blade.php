@extends('layout.master')
@section('title', 'Packages')
@section('description', 'Packages of FARS')
@section('keywords', 'admin panel, fars')
@section('author', 'FARS')

@section('css')
    {{-- <!-- BEGIN: Page CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/pickers/form-flat-pickr.css') }}">
<!-- END: Page CSS--> --}}

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/tables/datatable/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/tables/datatable/rowGroup.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">
    <!-- END: Vendor CSS-->
    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
    <link rel="stylesheet" href="{{asset('plugins/summernote/summernote-bs4.css') }}">
    <!-- END: Page CSS-->
    <style>
      @media screen and (max-width: 768px) {
      #package_image {
          width: 242px;
          height: 150px;
        }
      }
      @media screen and (min-width: 768px) {
      #package_image {
          width: 745px;
          height: 560px;
        }
      }
    </style>
@endsection
@section('breadcrumb_title', 'Packages')
@section('breadcrumb_2', 'Packages Detail')

@section('content')


    <section id="multiple-column-form">
                <div class="col-lg-12">
                      <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Package Details 
                            @if(isset($package['image']))
                              @php
                              $image = config('helper.image_url').$package['image']
                              @endphp
                              <img src="{{ $image }}" onerror="javascript:this.src='{{asset('default/no_image_available.png')}}'" alt="" id="package_image">
                            @else
                              <img src="{{asset('default/no_image_available.png')}}" class="" alt="" width="200px;" height="130px;">
                            @endif
                            </h4>
                        </div>
                          <div class="card-body">
                            <div class="table-responsive" >
                                <table class="table table-hover table-striped">
                               
                                  <tbody>
                                  <tr>
                                      <th scope="row">Package Name</th>
                                      <td>{{$package['name']  ?? '' }}</td>
                                  </tr>
                  
                                  {{-- <tr>
                                      <th scope="row">Package Image</th>
                                      <td><img src="{{asset( $package->image )}}" alt="" style="width:90px; border-radius: 50%;" ></td>
                                  </tr> --}}
                                  <tr>
                                      <th scope="row">Status</th>
                                      <td> @if(isset($package['status'])&& $package['status'] == 0) Inactive @else Active @endif </td>
                                  </tr>
                                  <tr>
                                      <th scope="row">Package Support</th>
                                      <td>{{$package['support']  ?? '' }}</td>
                                  </tr>
                                  <tr>
                                      <th scope="row">Package Default Status</th>
                                      <td> @if(isset($package['is_default'])&& $package['is_default'] == 0) not_default @else default @endif</td>
                                  </tr>
                                  
                                  <tr>
                                      <th scope="row">Free Trial</th>
                                      <td>  @if(isset($package['is_free_trial'])&& $package['is_free_trial'] == 0) not_free_trial @else free_trial @endif</td>
                                  </tr>
                
                                  <tr>
                                    <th scope="row">Billing Type</th>
                                    <td>@if(isset($package['billing_type'])&& $package['billing_type'] == 0) Monthly @else yearly @endif</td>
                                  </tr>
                
                                  <tr>
                                    <th scope="row">Package Type</th>
                                    <td>  @if(isset($package['package_type'])&& $package['package_type'] == 0) Corporate @else Retail @endif</td>
                                  </tr>
                
                                  <tr>
                                    <th scope="row">Currency</th>
                                    <td>{{$package['currency'] ?? ''}}</td>
                                  </tr>
                
                                  <tr>
                                    <th scope="row">Amount</th>
                                    <td>{{$package['amount']  ?? '' }}</td>
                                  </tr>
                
                                  <tr>
                                    <th scope="row">Number of Users for Facial Recognition (in K)</th>
                                    <td>{{$package['users_for_facial_recognition']	?? '' }}</td>
                                  </tr>
                
                                  <tr>
                                    <th scope="row">No. of Checkins</th>
                                    <td>{{$package['check_ins'] ?? '' }}</td>
                                  </tr>
                
                                  <tr>
                                    <th scope="row">Front Desk TouchPoints</th>
                                    <td>{{$package['front_touch_point']  ?? '' }}</td>
                                  </tr>
                                  
                                  
                                  <tr>
                                    <th scope="row">Admin Portal TouchPoints</th>
                                    <td>{{$package['admin_touch_point']	}}</td>
                                  </tr>
                
                                  <tr>
                                    <th scope="row">Edge Touch Points</th>
                                    <td>{{$package['edge_touch_point']  ?? '' }}</td>
                                  </tr>
                
                                  <tr>
                                    <th scope="row">Data maintained days</th>
                                    <td>{{$package['data_maintained_days']  ?? '' }}</td>
                                  </tr>
                
                                  <tr>
                                    <th scope="row">Number of BackEnd Integration</th>
                                    <td>{{$package['backend_integration']  ?? '' }}</td>
                                  </tr>
                
                                  <tr>
                                    <th scope="row">Number of Sites</th>
                                    <td>{{$package['sites']  ?? '' }}</td>
                                  </tr>
                
                                  <tr>
                                    <th scope="row">Rate-Limit</th>
                                    <td>{{$package['rate_limit']  ?? '' }}</td>
                                  </tr>
                
                                  <tr>
                                    <th scope="row">Number of Admin Users</th>
                                    <td>{{$package['admin_users'] ?? '' }}</td>
                                  </tr>
                
                                  <tr>
                                    <th scope="row">Package Time</th>
                                    <td>{{$package['package_time']  ?? '' }}</td>
                                  </tr>
                                  
                                  </tbody>
                
                              </table>
                            </div>  
                          </div>
                          <!-- /.card-body -->
                        <!-- /.card -->
                      </div>
                      <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Package Description</h4>
                        </div>
                          <div class="card-body">
                          {!! $package['description']  ?? '' !!}
                          </div>
                          <!-- /.card-body -->
                        <!-- /.card -->
                      </div>

                     @if( $customs != '' && count($customs) > 0)
                     @foreach($customs as $key => $custom)
                     <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Package Custom Fields - {{ $key+1 }}</h4>
                        </div>
                          <div class="card-body">
                            <div class="table-responsive" >
                              <table class="table table-hover table-striped">
              
                                <tbody>
                                <tr>
                                    <th>Name</th>
                                    <td>{{ $custom->name ?? '' }} </td>
                                    <th>Category</th>
                                    <td> {{ $custom->category ?? '' }} </td>
                                </tr>
                               
                                <tr>
                                  {{-- <th scope="row">Value</th>
                                  <td>Value</td> --}}

                                </tr>
                                </tbody>
              
                               </table>
                            </div>
                            <div class="card">
                              <div class="card-header">
                                  <h4 class="card-title">Value</h4>
                              </div>
                                <div class="card-body">
                                    {!! $custom->value ?? '' !!}
                                </div>
                                <!-- /.card-body -->
                              <!-- /.card -->
                            </div>
                          </div>
                          <!-- /.card-body -->
                        <!-- /.card -->
                      </div>
                     @endforeach
                     @endif

                      
                </div>
    </section>
@endsection



@section('script')



@endsection
