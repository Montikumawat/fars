@extends('layout.master')
@section('title', 'Packages')
@section('description', 'Packages of FARS')
@section('keywords', 'admin panel, fars')
@section('author', 'FARS')

@section('css')
<link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">

    <!-- BEGIN: Vendor CSS-->
    <style>
        #example_filter {
            float: right;
        }
        #example_paginate {
            float: right;
        }
            /* width */
     ::-webkit-scrollbar {
     width: 10px;
     }

     /* Track */
     ::-webkit-scrollbar-track {
     background: #f1f1f1;

     }

     /* Handle */
     ::-webkit-scrollbar-thumb {
     background: #958cf4;
     border-radius: 10px;
     }

     /* Handle on hover */
     ::-webkit-scrollbar-thumb:hover {
     background: #958cf4;
     }
      </style>

    <style>
    .tooltipDescription {
      position: relative;
      display: inline-block;
      border-bottom: 1px dotted black;
    }

    .tooltipDescription .tooltiptext {
      visibility: hidden;
      width: 120px;
      background-color: black;
      color: #fff;
      text-align: center;
      border-radius: 6px;
      padding: 5px 0;

      /* Position the tooltip */
      position: absolute;
      z-index: 1;
    }

    .tooltipDescription:hover .tooltiptext {
      visibility: visible;
    }
    #apply_filters {
      margin-top: 23px;
    }
    </style>
    <!-- END: Page CSS-->
@endsection
@section('breadcrumb_title', 'Packages')
@section('breadcrumb_2', 'Packages')
@section('content')
    @section('content')
    <!-- Basic Tables start -->
    <div class="row" id="basic-table">
      <div class="col-12">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title">Packages Management</h4>
                  <a href="{{route('superadmin.package.add')}}" class="btn btn-secondary" style="float: right;">Add Packages</a>
              </div>
              <div class="table-responsive container-fluid">
                <form class="row" action={{route('superadmin.package.filter')}} method="get">
                    <div class="col-lg-3">
                      <div class="form-group">
                        <label for="status"> <b>Filter By Status</b> </label>
                        <select class="form-control" name="status">
                          <option value="">Filter By Status</option>
                          <option value="0" @if(isset($status) && $status == '0') selected @endif>Inactive</option>
                          <option value="1" @if(isset($status) && $status == '1') selected @endif>Active</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-lg-3">
                      <div class="form-group">
                        <label for="package_type"> <b>Filter By Package Type</b> </label>
                        <select class="form-control" name="package_type">
                            <option value="">Filter By Package Type</option>
                            <option value="0" @if(isset($package_type) && $package_type == '0') selected @endif  >Corporate</option>
                            <option value="1" @if(isset($package_type) && $package_type == '1') selected @endif >Retail</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                          <button class="btn btn-success" id="apply_filters" type="submit">Apply Filters</button>
                        </div>
                    </div>
                </form>
                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">

                      <thead>
                        <tr>
                           <th>Title </th>
                           <th>Description </th>
                           <th>Package Type </th>
                           <th>Price </th>
                           <th>Status </th>
                           <th>Default Status </th>
                           <th>Free Status </th>
                           <th>No. of Facial Recognition </th>
                           <th>No. of CheckIns </th>
                           <th>Action </th>
                        </tr>
                      </thead>
                      <tbody>
                          {{-- {{dd($packages)}} --}}
                        @if($packages != '' && count($packages) > 0)
                      @foreach($packages as $key => $value)
                          <tr>

                          <td >{{$value['name'] ?? ''}}</td>
                   <td>
                    <div class="tooltipDescription">Description
                      <span class="tooltiptext">
                      {!! $value['description'] ?? '' !!}
                      </span>
                    </div>

                   </td>
                   <td> @if(isset($value['package_type'])&& $value['package_type'] == 0) Corporate @else Retail @endif</td>
                   <td>{{$value['amount']}} </td>
                   <td>@if(isset($value['status'])&& $value['status'] == 0) Inactive @else Active @endif</td>
                   <td>@if(isset($value['is_default'])&& $value['is_default'] == 0) not_default @else default @endif</td>
                   <td>  @if(isset($value['is_free_trial'])&& $value['is_free_trial'] == 0) not_free_trial @else free_trial @endif</td>
                   <td>{{$value['users_for_facial_recognition'] ?? ''}}</td>
                   <td>{{$value['check_ins'] ?? ''}}</td>




                                <td>
                                  <div class="dropdown">
                                      <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                                          <i class="fas fa-ellipsis-v"></i>
                                      </button>
                                      <div class="dropdown-menu">
                                        <a class="dropdown-item" href="{{route('superadmin.user.package.show',$value['id'])}}">
                                            <i data-feather="view" class="mr-50"></i>
                                            <span>View</span>
                                        </a>
                                          <a class="dropdown-item" href="{{route('superadmin.user.package.edit',$value['id'])}}">
                                              <i data-feather="edit-2" class="mr-50"></i>
                                              <span>Edit</span>
                                          </a>
                                          <a class="dropdown-item actionDeletePackage" data-url="{{ route('superadmin.package.destroy',$value['id']) }}" custom="{{$value['id']}}" href="javascript:void(0);">
                                              <i data-feather="trash" class="mr-50"></i>
                                              <span>Delete</span>
                                          </a>
                                          </div>
                                          </div>
                                  </td>
                                          </tr>
                                          <form method="post"
                                          action="{{ route('superadmin.package.destroy',$value['id']) }}"
                                              class="packageDeleteForm">
                                              @csrf
                                              @method('DELETE')

                                          </form>
                          @endforeach
                          @endif
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
  </div>
  <!-- Basic Tables end -->
@endsection

@endsection

@section('script')
    <!-- BEGIN: Page JS-->
    <script src="{{ asset('app-assets/js/scripts/pages/dashboard-ecommerce.js') }}"></script>
    <!-- END: Page JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/responsive.bootstrap4.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/vfs_fonts.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.rowGroup.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
        <!-- END: Page Vendor JS-->
        <script>
       function EuToUsCurrencyFormat(input) {
	return input.replace(/[,.]/g, function(x) {
		return x == "," ? "." : ",";
	});
}

$(document).ready(function() {
	//Only needed for the filename of export files.
	//Normally set in the title tag of your page.
	// document.title = 'DataTable Excel';
	// DataTable initialisation
	$('#example').DataTable({
    "aaSorting": [],
    bPaginate: true,
    iDisplayLength: 10
		// "dom": '<"dt-buttons"Bf><"clear">lirtp',
		// "paging": true,
		// "autoWidth": true,
		// "buttons": [{
		// 	extend: 'excelHtml5',
		// 	text: 'Excel',
		// 	customize: function(xlsx) {
		// 		var sheet = xlsx.xl.worksheets['sheet1.xml'];
		// 		//All cells
		// 		$('row c', sheet).attr('s', '25');
		// 		//Second column
		// 		$('row c:nth-child(2)', sheet).attr('s', '42');
		// 		//First row
		// 		$('row:first c', sheet).attr('s', '36');
		// 		// One cell
		// 		$('row c[r^="D6"]', sheet).attr('s', '32');
		// 		// Loop over the cells in column `E` the amount column
		// 		$('row c[r^="E"]', sheet).each(function() {
		// 			if (parseFloat(EuToUsCurrencyFormat($('is t', this).text())) > 1500) {
		// 				$(this).attr('s', '17');
		// 			}
		// 		});
		// 		//All cells of row 10
		// 		$('row c[r*="10"]', sheet).attr('s', '49');
		// 		//Search all cells for a specific text
		// 		$('row* c[r]', sheet).each(function() {
		// 			if ($('is t', this).text().match(/(?:^|\b)(cover)(?=\b|$)/gmi)) {
		// 				$(this).attr('s', '20');
		// 			}
		// 		});
		// 	}
		// }]
	});
});

    </script>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    // Delete Landing Page
    $(".actionDeletePackage").on('click', function(e) {
        e.preventDefault();

        var element = $(this);
        var url = element.attr('data-url');
        var custom = element.attr('custom');
        console.log(custom);
        console.log(url);

        element.blur();


        //pop up
        swal({
                title: 'Are you sure you want to Delete',
                icon: "warning",
                buttons: true,
                dangerMode: true,
                showLoaderOnConfirm: true,
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: 'Confirm',
                cancelButtonText: 'Cancel',
                closeOnConfirm: false,
            })
            .then((willDelete) => {
                if (willDelete) {
                    swal("Your Item has been deleted!", {
                        icon: "success",
                    });
                     window.location.href = url;
                    // $('.packageDeleteForm').submit();
                } else {
                    swal("Your Item is safe!");
                }
            });


    });

</script>
@endsection
