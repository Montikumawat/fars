@extends('layouts.app')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>User Edit</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            {{-- <li class="breadcrumb-item active"><a href="#">User Update</a></li> --}}
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <form class="row" id="user" action="{{route('admin.users.update',$user->id)}}" method="POST" enctype="multipart/form-data">
       @csrf
      <div class="col-md-6">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">General</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
              </button>
            </div>
          </div>
          <div class="card-body">
            <div class="form-group">
              <label for="inputName">User Name</label>
              <input type="text" id="inputName" value="{{$user->name ?? ''}}" name="name" class="form-control" required>
            </div>
            <div class="form-group">
              <label for="inputDescription">User Email</label>
              <textarea id="inputDescription" name="email" class="form-control" rows="4" required>{{$user->email ?? ''}}</textarea>
            </div>
            <div class="form-group">
              <label for="inputStatus">Status</label>
              <select id="inputStatus"  name="status" class="form-control custom-select" required>
                <option >Status</option>
                <option value="1" {{$user->status==1 ? 'selected':''}}>Active</option>
                <option value="0" {{$user->status==0 ? 'selected':''}}>In Active</option>
                
              </select>
            </div>
            {{--  <div class="form-group">
              <label for="inputClientCompany">Amount</label>
              <input type="text" id="inputClientCompany" class="form-control">
            </div>
            <div class="form-group">
              <label for="inputuserLeader">user Leader</label>
              <input type="text" id="inputuserLeader" class="form-control">
            </div>  --}}
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>

      <div class="col-md-6">
        <div class="card card-secondary">
          <div class="card-header">
            <h3 class="card-title">Images</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
              </button>
            </div>
          </div>
          <div class="card-body">
            {{-- <div class="form-group">
              <label for="inputEstimatedBudget">Password</label>
              <input type="password" name="password" id="inputEstimatedBudget" class="form-control">
            </div> --}}
            {{-- {{dd($user->f_image)}} --}}
            <div class="form-group">
              <label for="inputImage">Images</label>
              <input type="file" id="file"  onchange="loadFilef(event)" name="images[]" multiple id="inputImage" class="form-control" required>
            </div>
            
            <?php 
                 $image = explode(',',$image->images);
             ?>

            @foreach ($image as $img)
            <br>                          
                <img src="{{ URL::to('/assets/uploads/'.$img)}}" style="width:100px"></br>
            </a>
        @endforeach
            {{-- <div class="form-group">
              <label for="inputImage">Left Image</label>
              <input type="file" name="l_image"  onchange="loadFilel(event)" id="l_image" class="form-control" required>
            </div>
            <img src="{{ asset($image->l_image ?? '')}}" id="output1" width="100" />	 --}}

            {{-- <div class="form-group">
              <label for="inputImage">Right Image</label>
              <input type="file" name="r_image" onchange="loadFiler(event)" id="r_image" class="form-control" required>
            </div>
            <img src="{{ asset($image->r_image ?? '')}}" id="output2" width="100" />	 --}}


            {{-- <div class="form-group">
              <label for="inputImage">Up Image</label>
              <input type="file" name="up_image" onchange="loadFileup(event)" id="up_image" class="form-control" required>
            </div>
            <img src="{{ asset($image->up_image ?? '')}}" id="output3" width="100" />	 --}}


            {{-- <div class="form-group">
              <label for="inputImage">Down Image</label>
              <input type="file" name="do_image" onchange="loadFiledo(event)" id="do_image" class="form-control" required>
            </div>
            <img src="{{ asset($image->do_image ?? '')}}" id="output4" width="100" />	 --}}


            {{-- <div class="form-group">
              <label for="inputImage">With specs Image</label>
              <input type="file" name="ws_image" onchange="loadFilews(event)" id="ws_image" class="form-control" required>
            </div>
            <img src="{{ asset($image->ws_image ?? '')}}" id="output5" width="100" />	 --}}


            {{-- <div class="form-group">
              <label for="inputImage">With Hat/cap Image</label>
              <input type="file" name="wh_image" onchange="loadFilewh(event)" id="wh_image" class="form-control" required>
            </div>
            <img src="{{ asset($image->wh_image ?? '')}}" id="output6" width="100" />	 --}}


            {{-- <div class="form-group">
              <label for="inputImage">With Beard Image</label>
              <input type="file" name="wb_image" onchange="loadFilewb(event)" id="wb_image" class="form-control" required>
            </div>
            <img src="{{ asset($image->wb_image ?? '')}}" id="output7" width="100" />	 --}}

            
            {{-- <div class="form-group">
              <label for="inputImage">Auto Add Beard Image</label>
              <input type="file" name="auto_b_image" onchange="loadFileb(event)" id="auto_b_image" class="form-control" required>
            </div>
            <img src="{{ asset($image->auto_b_image ?? '')}}" id="output8" width="100" />	 --}}

         
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>

      {{--  <div class="col-md-6">
        <div class="card card-secondary">
          <div class="card-header">
            <h3 class="card-title">user Detail</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
              </button>
            </div>
          </div>
          <div class="card-body">
            <div class="form-group">
              <label for="inputEstimatedBudget">Amount</label>
              <input type="number" name="amount" value="{{$user->amount}}" id="inputEstimatedBudget" class="form-control">
            </div>
            <div class="form-group">
             
              <label for="inputStatus">user Type</label>
              <select id="inputStatus"  name="user_type" class="form-control custom-select">
                <option selected disabled>Select One</option>
                @foreach (Config::get('usertype') as $key => $type)
                <option value="{{$key}}" {{$user->user_type==$key ? 'selected':''}}>{{$type}}</option>
                   
               @endforeach 
                
                
              </select>
            </div>
            <div class="form-group">
              <label for="inputEstimatedDuration">user Time(In Months)</label>
              <input type="number" name="user_time" value="{{$user->user_time}}" id="inputEstimatedDuration" class="form-control">
            </div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>  --}}
    
    </form>
    <div class="row">
      <div class="col-12">
        <a href="#" class="btn btn-secondary">Cancel</a>
        <button type="submit" onclick="document.getElementById('user').submit()"  class="btn btn-success float-right">Update user</button>
      </div>
    </div>
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('page_script')

<script>
  var loadFilef = function(event) {
    var image = document.getElementById('output');
    image.src = URL.createObjectURL(event.target.files[0]);
  };
  </script>

{{-- <script>
  var loadFilel = function(event) {
    var image = document.getElementById('output1');
    image.src = URL.createObjectURL(event.target.files[0]);
  };
  </script> --}}
  
  {{-- <script>
    var loadFiler = function(event) {
      var image = document.getElementById('output2');
      image.src = URL.createObjectURL(event.target.files[0]);
    };
    </script>

<script>
  var loadFileup = function(event) {
    var image = document.getElementById('output3');
    image.src = URL.createObjectURL(event.target.files[0]);
  };
  </script>

<script>
  var loadFiledo = function(event) {
    var image = document.getElementById('output4');
    image.src = URL.createObjectURL(event.target.files[0]);
  };
  </script>

<script>
  var loadFilews = function(event) {
    var image = document.getElementById('output5');
    image.src = URL.createObjectURL(event.target.files[0]);
  };
  </script>

<script>
  var loadFilewh = function(event) {
    var image = document.getElementById('output6');
    image.src = URL.createObjectURL(event.target.files[0]);
  };
  </script>

<script>
  var loadFilewb = function(event) {
    var image = document.getElementById('output7');
    image.src = URL.createObjectURL(event.target.files[0]);
  };
  </script>

<script>
  var loadFileb = function(event) {
    var image = document.getElementById('output8');
    image.src = URL.createObjectURL(event.target.files[0]);
  };
  </script> --}}

@endsection
