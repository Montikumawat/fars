@extends('layouts.app')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>users Add</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            {{-- <li class="breadcrumb-item active"><a href="#">User Add</a></li> --}}
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <form class="row" id="users" action="{{route('superadmin.users.store')}}" method="POST" enctype="multipart/form-data">
       @csrf
      <div class="col-md-6">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">General</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
              </button>
            </div>
          </div>
          <div class="card-body">
            <div class="form-group">
              <label for="inputName">User Name</label>
              <input type="text" id="inputName" name="name" class="form-control">
            </div>
            <div class="form-group">
              <label for="inputDescription">User Email</label>
              <input type="email" id="inputName" name="email" class="form-control">
            </div>
            <div class="form-group">
              <label for="inputStatus">Status</label>
              <select id="inputStatus"  name="status" class="form-control custom-select">
                <option selected disabled>Status</option>
                <option value="1">Active</option>
                <option value="0" >In Active</option>
                
              </select>
            </div>
            {{--  <div class="form-group">
              <label for="inputClientCompany">Amount</label>
              <input type="text" id="inputClientCompany" class="form-control">
            </div>
            <div class="form-group">
              <label for="inputusersLeader">users Leader</label>
              <input type="text" id="inputusersLeader" class="form-control">
            </div>  --}}
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <div class="col-md-6">
        <div class="card card-secondary">
          <div class="card-header">
            <h3 class="card-title">User Detail</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
              </button>
            </div>
          </div>
          <div class="card-body">
            <div class="form-group">
              <label for="inputEstimatedBudget">Password</label>
              <input type="password" name="password" id="inputEstimatedBudget" class="form-control">
            </div>

            {{-- <div class="form-group">
              <label for="inputImage">Front Image</label>
              <input type="file" id="file"  onchange="loadFilef(event)" name="f_image" id="inputImage" class="form-control" required>
            </div>
            <img id="output" width="100" />	

            <div class="form-group">
              <label for="inputImage">Left Image</label>
              <input type="file" name="l_image"  onchange="loadFilel(event)" id="l_image" class="form-control" required>
            </div>
            <img id="output1" width="100" />	

            <div class="form-group">
              <label for="inputImage">Right Image</label>
              <input type="file" name="r_image" onchange="loadFiler(event)" id="r_image" class="form-control" required>
            </div>
            <img id="output2" width="100" />	


            <div class="form-group">
              <label for="inputImage">Up Image</label>
              <input type="file" name="up_image" onchange="loadFileup(event)" id="up_image" class="form-control" required>
            </div>
            <img id="output3" width="100" />	


            <div class="form-group">
              <label for="inputImage">Down Image</label>
              <input type="file" name="do_image" onchange="loadFiledo(event)" id="do_image" class="form-control" required>
            </div>
            <img id="output4" width="100" />	


            <div class="form-group">
              <label for="inputImage">With specs Image</label>
              <input type="file" name="ws_image" onchange="loadFilews(event)" id="ws_image" class="form-control" required>
            </div>
            <img id="output5" width="100" />	


            <div class="form-group">
              <label for="inputImage">With Hat/cap Image</label>
              <input type="file" name="wh_image" onchange="loadFilewh(event)" id="wh_image" class="form-control" required>
            </div>
            <img id="output6" width="100" />	


            <div class="form-group">
              <label for="inputImage">With Beard Image</label>
              <input type="file" name="wb_image" onchange="loadFilewb(event)" id="wb_image" class="form-control" required>
            </div>
            <img id="output7" width="100" />	

            
            <div class="form-group">
              <label for="inputImage">Auto Add Beard Image</label>
              <input type="file" name="auto_b_image" onchange="loadFileb(event)" id="auto_b_image" class="form-control" required>
            </div>
            <img id="output8" width="100" />	 --}}

         
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>

      <div class="col-md-6">
        <div class="card card-secondary">
          <div class="card-header">
            <h3 class="card-title">Images</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
              </button>
            </div>
          </div>
          <div class="card-body" id="regForm" style="
          margin-top: 1px;
          margin-bottom: 1px;
      ">
            {{-- <div class="form-group">
              <label for="inputEstimatedBudget">Password</label>
              <input type="password" name="password" id="inputEstimatedBudget" class="form-control">
            </div> --}}

            {{-- <div class="form-group">
              <label for="inputImage">Images</label>
              <input type="file" id="file"  onchange="loadFilef(event)" name="images[]" multiple id="inputImage" class="form-control" required>
            </div> --}}
            {{-- <img id="output" width="100" />	 --}}

        <div class="form-group">
              <label for="inputStatus">Select Images Type</label>
              <select id="inputStatus"  name="status" class="form-control custom-select">
                <option selected disabled>Select images</option>
                <option value="1">Left Image</option>
                <option value="0" >Right Image</option>
                
              </select>
            </div>
            <div class="tab">
            <div class="form-group my-4">
              <label for="inputImage">Upload images</label>
              <input type="file" name="l_image"  onchange="loadFilel(event)" id="l_image" class="form-control" required>
            </div>
            <img id="output1" width="300px" />
            </div>
            	
            <div class="tab">
            <div class="form-group">
              <label for="inputImage">Right Image</label>
              <input type="file" name="r_image" onchange="loadFiler(event)" id="r_image" class="form-control" required>
            </div>
            <img id="output2" width="300px" />	
            </div>

            <div class="tab">
            <div class="form-group">
              <label for="inputImage">Up Image</label>
              <input type="file" name="up_image" onchange="loadFileup(event)" id="up_image" class="form-control" required>
            </div>
            <img id="output3" width="300px" />	
            </div>

            <div class="tab">
            <div class="form-group">
              <label for="inputImage">Down Image</label>
              <input type="file" name="do_image" onchange="loadFiledo(event)" id="do_image" class="form-control" required>
            </div>
            <img id="output4" width="300px" />
            </div>	

            <div class="my-3" style="overflow:auto;">
              <div style="float:right;">
                <button type="button" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
                <button type="button" id="nextBtn" onclick="nextPrev(1)">Next</button>
              </div>
            </div>

            <div style="text-align:center;margin-top:40px;">
              <span class="step"></span>
              <span class="step"></span>
              <span class="step"></span>
              <span class="step"></span>
            </div>


            {{-- <div class="form-group">
              <label for="inputImage">With specs Image</label>
              <input type="file" name="ws_image" onchange="loadFilews(event)" id="ws_image" class="form-control" required>
            </div>
            <img id="output5" width="100" />	 --}}


            {{-- <div class="form-group">
              <label for="inputImage">With Hat/cap Image</label>
              <input type="file" name="wh_image" onchange="loadFilewh(event)" id="wh_image" class="form-control" required>
            </div>
            <img id="output6" width="100" />	 --}}


            {{-- <div class="form-group">
              <label for="inputImage">With Beard Image</label>
              <input type="file" name="wb_image" onchange="loadFilewb(event)" id="wb_image" class="form-control" required>
            </div>
            <img id="output7" width="100" />	 --}}

            
            {{-- <div class="form-group">
              <label for="inputImage">Auto Add Beard Image</label>
              <input type="file" name="auto_b_image" onchange="loadFileb(event)" id="auto_b_image" class="form-control" required>
            </div>
            <img id="output8" width="100" />	 --}}

         
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
    
    </form>

    <div class="row">
      <div class="col-12">
        <a href="#" class="btn btn-secondary">Cancel</a>
        <button type="submit" onclick="document.getElementById('users').submit()"  class="btn btn-success float-right">Create New Porject</button>
      </div>
    </div>
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection
@section('page_css')
<style>
  * {
    box-sizing: border-box;
  }
  
  body {
    background-color: #f1f1f1;
  }
  
  #regForm {
    background-color: #ffffff;
    margin: 100px auto;
    font-family: Raleway;
    padding: 40px;
    width: 70%;
    min-width: 300px;
  }
  
  h1 {
    text-align: center;  
  }
  
  input {
    padding: 10px;
    width: 100%;
    font-size: 17px;
    font-family: Raleway;
    border: 1px solid #aaaaaa;
  }
  
  /* Mark input boxes that gets an error on validation: */
  input.invalid {
    background-color: #ffdddd;
  }
  
  /* Hide all steps by default: */
  .tab {
    display: none;
  }
  
  button {
    background-color: #04AA6D;
    color: #ffffff;
    border: none;
    padding: 10px 20px;
    font-size: 17px;
    font-family: Raleway;
    cursor: pointer;
  }
  
  button:hover {
    opacity: 0.8;
  }
  
  #prevBtn {
    background-color: #bbbbbb;
  }
  
  /* Make circles that indicate the steps of the form: */
  .step {
    height: 15px;
    width: 15px;
    margin: 0 2px;
    background-color: #bbbbbb;
    border: none;  
    border-radius: 50%;
    display: inline-block;
    opacity: 0.5;
  }
  
  .step.active {
    opacity: 1;
  }
  
  /* Mark the steps that are finished and valid: */
  .step.finish {
    background-color: #04AA6D;
  }
  </style>

    
@endsection

@section('page_script')

<script>
  var loadFilef = function(event) {
    var image = document.getElementById('output');
    image.src = URL.createObjectURL(event.target.files[0]);
  };
  </script>

<script>
  var loadFilel = function(event) {
    var image = document.getElementById('output1');
    image.src = URL.createObjectURL(event.target.files[0]);
  };
  </script>
  
  <script>
    var loadFiler = function(event) {
      var image = document.getElementById('output2');
      image.src = URL.createObjectURL(event.target.files[0]);
    };
    </script>

<script>
  var loadFileup = function(event) {
    var image = document.getElementById('output3');
    image.src = URL.createObjectURL(event.target.files[0]);
  };
  </script>

<script>
  var loadFiledo = function(event) {
    var image = document.getElementById('output4');
    image.src = URL.createObjectURL(event.target.files[0]);
  };
  </script>

<script>
  var loadFilews = function(event) {
    var image = document.getElementById('output5');
    image.src = URL.createObjectURL(event.target.files[0]);
  };
  </script>

<script>
  var loadFilewh = function(event) {
    var image = document.getElementById('output6');
    image.src = URL.createObjectURL(event.target.files[0]);
  };
  </script>

<script>
  var loadFilewb = function(event) {
    var image = document.getElementById('output7');
    image.src = URL.createObjectURL(event.target.files[0]);
  };
  </script>

<script>
  var loadFileb = function(event) {
    var image = document.getElementById('output8');
    image.src = URL.createObjectURL(event.target.files[0]);
  };
  </script>



<script>
  var currentTab = 0; // Current tab is set to be the first tab (0)
  showTab(currentTab); // Display the current tab
  
  function showTab(n) {
    // This function will display the specified tab of the form...
    var x = document.getElementsByClassName("tab");
    x[n].style.display = "block";
    //... and fix the Previous/Next buttons:
    if (n == 0) {
      document.getElementById("prevBtn").style.display = "none";
    } else {
      document.getElementById("prevBtn").style.display = "inline";
    }
    if (n == (x.length - 1)) {
      document.getElementById("nextBtn").innerHTML = "Submit";
    } else {
      document.getElementById("nextBtn").innerHTML = "Next";
    }
    //... and run a function that will display the correct step indicator:
    fixStepIndicator(n)
  }
  
  function nextPrev(n) {
    // This function will figure out which tab to display
    var x = document.getElementsByClassName("tab");
    // Exit the function if any field in the current tab is invalid:
    if (n == 1 && !validateForm()) return false;
    // Hide the current tab:
    x[currentTab].style.display = "none";
    // Increase or decrease the current tab by 1:
    currentTab = currentTab + n;
    // if you have reached the end of the form...
    if (currentTab >= x.length) {
      // ... the form gets submitted:
      document.getElementById("regForm").submit();
      return false;
    }
    // Otherwise, display the correct tab:
    showTab(currentTab);
  }
  
  function validateForm() {
    // This function deals with validation of the form fields
    var x, y, i, valid = true;
    x = document.getElementsByClassName("tab");
    y = x[currentTab].getElementsByTagName("input");
    // A loop that checks every input field in the current tab:
    for (i = 0; i < y.length; i++) {
      // If a field is empty...
      if (y[i].value == "") {
        // add an "invalid" class to the field:
        y[i].className += " invalid";
        // and set the current valid status to false
        valid = false;
      }
    }
    // If the valid status is true, mark the step as finished and valid:
    if (valid) {
      document.getElementsByClassName("step")[currentTab].className += " finish";
    }
    return valid; // return the valid status
  }
  
  function fixStepIndicator(n) {
    // This function removes the "active" class of all steps...
    var i, x = document.getElementsByClassName("step");
    for (i = 0; i < x.length; i++) {
      x[i].className = x[i].className.replace(" active", "");
    }
    //... and adds the "active" class on the current step:
    x[n].className += " active";
  }
  </script>

@endsection