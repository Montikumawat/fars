@extends('layout.master')
@section('title','Dashboard')
@section('description','Admin Dashboard of FARS')
@section('keywords','admin panel, fars')
@section('author','FARS')

@section('css')
<link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
 <style>
   #example_filter {
       float: right;
   }
   #example_paginate {
       float: right;
   }
       /* width */
::-webkit-scrollbar {
width: 10px;
}

/* Track */
::-webkit-scrollbar-track {
background: #f1f1f1;

}

/* Handle */
::-webkit-scrollbar-thumb {
background: #958cf4;
border-radius: 10px;
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
background: #958cf4;
}

#apply_filters {
      margin-top: 23px;
    }
 </style>
@endsection

@section('content')

<div class="row">
    <div class="col-lg-3 col-sm-6 col-12">
        <div class="card">
            <div class="card-header flex-column align-items-start pb-0">
                <div class="avatar bg-light-primary p-50 m-0">
                    <div class="avatar-content">
                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users font-medium-5"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="9" cy="7" r="4"></circle><path d="M23 21v-2a4 4 0 0 0-3-3.87"></path><path d="M16 3.13a4 4 0 0 1 0 7.75"></path></svg>
                    </div>
                </div>
                <h2 class="font-weight-bolder mt-1">{{ $total_customers ?? '0' }}</h2>
                <p class="card-text">Total Users</p>
            </div>
            <div id="line-area-chart-1" style="min-height: 100px;"><div id="apexchartsu0fdy1tf" class="apexcharts-canvas apexchartsu0fdy1tf apexcharts-theme-light" style="width: 237px; height: 100px;"><svg id="SvgjsSvg1942" width="237" height="100" xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" class="apexcharts-svg" xmlns:data="ApexChartsNS" transform="translate(0, 0)" style="background: transparent;"><g id="SvgjsG1944" class="apexcharts-inner apexcharts-graphical" transform="translate(0, 0)"><defs id="SvgjsDefs1943"><clipPath id="gridRectMasku0fdy1tf"><rect id="SvgjsRect1949" width="243.5" height="102.5" x="-3.25" y="-1.25" rx="0" ry="0" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0" fill="#fff"></rect></clipPath><clipPath id="gridRectMarkerMasku0fdy1tf"><rect id="SvgjsRect1950" width="241" height="104" x="-2" y="-2" rx="0" ry="0" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0" fill="#fff"></rect></clipPath><linearGradient id="SvgjsLinearGradient1955" x1="0" y1="0" x2="0" y2="1"><stop id="SvgjsStop1956" stop-opacity="0.7" stop-color="rgba(115,103,240,0.7)" offset="0"></stop><stop id="SvgjsStop1957" stop-opacity="0.5" stop-color="rgba(241,240,254,0.5)" offset="0.8"></stop><stop id="SvgjsStop1958" stop-opacity="0.5" stop-color="rgba(241,240,254,0.5)" offset="1"></stop></linearGradient></defs><line id="SvgjsLine1948" x1="0" y1="0" x2="0" y2="100" stroke="#b6b6b6" stroke-dasharray="3" class="apexcharts-xcrosshairs" x="0" y="0" width="1" height="100" fill="#b1b9c4" filter="none" fill-opacity="0.9" stroke-width="1"></line><g id="SvgjsG1961" class="apexcharts-xaxis" transform="translate(0, 0)"><g id="SvgjsG1962" class="apexcharts-xaxis-texts-g" transform="translate(0, -4)"></g></g><g id="SvgjsG1964" class="apexcharts-grid"><g id="SvgjsG1965" class="apexcharts-gridlines-horizontal" style="display: none;"><line id="SvgjsLine1967" x1="0" y1="0" x2="237" y2="0" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line><line id="SvgjsLine1968" x1="0" y1="20" x2="237" y2="20" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line><line id="SvgjsLine1969" x1="0" y1="40" x2="237" y2="40" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line><line id="SvgjsLine1970" x1="0" y1="60" x2="237" y2="60" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line><line id="SvgjsLine1971" x1="0" y1="80" x2="237" y2="80" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line><line id="SvgjsLine1972" x1="0" y1="100" x2="237" y2="100" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line></g><g id="SvgjsG1966" class="apexcharts-gridlines-vertical" style="display: none;"></g><line id="SvgjsLine1974" x1="0" y1="100" x2="237" y2="100" stroke="transparent" stroke-dasharray="0"></line><line id="SvgjsLine1973" x1="0" y1="1" x2="0" y2="100" stroke="transparent" stroke-dasharray="0"></line></g><g id="SvgjsG1951" class="apexcharts-area-series apexcharts-plot-series"><g id="SvgjsG1952" class="apexcharts-series" seriesName="Subscribers" data:longestSeries="true" rel="1" data:realIndex="0"><path id="SvgjsPath1959" d="M 0 100L 0 77.77777777777777C 13.825 77.77777777777777 25.675 51.111111111111114 39.5 51.111111111111114C 53.325 51.111111111111114 65.175 60 79 60C 92.825 60 104.675 24.444444444444443 118.5 24.444444444444443C 132.325 24.444444444444443 144.175 55.55555555555556 158 55.55555555555556C 171.825 55.55555555555556 183.675 6.666666666666657 197.5 6.666666666666657C 211.325 6.666666666666657 223.175 17.777777777777786 237 17.777777777777786C 237 17.777777777777786 237 17.777777777777786 237 100M 237 17.777777777777786z" fill="url(#SvgjsLinearGradient1955)" fill-opacity="1" stroke-opacity="1" stroke-linecap="butt" stroke-width="0" stroke-dasharray="0" class="apexcharts-area" index="0" clip-path="url(#gridRectMasku0fdy1tf)" pathTo="M 0 100L 0 77.77777777777777C 13.825 77.77777777777777 25.675 51.111111111111114 39.5 51.111111111111114C 53.325 51.111111111111114 65.175 60 79 60C 92.825 60 104.675 24.444444444444443 118.5 24.444444444444443C 132.325 24.444444444444443 144.175 55.55555555555556 158 55.55555555555556C 171.825 55.55555555555556 183.675 6.666666666666657 197.5 6.666666666666657C 211.325 6.666666666666657 223.175 17.777777777777786 237 17.777777777777786C 237 17.777777777777786 237 17.777777777777786 237 100M 237 17.777777777777786z" pathFrom="M -1 140L -1 140L 39.5 140L 79 140L 118.5 140L 158 140L 197.5 140L 237 140"></path><path id="SvgjsPath1960" d="M 0 77.77777777777777C 13.825 77.77777777777777 25.675 51.111111111111114 39.5 51.111111111111114C 53.325 51.111111111111114 65.175 60 79 60C 92.825 60 104.675 24.444444444444443 118.5 24.444444444444443C 132.325 24.444444444444443 144.175 55.55555555555556 158 55.55555555555556C 171.825 55.55555555555556 183.675 6.666666666666657 197.5 6.666666666666657C 211.325 6.666666666666657 223.175 17.777777777777786 237 17.777777777777786" fill="none" fill-opacity="1" stroke="#7367f0" stroke-opacity="1" stroke-linecap="butt" stroke-width="2.5" stroke-dasharray="0" class="apexcharts-area" index="0" clip-path="url(#gridRectMasku0fdy1tf)" pathTo="M 0 77.77777777777777C 13.825 77.77777777777777 25.675 51.111111111111114 39.5 51.111111111111114C 53.325 51.111111111111114 65.175 60 79 60C 92.825 60 104.675 24.444444444444443 118.5 24.444444444444443C 132.325 24.444444444444443 144.175 55.55555555555556 158 55.55555555555556C 171.825 55.55555555555556 183.675 6.666666666666657 197.5 6.666666666666657C 211.325 6.666666666666657 223.175 17.777777777777786 237 17.777777777777786" pathFrom="M -1 140L -1 140L 39.5 140L 79 140L 118.5 140L 158 140L 197.5 140L 237 140"></path><g id="SvgjsG1953" class="apexcharts-series-markers-wrap" data:realIndex="0"><g class="apexcharts-series-markers"><circle id="SvgjsCircle1980" r="0" cx="0" cy="0" class="apexcharts-marker w87gged3s no-pointer-events" stroke="#ffffff" fill="#7367f0" fill-opacity="1" stroke-width="2" stroke-opacity="0.9" default-marker-size="0"></circle></g></g></g><g id="SvgjsG1954" class="apexcharts-datalabels" data:realIndex="0"></g></g><line id="SvgjsLine1975" x1="0" y1="0" x2="237" y2="0" stroke="#b6b6b6" stroke-dasharray="0" stroke-width="1" class="apexcharts-ycrosshairs"></line><line id="SvgjsLine1976" x1="0" y1="0" x2="237" y2="0" stroke-dasharray="0" stroke-width="0" class="apexcharts-ycrosshairs-hidden"></line><g id="SvgjsG1977" class="apexcharts-yaxis-annotations"></g><g id="SvgjsG1978" class="apexcharts-xaxis-annotations"></g><g id="SvgjsG1979" class="apexcharts-point-annotations"></g></g><rect id="SvgjsRect1947" width="0" height="0" x="0" y="0" rx="0" ry="0" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0" fill="#fefefe"></rect><g id="SvgjsG1963" class="apexcharts-yaxis" rel="0" transform="translate(-18, 0)"></g><g id="SvgjsG1945" class="apexcharts-annotations"></g></svg><div class="apexcharts-legend" style="max-height: 50px;"></div><div class="apexcharts-tooltip apexcharts-theme-light"><div class="apexcharts-tooltip-series-group" style="order: 1;"><span class="apexcharts-tooltip-marker" style="background-color: rgb(115, 103, 240);"></span><div class="apexcharts-tooltip-text" style="font-family: Helvetica, Arial, sans-serif; font-size: 12px;"><div class="apexcharts-tooltip-y-group"><span class="apexcharts-tooltip-text-label"></span><span class="apexcharts-tooltip-text-value"></span></div><div class="apexcharts-tooltip-z-group"><span class="apexcharts-tooltip-text-z-label"></span><span class="apexcharts-tooltip-text-z-value"></span></div></div></div></div><div class="apexcharts-yaxistooltip apexcharts-yaxistooltip-0 apexcharts-yaxistooltip-left apexcharts-theme-light"><div class="apexcharts-yaxistooltip-text"></div></div></div></div>
        <div class="resize-triggers"><div class="expand-trigger"><div style="width: 238px; height: 238px;"></div></div><div class="contract-trigger"></div></div></div>
    </div>
    <div class="col-lg-3 col-sm-6 col-12">
        <div class="card">
            <div class="card-header flex-column align-items-start pb-0">
                <div class="avatar bg-light-success p-50 m-0">
                    <div class="avatar-content">
                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-credit-card font-medium-5"><rect x="1" y="4" width="22" height="16" rx="2" ry="2"></rect><line x1="1" y1="10" x2="23" y2="10"></line></svg>
                    </div>
                </div>
                <h2 class="font-weight-bolder mt-1"><i class="fa fa-inr"></i> {{$suminr ?? '0'}}</h2>
                <p class="card-text">Revenue (Total INR amount)</p>
            </div>
            <div id="line-area-chart-2" style="min-height: 100px;"><div id="apexchartsmafg44ym" class="apexcharts-canvas apexchartsmafg44ym apexcharts-theme-light" style="width: 237px; height: 100px;"><svg id="SvgjsSvg1982" width="237" height="100" xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" class="apexcharts-svg" xmlns:data="ApexChartsNS" transform="translate(0, 0)" style="background: transparent;"><g id="SvgjsG1984" class="apexcharts-inner apexcharts-graphical" transform="translate(0, 0)"><defs id="SvgjsDefs1983"><clipPath id="gridRectMaskmafg44ym"><rect id="SvgjsRect1989" width="243.5" height="102.5" x="-3.25" y="-1.25" rx="0" ry="0" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0" fill="#fff"></rect></clipPath><clipPath id="gridRectMarkerMaskmafg44ym"><rect id="SvgjsRect1990" width="241" height="104" x="-2" y="-2" rx="0" ry="0" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0" fill="#fff"></rect></clipPath><linearGradient id="SvgjsLinearGradient1995" x1="0" y1="0" x2="0" y2="1"><stop id="SvgjsStop1996" stop-opacity="0.7" stop-color="rgba(40,199,111,0.7)" offset="0"></stop><stop id="SvgjsStop1997" stop-opacity="0.5" stop-color="rgba(234,249,241,0.5)" offset="0.8"></stop><stop id="SvgjsStop1998" stop-opacity="0.5" stop-color="rgba(234,249,241,0.5)" offset="1"></stop></linearGradient></defs><line id="SvgjsLine1988" x1="0" y1="0" x2="0" y2="100" stroke="#b6b6b6" stroke-dasharray="3" class="apexcharts-xcrosshairs" x="0" y="0" width="1" height="100" fill="#b1b9c4" filter="none" fill-opacity="0.9" stroke-width="1"></line><g id="SvgjsG2001" class="apexcharts-xaxis" transform="translate(0, 0)"><g id="SvgjsG2002" class="apexcharts-xaxis-texts-g" transform="translate(0, -4)"></g></g><g id="SvgjsG2004" class="apexcharts-grid"><g id="SvgjsG2005" class="apexcharts-gridlines-horizontal" style="display: none;"><line id="SvgjsLine2007" x1="0" y1="0" x2="237" y2="0" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line><line id="SvgjsLine2008" x1="0" y1="20" x2="237" y2="20" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line><line id="SvgjsLine2009" x1="0" y1="40" x2="237" y2="40" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line><line id="SvgjsLine2010" x1="0" y1="60" x2="237" y2="60" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line><line id="SvgjsLine2011" x1="0" y1="80" x2="237" y2="80" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line><line id="SvgjsLine2012" x1="0" y1="100" x2="237" y2="100" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line></g><g id="SvgjsG2006" class="apexcharts-gridlines-vertical" style="display: none;"></g><line id="SvgjsLine2014" x1="0" y1="100" x2="237" y2="100" stroke="transparent" stroke-dasharray="0"></line><line id="SvgjsLine2013" x1="0" y1="1" x2="0" y2="100" stroke="transparent" stroke-dasharray="0"></line></g><g id="SvgjsG1991" class="apexcharts-area-series apexcharts-plot-series"><g id="SvgjsG1992" class="apexcharts-series" seriesName="Revenue" data:longestSeries="true" rel="1" data:realIndex="0"><path id="SvgjsPath1999" d="M 0 100L 0 60C 13.825 60 25.675 90 39.5 90C 53.325 90 65.175 40 79 40C 92.825 40 104.675 80 118.5 80C 132.325 80 144.175 60 158 60C 171.825 60 183.675 80 197.5 80C 211.325 80 223.175 20 237 20C 237 20 237 20 237 100M 237 20z" fill="url(#SvgjsLinearGradient1995)" fill-opacity="1" stroke-opacity="1" stroke-linecap="butt" stroke-width="0" stroke-dasharray="0" class="apexcharts-area" index="0" clip-path="url(#gridRectMaskmafg44ym)" pathTo="M 0 100L 0 60C 13.825 60 25.675 90 39.5 90C 53.325 90 65.175 40 79 40C 92.825 40 104.675 80 118.5 80C 132.325 80 144.175 60 158 60C 171.825 60 183.675 80 197.5 80C 211.325 80 223.175 20 237 20C 237 20 237 20 237 100M 237 20z" pathFrom="M -1 200L -1 200L 39.5 200L 79 200L 118.5 200L 158 200L 197.5 200L 237 200"></path><path id="SvgjsPath2000" d="M 0 60C 13.825 60 25.675 90 39.5 90C 53.325 90 65.175 40 79 40C 92.825 40 104.675 80 118.5 80C 132.325 80 144.175 60 158 60C 171.825 60 183.675 80 197.5 80C 211.325 80 223.175 20 237 20" fill="none" fill-opacity="1" stroke="#28c76f" stroke-opacity="1" stroke-linecap="butt" stroke-width="2.5" stroke-dasharray="0" class="apexcharts-area" index="0" clip-path="url(#gridRectMaskmafg44ym)" pathTo="M 0 60C 13.825 60 25.675 90 39.5 90C 53.325 90 65.175 40 79 40C 92.825 40 104.675 80 118.5 80C 132.325 80 144.175 60 158 60C 171.825 60 183.675 80 197.5 80C 211.325 80 223.175 20 237 20" pathFrom="M -1 200L -1 200L 39.5 200L 79 200L 118.5 200L 158 200L 197.5 200L 237 200"></path><g id="SvgjsG1993" class="apexcharts-series-markers-wrap" data:realIndex="0"><g class="apexcharts-series-markers"><circle id="SvgjsCircle2020" r="0" cx="0" cy="0" class="apexcharts-marker wq6uxj2fo no-pointer-events" stroke="#ffffff" fill="#28c76f" fill-opacity="1" stroke-width="2" stroke-opacity="0.9" default-marker-size="0"></circle></g></g></g><g id="SvgjsG1994" class="apexcharts-datalabels" data:realIndex="0"></g></g><line id="SvgjsLine2015" x1="0" y1="0" x2="237" y2="0" stroke="#b6b6b6" stroke-dasharray="0" stroke-width="1" class="apexcharts-ycrosshairs"></line><line id="SvgjsLine2016" x1="0" y1="0" x2="237" y2="0" stroke-dasharray="0" stroke-width="0" class="apexcharts-ycrosshairs-hidden"></line><g id="SvgjsG2017" class="apexcharts-yaxis-annotations"></g><g id="SvgjsG2018" class="apexcharts-xaxis-annotations"></g><g id="SvgjsG2019" class="apexcharts-point-annotations"></g></g><rect id="SvgjsRect1987" width="0" height="0" x="0" y="0" rx="0" ry="0" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0" fill="#fefefe"></rect><g id="SvgjsG2003" class="apexcharts-yaxis" rel="0" transform="translate(-18, 0)"></g><g id="SvgjsG1985" class="apexcharts-annotations"></g></svg><div class="apexcharts-legend" style="max-height: 50px;"></div><div class="apexcharts-tooltip apexcharts-theme-light"><div class="apexcharts-tooltip-series-group" style="order: 1;"><span class="apexcharts-tooltip-marker" style="background-color: rgb(40, 199, 111);"></span><div class="apexcharts-tooltip-text" style="font-family: Helvetica, Arial, sans-serif; font-size: 12px;"><div class="apexcharts-tooltip-y-group"><span class="apexcharts-tooltip-text-label"></span><span class="apexcharts-tooltip-text-value"></span></div><div class="apexcharts-tooltip-z-group"><span class="apexcharts-tooltip-text-z-label"></span><span class="apexcharts-tooltip-text-z-value"></span></div></div></div></div><div class="apexcharts-yaxistooltip apexcharts-yaxistooltip-0 apexcharts-yaxistooltip-left apexcharts-theme-light"><div class="apexcharts-yaxistooltip-text"></div></div></div></div>
        <div class="resize-triggers"><div class="expand-trigger"><div style="width: 238px; height: 238px;"></div></div><div class="contract-trigger"></div></div></div>
    </div>
    <div class="col-lg-3 col-sm-6 col-12">
        <div class="card">
            <div class="card-header flex-column align-items-start pb-0">
                <div class="avatar bg-light-success p-50 m-0">
                    <div class="avatar-content">
                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-credit-card font-medium-5"><rect x="1" y="4" width="22" height="16" rx="2" ry="2"></rect><line x1="1" y1="10" x2="23" y2="10"></line></svg>
                    </div>
                </div>
                <h2 class="font-weight-bolder mt-1"> <i class="fas fa-usd"></i> {{$sumusd ?? '0'}}</h2>
                <p class="card-text">Revenue (Total USD amount)</p>
            </div>
            <div id="line-area-chart-2" style="min-height: 100px;"><div id="apexchartsmafg44ym" class="apexcharts-canvas apexchartsmafg44ym apexcharts-theme-light" style="width: 237px; height: 100px;"><svg id="SvgjsSvg1982" width="237" height="100" xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" class="apexcharts-svg" xmlns:data="ApexChartsNS" transform="translate(0, 0)" style="background: transparent;"><g id="SvgjsG1984" class="apexcharts-inner apexcharts-graphical" transform="translate(0, 0)"><defs id="SvgjsDefs1983"><clipPath id="gridRectMaskmafg44ym"><rect id="SvgjsRect1989" width="243.5" height="102.5" x="-3.25" y="-1.25" rx="0" ry="0" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0" fill="#fff"></rect></clipPath><clipPath id="gridRectMarkerMaskmafg44ym"><rect id="SvgjsRect1990" width="241" height="104" x="-2" y="-2" rx="0" ry="0" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0" fill="#fff"></rect></clipPath><linearGradient id="SvgjsLinearGradient1995" x1="0" y1="0" x2="0" y2="1"><stop id="SvgjsStop1996" stop-opacity="0.7" stop-color="rgba(40,199,111,0.7)" offset="0"></stop><stop id="SvgjsStop1997" stop-opacity="0.5" stop-color="rgba(234,249,241,0.5)" offset="0.8"></stop><stop id="SvgjsStop1998" stop-opacity="0.5" stop-color="rgba(234,249,241,0.5)" offset="1"></stop></linearGradient></defs><line id="SvgjsLine1988" x1="0" y1="0" x2="0" y2="100" stroke="#b6b6b6" stroke-dasharray="3" class="apexcharts-xcrosshairs" x="0" y="0" width="1" height="100" fill="#b1b9c4" filter="none" fill-opacity="0.9" stroke-width="1"></line><g id="SvgjsG2001" class="apexcharts-xaxis" transform="translate(0, 0)"><g id="SvgjsG2002" class="apexcharts-xaxis-texts-g" transform="translate(0, -4)"></g></g><g id="SvgjsG2004" class="apexcharts-grid"><g id="SvgjsG2005" class="apexcharts-gridlines-horizontal" style="display: none;"><line id="SvgjsLine2007" x1="0" y1="0" x2="237" y2="0" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line><line id="SvgjsLine2008" x1="0" y1="20" x2="237" y2="20" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line><line id="SvgjsLine2009" x1="0" y1="40" x2="237" y2="40" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line><line id="SvgjsLine2010" x1="0" y1="60" x2="237" y2="60" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line><line id="SvgjsLine2011" x1="0" y1="80" x2="237" y2="80" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line><line id="SvgjsLine2012" x1="0" y1="100" x2="237" y2="100" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line></g><g id="SvgjsG2006" class="apexcharts-gridlines-vertical" style="display: none;"></g><line id="SvgjsLine2014" x1="0" y1="100" x2="237" y2="100" stroke="transparent" stroke-dasharray="0"></line><line id="SvgjsLine2013" x1="0" y1="1" x2="0" y2="100" stroke="transparent" stroke-dasharray="0"></line></g><g id="SvgjsG1991" class="apexcharts-area-series apexcharts-plot-series"><g id="SvgjsG1992" class="apexcharts-series" seriesName="Revenue" data:longestSeries="true" rel="1" data:realIndex="0"><path id="SvgjsPath1999" d="M 0 100L 0 60C 13.825 60 25.675 90 39.5 90C 53.325 90 65.175 40 79 40C 92.825 40 104.675 80 118.5 80C 132.325 80 144.175 60 158 60C 171.825 60 183.675 80 197.5 80C 211.325 80 223.175 20 237 20C 237 20 237 20 237 100M 237 20z" fill="url(#SvgjsLinearGradient1995)" fill-opacity="1" stroke-opacity="1" stroke-linecap="butt" stroke-width="0" stroke-dasharray="0" class="apexcharts-area" index="0" clip-path="url(#gridRectMaskmafg44ym)" pathTo="M 0 100L 0 60C 13.825 60 25.675 90 39.5 90C 53.325 90 65.175 40 79 40C 92.825 40 104.675 80 118.5 80C 132.325 80 144.175 60 158 60C 171.825 60 183.675 80 197.5 80C 211.325 80 223.175 20 237 20C 237 20 237 20 237 100M 237 20z" pathFrom="M -1 200L -1 200L 39.5 200L 79 200L 118.5 200L 158 200L 197.5 200L 237 200"></path><path id="SvgjsPath2000" d="M 0 60C 13.825 60 25.675 90 39.5 90C 53.325 90 65.175 40 79 40C 92.825 40 104.675 80 118.5 80C 132.325 80 144.175 60 158 60C 171.825 60 183.675 80 197.5 80C 211.325 80 223.175 20 237 20" fill="none" fill-opacity="1" stroke="#28c76f" stroke-opacity="1" stroke-linecap="butt" stroke-width="2.5" stroke-dasharray="0" class="apexcharts-area" index="0" clip-path="url(#gridRectMaskmafg44ym)" pathTo="M 0 60C 13.825 60 25.675 90 39.5 90C 53.325 90 65.175 40 79 40C 92.825 40 104.675 80 118.5 80C 132.325 80 144.175 60 158 60C 171.825 60 183.675 80 197.5 80C 211.325 80 223.175 20 237 20" pathFrom="M -1 200L -1 200L 39.5 200L 79 200L 118.5 200L 158 200L 197.5 200L 237 200"></path><g id="SvgjsG1993" class="apexcharts-series-markers-wrap" data:realIndex="0"><g class="apexcharts-series-markers"><circle id="SvgjsCircle2020" r="0" cx="0" cy="0" class="apexcharts-marker wq6uxj2fo no-pointer-events" stroke="#ffffff" fill="#28c76f" fill-opacity="1" stroke-width="2" stroke-opacity="0.9" default-marker-size="0"></circle></g></g></g><g id="SvgjsG1994" class="apexcharts-datalabels" data:realIndex="0"></g></g><line id="SvgjsLine2015" x1="0" y1="0" x2="237" y2="0" stroke="#b6b6b6" stroke-dasharray="0" stroke-width="1" class="apexcharts-ycrosshairs"></line><line id="SvgjsLine2016" x1="0" y1="0" x2="237" y2="0" stroke-dasharray="0" stroke-width="0" class="apexcharts-ycrosshairs-hidden"></line><g id="SvgjsG2017" class="apexcharts-yaxis-annotations"></g><g id="SvgjsG2018" class="apexcharts-xaxis-annotations"></g><g id="SvgjsG2019" class="apexcharts-point-annotations"></g></g><rect id="SvgjsRect1987" width="0" height="0" x="0" y="0" rx="0" ry="0" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0" fill="#fefefe"></rect><g id="SvgjsG2003" class="apexcharts-yaxis" rel="0" transform="translate(-18, 0)"></g><g id="SvgjsG1985" class="apexcharts-annotations"></g></svg><div class="apexcharts-legend" style="max-height: 50px;"></div><div class="apexcharts-tooltip apexcharts-theme-light"><div class="apexcharts-tooltip-series-group" style="order: 1;"><span class="apexcharts-tooltip-marker" style="background-color: rgb(40, 199, 111);"></span><div class="apexcharts-tooltip-text" style="font-family: Helvetica, Arial, sans-serif; font-size: 12px;"><div class="apexcharts-tooltip-y-group"><span class="apexcharts-tooltip-text-label"></span><span class="apexcharts-tooltip-text-value"></span></div><div class="apexcharts-tooltip-z-group"><span class="apexcharts-tooltip-text-z-label"></span><span class="apexcharts-tooltip-text-z-value"></span></div></div></div></div><div class="apexcharts-yaxistooltip apexcharts-yaxistooltip-0 apexcharts-yaxistooltip-left apexcharts-theme-light"><div class="apexcharts-yaxistooltip-text"></div></div></div></div>
        <div class="resize-triggers"><div class="expand-trigger"><div style="width: 238px; height: 238px;"></div></div><div class="contract-trigger"></div></div></div>
    </div>
    <div class="col-lg-3 col-sm-6 col-12">
        <div class="card">
            <div class="card-header flex-column align-items-start pb-0">
                <div class="avatar bg-light-danger p-50 m-0">
                    <div class="avatar-content">
                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user-check font-medium-5"><path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="8.5" cy="7" r="4"></circle><polyline points="17 11 19 13 23 9"></polyline></svg>                    </div>
                </div>
                <h2 class="font-weight-bolder mt-1">36K</h2>
                <p class="card-text"    >Total Checkins</p>
            </div>
            <div id="line-area-chart-3" style="min-height: 100px;"><div id="apexchartszhqhzqyw" class="apexcharts-canvas apexchartszhqhzqyw apexcharts-theme-light" style="width: 237px; height: 100px;"><svg id="SvgjsSvg2022" width="237" height="100" xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" class="apexcharts-svg" xmlns:data="ApexChartsNS" transform="translate(0, 0)" style="background: transparent;"><g id="SvgjsG2024" class="apexcharts-inner apexcharts-graphical" transform="translate(0, 0)"><defs id="SvgjsDefs2023"><clipPath id="gridRectMaskzhqhzqyw"><rect id="SvgjsRect2029" width="243.5" height="102.5" x="-3.25" y="-1.25" rx="0" ry="0" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0" fill="#fff"></rect></clipPath><clipPath id="gridRectMarkerMaskzhqhzqyw"><rect id="SvgjsRect2030" width="241" height="104" x="-2" y="-2" rx="0" ry="0" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0" fill="#fff"></rect></clipPath><linearGradient id="SvgjsLinearGradient2035" x1="0" y1="0" x2="0" y2="1"><stop id="SvgjsStop2036" stop-opacity="0.7" stop-color="rgba(234,84,85,0.7)" offset="0"></stop><stop id="SvgjsStop2037" stop-opacity="0.5" stop-color="rgba(253,238,238,0.5)" offset="0.8"></stop><stop id="SvgjsStop2038" stop-opacity="0.5" stop-color="rgba(253,238,238,0.5)" offset="1"></stop></linearGradient></defs><line id="SvgjsLine2028" x1="0" y1="0" x2="0" y2="100" stroke="#b6b6b6" stroke-dasharray="3" class="apexcharts-xcrosshairs" x="0" y="0" width="1" height="100" fill="#b1b9c4" filter="none" fill-opacity="0.9" stroke-width="1"></line><g id="SvgjsG2041" class="apexcharts-xaxis" transform="translate(0, 0)"><g id="SvgjsG2042" class="apexcharts-xaxis-texts-g" transform="translate(0, -4)"></g></g><g id="SvgjsG2044" class="apexcharts-grid"><g id="SvgjsG2045" class="apexcharts-gridlines-horizontal" style="display: none;"><line id="SvgjsLine2047" x1="0" y1="0" x2="237" y2="0" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line><line id="SvgjsLine2048" x1="0" y1="20" x2="237" y2="20" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line><line id="SvgjsLine2049" x1="0" y1="40" x2="237" y2="40" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line><line id="SvgjsLine2050" x1="0" y1="60" x2="237" y2="60" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line><line id="SvgjsLine2051" x1="0" y1="80" x2="237" y2="80" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line><line id="SvgjsLine2052" x1="0" y1="100" x2="237" y2="100" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line></g><g id="SvgjsG2046" class="apexcharts-gridlines-vertical" style="display: none;"></g><line id="SvgjsLine2054" x1="0" y1="100" x2="237" y2="100" stroke="transparent" stroke-dasharray="0"></line><line id="SvgjsLine2053" x1="0" y1="1" x2="0" y2="100" stroke="transparent" stroke-dasharray="0"></line></g><g id="SvgjsG2031" class="apexcharts-area-series apexcharts-plot-series"><g id="SvgjsG2032" class="apexcharts-series" seriesName="Sales" data:longestSeries="true" rel="1" data:realIndex="0"><path id="SvgjsPath2039" d="M 0 100L 0 53.33333333333333C 16.59 53.33333333333333 30.81 20 47.4 20C 63.989999999999995 20 78.21 73.33333333333333 94.8 73.33333333333333C 111.39 73.33333333333333 125.61000000000001 40 142.20000000000002 40C 158.79000000000002 40 173.01 100 189.6 100C 206.19 100 220.41 13.333333333333329 237 13.333333333333329C 237 13.333333333333329 237 13.333333333333329 237 100M 237 13.333333333333329z" fill="url(#SvgjsLinearGradient2035)" fill-opacity="1" stroke-opacity="1" stroke-linecap="butt" stroke-width="0" stroke-dasharray="0" class="apexcharts-area" index="0" clip-path="url(#gridRectMaskzhqhzqyw)" pathTo="M 0 100L 0 53.33333333333333C 16.59 53.33333333333333 30.81 20 47.4 20C 63.989999999999995 20 78.21 73.33333333333333 94.8 73.33333333333333C 111.39 73.33333333333333 125.61000000000001 40 142.20000000000002 40C 158.79000000000002 40 173.01 100 189.6 100C 206.19 100 220.41 13.333333333333329 237 13.333333333333329C 237 13.333333333333329 237 13.333333333333329 237 100M 237 13.333333333333329z" pathFrom="M -1 120L -1 120L 47.4 120L 94.8 120L 142.20000000000002 120L 189.6 120L 237 120"></path><path id="SvgjsPath2040" d="M 0 53.33333333333333C 16.59 53.33333333333333 30.81 20 47.4 20C 63.989999999999995 20 78.21 73.33333333333333 94.8 73.33333333333333C 111.39 73.33333333333333 125.61000000000001 40 142.20000000000002 40C 158.79000000000002 40 173.01 100 189.6 100C 206.19 100 220.41 13.333333333333329 237 13.333333333333329" fill="none" fill-opacity="1" stroke="#ea5455" stroke-opacity="1" stroke-linecap="butt" stroke-width="2.5" stroke-dasharray="0" class="apexcharts-area" index="0" clip-path="url(#gridRectMaskzhqhzqyw)" pathTo="M 0 53.33333333333333C 16.59 53.33333333333333 30.81 20 47.4 20C 63.989999999999995 20 78.21 73.33333333333333 94.8 73.33333333333333C 111.39 73.33333333333333 125.61000000000001 40 142.20000000000002 40C 158.79000000000002 40 173.01 100 189.6 100C 206.19 100 220.41 13.333333333333329 237 13.333333333333329" pathFrom="M -1 120L -1 120L 47.4 120L 94.8 120L 142.20000000000002 120L 189.6 120L 237 120"></path><g id="SvgjsG2033" class="apexcharts-series-markers-wrap" data:realIndex="0"><g class="apexcharts-series-markers"><circle id="SvgjsCircle2060" r="0" cx="0" cy="0" class="apexcharts-marker wb9403wz2 no-pointer-events" stroke="#ffffff" fill="#ea5455" fill-opacity="1" stroke-width="2" stroke-opacity="0.9" default-marker-size="0"></circle></g></g></g><g id="SvgjsG2034" class="apexcharts-datalabels" data:realIndex="0"></g></g><line id="SvgjsLine2055" x1="0" y1="0" x2="237" y2="0" stroke="#b6b6b6" stroke-dasharray="0" stroke-width="1" class="apexcharts-ycrosshairs"></line><line id="SvgjsLine2056" x1="0" y1="0" x2="237" y2="0" stroke-dasharray="0" stroke-width="0" class="apexcharts-ycrosshairs-hidden"></line><g id="SvgjsG2057" class="apexcharts-yaxis-annotations"></g><g id="SvgjsG2058" class="apexcharts-xaxis-annotations"></g><g id="SvgjsG2059" class="apexcharts-point-annotations"></g></g><rect id="SvgjsRect2027" width="0" height="0" x="0" y="0" rx="0" ry="0" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0" fill="#fefefe"></rect><g id="SvgjsG2043" class="apexcharts-yaxis" rel="0" transform="translate(-18, 0)"></g><g id="SvgjsG2025" class="apexcharts-annotations"></g></svg><div class="apexcharts-legend" style="max-height: 50px;"></div><div class="apexcharts-tooltip apexcharts-theme-light"><div class="apexcharts-tooltip-series-group" style="order: 1;"><span class="apexcharts-tooltip-marker" style="background-color: rgb(234, 84, 85);"></span><div class="apexcharts-tooltip-text" style="font-family: Helvetica, Arial, sans-serif; font-size: 12px;"><div class="apexcharts-tooltip-y-group"><span class="apexcharts-tooltip-text-label"></span><span class="apexcharts-tooltip-text-value"></span></div><div class="apexcharts-tooltip-z-group"><span class="apexcharts-tooltip-text-z-label"></span><span class="apexcharts-tooltip-text-z-value"></span></div></div></div></div><div class="apexcharts-yaxistooltip apexcharts-yaxistooltip-0 apexcharts-yaxistooltip-left apexcharts-theme-light"><div class="apexcharts-yaxistooltip-text"></div></div></div></div>
        <div class="resize-triggers"><div class="expand-trigger"><div style="width: 238px; height: 238px;"></div></div><div class="contract-trigger"></div></div></div>
    </div>

    <!-- Basic Tables start -->
    <div class="row" id="basic-table">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                   <h4 class="card-title">Orders & Payments</h4>
                </div>
                <div class="table-responsive container-fluid">
                    <form action="{{ route('superadmin.payment.filter') }}" class="row" method="get">
                        
                            <div class="col-lg-2">
                              <div class="form-group">
                                <label for="subscription"> <b>Filter By Payment Status</b> </label>
                                <select class="form-control" name="status">
                                  <option value="" >Filter By Payment Status</option>
                                  <option value="0" @if(isset($status) && $status == '0') selected @endif>UnPaid</option>
                                  <option value="1" @if(isset($status) && $status == '1') selected @endif>Paid</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label for="range"> <b>Filter By Date Range</b> </label>
                                  <select class="form-control" name="range" id="range">
                                    <option value="">Filter By </option>
                                    <option value="today" @if(isset($range) && $range == 'today') selected @endif>Today</option>
                                    <option value="week"  @if(isset($range) && $range == 'week') selected @endif>This Week</option>
                                    <option value="month" @if(isset($range) && $range == 'month') selected @endif>This Month</option>
                                    <option value="year"  @if(isset($range) && $range == 'year') selected @endif>This Year</option>
                                    <option value="custom" @if(isset($range) && $range == 'custom') selected @endif>Custom Range</option>
                                  </select>
                                </div>
                              </div>
    
                              <div class="col-lg-2" id="from_date" @if(isset($from_date) && $from_date != '') style="display: block;" @else style="display: none;" @endif >
                                <div class="form-group">
                                  <input placeholder="From Date" id="from_date_input" class="form-control" value="{{ $from_date ?? '' }}" name="from_date" type="text" onfocus="(this.type='date')" >
                                </div>
                              </div>
                              <div class="col-lg-2" id="end_date" @if(isset($end_date) && $end_date != '') style="display: block;" @else style="display: none;" @endif>
                                <div class="form-group">
                                    <input placeholder="To Date" id="end_date_input" class="form-control" value="{{ $end_date ?? '' }}" name="end_date" type="text" onfocus="(this.type='date')" >
                                </div>
                              </div>  
                            <div class="col-lg-4">
                                <div class="form-group">
                                  <button class="btn btn-success" id="apply_filters" type="submit">Apply Filters</button>
                                </div>
                              </div>
                        
                    </form>
                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Order ID </th>
                                <th>Name </th>
                                <th>Email </th>
                                <th>Phone No. </th>
                                <th>Role </th>
                                <th>Package ID </th>
                                <th>Package Name </th>
                                <th>Amount </th>
                                <th>Payment Status </th>
                                <th>Transaction ID </th>
                                <th>Purchased Date </th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            @if($orders != '' && count($orders) > 0)  
                            @foreach($orders as $key => $order)
                            
                            <tr>

                                <td>{{ $order['id'] ?? '' }}</td>
                                <td>{{$users[$order['user_id']]['name'] ?? ''}}</td>
                                <td>{{$users[$order['user_id']]['email'] ?? ''}}</td>
                                <td>{{$users[$order['user_id']]['phone_number'] ?? ''}}</td>
                                <td>Business Admin</td>
                                <td>{{ $order['package_id'] ?? '' }}</td>
                                <td>{{ $packages[$order['package_id']]['name'] ?? '' }}</td>
                                <td> @if($order['currency'] == 'INR') <i class="fa fa-inr"></i> @endif @if($order['payment_type'] == 'USD') <i class="fas fa-usd"></i> @endif {{ $order['amount'] ?? '' }}</td>
                                <td> @if(isset($order['status']) && $order['status'] == 1) <a href="#" title="Paid" class="btn btn-success"> Paid </a> @else <a href="#" title="Pending" class="btn btn-danger"> Pending </a> @endif </td>
                                <td><a href="#">{{ $order['transaction_id'] ?? '' }}</a></td>
                                <td><a href="#">{{ $order['transaction_date'] ?? '' }}</a></td>
                                
                                <td>
                                    <div class="dropdown">
                                        <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                                            <i class="fas fa-ellipsis-v"></i>
                                        </button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="{{route('superadmin.userAccount.invoice.download',['id' => $order['id']])}}" title="Download Invoice">
                                                <i class="fa fa-download"></i>
                                                <span>Download Invoice</span>
                                            </a>
                                            <a class="dropdown-item" href="{{route('superadmin.userAccount.invoice.detail',['id' => $order['id']])}}" title="See Invoice">
                                                <i class="fa fa-eye"></i>
                                              <span>See Invoice</span>
                                            </a>
                                        </div>
                                    </div>
                    
                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>                 
                </div>
            </div>
        </div>
     </div>
     <!-- Basic Tables end --> 
@endsection

@section('script')
        <!-- BEGIN: Page JS-->
        <script src="{{ asset('app-assets/js/scripts/pages/dashboard-ecommerce.js') }}"></script>
        <!-- END: Page JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/responsive.bootstrap4.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/vfs_fonts.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.rowGroup.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
        <!-- END: Page Vendor JS-->
        <script>


       function EuToUsCurrencyFormat(input) {
	return input.replace(/[,.]/g, function(x) {
		return x == "," ? "." : ",";
	});
}

$(document).ready(function() {
	//Only needed for the filename of export files.
	//Normally set in the title tag of your page.
	// document.title = 'DataTable Excel';
	// DataTable initialisation
	var table = $('#example').DataTable({
      
        "aaSorting": [],
        bPaginate: true,
        iDisplayLength: 10
		// "dom": '<"dt-buttons"Bf><"clear">lirtp',
		// "paging": true,
		// "autoWidth": true,
		// "buttons": [{
		// 	extend: 'excelHtml5',
		// 	text: 'Excel',
		// 	customize: function(xlsx) {
		// 		var sheet = xlsx.xl.worksheets['sheet1.xml'];
		// 		//All cells
		// 		$('row c', sheet).attr('s', '25');
		// 		//Second column
		// 		$('row c:nth-child(2)', sheet).attr('s', '42');
		// 		//First row
		// 		$('row:first c', sheet).attr('s', '36');
		// 		// One cell
		// 		$('row c[r^="D6"]', sheet).attr('s', '32');
		// 		// Loop over the cells in column `E` the amount column
		// 		$('row c[r^="E"]', sheet).each(function() {
		// 			if (parseFloat(EuToUsCurrencyFormat($('is t', this).text())) > 1500) {
		// 				$(this).attr('s', '17');
		// 			}
		// 		});
		// 		//All cells of row 10
		// 		$('row c[r*="10"]', sheet).attr('s', '49');
		// 		//Search all cells for a specific text
		// 		$('row* c[r]', sheet).each(function() {
		// 			if ($('is t', this).text().match(/(?:^|\b)(cover)(?=\b|$)/gmi)) {
		// 				$(this).attr('s', '20');
		// 			}
		// 		});
		// 	}
		// }]


	});
  

     

});

</script>
<script>
     $('#range').on('change', function() {
        console.log(this.value);
        if (this.value == 'custom')
        //.....................^.......
        {
            $("#from_date").css('display', 'block');
            $("#end_date").css('display', 'block');

        } else {
            $("#from_date").css('display', 'none');
            $("#end_date").css('display', 'none');
            
            $("#from_date_input").attr('value', '');
            $("#end_date_input").attr('value', '');
        }
    });
</script>
 
@endsection
