@extends('layout.master')
@section('title', 'Tags')
@section('description', 'Tags of FARS')
@section('keywords', 'admin panel, fars')
@section('author', 'FARS')

@section('css')

<!-- BEGIN: Vendor CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/vendors.min.css') }}">
<link rel="stylesheet" type="text/css"
    href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css"
    href="{{ asset('app-assets/vendors/css/tables/datatable/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css"
    href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css"
    href="{{ asset('app-assets/vendors/css/tables/datatable/rowGroup.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">
<!-- END: Vendor CSS-->
<!-- BEGIN: Page CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
<!-- END: Page CSS-->
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
<!-- END: Page CSS-->
@endsection
@section('breadcrumb_title', 'Tags')
@section('breadcrumb_2', 'Tags')
@section('content')
<section id="multiple-column-form">
    <form class="row" id="users" action="{{ route('superadmin.tag.update',$tag['id']) }}" method="POST"
        enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Tag Add</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-6 col-md-6 col-12 mb-1">
                                <div class="form-group">
                                    <label for="inputName"> <b>Tag Name</b> </label>
                                    <input type="text" id="inputName" value="{{ $tag['name'] ?? '' }}" name="name" class="form-control"
                                        placeholder="Tag Name">
                                </div>
                            </div>

                            <div class="col-xl-6 col-md-6 col-12 mb-1">
                                <div class="form-group">
                                    <label for="inputStatus"> <b>Tag Status</b> </label>
                                    <select id="inputStatus" name="status" class="form-control custom-select">
                                        <option value="0" @if(isset($tag['status']) && $tag['status'] == '0') selected @endif>InActive</option>
                                        <option value="1" @if(isset($tag['status']) && $tag['status'] == '1') selected @endif>Active</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-xl-12 col-md-12 col-12 mb-1">
                                <label for="inputDescription"> <b>Tag Description</b> </label>

                                <textarea id="inputDescription" name="description" class="form-control textarea"
                                    placeholder="Place some text here">{!! $tag['description'] ?? '' !!}</textarea>

                            </div>

                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary" style="float:left;">Update Tag</button>
    </form>
    
    </div>
</section>
@endsection



@section('script')

<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>

{{-- <script src="{{asset('plugins/summernote/summernote-bs4.min.js') }}"></script>--}}
<script>
$(function() {
    // Summernote
    $('.textarea').summernote()
})
</script>


@endsection