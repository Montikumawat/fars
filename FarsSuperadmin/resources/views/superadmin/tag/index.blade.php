@extends('layout.master')
@section('title', 'Tags')
@section('description', 'Tags of FARS')
@section('keywords', 'admin panel, fars')
@section('author', 'FARS')

@section('css')
<link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">

    <!-- BEGIN: Vendor CSS-->
    <style>
        #example_filter {
            float: right;
        }
        #example_paginate {
            float: right;
        }
            /* width */
     ::-webkit-scrollbar {
     width: 10px;
     }
     
     /* Track */
     ::-webkit-scrollbar-track {
     background: #f1f1f1;
     
     }
     
     /* Handle */
     ::-webkit-scrollbar-thumb {
     background: #958cf4;
     border-radius: 10px;
     }
     
     /* Handle on hover */
     ::-webkit-scrollbar-thumb:hover {
     background: #958cf4;
     }
      </style>
    
    <style>
    .tooltipDescription {
      position: relative;
      display: inline-block;
      border-bottom: 1px dotted black;
    }
    
    .tooltipDescription .tooltiptext {
      visibility: hidden;
      width: 120px;
      background-color: black;
      color: #fff;
      text-align: center;
      border-radius: 6px;
      padding: 5px 0;
    
      /* Position the tooltip */
      position: absolute;
      z-index: 1;
    }
    
    .tooltipDescription:hover .tooltiptext {
      visibility: visible;
    }
    </style>
    <!-- END: Page CSS-->
@endsection
@section('breadcrumb_title', 'Tags')
@section('breadcrumb_2', 'Tags')
@section('content')
    @section('content')
    <!-- Basic Tables start -->
    <div class="row" id="basic-table">
      <div class="col-12">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title">Tags Management</h4>
                  <a href="{{route('superadmin.tag.add')}}" class="btn btn-secondary" style="float: right;">Add Tag</a>
              </div>
              <div class="table-responsive container-fluid">

                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">

                      <thead>
                        <tr>
                           <th>Name </th>
                           <th>Description </th>
                           <th>Status </th>
                           <th>Action </th>
                        </tr>
                      </thead>
                      <tbody>
                        @if($tags != '' && count($tags) > 0)
                      @foreach($tags as $key => $value)
                          <tr>
                         
                          <td >{{$value['name'] ?? ''}}</td>
                          <td>
                            <div class="tooltipDescription">Description
                                <span class="tooltiptext">
                                  {!! $value['description'] ?? '' !!}
                                </span>
                            </div>                   
                           </td>

                        <td>@if(isset($value['status'])&& $value['status'] == 0) Inactive @endif @if(isset($value['status'])&& $value['status'] == 1)  Active @endif</td>

                            
                                <td>
                                  <div class="dropdown">
                                      <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                                          <i class="fas fa-ellipsis-v"></i>
                                      </button>
                                      <div class="dropdown-menu">
                                        <a class="dropdown-item" href="{{route('superadmin.tag.edit',$value['id'])}}">
                                            <i class="fa fa-pencil" aria-hidden="true"></i>
                                            <span>Edit</span>
                                        </a>
                                        <a class="dropdown-item actionDeletePackage" data-url="{{ route('superadmin.tag.destroy',$value['id']) }}" custom="{{$value['id']}}" href="javascript:void(0);">
                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                            <span>Delete</span>
                                        </a>
                                         @if(isset($value['status'])&& $value['status'] == 0) 
                                          <a class="dropdown-item" href="{{ route('superadmin.tag.activate',$value['id']) }}" custom="{{$value['id']}}">
                                            <i class="fa fa-unlock" aria-hidden="true"></i>
                                            <span>Activate</span>
                                          </a>
                                          @endif
                                          
                                          @if(isset($value['status'])&& $value['status'] == 1) 
                                          <a class="dropdown-item" href="{{ route('superadmin.tag.deactivate',$value['id']) }}" custom="{{$value['id']}}">
                                            <i class="fa fa-ban" aria-hidden="true"></i>
                                            <span>Deactivate</span>
                                          </a>
                                          @endif
                                          </div>
                                          </div>
                                  </td>
                                          </tr> 
                                          <form method="post"
                                          action="{{ route('superadmin.package.destroy',$value['id']) }}"
                                              class="packageDeleteForm">
                                              @csrf
                                              @method('DELETE')
                                              
                                          </form>
                          @endforeach
                          @endif
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
  </div>
  <!-- Basic Tables end -->
@endsection

@endsection

@section('script')
    <!-- BEGIN: Page JS-->
    <script src="{{ asset('app-assets/js/scripts/pages/dashboard-ecommerce.js') }}"></script>
    <!-- END: Page JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/responsive.bootstrap4.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/vfs_fonts.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.rowGroup.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
        <!-- END: Page Vendor JS-->
        <script>
       function EuToUsCurrencyFormat(input) {
	return input.replace(/[,.]/g, function(x) {
		return x == "," ? "." : ",";
	});
}

$(document).ready(function() {
	//Only needed for the filename of export files.
	//Normally set in the title tag of your page.
	// document.title = 'DataTable Excel';
	// DataTable initialisation
	$('#example').DataTable({
    "aaSorting": [],
    bPaginate: true,
    iDisplayLength: 10
		// "dom": '<"dt-buttons"Bf><"clear">lirtp',
		// "paging": true,
		// "autoWidth": true,
		// "buttons": [{
		// 	extend: 'excelHtml5',
		// 	text: 'Excel',
		// 	customize: function(xlsx) {
		// 		var sheet = xlsx.xl.worksheets['sheet1.xml'];
		// 		//All cells
		// 		$('row c', sheet).attr('s', '25');
		// 		//Second column
		// 		$('row c:nth-child(2)', sheet).attr('s', '42');
		// 		//First row
		// 		$('row:first c', sheet).attr('s', '36');
		// 		// One cell
		// 		$('row c[r^="D6"]', sheet).attr('s', '32');
		// 		// Loop over the cells in column `E` the amount column
		// 		$('row c[r^="E"]', sheet).each(function() {
		// 			if (parseFloat(EuToUsCurrencyFormat($('is t', this).text())) > 1500) {
		// 				$(this).attr('s', '17');
		// 			}
		// 		});
		// 		//All cells of row 10
		// 		$('row c[r*="10"]', sheet).attr('s', '49');
		// 		//Search all cells for a specific text
		// 		$('row* c[r]', sheet).each(function() {
		// 			if ($('is t', this).text().match(/(?:^|\b)(cover)(?=\b|$)/gmi)) {
		// 				$(this).attr('s', '20');
		// 			}
		// 		});
		// 	}
		// }]
	});
});
    
    </script>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    // Delete Landing Page
    $(".actionDeletePackage").on('click', function(e) {
        e.preventDefault();

        var element = $(this);
        var url = element.attr('data-url');
        var custom = element.attr('custom');
        console.log(custom);
        console.log(url);

        element.blur();


        //pop up
        swal({
                title: 'Are you sure you want to Delete',
                icon: "warning",
                buttons: true,
                dangerMode: true,
                showLoaderOnConfirm: true,
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: 'Confirm',
                cancelButtonText: 'Cancel',
                closeOnConfirm: false,
            })
            .then((willDelete) => {
                if (willDelete) {
                    swal("Your Item has been deleted!", {
                        icon: "success",
                    });
                     window.location.href = url;
                    // $('.packageDeleteForm').submit();
                } else {
                    swal("Your Item is safe!");
                }
            });


    });

</script>
@endsection
