@extends('layout.master')
@section('title', 'Payment Create')
@section('description', 'Payment Create of FARS')
@section('keywords', 'admin panel, fars')
@section('author', 'FARS')

@section('css')
    {{-- <!-- BEGIN: Page CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/pickers/form-flat-pickr.css') }}">
<!-- END: Page CSS--> --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/select/select2.min.css') }}">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/tables/datatable/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/tables/datatable/rowGroup.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">
    <!-- END: Vendor CSS-->
    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
    <!-- END: Page CSS-->
@endsection
@section('breadcrumb_title', 'Payment Create')
@section('breadcrumb_2', 'Payment Create')
@section('content')
    <section id="multiple-column-form">
        <form class="row" id="users" action="#" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Payment Create</h4>
                            <button type="button" class="btn btn-success" style="float: right;"  data-toggle="modal" data-target="#default">Add Fields</button>
                        </div>
                        <div class="card-body">
                            <form class="row" id="users" action="#" method="POST" enctype="multipart/form-data">
                                <div class="row">
                                   
                                    <div class="col-xl-6 col-md-12 col-12">
                                        <div class="form-group">
                                            <label for="inputName"> <b>Payment Provider</b> </label>
                                           <input type="text" id="inputName" name="MAIL_HOST" class="form-control">
                                        </div>
                                    </div>
                                    {{-- data-placeholder="Select Users" --}}
                                    <div class="col-xl-6 col-md-12 col-12 mb-1">
                                        <label> <b>Select Fields</b> </label>
                                        <select class="select2" multiple="multiple" >
                                            <option value="0">Field1</option>
                                            <option value="0">Field2</option>
                                            <option value="0">Field3</option>
                                            <option value="0">Field4</option>
                                        </select>

                                    </div>
                                    <div class="col-12">
                                        <button type="reset" class="btn btn-primary mr-1">Submit</button>
                                        <button type="reset" class="btn btn-outline-secondary">Reset</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </form> 
    </section>
@endsection

<!-- Modal -->
<div class="modal fade text-left" id="default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Add Field</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="inputPackage"> <b>Field</b> </label>                  
                    <input type="text" name="field_name" id="field_name" class="form-control">
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" id="acceptPaymentGateway" class="btn btn-primary" data-dismiss="modal">Save</button>
            </div>
        </div>
    </div>
</div>
@section('script')


<!-- BEGIN: Page Vendor JS-->
<script src="{{ asset('app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
<!-- END: Page Vendor JS-->

<script src="{{ asset('app-assets/js/scripts/forms/form-select2.js') }}"></script>
@endsection
