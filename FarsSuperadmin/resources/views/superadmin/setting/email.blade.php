@extends('layout.master')
@section('title', 'Email')
@section('description', 'Email of FARS')
@section('keywords', 'admin panel, fars')
@section('author', 'FARS')

@section('css')
    {{-- <!-- BEGIN: Page CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/pickers/form-flat-pickr.css') }}">
<!-- END: Page CSS--> --}}

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/tables/datatable/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/tables/datatable/rowGroup.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">
    <!-- END: Vendor CSS-->
    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
    <!-- END: Page CSS-->
@endsection
@section('breadcrumb_title', 'Email')
@section('breadcrumb_2', 'Email')
@section('content')
    <section id="multiple-column-form">
        <form class="row" id="users" action="{{route('superadmin.setting.email.store')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Email SMTP Setting</h4>
                        </div>
                        <div class="card-body">
                            <form class="form">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="inputStatus"><b>Type</b></label>
                                            <select  name="type" class="form-control custom-select" required>
                                              <option selected disabled>Select SMTP</option>
                                              {{-- <option value="sendmail">Sendmail</option> --}}
                                              <option value="smtp" @if(isset($email['type']) && $email['type'] == 'smtp' ) selected @endif>SMTP</option>

                                            </select>
                                        </div>
                                      </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="inputName"> <b>MAIL HOST</b> </label>
                                            <input type="text" name="mail_host" value="{{$email['mail_host'] ?? '' }}" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="inputName"> <b>MAIL PORT</b> </label>
                                            <input type="text" name="mail_port" value="{{$email['mail_port'] ?? '' }}" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                      <div class="form-group">
                                        <label for="inputName"> <b>MAIL USERNAME</b> </label>
                                            <input type="text" name="mail_username" value="{{$email['mail_username'] ?? '' }}" class="form-control" required>
                                      </div>
                                    </div>
                                    <div class="col-md-6 col-12 form-password-toggle">
                                      <div class="form-group">
                                        <label for="inputName"> <b>MAIL PASSWORD</b> </label>
                                        {{-- <input type="text" name="mail_password" value="{{$email['mail_password'] ?? '' }}" class="form-control" required> --}}
                                        <input id="password" type="password" class="form-control login-form @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Enter your password">
                                        <div class="input-group-append">
                                            <span class="input-group-text cursor-pointer"><i data-feather="eye"></i></span>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6 col-12 input-group input-group-merge form-password-toggle">
                                       <div class="from-group">

                                       </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                        <label for="inputStatus"><b>MAIL ENCRYPTION</b></label>
                                        <select  name="mail_encryption" class="form-control custom-select" required>
                                          <option selected disabled>Select Mail Encryption</option>
                                          <option value="tls" @if(isset($email['mail_encryption']) && $email['mail_encryption'] == 'tls' ) selected @endif>TLS</option>
                                          <option value="ssl" @if(isset($email['mail_encryption']) && $email['mail_encryption'] == 'ssl' ) selected @endif>SSL</option>

                                        </select>
                                        </div>
                                    </div>
                                      <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="inputName"> <b>MAIL FROM ADDRESS</b> </label>
                                            <input type="text" name="mail_from_address" value="{{$email['mail_from_address'] ?? '' }}" class="form-control" required>
                                        </div>
                                      </div>
                                      <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="inputName"> <b>MAIL FROM NAME</b> </label>
                                            <input type="text" name="mail_from_name" value="{{$email['mail_from_name'] ?? '' }}" class="form-control" required>
                                        </div>
                                      </div>

                                    <div class="col-12">
                                        <button type="submit" class="btn btn-primary mr-1">Submit</button>
                                        <button type="reset" class="btn btn-outline-secondary">Reset</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection


@section('script')


<!-- BEGIN: Page Vendor JS-->
<script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/responsive.bootstrap4.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/vfs_fonts.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.rowGroup.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
<!-- END: Page Vendor JS-->

@endsection
