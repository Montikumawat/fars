@extends('layout.master')
@section('title', 'Add Payment')
@section('description', 'Add Payment')
@section('keywords', 'admin panel, fars')
@section('author', 'FARS')

@section('css')
    {{-- <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/pickers/form-flat-pickr.css') }}">
    <!-- END: Page CSS--> --}}

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/tables/datatable/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/tables/datatable/rowGroup.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">
    <!-- END: Vendor CSS-->
    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
    <!-- END: Page CSS-->
@endsection
@section('breadcrumb_title', 'Add Payment')
@section('breadcrumb_2', 'Add Payment')

@section('content')

<section id="multiple-column-form">


        <div class="row">
            {{-- <div class="col-lg-12" id="paymentDiv">

                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"> Click on button to add a payment gateway  </h4>
                        <button type="button" class="btn btn-success" style="float: right;"  data-toggle="modal" data-target="#default">Add Payment Gateway</button>
                         <a href="{{ route('superadmin.paymentSetting.create') }}" class="btn btn-success" style="float: right;">Add Payment Gateway</a>
                    </div>

                </div>
            </div> --}}
            <div class="col-md-6 col-lg-6">
                <form action="{{ route('superadmin.setting.razorpay.store') }}" method="post">
                    @csrf
                    <div class="card text-center">
                    <div class="card-header">Razorpay</div>
                    <div class="card-body">
                        <h4 class="card-title">Razorpay Credentials</h4>


                            <div class="form-group">
                                <label for="inputRazorpayClientId" style="float: left;"> <b>Razorpay Client Id</b> </label>
                                    <input type="text" name="razorpay_key" value="{{$razorpay['key'] ?? ''}}" id="inputRazorpayClientId" class="form-control" required>
                            </div>

                                <div class="form-group">
                                  <label for="inputRazorpaySecretKey" style="float: left;"> <b>Razorpay Secret Key</b> </label>
                                  <input type="text" name="razorpay_secret_key" value="{{$razorpay['secret_key'] ?? ''}}" id="inputRazorpaySecretKey" class="form-control" required>
                                </div>
                                <br>
                                <div class="form-group">
                                    <label for="inputRazorpayStatus" style="float: left;"> <b>Status</b> </label>
                                    <div class="custom-control custom-switch custom-switch-primary">

                                        <input type="checkbox" name="status" value="{{ $razorpay['status'] ?? '' }}" class="custom-control-input" id="customSwitch10" checked="checked" >
                                        <label class="custom-control-label" for="customSwitch10">
                                            <span class="switch-icon-left"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check"><polyline points="20 6 9 17 4 12"></polyline></svg></span>
                                            <span class="switch-icon-right"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></span>
                                        </label>
                                    </div>
                                </div>


                    </div>
                    <div class="card-footer text-muted">
                        <button type="submit" class="btn btn-primary mr-1">Submit</button>
                        {{-- <button type="reset" class="btn btn-outline-secondary">Reset</button> --}}
                    </div>

                    </div>
                </form>
            </div>

            <div class="col-md-6 col-lg-6">
                <form action="{{ route('superadmin.setting.stripe.store') }}" method="post">
                    @csrf
                <div class="card text-center">
                    <div class="card-header">Stripe</div>
                    <div class="card-body">
                        <h4 class="card-title">Stripe Credentials</h4>

                        <div class="form-group">
                            <label for="inputStripeKey" style="float: left;"> <b>Stripe Key</b> </label>
                                <input type="text" name="stripe_key" value="{{$stripe['key'] ?? ''}}" id="inputStripeKey" class="form-control" required>
                        </div>

                            <div class="form-group">
                              <label for="inputStripeSecretKey" style="float: left;"> <b>Stripe Secret Key</b> </label>
                              <input type="text" name="stripe_secret_key" value="{{$stripe['secret_key'] ?? ''}}" id="inputStripeSecretKey" class="form-control" required>
                            </div>
                            <br>
                            <div class="form-group">
                                <label for="inputStripeStatus" style="float: left;"> <b>Status</b> </label>
                                <div class="custom-control custom-switch custom-switch-primary">

                                    <input type="checkbox" name="status" class="custom-control-input" id="customSwitch" checked="checked">
                                    <label class="custom-control-label" for="customSwitch">
                                        <span class="switch-icon-left"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check"><polyline points="20 6 9 17 4 12"></polyline></svg></span>
                                        <span class="switch-icon-right"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></span>
                                    </label>
                                </div>
                            </div>

                    </div>
                    <div class="card-footer text-muted">
                        <button type="submit" class="btn btn-primary mr-1">Submit</button>
                        {{-- <button type="reset" class="btn btn-outline-secondary">Reset</button> --}}
                    </div>
                </div>
               </form>
            </div>


        </div>

</section>

<!-- Modal -->
<div class="modal fade text-left" id="default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Add a Payment Gateway</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="inputPackage"> <b>Name</b> </label>
                    <input type="text" name="payment_gateway_name" id="payment_gateway_name" class="form-control">
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" id="acceptPaymentGateway" class="btn btn-primary" data-dismiss="modal">Save</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')

<script>

  $(document).ready(function(){
    $('#acceptPaymentGateway').on('click', function() {

      var name = '';

      var html = '';

      name = $('#payment_gateway_name').val();

      html = `<div class="card">
                        <div class="card-header">
                            <h4 class="card-title"> `+name+` </h4>
                        </div>
                        <div class="card-body">
                            <form class="form">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                          <label for="inputName"> <b> `+name+` Key</b> </label>
                                          <input type="text" id="inputPKey" name="pkey" class="form-control">
                                        </div>
                                      </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                          <label for="inputName"> <b> `+name+` Secret Key </b> </label>
                                          <input type="text" id="inputSKey" name="skey" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <button type="reset" class="btn btn-primary mr-1">Submit</button>
                                        <button type="reset" class="btn btn-outline-secondary">Reset</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>`

      $('#paymentDiv').append(html);

  });
});

</script>

@endsection
