@extends('layout.master')
@section('title', 'SMS Settings')
@section('description', 'SMS Settings')
@section('keywords', 'admin panel, fars')
@section('author', 'FARS')

@section('css')
    {{-- <!-- BEGIN: Page CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/pickers/form-flat-pickr.css') }}">
<!-- END: Page CSS--> --}}

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/tables/datatable/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/tables/datatable/rowGroup.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">
    <!-- END: Vendor CSS-->
    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
    <!-- END: Page CSS-->
@endsection
@section('breadcrumb_title', 'SMS Settings')
@section('breadcrumb_2', 'SMS Settings')
@section('content')
    <section id="multiple-column-form">

        <div class="row">

            <div class="col-lg-6 col-md-6">
                <form class="row" action="{{route('superadmin.setting.sms.store', $sms[0]['id'])}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" value="{{ $sms[0]['id']}}">
                    <div class="row">
                        <div class="col-11">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">SMS Settings</h4>
                                </div>
                                <div class="card-body">
                                    <form class="form">
                                        <div class="row">
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label for="inputName"> <b>Api Key</b> </label>
                                        <input type="text" id="inputName"  name="api_key" value="{{$sms[0]['api_key'] ?? '' }}" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label for="inputName"> <b>Api Secret Key</b> </label>
                                                    <input type="text" id="inputName" name="api_secret_key" value="{{$sms[0]['api_secret_key'] ?? '' }}"  class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                              <div class="form-group">
                                                <label for="inputName"> <b>Signature Secret</b> </label>
                                                <input type="text" id="inputName" name="signature_secret" value="{{$sms[0]['signature_secret'] ?? '' }}" class="form-control" required>
                                              </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="inputStripeStatus" style="float: left;"> <b>Status</b> </label>
                                                <div class="custom-control custom-switch custom-switch-primary">

                                                    <input type="checkbox" name="status" class="custom-control-input" id="customSwitch" {{ $sms[0]['status'] == 1 ? 'checked' : '' }}>
                                                    <label class="custom-control-label" for="customSwitch">
                                                        <span class="switch-icon-left"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check"><polyline points="20 6 9 17 4 12"></polyline></svg></span>
                                                        <span class="switch-icon-right"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></span>
                                                    </label>
                                                </div>
                                            </div>

                                            <input type="hidden" id="inputName" name="type" value="{{$sms[0]['type'] ?? 'sms' }}"  class="form-control">

                                            <div class="col-12">
                                                <button type="submit" class="btn btn-primary mr-1">Submit</button>
                                                {{-- <button type="reset" class="btn btn-outline-secondary">Reset</button> --}}
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="col-lg-6 col-md-6">
                <form class="row" action="{{route('superadmin.setting.sms.store', $sms[1]['id'])}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" value="{{ $sms[1]['id']}}">

                    <div class="row">
                        <div class="col-11">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">SMS Settings</h4>
                                </div>
                                <div class="card-body">
                                    <form class="form">
                                        <div class="row">
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label for="inputName"> <b>Api Key</b> </label>
                                        <input type="text" id="inputName"  name="api_key" value="{{$sms[1]['api_key'] ?? '' }}" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label for="inputName"> <b>Api Secret Key</b> </label>
                                                    <input type="text" id="inputName" name="api_secret_key" value="{{$sms[1]['api_secret_key'] ?? '' }}"  class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                              <div class="form-group">
                                                <label for="inputName"> <b>Signature Secret</b> </label>
                                                <input type="text" id="inputName" name="signature_secret" value="{{$sms[1]['signature_secret'] ?? '' }}" class="form-control" required>
                                              </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="inputStripeStatus" style="float: left;"> <b>Status</b> </label>
                                                <div class="custom-control custom-switch custom-switch-primary">

                                                    <input type="checkbox" name="status" class="custom-control-input" id="customSwitch1" {{$sms[1]['status'] == 1 ? 'checked' : '' }}>
                                                    <label class="custom-control-label" for="customSwitch1">
                                                        <span class="switch-icon-left"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check"><polyline points="20 6 9 17 4 12"></polyline></svg></span>
                                                        <span class="switch-icon-right"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></span>
                                                    </label>
                                                </div>
                                            </div>

                                            <input type="hidden" id="inputName" name="type" value="{{$sms[1]['type'] ?? 'sms' }}"  class="form-control">

                                            <div class="col-12">
                                                <button type="submit" class="btn btn-primary mr-1">Submit</button>
                                                {{-- <button type="reset" class="btn btn-outline-secondary">Reset</button> --}}
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

        </div>







    </section>
@endsection

@section('script')


<!-- BEGIN: Page Vendor JS-->
<script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/responsive.bootstrap4.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/vfs_fonts.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.rowGroup.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
<!-- END: Page Vendor JS-->

@endsection
