@extends('layout.master')
@section('title', 'Customers')
@section('description', 'Customers of FARS')
@section('keywords', 'admin panel, fars')
@section('author', 'FARS')

@section('css')


<link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
    <style>
      #example_filter {
          float: right;
      }
      #example_paginate {
          float: right;
      }
          /* width */
   ::-webkit-scrollbar {
   width: 10px;
   }

   /* Track */
   ::-webkit-scrollbar-track {
   background: #f1f1f1;

   }

   /* Handle */
   ::-webkit-scrollbar-thumb {
   background: #958cf4;
   border-radius: 10px;
   }

   /* Handle on hover */
   ::-webkit-scrollbar-thumb:hover {
   background: #958cf4;
   }
    </style>
    <style>
    .tooltipDescription {
      position: relative;
      display: inline-block;
      border-bottom: 1px dotted black;
    }

    .tooltipDescription .tooltiptext {
      visibility: hidden;
      width: 120px;
      background-color: black;
      color: #fff;
      text-align: center;
      border-radius: 6px;
      padding: 5px 0;

      /* Position the tooltip */
      position: absolute;
      z-index: 1;
    }

    .tooltipDescription:hover .tooltiptext {
      visibility: visible;
    }
    #apply_filters {
      margin-top: 23px;
    }
</style>
    <!-- END: Page CSS-->
@endsection
@section('breadcrumb_title', 'Customers')
@section('breadcrumb_2', 'Customers')
@section('content')

  <!-- Basic Tables start -->
    <div class="row" id="basic-table">
      <div class="col-12">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title">Customers Management</h4>
                  <a href="{{route('superadmin.userAccount.create')}}" class="btn btn-secondary" style="float: right;">Add Customers</a>
              </div>
              <div class="table-responsive container-fluid">
                <form class="row" action={{route('superadmin.userAccount.filter')}} method="get">
                  <div class="col-lg-3">
                    <div class="form-group">
                      <label for="status"> <b>Filter By Status</b> </label>
                      <select class="form-control" name="status">
                        <option value="">Filter By Status</option>
                        <option value="0" @if(isset($status) && $status == '0') selected @endif>Inactive</option>
                        <option value="1" @if(isset($status) && $status == '1') selected @endif>Active</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-lg-3">
                    <div class="form-group">
                      <label for="subscription"> <b>Filter By Subscription</b> </label>
                      <select class="form-control" name="package_id">
                        <option value="">Filter By Subscription</option>
                        @if($packs != '' && count($packs) > 0)
                        @foreach($packs as $pack)
                        <option value="{{$pack['id']}}" @if(isset($package_id) && $package_id == $pack['id']) selected @endif>{{ $pack['name'] ?? '' }}</option>
                        @endforeach
                        @endif
                      </select>
                    </div>
                  </div>
                  <div class="col-lg-3">
                      <div class="form-group">
                        <button class="btn btn-success" id="apply_filters" type="submit">Apply Filters</button>
                      </div>
                  </div>
                </form>
                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                      <thead>
                      <tr>
                            <th>Name </th>
                            <th>Email </th>
                            <th>Phone </th>
                            <th>Account Type </th>
                            <th>Company </th>
                            <th>Customer Type</th>
                            <th>Account </th>
                            <th>Status </th>
                            <th>Payment </th>
                            <th>Subscription </th>
                            <th>Billing Type </th>
                            <th>Action</th>
                      </tr>
                      </thead>
                      <tbody>
                        @if($users != '' && count($users) > 0)
                        @foreach($users as $key => $user)
                          <tr>
                              <td>{{ $key+1 }}</td>
                              <td>{{ $user['email'] ?? ''}}</td>
                              <td>{{ $user['phone_number'] ?? ''}}</td>
                              <td>Business Admin</td>
                              <td>{{ $user['company_name'] ?? ''}}</td>
                              <td>{{ $user['customer_type'] ?? ''}}</td>
                              <td>{{ $user['account_number'] ?? '' }}</td>
                              {{-- <td>{{ $user->status }}</td> --}}
                              <td>@if($user['status'] == 0)InActive @endif @if($user['status'] == 1)Active @endif @if($user['status'] == 2)Block @endif @if($user['status'] == 3)UnBlock @endif</td>
                              <td>@if(isset($user['payment_method']) && $user['payment_method'] == 0) Stripe @else Razorpay @endif </td>

                              <td>{{ $packages[$user['id']]['name'] ?? ''}}</td>
                              <td> {{($packages[$user['id']]['billing_type'] ?? '')}}</td>

                              <td>
                                  <div class="dropdown">
                                      <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                                          <i class="fas fa-ellipsis-v"></i>
                                      </button>
                                      <div class="dropdown-menu">
                                          <a class="dropdown-item" href="{{route('superadmin.userAccount.edit',$user['id'])}}">
                                              <i class="fa fa-edit"></i>
                                              <span>Edit</span>
                                          </a>
                                          <a class="dropdown-item" href="{{route('superadmin.userAccount.destroy',$user['id'])}}">
                                            <i class="fa fa-trash"></i>
                                            <span>Delete</span>
                                          </a>
                                          <a class="dropdown-item" href="{{route('superadmin.userAccount.view',$user['id'])}}">
                                            <i class="fas fa-eye"></i>
                                            <span>View</span>
                                          </a>
                                          <a class="dropdown-item" href="{{route('superadmin.userAccount.block',$user['id'])}}">
                                            <i class="fa fa-ban" aria-hidden="true"></i>
                                            <span>Block</span>
                                          </a>
                                          <a class="dropdown-item" href="{{route('superadmin.userAccount.unblock',$user['id'])}}">
                                            <i class="fa fa-unlock" aria-hidden="true"></i>
                                            <span>UnBlock</span>
                                          </a>

                                      </div>
                                  </div>
                              </td>
                          </tr>
                        @endforeach
                        @endif
                      </tbody>
                </table>
              </div>
          </div>
      </div>
    </div>
  <!-- Basic Tables end -->


@endsection

@section('script')

        <!-- BEGIN: Page Vendor JS-->
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/responsive.bootstrap4.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
        {{-- <script src="{{ asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script> --}}
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/vfs_fonts.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.rowGroup.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
        <!-- END: Page Vendor JS-->
        <script>
       function EuToUsCurrencyFormat(input) {
	           return input.replace(/[,.]/g, function(x) {
		         return x == "," ? "." : ",";
	            });
            }

   $(document).ready(function() {
	//Only needed for the filename of export files.
	//Normally set in the title tag of your page.
	// document.title = 'DataTable Excel';
	// DataTable initialisation
	$('#example').DataTable({
    "aaSorting": [],
    bPaginate: true,
    iDisplayLength: 10
	});
  });

</script>
@endsection
