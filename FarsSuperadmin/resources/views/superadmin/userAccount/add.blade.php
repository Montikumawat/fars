@extends('layout.master')
@section('title', 'Users')
@section('description', 'Users of FARS')
@section('keywords', 'admin panel, fars')
@section('author', 'FARS')

@section('css')

@endsection
@section('breadcrumb_title', 'Users')
@section('breadcrumb_2', 'Add User')

@section('content')

    <section id="multiple-column-form">

        <form class="row" id="users" action="{{ route('superadmin.userAccount.store') }}" method="POST" enctype="multipart/form-data">
            @csrf

            <div class="row">
                <div class="col-12">

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Customer Info</h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-xl-3 col-md-6 col-12 mb-1">
                                    <label for="inputName"> <b>Name</b> </label>
                                    <input type="text" id="inputName" name="name" class="form-control" required>
                                </div>
                                <div class="col-xl-3 col-md-6 col-12 mb-1">
                                    <label for="inputName"> <b>Phone Number</b> </label>
                                    <input type="number" id="inputPhone" name="phone_number" class="form-control" required>
                                </div>
                                <div class="col-xl-3 col-md-6 col-12 mb-1">
                                    <label for="inputName"> <b>Email</b> </label>
                                    <input type="email" id="inputEmail" name="email" class="form-control @error('email') is-invalid @enderror" required>
                                    @error('email')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-xl-3 col-md-6 col-12 mb-1">
                                    <label for="inputName"> <b>Password</b> </label>
                                    <input type="password" id="inputPassword" name="password" class="form-control" required>
                                </div>

                                {{-- <div class="col-xl-3 col-md-6 col-12 mb-1">
                                    <label for="inputRole"> <b>Role</b> </label>
                                    <select id="inputRole" name="role" class="form-control custom-select" required>
                                        <option selected disabled>Select Role</option>
                                        <option value="0">Admin</option>
                                        <option value="1">User</option>


                                    </select>
                                </div> --}}
                                <div class="col-xl-6 col-md-6 col-12 mb-1">
                                    <label for="inputDescription"> <b>Address Line 1</b> </label>
                                    {{-- <textarea id="inputAddress" name="address" class="form-control" rows="4"></textarea> --}}
                                    <input type="text" id="inputaddress" name="address" class="form-control" required>
                                </div>
                                <div class="col-xl-6 col-md-6 col-12 mb-1">
                                    <label for="inputDescription"> <b>Address Line 2</b> </label>
                                    {{-- <textarea id="inputAddress" name="address" class="form-control" rows="4"></textarea> --}}
                                    <input type="text" id="inputaddress" name="address_line_2" class="form-control" required>
                                </div>
                                <div class="col-xl-3 col-md-6 col-12 mb-1">
                                    <label for="inputCompanyName"> <b>Company Name</b> </label>
                                    <input type="text" id="inputCompanyName" name="company_name" class="form-control" required>
                                </div>

                                <div class="col-xl-3 col-md-6 col-12 mb-1">
                                   <label for="inputCustomerType"> <b>Customer Type</b> </label>
                                    {{-- <input type="text" id="inputCustomerType" name="customer_type" value="{{ $customer['customer_type'] ?? '' }}" class="form-control" required> --}}
                                    <select id="customer_type" name="customer_type" class="form-control custom-select" required>
                                        <option selected disabled>Select Status</option>
                                        <option value="0">Corporate</option>
                                        <option value="1">Retail</option>


                                    </select>
                                </div>

                                <div class="col-xl-3 col-md-6 col-12 mb-1">
                                    <label for="inputStatus"> <b>Customer Status</b> </label>
                                    <select id="inputStatus" name="status" class="form-control custom-select" required>
                                        <option selected disabled>Select Status</option>
                                        <option value="0">InActive</option>
                                        <option value="1">Active</option>
                                        <option value="2">Block</option>
                                        <option value="3">UnBlock</option>

                                    </select>
                                </div>

                                <div class="col-xl-3 col-md-6 col-12 mb-1">
                                    <label for="inputPackage"> <b>Customer Package</b> </label>
                                    <select id="inputPackage" name="package_id" class="form-control custom-select" required>
                                        <option selected disabled>Select Package</option>
                                        @if(count($packages) > 0)
                                        @foreach($packages as $package)
                                        <option value="{{ $package['id'] }}">{{ $package['name'] }} (@if($package['billing_type'] == 0) Monthly @else Yearly @endif)</option>
                                        @endforeach
                                        @endif

                                    </select>

                                </div>


                                {{-- <div class="col-xl-3 col-md-6 col-12 mb-1">
                                    <label for="inputPackage"> <b>Customer Billing Type</b> </label>
                                    <select id="inputPackage" name="billing_type" class="form-control custom-select" required>
                                        <option selected disabled>Select Billing Type</option>
                                        <option value="0">Monthly</option>
                                        <option value="1">Yearly</option>

                                    </select>
                                </div> --}}

                                {{-- <div class="col-lg-4 col-md-6 col-12 mb-1">
                                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-default">Upgrade Package</button>
                                </div> --}}


                              </div>


                        </div>
                         <!-- /.card-body -->
                    </div>
                        <!-- /.card -->

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Billing Information</h3>
                            </div>
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-xl-6 col-md-6 col-12 mb-1">
                                        <label for="inputEstimatedBudget"> <b>Billing Address Line - 1</b> </label>
                                        <input type="text" name="billing_address" id="inputEstimatedBudget" class="form-control" required>
                                    </div>

                                    <div class="col-xl-6 col-md-6 col-12 mb-1">
                                        <label for="inputEstimatedBudget"> <b>Billing Address Line - 2</b> </label>
                                        <input type="text" name="billing_address_line_2" id="inputEstimatedBudget2" class="form-control" required>
                                    </div>

                                    <div class="col-xl-6 col-md-6 col-12 mb-1">
                                        <label for="inputEstimatedBudget"> <b>Street Address Line - 1</b> </label>
                                        <input type="text" name="street_address" id="inputEstimatedBudget3" class="form-control" required>
                                    </div>

                                    <div class="col-xl-6 col-md-6 col-12 mb-1">
                                        <label for="inputEstimatedBudget"> <b>Street Address Line - 2</b> </label>
                                        <input type="text" name="street_address_line_2" id="inputEstimatedBudget4" class="form-control" required>
                                    </div>

                                    <div class="col-xl-3 col-md-6 col-12 mb-1">
                                        <label for="inputEstimatedBudget"> <b>City</b> </label>
                                        <input type="text" name="city" id="inputEstimatedBudget" class="form-control" required>
                                    </div>
                                    <div class="col-xl-3 col-md-6 col-12 mb-1">
                                        <label for="inputEstimatedBudget"> <b>State / Province</b> </label>
                                        <input type="text" name="state" id="inputEstimatedBudget" class="form-control" required>
                                    </div>
                                    <div class="col-xl-3 col-md-6 col-12 mb-1">
                                        <label for="inputEstimatedBudget"> <b>Postal / Zip Code</b> </label>
                                        <input type="text" name="zip_code" id="inputEstimatedBudget" class="form-control" required>
                                    </div>
                                    <div class="col-xl-3 col-md-6 col-12 mb-1" id="divTouchPoint">
                                        <label for="inputTouchPoint"> <b>Country</b> </label>
                                        <select id="inputCountry"  name="country_id" class="form-control custom-select" required>
                                          <option selected disabled>Select Country </option>
                                          @if($countries->count() > 0)
                                          @foreach($countries as $country)
                                          <option value="{{ $country->id }}">{{ $country->name }}</option>
                                          @endforeach
                                          @endif

                                        </select>
                                      </div>

                                      <div class="col-xl-3 col-md-6 col-12 mb-1">
                                        <label for="inputName"> <b>Account Number</b> </label>
                                        <input type="number" id="inputAccount" name="account_number" class="form-control" required>
                                    </div>

                                      <div class="col-xl-3 col-md-6 col-12 mb-1" id="divTouchPoint">
                                        <label for="inputTouchPoint"> <b>Payment Method</b> </label>
                                        <select id="inputpayment"  name="payment_method" class="form-control custom-select" required>
                                          <option selected disabled>Select Payment Method </option>
                                          <option value="0">Stripe </option>
                                          <option value="1">Razorpay</option>
                                        </select>
                                      </div>
                                </div>

                            </div>
                             <!-- /.card-body -->
                        </div>
                            <!-- /.card -->


                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">Tax Information</h3>
                                </div>
                                <div class="card-body">

                                    <div class="row">
                                        <div class="col-xl-6 col-md-6 col-12 mb-1">
                                            <label for="inputEstimatedBudget"> <b>Pan Card Number</b> </label>
                                            <input type="text" name="pan_card_number" id="inputEstimatedBudget12" class="form-control">
                                        </div>

                                        <div class="col-xl-6 col-md-6 col-12 mb-1">
                                            <label for="inputEstimatedBudget"> <b>GST Number</b> </label>
                                            <input type="text" name="gst_number" id="inputEstimatedBudget34" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                 <!-- /.card-body -->
                            </div>
                                <!-- /.card -->

                                <div class="col-12" style="margin-bottom: 30px;">
                                    <button type="submit" class="btn btn-primary mr-1 waves-effect waves-float waves-light" style="float: right;">Submit</button>
                                    {{-- <button type="reset" class="btn btn-outline-secondary waves-effect">Reset</button> --}}
                                    <a href="{{url()->previous()}}" class="btn btn-outline-secondary waves-effect">Back</a>
                                </div>

                            <!-- /.card -->

                </div>
            </div>
        </form>
    </section>
    <!-- Modal -->
    <div class="modal fade text-left" id="default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">Upgrade Package</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="inputPackage">Customer Current Package</label>
                        <select id="inputPackage" name="package" class="form-control custom-select">
                            {{-- <option selected disabled>Select Package</option> --}}
                            <option value="0">Corporate</option>
                            <option value="1">Retail</option>

                        </select>

                    </div>
                    <div class="form-group">
                        <label for="inputPackage">Upgrade to</label>
                        <select id="inputPackage" name="package" class="form-control custom-select">
                            <option selected disabled>Select Package</option>
                            <option value="0">Corporate</option>
                            <option value="1">Retail</option>

                        </select>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Accept</button>
                </div>
            </div>
        </div>
    </div>
@endsection



@section('script')

@endsection
