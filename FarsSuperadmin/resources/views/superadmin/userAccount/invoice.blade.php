@extends('layout.master')
@section('title', 'Invoice')
@section('description', 'Invoice of FARS')
@section('keywords', 'admin panel, fars')
@section('author', 'FARS')

@section('css')
    {{-- <!-- BEGIN: Page CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/pickers/form-flat-pickr.css') }}">
<!-- END: Page CSS--> --}}

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/tables/datatable/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/tables/datatable/rowGroup.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">
    <!-- END: Vendor CSS-->
    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/pickers/form-flat-pickr.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/pages/app-invoice.css') }}">

    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">
        <style>
			.invoice-box {
				max-width: 800px;
				margin: auto;
				padding: 30px;
				border: 1px solid #eee;
				box-shadow: 0 0 10px rgba(0, 0, 0, 0.15);
				font-size: 16px;
				line-height: 24px;
				font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
				color: #555;
			}

			.invoice-box table {
				width: 100%;
				line-height: inherit;
				text-align: left;
			}

			.invoice-box table td {
				padding: 5px;
				vertical-align: top;
			}

			.invoice-box table tr td:nth-child(2) {
				text-align: right;
			}

			.invoice-box table tr.top table td {
				padding-bottom: 20px;
			}

			.invoice-box table tr.top table td.title {
				font-size: 45px;
				line-height: 45px;
				color: #333;
			}

			.invoice-box table tr.information table td {
				padding-bottom: 40px;
			}

			.invoice-box table tr.heading td {
				background: #eee;
				border-bottom: 1px solid #ddd;
				font-weight: bold;
			}

			.invoice-box table tr.details td {
				padding-bottom: 20px;
			}

			.invoice-box table tr.item td {
				border-bottom: 1px solid #eee;
			}

			.invoice-box table tr.item.last td {
				border-bottom: none;
			}

			.invoice-box table tr.total td:nth-child(2) {
				border-top: 2px solid #eee;
				font-weight: bold;
			}

			@media only screen and (max-width: 600px) {
				.invoice-box table tr.top table td {
					width: 100%;
					display: block;
					text-align: center;
				}

				.invoice-box table tr.information table td {
					width: 100%;
					display: block;
					text-align: center;
				}
			}

			/** RTL **/
			.invoice-box.rtl {
				direction: rtl;
				font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
			}

			.invoice-box.rtl table {
				text-align: right;
			}

			.invoice-box.rtl table tr td:nth-child(2) {
				text-align: left;
			}
		</style>    
@endsection
@section('breadcrumb_title', 'Invoice')
@section('breadcrumb_2', 'Invoice')

@section('content')

<div class="content-wrapper">
  <div class="content-header row">
  </div>
  <div class="content-body">

    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="2">
                    <table>
                        <tr>
                            <td class="title">
                                {{-- <img src="https://www.sparksuite.com/images/logo.png" style="width: 100%; max-width: 300px" /> --}}
                                <h1>Logo Here</h1>
                            </td>
                            <a href={{route('superadmin.userAccount.invoice.download',['id' => $order['id']])}} class="btn btn-outline-secondary" title="Download Invoice" style="float: right;">  <i class="fa fa-download"></i> </a>
                            <td>
                                Invoice #: {{ $order['id'] ?? '' }}<br />
                                Created: {{ $order['transaction_date'] ?? '' }}<br />
                                Due: {{ $order['userpurchasedpackage']['userpurchasedpackage']['end_date'] ?? '' }}
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr class="information">
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
                                FARS<br />
                                Address<br />
                                State, Country Pin Code
                            </td>

                            <td>
                                {{-- Acme Corp.<br /> --}}
                                {{ $order['user']['user']['name'] ?? '' }}<br />
                                {{ $order['user']['user']['email'] ?? '' }}
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr class="heading">
                <td>Payment Method</td>

                <td>{{ $order['payment_type'] ?? '' }}</td>
            </tr>

            <tr class="details">
                <td>{{ $order['transaction_id'] ?? '' }}</td>

                <td> @if($order['currency'] == 'INR') <i class="fa fa-inr"></i> @endif @if($order['currency'] == 'USD') <i class="fas fa-usd"></i> @endif {{ $order['amount'] ?? '' }}</td>
            </tr>

            <tr class="heading">
                <td>Item</td>

                <td>Price</td>
            </tr>

            <tr class="item">
                <td>{{ $order['package']['package']['name'] ?? '' }}</td>

                <td>@if($order['package']['package']['currency'] == 'INR') <i class="fa fa-inr"></i> @endif @if($order['package']['package']['currency'] == 'USD') <i class="fas fa-usd"></i> @endif {{ $order['package']['package']['amount'] ?? '' }}</td>
            </tr>


            <tr class="total">
                <td></td>

                <td>Total: @if($order['currency'] == 'INR') <i class="fa fa-inr"></i> @endif @if($order['currency'] == 'USD') <i class="fas fa-usd"></i> @endif {{ $order['amount'] ?? '' }}</td>
            </tr>
        </table>
    </div>

  </div>
</div>
@endsection


@section('script')


<!-- BEGIN: Page Vendor JS-->
<script src="{{ asset('app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
<script src="{{ asset('app-assets/js/scripts/pages/app-invoice.js') }}"></script>
<!-- END: Page Vendor JS-->
      
@endsection
