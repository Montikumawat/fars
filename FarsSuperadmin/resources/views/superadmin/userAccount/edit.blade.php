@extends('layout.master')
@section('title', 'Users')
@section('description', 'Users of FARS')
@section('keywords', 'admin panel, fars')
@section('author', 'FARS')

@section('css')
<link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
    <style>
      #example_filter {
          float: right;
      }
      #example_paginate {
          float: right;
      }
          /* width */
   ::-webkit-scrollbar {
   width: 10px;
   }

   /* Track */
   ::-webkit-scrollbar-track {
   background: #f1f1f1;

   }

   /* Handle */
   ::-webkit-scrollbar-thumb {
   background: #958cf4;
   border-radius: 10px;
   }

   /* Handle on hover */
   ::-webkit-scrollbar-thumb:hover {
   background: #958cf4;
   }
    </style>
    <style>
    .tooltipDescription {
      position: relative;
      display: inline-block;
      border-bottom: 1px dotted black;
    }

    .tooltipDescription .tooltiptext {
      visibility: hidden;
      width: 120px;
      background-color: black;
      color: #fff;
      text-align: center;
      border-radius: 6px;
      padding: 5px 0;

      /* Position the tooltip */
      position: absolute;
      z-index: 1;
    }

    .tooltipDescription:hover .tooltiptext {
      visibility: visible;
    }
</style>
@endsection
@section('breadcrumb_title', 'Users')
@section('breadcrumb_2', 'Edit User')
@section('content')

<section id="multiple-column-form">
    <form id="users" action="{{ route('superadmin.userAccount.update',$customer['id']) }}" method="POST" enctype="multipart/form-data">

        @csrf
        <div class="row">
            <div class="col-12">

                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Customer Info</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-4 col-md-6 col-12 mb-1">
                                <label for="inputName"> <b>Name</b> </label>
                                <input type="text" id="inputName" name="name" value="{{ $customer['name'] ?? '' }}" class="form-control" required>
                            </div>
                            <div class="col-xl-4 col-md-6 col-12 mb-1">

                                <label for="inputPhone"> <b>Phone Number</b> </label>
                                <input type="number" id="inputPhone" name="phone_number" value="{{ $customer['phone_number'] ?? '' }}" class="form-control" required>
                            </div>
                            <div class="col-xl-4 col-md-6 col-12 mb-1">
                                <label for="inputName"> <b>Email</b> </label>
                                <input type="email" id="inputEmail" name="email" value="{{ $customer['email'] ?? '' }}" class="form-control" disabled required>
                            </div>
                            <div class="col-xl-6 col-md-6 col-12 mb-1">
                                <label for="inputDescription"> <b>Address Line 1</b> </label>
                                {{-- <textarea id="inputAddress" name="address" class="form-control" rows="4"></textarea> --}}
                                <input type="text" id="inputaddress" name="address" value="{{ $customer['address'] ?? '' }}" class="form-control" required>
                            </div>
                            <div class="col-xl-6 col-md-6 col-12 mb-1">
                                <label for="inputDescription"> <b>Address Line 2</b> </label>
                                {{-- <textarea id="inputAddress" name="address" class="form-control" rows="4"></textarea> --}}
                                <input type="text" id="inputaddress" name="address_line_2" value="{{ $customer['address_line_2'] }}" class="form-control" required>
                            </div>
                            <div class="col-xl-3 col-md-6 col-12 mb-1">
                                <label for="inputCompanyName"> <b>Company Name</b> </label>
                                <input type="text" id="inputCompanyName" name="company_name" value="{{ $customer['company_name'] ?? '' }}" class="form-control" required>
                            </div>

                            <div class="col-xl-3 col-md-6 col-12 mb-1">
                                 <label for="inputCustomerType"> <b>Customer Type</b> </label>
                                {{-- <input type="text" id="inputCustomerType" name="customer_type" value="{{ $customer['customer_type'] ?? '' }}" class="form-control" required> --}}
                                <select id="customer_type" name="customer_type" class="form-control custom-select" required>
                                    <option selected disabled>Select Type</option>
                                    <option value="corporate" @if(isset($customer['customer_type']) == 0) selected @endif>Corporate</option>
                                    <option value="retail" @if(isset($customer['customer_type']) == 0) selected @endif>Retail</option>
                                </select>
                            </div>

                            <div class="col-xl-3 col-md-6 col-12 mb-1">
                                <label for="inputStatus"> <b>Customer Status</b> </label>
                                <select id="inputStatus" name="status"  class="form-control custom-select" required>
                                    <option selected disabled>Select Status</option>
                                    <option value="0" @if(isset($customer['status'])&& $customer['status'] == 0) selected @endif>InActive</option>
                                    <option value="1" @if(isset($customer['status'])&& $customer['status'] == 1) selected @endif>Active</option>
                                    <option value="2" @if(isset($customer['status'])&& $customer['status'] == 2) selected @endif>Block</option>
                                    <option value="3" @if(isset($customer['status'])&& $customer['status'] == 3) selected @endif>UnBlock</option>

                                </select>
                            </div>

                            <div class="col-xl-3 col-md-6 col-12 mb-1">
                                <label for="inputPackage"> <b>Customer Current Package</b> </label>
                                <select id="inputPackage" name="package_id" class="form-control custom-select" required>
                                    <option selected disabled>Select Package</option>
                                    @if(count($packages) > 0)
                                    @foreach($packages as $package)
                                    <option value="{{ $package['id'] }}" @if(isset($customer['package_id'])&& $customer['package_id'] == $package['id']) selected @endif>{{ $package['name'] }} (@if($package['billing_type'] == 0) Monthly @else Yearly @endif)</option>
                                    @endforeach
                                    @endif

                                </select>

                            </div>

                        </div>


                    </div>
                     <!-- /.card-body -->
                </div>
                    <!-- /.card -->

                <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Billing Information</h3>
                        </div>
                        <div class="card-body">

                            <div class="row">
                                <div class="col-xl-6 col-md-6 col-12 mb-1">
                                    <label for="inputEstimatedBudget"> <b>Billing Address Line - 1</b> </label>
                                    <input type="text" name="billing_address" value="{{ $customer['billing_address'] ?? '' }}" id="inputEstimatedBudget" class="form-control" required>
                                </div>

                                <div class="col-xl-6 col-md-6 col-12 mb-1">
                                    <label for="inputEstimatedBudget"> <b>Billing Address Line - 2</b> </label>
                                    <input type="text" name="billing_address_line_2" value="{{ $customer['billing_address_line_2'] ?? '' }}" id="inputEstimatedBudget2" class="form-control" required>
                                </div>

                                <div class="col-xl-6 col-md-6 col-12 mb-1">
                                    <label for="inputEstimatedBudget"> <b>Street Address Line - 1</b> </label>
                                    <input type="text" name="street_address" value="{{ $customer['street_address'] ?? '' }}" id="inputEstimatedBudget3" class="form-control" required>
                                </div>

                                <div class="col-xl-6 col-md-6 col-12 mb-1">
                                    <label for="inputEstimatedBudget"> <b>Street Address Line - 2</b> </label>
                                    <input type="text" name="street_address_line_2" value="{{ $customer['street_address_line_2'] ?? '' }}" id="inputEstimatedBudget4" class="form-control" required>
                                </div>

                                <div class="col-xl-3 col-md-6 col-12 mb-1">
                                    <label for="inputEstimatedBudget"> <b>City</b> </label>
                                    <input type="text" name="city" value="{{ $customer['city'] ?? '' }}" id="inputEstimatedBudget" class="form-control" required>
                                </div>
                                <div class="col-xl-3 col-md-6 col-12 mb-1">
                                    <label for="inputEstimatedBudget"> <b>State / Province</b> </label>
                                    <input type="text" name="state" value="{{ $customer['state'] ?? '' }}" id="inputEstimatedBudget" class="form-control" required>
                                </div>
                                <div class="col-xl-3 col-md-6 col-12 mb-1">
                                    <label for="inputEstimatedBudget"> <b>Postal / Zip Code</b> </label>
                                    <input type="text" name="zip_code" value="{{ $customer['zip_code'] ?? '' }}" id="inputEstimatedBudget" class="form-control" required>
                                </div>
                                <div class="col-xl-3 col-md-6 col-12 mb-1" id="divTouchPoint">
                                    <label for="inputTouchPoint"> <b>Country</b> </label>
                                    <select id="inputCountry"  name="country_id" class="form-control custom-select" required>
                                      <option selected disabled>Select Country </option>
                                      @if($countries->count() > 0)
                                          @foreach($countries as $country)
                                          <option value="{{ $country->id }}" @if(isset($customer['country_id'])&& $customer['country_id'] == $country->id) selected @endif>{{ $country->name }}</option>
                                          @endforeach
                                          @endif

                                    </select>
                                  </div>

                                  <div class="col-xl-4 col-md-6 col-12 mb-1">
                                    <label for="inputName"> <b>Account Number</b> </label>
                                    <input type="number" id="inputAccount" name="account_number" value="{{ $customer['account_number'] ?? '' }}" class="form-control" required>
                                </div>

                                  <div class="col-xl-3 col-md-6 col-12 mb-1" id="divTouchPoint">
                                    <label for="inputTouchPoint"> <b>Payment Method</b> </label>
                                    <select id="inputpayment"  name="payment_method" class="form-control custom-select" required>
                                      <option selected disabled>Select Payment Method </option>
                                      <option value="0" @if(isset($customer['payment_method'])&& $customer['payment_method'] == 0) selected @endif>Stripe </option>
                                      <option value="1" @if(isset($customer['payment_method'])&& $customer['payment_method'] == 1) selected @endif>Razorpay</option>
                                    </select>
                                  </div>
                            </div>

                        </div>
                         <!-- /.card-body -->
                </div>
                        <!-- /.card -->


                <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Tax Information</h3>
                            </div>
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-xl-6 col-md-6 col-12 mb-1">
                                        <label for="inputEstimatedBudget"> <b>Pan Card Number</b> </label>
                                        <input type="text" name="pan_card_number" value="{{ $customer['pan_card_number'] ?? '' }}" id="inputEstimatedBudget12" class="form-control" required>
                                    </div>

                                    <div class="col-xl-6 col-md-6 col-12 mb-1">
                                        <label for="inputEstimatedBudget"> <b>GST Number</b> </label>
                                        <input type="text" name="gst_number" value="{{ $customer['gst_number'] ?? '' }}" id="inputEstimatedBudget34" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                             <!-- /.card-body -->
                </div>
                        <!-- /.card -->

                <div class="col-12" style="margin-bottom: 30px;">
                                <button type="submit" class="btn btn-primary mr-1 waves-effect waves-float waves-light" style="float: right;">Submit</button>
                                <a href="{{url()->previous()}}" class="btn btn-outline-secondary waves-effect">Back</a>
                </div>



                     <!-- /.card -->



            </div>
                 <!-- /.col-12 -->

        </div>
                        <!-- /.row -->
    </form>
         <!-- Basic Tables start -->
         <div class="row" id="basic-table">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                       <h4 class="card-title">Payment History</h4>
                    </div>
                    <div class="table-responsive container-fluid">
                        <form action="{{ route('superadmin.userAccount.payment.filter') }}" class="row" method="get">
                            <div class="col-lg-3">
                                <div class="form-group">

                                  <select class="form-control" name="payment_type">
                                    <option value="">Filter By Payment Method</option>
                                    <option value="Stripe" @if(isset($payment_type) && $payment_type == 'Stripe') selected @endif>Stripe</option>
                                    <option value="Razorpay" @if(isset($payment_type) && $payment_type == 'Razorpay') selected @endif>Razorpay</option>
                                  </select>
                                </div>
                            </div>
                            <input type="hidden" name="user_id" value="{{ $customer['id'] }}" hidden>
                            <div class="col-lg-3">
                                <div class="form-group">
                                  <input type="text" name="amount" value="{{ $amount ?? '' }}" class="form-control" placeholder="Search by Amount">
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                  <button class="btn btn-success" type="submit">Apply Filters</button>
                                </div>
                            </div>
                        </form>
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Payment Date</th>
                                    <th>Invoice ID</th>
                                    <th>Payment Status</th>
                                    <th>Transaction Type</th>
                                    <th>Payment Method</th>
                                    <th>Transaction Amount</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>

                                @if($orders != '' && count($orders) > 0)
                                @foreach($orders as $key => $order)

                                <tr>
                                    <td>{{ $order['created_at'] ?? '' }}</td>
                                    <td><a href="#">{{ $order['transaction_id'] ?? '' }}</a></td>
                                    <td> @if(isset($order['status']) && $order['status'] == 1) <a href="#" title="Paid" class="btn btn-success"> Paid </a> @else <a href="#" title="Pending" class="btn btn-danger"> Pending </a> @endif </td>
                                    <td>Subscription</td>
                                    <td>{{ $order['payment_type'] ?? '' }}</td>
                                    <td> @if($order['currency'] == 'INR') <i class="fa fa-inr"></i> @endif @if($order['payment_type'] == 'USD') <i class="fas fa-usd"></i> @endif {{ $order['amount'] ?? '' }}</td>
                                    <td>
                                        <div class="dropdown">
                                            <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                                                <i class="fas fa-ellipsis-v"></i>
                                            </button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" href="{{route('superadmin.userAccount.invoice.download',['id' => $order['id']])}}" title="Download Invoice">
                                                    <i class="fa fa-download"></i>
                                                    <span>Download Invoice</span>
                                                </a>
                                                <a class="dropdown-item" href="{{route('superadmin.userAccount.invoice.detail',['id' => $order['id']])}}" title="See Invoice">
                                                    <i class="fa fa-eye"></i>
                                                  <span>See Details</span>
                                                </a>
                                            </div>
                                        </div>

                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
         </div>
         <!-- Basic Tables end -->

</section>

@endsection



@section('script')

        <!-- BEGIN: Page Vendor JS-->
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/responsive.bootstrap4.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/vfs_fonts.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.rowGroup.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
        <!-- END: Page Vendor JS-->
        <script>
       function EuToUsCurrencyFormat(input) {
	           return input.replace(/[,.]/g, function(x) {
		         return x == "," ? "." : ",";
	            });
            }

   $(document).ready(function() {
	//Only needed for the filename of export files.
	//Normally set in the title tag of your page.
	// document.title = 'DataTable Excel';
	// DataTable initialisation
	$('#example').DataTable({

	});
  });

</script>
@endsection
