@extends('layout.master')
@section('title', 'Customer')
@section('description', 'Customer of FARS')
@section('keywords', 'admin panel, fars')
@section('author', 'FARS')

@section('css')
    {{-- <!-- BEGIN: Page CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/pickers/form-flat-pickr.css') }}">
<!-- END: Page CSS--> --}}

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/tables/datatable/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/tables/datatable/rowGroup.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">
    <!-- END: Vendor CSS-->
    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
    <link rel="stylesheet" href="{{asset('plugins/summernote/summernote-bs4.css') }}">
    <!-- END: Page CSS-->
    <link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
    <style>
      #example_filter {
          float: right;
      }
      #example_paginate {
          float: right;
      }
          /* width */
   ::-webkit-scrollbar {
   width: 10px;
   }
   
   /* Track */
   ::-webkit-scrollbar-track {
   background: #f1f1f1;
   
   }
   
   /* Handle */
   ::-webkit-scrollbar-thumb {
   background: #958cf4;
   border-radius: 10px;
   }
   
   /* Handle on hover */
   ::-webkit-scrollbar-thumb:hover {
   background: #958cf4;
   }
    </style>
    <style>
    .tooltipDescription {
      position: relative;
      display: inline-block;
      border-bottom: 1px dotted black;
    }
    
    .tooltipDescription .tooltiptext {
      visibility: hidden;
      width: 120px;
      background-color: black;
      color: #fff;
      text-align: center;
      border-radius: 6px;
      padding: 5px 0;
    
      /* Position the tooltip */
      position: absolute;
      z-index: 1;
    }
    
    .tooltipDescription:hover .tooltiptext {
      visibility: visible;
    }
</style>
@endsection
@section('breadcrumb_title', 'Customer')
@section('breadcrumb_2', 'Customer Detail')
@section('content')
    <section id="multiple-column-form">
        <div class="col-lg-12">
          <div class="card-body">
            <div class="table-responsive" >
                <table class="table table-hover table-striped">

                  <tbody>
                  <tr>
                      <th scope="row">Customer Name</th>
                      <td>{{ $user['name'] ?? 'N/A' }}</td>
                  </tr>
  
                  <tr>
                      <th scope="row">Customer Phone Number</th>
                      <td>{{ $user['phone_number'] ?? 'N/A' }} </td>
                  </tr>
                  <tr>
                      <th scope="row">Customer Email</th>
                      <td>{{ $user['email'] ?? 'N/A' }} </td>
                  </tr>
                  <tr>
                      <th scope="row">Customer Company Name</th>
                      <td> {{ $user['company_name'] ?? 'N/A' }} </td>
                  </tr>
                  <tr>
                      <th scope="row">Customer Domain Name</th>
                      <td>{{ $user['domain_name'] ?? 'N/A' }} </td>
                  </tr>
                  
                  <tr>
                      <th scope="row">Account Number</th>
                      <td>{{ $user['account_number'] ?? 'N/A' }}</td>
                  </tr>

                  <tr>
                    <th scope="row">Package</th>
                      <td>{{ $package['name'] ?? 'N/A' }}</td>
                  </tr>
                  <tr>
                    <th scope="row">Billing Type</th>
                    <td>@if(isset($package['billing_type'])) @if($package['billing_type'] == '0') Monthly @else Yearly @endif  @else N/A @endif</td>
                  </tr>

                  <tr>
                    <th scope="row">Payment Method</th>
                    <td>@if(isset($user['payment_method'])) @if($user['payment_method'] == '0') Stripe @else Razorpay @endif @else N/A @endif</td>
                  </tr>

                  <tr>
                    <th scope="row">Address</th>
                    <td>{{ $user['address'] ?? 'N/A' }}</td>
                  </tr>

                  <tr>
                    <th scope="row">Address Line 2</th>
                    <td>{{ $user['address_line_2'] ?? 'N/A' }}</td>
                  </tr>

                  <tr>
                    <th scope="row">Billing Address</th>
                    <td>{{ $user['billing_address'] ?? 'N/A'}}</td>
                  </tr>

                  <tr>
                    <th scope="row">Billing Address Line 2</th>
                    <td>{{ $user['billing_address_line_2'] ?? 'N/A' }}</td>
                  </tr>

                  <tr>
                    <th scope="row">Street Address</th>
                    <td>{{ $user['street_address'] ?? 'N/A' }}</td>
                  </tr>

                  <tr>
                    <th scope="row">Street Address Line 2</th>
                    <td>{{ $user['street_address_line_2'] ?? 'N/A' }}</td>
                  </tr>

                  <tr>
                    <th scope="row">City</th>
                    <td>{{ $user['city'] ?? 'N/A'}}</td>
                  </tr>

                  <tr>
                    <th scope="row">State</th>
                    <td>{{ $user['state'] ?? 'N/A' }}</td>
                  </tr>

                  <tr>
                    <th scope="row">Zip Code</th>
                    <td>{{ $user['zip_code'] ?? 'N/A' }}</td>
                  </tr>

                  <tr>
                    <th scope="row">Pan Card Number</th>
                    <td>{{ $user['pan_card_number'] ?? 'N/A' }}</td>
                  </tr>

                  <tr>
                    <th scope="row">Gst Number</th>
                    <td>{{ $user['gst_number'] ?? 'N/A' }}</td>
                  </tr>

                  <tr>
                    <th scope="row">Status</th>
                     <td>@if(isset($user['status']) && $user['status']== 0)InActive @endif @if(isset($user['status']) && $user['status'] == 1)Active @endif @if($user['status'] == 2)Block @endif @if($user['status'] == 3)UnBlock @endif</td>
                  </tr>

                 
                  </tbody>

              </table>
            </div>  
          </div>
                          <!-- /.card-body -->
                        <!-- /.card -->
        </div>                        
          <!-- /.col-12 -->

        <!-- Basic Tables start -->
        <div class="row" id="basic-table">
          <div class="col-12">
              <div class="card">
                  <div class="card-header">
                     <h4 class="card-title">Payment History</h4>
                  </div>
                  <div class="table-responsive container-fluid">
                      <form action="{{ route('superadmin.userAccount.payment.filter') }}" class="row" method="get">
                          <div class="col-lg-3">
                              <div class="form-group">

                                <select class="form-control" name="payment_type">
                                  <option value="">Filter By Payment Method</option>
                                  <option value="Stripe" @if(isset($payment_type) && $payment_type == 'Stripe') selected @endif>Stripe</option>
                                  <option value="Razorpay" @if(isset($payment_type) && $payment_type == 'Razorpay') selected @endif>Razorpay</option>
                                </select>
                              </div>
                          </div>
                          <input type="hidden" name="user_id" value="{{ $user['id'] }}" hidden>
                          <input type="hidden" name="page" value="1" hidden>
                          <div class="col-lg-3">
                              <div class="form-group">
                                <input type="text" name="amount" value="{{ $amount ?? '' }}" class="form-control" placeholder="Search by Amount">
                              </div>
                          </div>
                          <div class="col-lg-3">
                              <div class="form-group">
                                <button class="btn btn-success" type="submit">Apply Filters</button>
                              </div>
                          </div>
                      </form>
                      <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                          <thead>
                              <tr>
                                  <th>Payment Date</th>
                                  <th>Invoice ID</th>
                                  <th>Payment Status</th>
                                  <th>Transaction Type</th>
                                  <th>Payment Method</th>
                                  <th>Transaction Amount</th>
                                  <th>Actions</th>
                              </tr>
                          </thead>
                          <tbody>
                              
                              @if($orders != '' && count($orders) > 0)  
                              @foreach($orders as $key => $order)
                              
                              <tr>
                                  <td>{{ $order['created_at'] ?? '' }}</td>
                                  <td><a href="#">{{ $order['transaction_id'] ?? '' }}</a></td>
                                  <td> @if(isset($order['status']) && $order['status'] == 1) <a href="#" title="Paid" class="btn btn-success"> Paid </a> @else <a href="#" title="Pending" class="btn btn-danger"> Pending </a> @endif </td>
                                  <td>Subscription</td>
                                  <td>{{ $order['payment_type'] ?? '' }}</td>
                                  <td> @if($order['currency'] == 'INR') <i class="fa fa-inr"></i> @endif @if($order['payment_type'] == 'USD') <i class="fas fa-usd"></i> @endif {{ $order['amount'] ?? '' }}</td>
                                  <td>
                                      <div class="dropdown">
                                          <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                                              <i class="fas fa-ellipsis-v"></i>
                                          </button>
                                          <div class="dropdown-menu">
                                              <a class="dropdown-item" href="#" title="Download Invoice">
                                                  <i class="fa fa-download"></i>
                                                  <span>Download Invoice</span>
                                              </a>
                                              <a class="dropdown-item" href="{{route('superadmin.userAccount.detail')}}" title="See Invoice">
                                                  <i class="fa fa-eye"></i>
                                                <span>See Details</span>
                                              </a>
                                          </div>
                                      </div>
                      
                                  </td>
                              </tr>
                              @endforeach
                              @endif
                          </tbody>
                      </table>                 
                  </div>
              </div>
          </div>
       </div>
       <!-- Basic Tables end -->           

    </section>
@endsection



@section('script')


        <!-- BEGIN: Page Vendor JS-->
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/responsive.bootstrap4.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/vfs_fonts.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.rowGroup.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
        <!-- END: Page Vendor JS-->
        <script>
       function EuToUsCurrencyFormat(input) {
	           return input.replace(/[,.]/g, function(x) {
		         return x == "," ? "." : ",";
	            });
            }

   $(document).ready(function() {
	//Only needed for the filename of export files.
	//Normally set in the title tag of your page.
	// document.title = 'DataTable Excel';
	// DataTable initialisation
	$('#example').DataTable({
	
	});
  });
    
</script>

@endsection
