<!DOCTYPE html>

<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4 & Angular 8
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="en">

    <!-- begin::Head -->
    <head>

        <!--begin::Base Path (base relative path for assets of this page) -->
        <base href="../../../../">

        <!--end::Base Path -->
        <meta charset="utf-8" />
        <title>@yield('title')</title>
        <meta name="description" content="Login">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!--begin::Fonts -->
       

        <!--end::Fonts -->
        <link rel="shortcut icon" href="{{ asset('metronic/assets/media/logos/favicon.ico') }}" />


<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet">
  <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">  


  <!-- <link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/charts/apexcharts.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/extensions/toastr.min.css')}}" /> -->
 <!-- BEGIN: Vendor CSS-->
 <link href="{{ asset('app-assets/vendors/css/vendors.min.css') }}" rel="stylesheet" type="text/css" />
 <link rel="stylesheet" href="{{ asset('app-assets/vendors/css/forms/wizard/bs-stepper.min.css') }}">
<link rel="stylesheet" href="{{ asset('app-assets/vendors/css/forms/select/select2.min.css') }}">
 <!-- <link href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
 <link href="{{ asset('app-assets/vendors/css/tables/datatable/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
 <link href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" /> -->


  <!-- BEGIN: Theme CSS-->
  <link href="{{ asset('app-assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('app-assets/css/bootstrap-extended.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('app-assets/css/colors.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('app-assets/css/components.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('app-assets/css/themes/dark-layout.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('app-assets/css/themes/bordered-layout.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('app-assets/css/themes/semi-dark-layout.css') }}" rel="stylesheet" type="text/css" />



  <!-- <link rel="stylesheet" type="text/css" href="asset('app-assets/css/plugins/extensions/ext-component-toastr.css')">
  <link rel="stylesheet" type="text/css" href="asset('app-assets/css/pages/app-invoice-list.css')"> -->
  <!-- BEGIN: Page CSS-->
  <link href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('app-assets/css/plugins/forms/form-validation.css') }}" rel="stylesheet" type="text/css" />
  
  <!-- END: Page CSS-->

@yield('style')
<!-- <link rel="stylesheet" type="text/css" href="../../../app-assets/css/pages/app-user.css"> -->


  <!-- BEGIN: Custom CSS-->
  <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" type="text/css" />
<!--end::Custom Page Styles -->
    </head>

    <!-- end::Head -->

    <!-- begin::Body -->
    <body class="vertical-layout vertical-menu-modern blank-page navbar-floating footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="blank-page">
@yield('auth-content')

    <script src="{{ asset('app-assets/vendors/js/vendors.min.js') }}"></script>

    <script src="{{ asset('app-assets/js/core/app-menu.js') }}"></script>
    <script src="{{ asset('app-assets/js/core/app.js') }}"></script>
    @yield('script')
    </body>

    <!-- end::Body -->
</html>