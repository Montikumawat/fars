<?php

//For Local Uses
$api='';

return [
    'login_api_url' => 'http://localhost:8000/api',
    'package_api_url' => 'http://localhost:8002/api',
    'customer_api_url' => 'http://localhost:8001/api',
    'setting_api_url' => 'http://localhost:8003/api',
    'tag_api_url' => 'http://localhost:8004/api',
//     'login_api_url' => 'http://faras.demolinks.tech/fars-all/lumens/AuthMicroservice/public/api',
    'image_url'=>'http://localhost:8002/uploads/packages/',
    'billing_type' => array(0 => 'Monthly',1 => 'Yearly'),

    // 'billing_type' => array(0 => 'Monthly',1 => 'Yearly'),
        // 'package_type' => array(0 => 'Corporate',1 => 'Retail'),
        // 'status' => array(0 => 'InActive',1 => 'Active'),
        // 'currency' => array('INR' => '₹','USD' => '$')

        // 'package_api_url' => 'http://expendablesolutions.com/fars-all/lumens/PackageLumen/public/api',
        // 'customer_api_url' => 'http://expendablesolutions.com/fars-all/lumens/CustomerMicroService/public/api',
        // 'setting_api_url' => 'http://expendablesolutions.com/fars-all/lumens/SettingsMicroService/public/api',
        // 'login_api_url' => 'http://expendablesolutions.com/fars-all/lumens/AuthCustom/public/api',
        // 'image_url'=>'http://expendablesolutions.com/fars-all/lumens/PackageLumen/public/uploads/packages/'

];


//Base Api For Live Use
// $base_api='http://faras.demolinks.tech/lumens';

// return [

//     'package_api_url' => $base_api.'/PackageLumen/public/api',
//     'customer_api_url' => $base_api.'/CustomerMicroService/public/api',
//     'setting_api_url' => $base_api.'/settingsmicroservice/public/api',

// ];

?>
