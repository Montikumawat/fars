<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable=['user_id','package_id','currency','payment_type','amount','transaction_id','status'];

    public function orderItems(){
        return $this->hasMany(OrderItem::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function plan(){
        return $this->hasMany(UserPurchasedPlan::class,'order_id');

    }
}
