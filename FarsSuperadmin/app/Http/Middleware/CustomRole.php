<?php

namespace App\Http\Middleware;

use Closure;

class CustomRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        dd(auth()->user());
        if (Auth::user()) {
            return $next($request);
        }
    }
}
