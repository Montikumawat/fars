<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;

use Illuminate\Support\Facades\DB;

class Package
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();


        $mytime=Carbon::now()->format('Y-m-d');


        if ($user->subscription_expire_date<$mytime || $user->subscription_expire_date==NULL) {
            if($user->isUser())
            {
                return redirect()->route('front')->with('danger',"Your Subscription has expired or you haven't purchased one yet");
            }
            if($user->isAdmin())
            {
                return redirect()->route('front')->with('danger',"Your Subscription has expired or you haven't purchased one yet");
            }
        }
        return $next($request);
    }
}
