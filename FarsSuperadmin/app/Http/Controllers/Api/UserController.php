<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
use Illuminate\Validation\Rule;
use Hash;

class UserController extends Controller
{
    public function register(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'password' => 'required',
            'email' => 'email|required|unique:users',
            'status' => 'required',
            'confirm_password' => 'required|same:password',
        ]);


        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $user = User::create([
            'name'=>$request->name,
            'status'=>$request->status,
            'email'=>$request->email,
            'password'=>Hash::make($request->password),
        ]);
        $success = $user;
        $success['token'] =  $user->createToken('MyApp')->accessToken;
        $user->token=$user->createToken('MyApp')->accessToken;
        $user->save();
        return $this->sendResponse($success, 'User register successfully.');
    }

    public function login (Request $request) {
        $user = User::where('email', $request->email)->first();
        if ($user) {

            if (Hash::check($request->password, $user->password)) {
                $success = $user;
                $success['token'] =  $user->createToken('MyApp')->accessToken;
                 $user->api_token=$success['token'];
                 $user->save();
                return $this->sendResponse($success, 'User has been logged in successfully.');
            } else {
                return $this->sendError('Password missmatch');
            }
        } else {
            return $this->sendError('User does not exist');
        }
    }
}
