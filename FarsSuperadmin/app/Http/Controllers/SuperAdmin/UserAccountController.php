<?php

namespace App\Http\Controllers\SuperAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use App\User;
use Carbon\Carbon;
use App\Country;
use PDF;

class UserAccountController extends Controller
{
    public function index(){
        $response = Http::get(config('helper.customer_api_url').'/get/customers');
        $pack = Http::get(config('helper.package_api_url').'/get/packages');
        $packs = $pack->json() ?? '';
        // dd($packs);
        $users = $response->json() ?? '';

        foreach($packs as $pac)
        {
            if(isset($pac['billing_type'])){
                $packages[$pac['id']] =['name'=>$pac['name'],
                'billing_type'=>config('helper.billing_type')[$pac['billing_type']]] ;
            }

        }

        // dd($users[0]);

        return view('superadmin.userAccount.index',compact('users','packages','packs'));
    }

    public function create(){
        $countries = Country::select(['id','name'])->get();
        $response = Http::get(config('helper.package_api_url').'/get/packages');
        // dd($response->json());

        $packages = $response->json() ?? '';
        return view('superadmin.userAccount.add',compact('countries','packages'));
    }

    public function store(Request $request){
        $password = Hash::make($request->password);
        // dd($request->all());
        $response =  Http::post(config('helper.customer_api_url').'/create/customer', [
            'user_unique_id'=>$request->user_unique_id ?? '0' ,
            'org_unique_id'=>$request->org_unique_id ?? '0' ,
            'country_id'=>$request->country_id ?? '0' ,
            'name'=>$request->name ?? '' ,
            'phone_number'=>$request->phone_number ?? '' ,
            'email'=>$request->email ?? '' ,
            'company_name'=>$request->company_name ?? '' ,
            'customer_type'=>$request->customer_type ?? '' ,
            'domain_name'=>$request->domain_name ?? '' ,
            'account_number'=>$request->account_number ?? '' ,
            'billing_type'=>$request->billing_type ?? '0' ,
            'payment_method'=>$request->payment_method ?? '0' ,
            'address'=>$request->address ?? '',
            'address_line_2'=>$request->address_line_2 ?? '' ,
            'billing_address'=>$request->billing_address ?? '' ,
            'billing_address_line_2'=>$request->billing_address_line_2 ?? '' ,
            'street_address'=>$request->street_address ?? '' ,
            'street_address_line_2'=>$request->street_address_line_2 ?? '' ,
            'city'=>$request->city ?? '' ,
            'state'=>$request->state ?? '' ,
            'status'=>$request->status ?? '1',
            'package_id'=>$request->package_id ?? '',
            'zip_code'=>$request->zip_code ?? '',
            'pan_card_number'=>$request->pan_card_number ?? '',
            'gst_number'=>$request->gst_number ?? '',
            'email_verified_at'=>$request->email_verified_at ?? '',
            'subscription_expire_date'=> Carbon::now()->addDays(7)->format('Y-m-d') ?? '',
            'password'=>$password ?? '',
        ]);



        // dd($order->json(),$orderItem->json(),$userPurchasedPackage->json(),$user->json());



        // dd($response->json());

        if($response->json() == 'Email Already Exists')
        {
            return redirect()->route('superadmin.userAccount.create')->with('danger','Email Already Exists');
        }
        else
        {
            return redirect()->route('superadmin.userAccount.index')->with('success','User Created Successfully');
        }
    }
    public function edit(Request $request, $id){
        $response = Http::get(config('helper.customer_api_url').'/get/customer/detail/'.$id);
        $customer = $response->json() ?? '';
        $countries = Country::select(['id','name'])->get();

        $responses = Http::get(config('helper.package_api_url').'/get/packages');
        // dd($response->json());
        $packages = $responses->json() ?? '';

        $response_order = Http::get(config('helper.login_api_url').'/get/order/detail/user/'.$id);
        $orders = $response_order->json() ?? '';

        return view('superadmin.userAccount.edit',compact('customer','countries','packages','orders'));
    }

    public function update(Request $request, $id)
    {

        // dd($request->all());

        if($request->custom)
        {
            foreach($request->custom as $key=>$value)
            {
                $data[] = $value;
            }
            $custom = json_encode($data);
        }

        $response =  Http::post(config('helper.customer_api_url').'/update/customer/'.$id, [
            'user_unique_id'=>$request->user_unique_id ?? '0' ,
            'org_unique_id'=>$request->org_unique_id ?? '0' ,
            'country_id'=>$request->country_id ?? '0' ,
            'name'=>$request->name ?? '' ,
            'phone_number'=>$request->phone_number ?? '' ,
            'company_name'=>$request->company_name ?? '' ,
            'customer_type'=>$request->customer_type ?? '' ,
            'domain_name'=>$request->domain_name ?? '' ,
            'account_number'=>$request->account_number ?? '' ,
            'billing_type'=>$request->billing_type ?? '0' ,
            'payment_method'=>$request->payment_method ?? '0' ,
            'address'=>$request->address ?? '',
            'address_line_2'=>$request->address_line_2 ?? '' ,
            'billing_address'=>$request->billing_address ?? '' ,
            'billing_address_line_2'=>$request->billing_address_line_2 ?? '' ,
            'street_address'=>$request->street_address ?? '' ,
            'street_address_line_2'=>$request->street_address_line_2 ?? '' ,
            'city'=>$request->city ?? '' ,
            'state'=>$request->state ?? '' ,
            'status'=>$request->status ?? '1',
            'package_id'=>$request->package_id ?? '',
            'zip_code'=>$request->zip_code ?? '',
            'pan_card_number'=>$request->pan_card_number ?? '',
            'gst_number'=>$request->gst_number ?? '',
            'email_verified_at'=>$request->email_verified_at ?? '',
        ]);


    return redirect()->route('superadmin.userAccount.index');
    }

    public function destroy($id){
        $response =  Http::post(config('helper.customer_api_url').'/destroy/customer/'.$id);
        return redirect()->route('superadmin.userAccount.index')->with('danger','Customer Deleted Successfully');
    }

    public function view($id){
        $response = Http::get(config('helper.customer_api_url').'/get/customer/detail/'.$id);
        $user = $response->json() ?? '';
        $package_id = $user['package_id'];
        $package_response = Http::get(config('helper.package_api_url').'/get/package/detail/'.$package_id);
        $package = $package_response->json() ?? '';
        // dd($package);
        $response_order = Http::get(config('helper.login_api_url').'/get/order/detail/'.$id);
        $orders = $response_order->json() ?? '';
        return view('superadmin.userAccount.view',compact('user','package','orders'));
    }

    public function block($id){
        $response =  Http::post(config('helper.customer_api_url').'/status/block/customer/'.$id);
    //    dd($response->json());
        return redirect()->back();
    }

    public function unblock($id){
        $response =  Http::post(config('helper.customer_api_url').'/status/unblock/customer/'.$id);
        // dd($response->json());
       return redirect()->back();
    }

    public function filter(Request $request)
    {
        $response =  Http::get(config('helper.customer_api_url').'/filter/customer', [
            'status'=>$request->status ?? '' ,
            'package_id'=>$request->package_id ?? '' ,
        ]);

        $users = $response->json() ?? '';

        $status = $request->status ?? '' ;
        $package_id = $request->package_id ?? '';

        $pack = Http::get(config('helper.package_api_url').'/get/packages');
        // dd($response->json());
        $packs = $pack->json() ?? '';

        foreach($packs as $pack)
        {
            if(isset($pac['billing_type'])){
                $packages[$pac['id']] =['name'=>$pac['name'],
                'billing_type'=>config('helper.billing_type')[$pac['billing_type']]] ;
            }
        }
        // dd($users);

        return view('superadmin.userAccount.index',compact('users','status','package_id','packs','packages'));
    }

    public function filterPayment(Request $request)
    {
        $response =  Http::get(config('helper.login_api_url').'/filter/userAccount/payment', [
            'payment_type'=>$request->payment_type ?? '' ,
            'amount'=>$request->amount ?? '' ,
            'user_id'=>$request->user_id ?? ''
        ]);
        $payment_type = $request->payment_type ?? '';
        $amount = $request->amount ?? '';
        $id = $request->user_id ?? '';

        $responsec = Http::get(config('helper.customer_api_url').'/get/customer/detail/'.$id);
        $customer = $responsec->json() ?? '';
        $countries = Country::select(['id','name'])->get();

        $responses = Http::get(config('helper.package_api_url').'/get/packages');
        // dd($response->json());
        $packages = $responses->json() ?? '';
        $orders = $response->json() ?? '';

        $payment_type = $request->payment_type ?? '' ;
        $amount = $request->amount ?? '';

        // dd($users);
        if(isset($request->page) && $request->page=='1')
        {
            $user = $responsec->json() ?? '';
            return view('superadmin.userAccount.view',compact('user','countries','packages','orders','payment_type','amount'));
        } else {
            return view('superadmin.userAccount.edit',compact('customer','countries','packages','orders','payment_type','amount'));
        }

    }

    public function filterPaymentView(Request $request)
    {
        $response =  Http::get(config('helper.login_api_url').'/filter/userAccount/payment', [
            'payment_type'=>$request->payment_type ?? '' ,
            'amount'=>$request->amount ?? '' ,
            'user_id'=>$request->user_id ?? ''
        ]);
        $payment_type = $request->payment_type ?? '';
        $amount = $request->amount ?? '';
        $id = $request->user_id ?? '';

        $responsec = Http::get(config('helper.customer_api_url').'/get/customer/detail/'.$id);
        $customer = $responsec->json() ?? '';
        $countries = Country::select(['id','name'])->get();

        $responses = Http::get(config('helper.package_api_url').'/get/packages');
        // dd($response->json());
        $packages = $responses->json() ?? '';
        $orders = $response->json() ?? '';

        $payment_type = $request->payment_type ?? '' ;
        $amount = $request->amount ?? '';

        // dd($users);

        return view('superadmin.userAccount.edit',compact('customer','countries','packages','orders','payment_type','amount'));
    }

    public function invoiceDetail($id){
        $responseo = Http::get(config('helper.login_api_url').'/get/order/detail/'.$id);
        $order = $responseo->json() ?? '';

        $responseu = Http::get(config('helper.customer_api_url').'/get/customer/detail/'.$order['user_id']);
        $user = $responseu->json() ?? '';
        $order['user'] = ['user'=>$user];

        $responsep = Http::get(config('helper.package_api_url').'/get/package/detail/'.$order['package_id']);
        $package = $responsep->json() ?? '';
        $order['package'] = ['package'=>$package];

        $responseup = Http::get(config('helper.login_api_url').'/get/user/package/detail/'.$order['id']);
        $userpurchasedpackage = $responseup->json() ?? '';
        $order['userpurchasedpackage'] = ['userpurchasedpackage'=>$userpurchasedpackage];
        // dd($order);
        return view('superadmin.userAccount.invoice',compact('order'));
    }

    public function invoiceDownload($id){

        $responseo = Http::get(config('helper.login_api_url').'/get/order/detail/'.$id);
        $order = $responseo->json() ?? '';

        $responseu = Http::get(config('helper.customer_api_url').'/get/customer/detail/'.$order['user_id']);
        $user = $responseu->json() ?? '';
        $order['user'] = ['user'=>$user];

        $responsep = Http::get(config('helper.package_api_url').'/get/package/detail/'.$order['package_id']);
        $package = $responsep->json() ?? '';
        $order['package'] = ['package'=>$package];

        $responseup = Http::get(config('helper.login_api_url').'/get/user/package/detail/'.$order['id']);
        $userpurchasedpackage = $responseup->json() ?? '';
        $order['userpurchasedpackage'] = ['userpurchasedpackage'=>$userpurchasedpackage];
            view()->share('order',$order);
            $pdf = PDF::loadView('superadmin.userAccount.invoiceDownload');
            $pdf_name = $order['package']['package']['name'].'_invoice.pdf' ?? 'invoice.pdf';
            return $pdf->download($pdf_name);

        return view('superadmin.userAccount.invoiceDownload');
    }

}
