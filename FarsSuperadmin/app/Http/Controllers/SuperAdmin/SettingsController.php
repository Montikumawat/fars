<?php

namespace App\Http\Controllers\SuperAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use App\Settings;

class SettingsController extends Controller
{
    public function smsIndex(){
        $response = Http::get(config('helper.setting_api_url').'/get/sms/detail');

        $sms  = $response->json() ?? '';
        // dd($sms);
        return view('superadmin.setting.sms',compact('sms'));
    }

    public function smsStore(Request $request, $id){

        // dd($id);

        $response =  Http::post(config('helper.setting_api_url').'/update/sms/'. $id, [
            'type'=>$request->type ?? 'sms' ,
            'api_key'=>$request->api_key ?? '' ,
            'api_secret_key'=>$request->api_secret_key ?? '' ,
            'signature_secret'=>$request->signature_secret ?? '',
            'status' => $request->status ?? '0',
        ]);
        // dd($response);
        if($response->status() == '404')
        {
            return redirect()->back()->with('danger','Something went wrong! Please try again later');
        }
        else
        {
            return redirect()->route('superadmin.setting.sms.index')->with('success','SMS Settings Configured Successfully!');
        }
    }

    public function emailIndex(){
        $response = Http::get(config('helper.setting_api_url').'/get/email/detail');
        // dd($response->json());
        $email  = $response->json() ?? '';
        // dd($email);
        return view('superadmin.setting.email',compact('email'));
    }

    public function emailStore(Request $request){

        // dd($request->all());
        $response =  Http::post(config('helper.setting_api_url').'/update/email', [
            'type'=>$request->type ?? 'email' ,
            'mail_host'=>$request->mail_host ?? '' ,
            'mail_port'=>$request->mail_port ?? '' ,
            'mail_username'=>$request->mail_username ?? '',
            'mail_password'=>$request->mail_password ?? '',
            'mail_encryption'=>$request->mail_encryption ?? '',
            'mail_from_address'=>$request->mail_from_address ?? '',
            'mail_from_name'=>$request->mail_from_name ?? '',

        ]);
        //  dd($response->json());
        if($response->status() == '404')
        {
            return redirect()->back()->with('danger','Something went wrong! Please try again later');
        }
        else
        {
            return redirect()->route('superadmin.setting.email.index')->with('success','SMS Settings Configured Successfully!');
        }
    }


    public function paymentIndex(){
        $razor = Http::get(config('helper.setting_api_url').'/get/razorpay/detail');
        $razorpay  = $razor->json() ?? '';

        $str = Http::get(config('helper.setting_api_url').'/get/stripe/detail');
        $stripe  = $str->json() ?? '';

        // dd($razorpay['id']);
        return view('superadmin.setting.payment',compact('razorpay','stripe'));
    }

    public function razorpayStore(Request $request){

        // dd($request->all());
        $response =  Http::post(config('helper.setting_api_url').'/update/razorpay', [
            'key'=>$request->razorpay_key ?? '',
            'secret_key'=>$request->razorpay_secret_key ?? '' ,
            'status'=> 1,

        ]);

        if($response->status() == '404')
        {
            return redirect()->back()->with('danger','Something went wrong! Please try again later');
        }
        else
        {
            return redirect()->route('superadmin.paymentSetting')->with('success','Razorpay Credentials Configured Successfully!');
        }
    }

    public function stripeStore(Request $request){

        // dd($request->all());
        $response =  Http::post(config('helper.setting_api_url').'/update/stripe', [
            'key'=>$request->stripe_key ?? '',
            'secret_key'=>$request->stripe_secret_key ?? '' ,
            'status'=> 1,

        ]);
        // dd($response->json());
        if($response->status() == '404')
        {
            return redirect()->back()->with('danger','Something went wrong! Please try again later');
        }
        else
        {
            return redirect()->route('superadmin.paymentSetting')->with('success','Stripe Credentials Configured Successfully!');
        }
    }
}
