<?php

namespace App\Http\Controllers\SuperAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;


class TagController extends Controller
{
    public function index()
    {
        $response = Http::get(config('helper.tag_api_url').'/get/tags');
        $tags = $response->json() ?? '';
        return view('superadmin.tag.index',compact('tags'));
    }
    public function add()
    {        
        return view('superadmin.tag.add');
    }
     public function store(Request $request){
        

        //Post the data to tag microservice
        $response =  Http::post(config('helper.tag_api_url').'/create/tag', [
            'name' => $request->name,
            'status' => $request->status,
            'description' => $request->description
           
        ]);
        
      
        return redirect()->route('superadmin.tag.index')->with('success','Tag Added Successfully');
    }

    public function edit($id){
        $response = Http::get(config('helper.tag_api_url').'/get/tag/detail/'.$id);
        $tag = $response->json() ?? '';

        return view('superadmin.tag.edit',compact('tag'));
    } 

    public function update(Request $request, $id){

        //Post the data to package microservice
                //Post the data to tag microservice
        $response =  Http::post(config('helper.tag_api_url').'/update/tag/'.$id, [
            'name' => $request->name,
            'status' => $request->status,
            'description' => $request->description
           
        ]);

        // dd($response->json());
        
      
        return redirect()->route('superadmin.tag.index')->with('success','Tag Updated Successfully');
    }
    public function destroy($id){
        $response =  Http::post(config('helper.tag_api_url').'/destroy/tag/'.$id);
        // dd($response);
        return redirect()->route('superadmin.tag.index')->with('danger','Tag Destroyed Successfully');
     } 

     public function activate($id){
        $response =  Http::post(config('helper.tag_api_url').'/tag/activate/'.$id);
    //    dd($response->json());
        return redirect()->back()->with('success','Tag is now Activated');
    }

    public function deactivate($id){
        $response =  Http::post(config('helper.tag_api_url').'/tag/deactivate/'.$id);
        // dd($response->json());
       return redirect()->back()->with('danger','Tag is now DeActivated');
    }

}
