<?php

namespace App\Http\Controllers\SuperAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use App\Package;
use App\Order;
use App\User;



class PackageController extends Controller
{
    public function index()
    {
        $response = Http::get(config('helper.package_api_url').'/get/packages');
        $packages = $response->json() ?? '';
        return view('superadmin.package.index',compact('packages'));
    }
    public function add()
    {        
        return view('superadmin.package.add');
    }
     public function store(Request $request){
       $image=$request->image;
        // $aee=json_encode($request->image);
        // dd($aee);
         //Encoded the custom in json
        if($request->custom)
        {
            foreach($request->custom as $key=>$value)
            {
                $data[] = $value;
            }
            $custom = json_encode($data);
        }
        //Post the data to package microservice
        
        $response =  Http::attach(
            'attachment', file_get_contents($image), $image->getClientOriginalName()
        )->post(config('helper.package_api_url').'/create/package', [
            'name'=>$request->name ?? '',
            'description'=>$request->description ?? '' ,
            'status'=>$request->status ?? '0' ,
            'package_type'=>$request->package_type ?? '0' ,
            'package_time'=>$request->package_time ?? '0' ,
            'amount'=>$request->amount ?? '0' ,
            'support'=>$request->support ?? '' ,
            'is_default'=>$request->is_default ?? '0' ,
            'is_free_trial'=>$request->is_free_trial ?? '0' ,
            'billing_type'=>$request->billing_type ?? '0' ,
            'currency'=>$request->currency ?? '0' ,
            'users_for_facial_recognition'=>$request->users_for_facial_recognition ?? '0' ,
            'check_ins'=>$request->check_ins ?? '0',
            'touch_point'=>$request->touch_point ?? '0' ,
            'admin_touch_point'=>$request->admin_touch_point ?? '0' ,
            'front_touch_point'=>$request->front_touch_point ?? '0' ,
            'edge_touch_point'=>$request->edge_touch_point ?? '0' ,
            'data_maintained_days'=>$request->data_maintained_days ?? '0' ,
            'backend_integration'=>$request->backend_integration ?? '0' ,
            'sites'=>$request->sites ?? '0' ,
            'rate_limit'=>$request->rate_limit ?? '0',
            'admin_users'=>$request->admin_users ?? '0',
            'custom'=>$custom ?? '',
            'image'=>$image ?? '',
            'image_name'=>$image->getClientOriginalName() ?? ''
        ]);

        
        // $response = $client->request('POST', 'http://mydomain.de:8080/spots', [
        //     'multipart' => [
        //         [
        //             'name'     => 'body',
        //             'contents' => json_encode(['name' => 'Test', 'country' => 'Deutschland']),
        //             'headers'  => ['Content-Type' => 'application/json']
        //         ],
        //         [
        //             'name'     => 'file',
        //             'contents' => fopen('617103.mp4', 'r'),
        //             'headers'  => ['Content-Type' => 'video/mp4']
        //         ],
        //     ],
        // ]);

        // dd($response->json());
        
      
        return redirect()->route('superadmin.package.index')->with('success','Package Added Successfully');
    }
    public function edit($id){
        $response = Http::get(config('helper.package_api_url').'/get/package/detail/'.$id);
        $package = $response->json() ?? '';
        $variable=$package['custom'];
        if($variable)
        {
            $customs = json_decode($variable);
        }
        else{
            $customs = '';
        }

        return view('superadmin.package.edit',compact('package','customs'));
    } 
    public function show($id){
        $response = Http::get(config('helper.package_api_url').'/get/package/detail/'.$id);
        $package = $response->json() ?? '';
        // dd($package);

        $variable=$package['custom'];
        if($variable)
        {
            $customs = json_decode($variable);
        }
        else{
            $customs = '';
        }
        // dd($custom);
         return view('superadmin.package.show',compact('package','customs'));
     } 

    public function update(Request $request, $id){
        if($request->custom)
        {
            foreach($request->custom as $key=>$value)
            {
                $data[] = $value;
            }
            $custom = json_encode($data);
        }
        $image=$request->image ?? '';
        //Post the data to package microservice
        $response =  Http::attach(
            'attachment', file_get_contents($image), $image->getClientOriginalName()
        )->post(config('helper.package_api_url').'/update/package/'.$id, [
            'name'=>$request->name ?? '',
            'description'=>$request->description ?? '' ,
            'status'=>$request->status ?? '0' ,
            'package_type'=>$request->package_type ?? '0' ,
            'package_time'=>$request->package_time ?? '0' ,
            'amount'=>$request->amount ?? '0' ,
            'support'=>$request->support ?? '' ,
            'is_default'=>$request->is_default ?? '0' ,
            'is_free_trial'=>$request->is_free_trial ?? '0' ,
            'billing_type'=>$request->type ?? '0' ,
            'currency'=>$request->currency ?? '0' ,
            'users_for_facial_recognition'=>$request->users_for_facial_recognition ?? '0' ,
            'check_ins'=>$request->check_ins ?? '0',
            'touch_point'=>$request->touch_point ?? '0' ,
            'admin_touch_point'=>$request->admin_touch_point ?? '0' ,
            'front_touch_point'=>$request->front_touch_point ?? '0' ,
            'edge_touch_point'=>$request->edge_touch_point ?? '0' ,
            'data_maintained_days'=>$request->data_maintained_days ?? '0' ,
            'backend_integration'=>$request->backend_integration ?? '0' ,
            'sites'=>$request->sites ?? '0' ,
            'rate_limit'=>$request->rate_limit ?? '0',
            'admin_users'=>$request->admin_users ?? '0',
            'custom'=>$custom ?? '',
            'image'=>$image ?? '',
            'image_name'=>$image->getClientOriginalName() ?? ''
        ]);

        // dd($response->json());
        
      
        return redirect()->route('superadmin.package.index')->with('success','Package Updated Successfully');
    }
    public function destroy($id){
        $response =  Http::post(config('helper.package_api_url').'/destroy/package/'.$id);
        // dd($response);
        return redirect()->route('superadmin.package.index')->with('danger','Package Destroyed Successfully');
     } 

     public function user()
     {
            $users=User::all();
            // $users=auth('api')->user();
             return view('superadmin.userPackage.index',compact('users'));
         

     }

    public function filter(Request $request)
    {
        $response =  Http::get(config('helper.package_api_url').'/filter/package', [
            'status'=>$request->status ?? '' ,
            'package_type'=>$request->package_type ?? '' ,
        ]);

        $packages = $response->json() ?? '';

        $status = $request->status ?? '' ;
        $package_type = $request->package_type ?? '';

        return view('superadmin.package.index',compact('packages','status','package_type'));
    }

}
