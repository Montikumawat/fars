<?php

namespace App\Http\Controllers;

use App\Package;
use App\User;
use App\Order;
use App\OrderItem;
use App\UserPurchasedPackage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Razorpay\Api\Api;
use Session;
use Stripe;
use Exception;

class FrontendController extends Controller
{
    public function index()
    {
        // $packages= Package::all();
        return redirect()->route('login');
    }

    
}
