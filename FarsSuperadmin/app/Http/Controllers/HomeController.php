<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(auth()->user()->isSuperadmin()){
            $response =  Http::get(config('helper.login_api_url').'/get/orders');
            $orders = $response->json() ?? '';

            $responseu = Http::get(config('helper.customer_api_url').'/get/customers');
            $use = $responseu->json() ?? '';

            $pack = Http::get(config('helper.package_api_url').'/get/packages');
            $packs = $pack->json() ?? '';

            $total_customers = 0;
            $suminr = 0;
            $sumusd = 0;

            if($use != '')
            {
                $total_customers = count($use);
            }

            if($orders != ''){
               foreach($orders as $order){
                   if($order['currency'] == 'INR')
                   {
                    $suminr += $order['amount'];
                   }
                   if($order['currency'] == 'USD')
                   {
                    $sumusd += $order['amount'];
                   }

               }
            }

            foreach($use as $us)
            {
            $users[$us['id']] =['name'=>$us['name'],
                                      'email'=>$us['email'],
                                        'phone_number'=>$us['phone_number'],] ;
            }



            foreach($packs as $pac)
            {
            if(isset($pac['billing_type'])){
                $packages[$pac['id']] =['name'=>$pac['name'],
                'billing_type'=>config('helper.billing_type')[$pac['billing_type']]] ;
            }

            }



            return view('superadmin.dashboard',compact('orders','users','packages','total_customers','suminr','sumusd','total_customers'));
        }
    }

    public function paymentFilter(Request $request)
    {
        $status = $request->status ?? '';
        $range = $request->range ?? '';
        $from_date = $request->from_date ?? '';
        $end_date = $request->end_date ?? '';
        if($request->range == 'custom')
        {
            $range_1 = '';
            $range_2 = '';
        } else {
            $range_1 = Carbon::now()->format('Y-m-d');
            $range_2 = Carbon::now()->format('Y-m-d');
        }

        if($request->range != '')
        {
            if($request->range == 'today')
            {
                $range_2 = Carbon::now()->format('Y-m-d');
            }
            if($request->range == 'week')
            {
                $range_2 = Carbon::now()->subDays(7)->format('Y-m-d');
            }
            if($request->range == 'month')
            {
                $range_2 = Carbon::now()->subMonth()->format('Y-m-d');
            }
            if($request->range == 'year')
            {
                $range_2 = Carbon::now()->subYear()->format('Y-m-d');
            }
        }

        $response =  Http::get(config('helper.login_api_url').'/filter/orders', [
            'status'=>$request->status ?? '' ,
            'range_1'=>$range_2 ?? '' ,
            'range_2'=>$range_1 ?? '' ,
            'from_date'=>$request->from_date ?? '' ,
            'end_date'=>$request->end_date ?? '' ,
        ]);

        $orders = $response->json() ?? '';

        $responseu = Http::get(config('helper.customer_api_url').'/get/customers');
        $use = $responseu->json() ?? '';

        $pack = Http::get(config('helper.package_api_url').'/get/packages');
        $packs = $pack->json() ?? '';

        $total_customers = 0;
        $suminr = 0;
        $sumusd = 0;

        if($use != '')
        {
            $total_customers = count($use);
        }

        if($orders != ''){
           foreach($orders as $order){
               if($order['currency'] == 'INR')
               {
                $suminr =+ $order['amount'];
               }
               if($order['currency'] == 'USD')
               {
                $sumusd =+ $order['amount'];
               }
           }
        }


        foreach($use as $us)
        {
        $users[$us['id']] =['name'=>$us['name'],
                                  'email'=>$us['email'],
                                    'phone_number'=>$us['phone_number'],] ;
        }

        foreach($packs as $pac)
        {
        $packages[$pac['id']] =['name'=>$pac['name'],
                                  'billing_type'=>config('helper.package_type')[$pac['package_type']]] ;
        }



        return view('superadmin.dashboard',compact('orders','users','packages','total_customers','suminr','sumusd','total_customers','status','range','from_date','end_date'));
    }
}
