<?php

Route::group(['middleware' => ['role:superadmin']], function () {
    //Package Routes
    Route::get('/package','SuperAdmin\PackageController@index')->name('superadmin.package.index');
    Route::get('/package/add','SuperAdmin\PackageController@add')->name('superadmin.package.add');
    Route::post('/package/store','SuperAdmin\PackageController@store')->name('superadmin.package.store');
    // Route::get('/package/edit/{id}','SuperAdmin\PackageController@edit')->name('superadmin.package.edit');
    Route::post('/package/update/{id}','SuperAdmin\PackageController@update')->name('superadmin.package.update');
    Route::get('/package/destroy/{id}','SuperAdmin\PackageController@destroy')->name('superadmin.package.destroy');
    Route::get('/user/package','SuperAdmin\PackageController@user')->name('superadmin.user.package');
    Route::get('/package/view/{id}','SuperAdmin\PackageController@view')->name('superadmin.user.package.view');
    Route::get('/package/edit/{id}','SuperAdmin\PackageController@edit')->name('superadmin.user.package.edit');
    Route::get('/package/show/{id}','SuperAdmin\PackageController@show')->name('superadmin.user.package.show');
    Route::get('/package/filter','SuperAdmin\PackageController@filter')->name('superadmin.package.filter');
    Route::get('/package/edit/', function () {
      return view('superadmin.package.edit');
  })->name('superadmin.package.edit');
  Route::get('/package/show/', function () {
    return view('superadmin.package.show');
})->name('superadmin.package.show');
  //User Routes
  Route::get('/users','SuperAdmin\UsersController@index')->name('superadmin.users.index');
  Route::get('/users/add','SuperAdmin\UsersController@add')->name('superadmin.users.add');
  Route::post('/users/store','SuperAdmin\UsersController@store')->name('superadmin.users.store');
  Route::get('/users/edit/{id}','SuperAdmin\UsersController@edit')->name('superadmin.users.edit');
  Route::post('/users/update/{id}','SuperAdmin\UsersController@update')->name('superadmin.users.update');
  Route::get('/users/destroy/{id}','SuperAdmin\UsersController@destroy')->name('superadmin.users.destroy');
  Route::get('/users/show/{id}','SuperAdmin\UsersController@show')->name('superadmin.users.show');



  //Training

  Route::get('/training','SuperAdmin\TrainingController@index')->name('superadmin.training.index');
  Route::get('/home', 'HomeController@index')->name('home.superadmin');

    Route::get('/setting/payment/create', function () {
      return view('superadmin.setting.paymentCreate');
  })->name('superadmin.paymentSetting.create');

  Route::get('/userAccount/listing','SuperAdmin\UserAccountController@index')->name('superadmin.userAccount.index');
  Route::get('/userAccount/create','SuperAdmin\UserAccountController@create')->name('superadmin.userAccount.create');
  Route::get('/userAccount/invoice/{id}','SuperAdmin\UserAccountController@invoiceDetail')->name('superadmin.userAccount.invoice.detail');
  Route::get('/userAccount/invoice/{id}/download','SuperAdmin\UserAccountController@invoiceDownload')->name('superadmin.userAccount.invoice.download');
  Route::post('/userAccount/store','SuperAdmin\UserAccountController@store')->name('superadmin.userAccount.store');
  Route::get('/userAccount/edit/{id}','SuperAdmin\UserAccountController@edit')->name('superadmin.userAccount.edit');
  Route::post('/userAccount/update/{id}','SuperAdmin\UserAccountController@update')->name('superadmin.userAccount.update');
  Route::get('/userAccount/destroy/{id}','SuperAdmin\UserAccountController@destroy')->name('superadmin.userAccount.destroy');
  Route::get('/userAccount/view/{id}','SuperAdmin\UserAccountController@view')->name('superadmin.userAccount.view');
  Route::get('/user/status/block/{id}','SuperAdmin\UserAccountController@block')->name('superadmin.userAccount.block');
  Route::get('/user/status/unblock/{id}','SuperAdmin\UserAccountController@unblock')->name('superadmin.userAccount.unblock');
  Route::get('/userAccount/filter','SuperAdmin\UserAccountController@filter')->name('superadmin.userAccount.filter');
  Route::get('/userAccount/filter/payment','SuperAdmin\UserAccountController@filterPayment')->name('superadmin.userAccount.payment.filter');

  Route::get('/setting/sms/index','SuperAdmin\SettingsController@smsIndex')->name('superadmin.setting.sms.index');
  Route::post('/setting/sms/store/{id}','SuperAdmin\SettingsController@smsStore')->name('superadmin.setting.sms.store');


  Route::get('/setting/email/index','SuperAdmin\SettingsController@emailIndex')->name('superadmin.setting.email.index');
  Route::post('/setting/email/store','SuperAdmin\SettingsController@emailStore')->name('superadmin.setting.email.store');

  Route::get('/setting/payment','SuperAdmin\SettingsController@paymentIndex')->name('superadmin.paymentSetting');
  Route::post('/setting/razorpay/store','SuperAdmin\SettingsController@razorpayStore')->name('superadmin.setting.razorpay.store');
  Route::post('/setting/stripe/store','SuperAdmin\SettingsController@stripeStore')->name('superadmin.setting.stripe.store');

  Route::get('/home/filter','HomeController@paymentFilter')->name('superadmin.payment.filter');

  Route::get('/tag','SuperAdmin\TagController@index')->name('superadmin.tag.index');
  Route::get('/tag/add','SuperAdmin\TagController@add')->name('superadmin.tag.add');
  Route::post('/tag/store','SuperAdmin\TagController@store')->name('superadmin.tag.store');
  Route::get('/tag/edit/{id}','SuperAdmin\TagController@edit')->name('superadmin.tag.edit');
  Route::post('/tag/update/{id}','SuperAdmin\TagController@update')->name('superadmin.tag.update');
  Route::get('/tag/destroy/{id}','SuperAdmin\TagController@destroy')->name('superadmin.tag.destroy');
  Route::get('/tag/activate/{id}','SuperAdmin\TagController@activate')->name('superadmin.tag.activate');
  Route::get('/tag/deactivate/{id}','SuperAdmin\TagController@deactivate')->name('superadmin.tag.deactivate');

});

