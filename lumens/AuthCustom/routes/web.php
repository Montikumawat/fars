<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});



$router->group(['prefix' => 'api'], function () use ($router) {
    // Matches "/api/register
//    $router->post('register', 'AuthController@register');
     // Matches "/api/login
     $router->post('/auth/login', [
        'as' => 'api.auth.login',
        'uses' => 'Auth\AuthController@postLogin',
    ]);

    // Matches "/api/profile
    $router->get('/auth/profile', 'UserController@profile');

    // Matches "/api/users/1 
    //get one user by id
    $router->get('/auth/users/{id}', 'UserController@singleUser');

    // Matches "/api/users
    $router->get('/auth/users', 'UserController@allUsers');

    $router->post('/create/order', 'Api\UserPackageController@createOrder');

    $router->post('/create/order/item', 'Api\UserPackageController@createOrderItem');

    $router->post('/create/user/package', 'Api\UserPackageController@createUserPackage');

    $router->post('/update/user/package/expire_date', 'Api\UserPackageController@userExpireDate');

    $router->get('/get/user/subscription_expire_date/{id}', 'Api\UserPackageController@getUserExpireDate');

    $router->get('/get/order/detail/{id}', 'Api\UserPackageController@getOrderDetail');

    $router->get('/get/order/detail/user/{id}', 'Api\UserPackageController@getOrderDetailFromUser');

    $router->get('/get/user/package/detail/{id}', 'Api\UserPackageController@getUserPackageDetail');

    $router->get('/get/user/packages/{id}', 'Api\UserPackageController@getUserPackages');

    $router->get('/filter/userAccount/payment', 'Api\UserPackageController@filterOrder');

    $router->get('get/orders', [
        'as' => 'orders', 'uses' => 'Api\UserPackageController@getOrders'
    ]);

    $router->get('filter/orders', [
        'as' => 'orders', 'uses' => 'Api\UserPackageController@filterOrders'
    ]);


});