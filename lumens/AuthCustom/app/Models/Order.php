<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Order extends Model{

    protected $table='orders';
    protected $fillable=['user_id','package_id','payment_type','currency','amount','transaction_id','transaction_date','status','created_at','updated_at'];
    // protected $guarded = [];

}