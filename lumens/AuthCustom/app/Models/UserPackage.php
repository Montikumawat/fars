<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class UserPackage extends Model{

    protected $table='user_purchased_packages';
    // protected $fillable=['payment_provider','status','fields','key','secret','created_at','updated_at'];
    protected $guarded = [];

}