<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Models\UserPackage;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\User;
use Illuminate\Http\Request;
use Throwable;

class UserPackageController extends Controller
{
    /**
     * Retrieve the user for the given ID.
     *
     * @param  int  $id
     * @return Response
     */
    public function createOrder(Request $request){

    try {
           
        $order = Order::create([
            'user_id' => $request->user_id,
            'package_id' => $request->package_id ?? '',
            'currency' => $request->currency ?? '',
            'amount' => $request->amount ?? '0',
            'payment_type' => $request->payment_type,
            'transaction_id' => $request->transaction_id,
            'status' => 1
          ]);

         return response()->json($order,200);
        } catch (Throwable $e) {
            report($e);
    
            return response()->json('Internal Server Error',404);
        }
    }

    public function createOrderItem(Request $request){

        try {
               
            $order = OrderItem::create([
                'order_id' => $request->order_id,
              ]);
    
             return response()->json($order,200);
            } catch (Throwable $e) {
                report($e);
        
                return response()->json('Internal Server Error',404);
            }
    }

    public function createUserPackage(Request $request){
        // return response()->json($request->start_date,200);
        try {
               
            $order = UserPackage::create([
                    'order_id' => $request->order_id,
                    'user_id' => $request->user_id,
                    'package_id' => $request->package_id,
                    'start_date' => $request->start_date,
                    'end_date' => $request->end_date
              ]);
    
             return response()->json($order,200);
            } catch (Throwable $e) {
                report($e);
        
                return response()->json('Internal Server Error',404);
            }
    }

    public function userExpireDate(Request $request){

        try {
               
             $user = User::find($request->user_id);
             $user->subscription_expire_date = $request->subscription_expire_date;
             $user->package_id = $request->package_id;
             $user->save();
    
             return response()->json($user,200);
            } catch (Throwable $e) {
                report($e);
        
                return response()->json('Internal Server Error',404);
            }
    }

    public function getUserExpireDate($id){

        try {
               
             $user = User::find($id);
             $subscription_expire_date = $user->subscription_expire_date;
    
             return response()->json($subscription_expire_date,200);
            } catch (Throwable $e) {
                report($e);
        
                return response()->json('Internal Server Error',404);
            }
    }

    public function getOrderDetailFromUser($id){
        // return response()->json($id,200);
        try {
            $order=Order::where('user_id',$id)->get();
            
            // return response()->json($id,200);
            return response()->json($order,200);
        } catch (Throwable $e) {
            report($e);
    
            return response()->json('Internal Server Error',404);
        }
        
     }

     public function getOrderDetail($id){
        // return response()->json($id,200);
        try {
            $order=Order::find($id);
            
            // return response()->json($id,200);
            return response()->json($order,200);
        } catch (Throwable $e) {
            report($e);
    
            return response()->json('Internal Server Error',404);
        }
        
     }

     public function filterOrder(Request $request)
             {
                
                try {
                    if($request->payment_type != '' && $request->amount != ''){
                        $orders = Order::where('user_id',$request->user_id)->where('payment_type',$request->payment_type)->where('amount',$request->amount)->get();
                        return response()->json($orders,200);
                    } else {
                        if($request->payment_type != ''){
                            $orders = Order::where('user_id',$request->user_id)->where('payment_type',$request->payment_type)->get();
                            return response()->json($orders,200);
                        } 
                        if($request->amount != ''){
                            $orders = Order::where('user_id',$request->user_id)->where('amount',$request->amount)->get();
                            return response()->json($orders,200);
                        }
                        if($request->payment_type == '' && $request->amount == '')
                        {
                            $orders = Order::where('user_id',$request->user_id)->get();
                            return response()->json($orders,200);
                        }
                    }
                
                } catch (Throwable $e) {
                    report($e);
            
                    return response()->json('Internal Server Error',404);
                }
            }    
     
            public function getOrders(){
                try {
                    $orders=Order::orderBy('created_at', 'DESC')->get();
                
                 return response()->json($orders,200);
                } catch (Throwable $e) {
                    report($e);
            
                    return response()->json('Internal Server Error',404);
                }
                
             
            }        

            public function filterOrders(Request $request)
            {
                
                try {

                    $orders=Order::where('id','>','0');
                    if($request->status != '')
                    {
                       $orders = $orders->where('status',$request->status);
                    }
                    if($request->range_1 != '' && $request->range_2 != '')
                    {
                       $orders = $orders->whereBetween('transaction_date', [$request->range_1, $request->range_2]);
                    }
                    if($request->from_date != '' && $request->end_date != '')
                    {          
                        $orders =  $orders->whereBetween('transaction_date', [$request->from_date, $request->end_date]);
                    }
                   

                    $order = $orders->get();
                
                 return response()->json($order,200);
                } catch (Throwable $e) {
                    report($e);
            
                    return response()->json('Internal Server Error',404);
                }
            }

            public function getUserPackageDetail($id){
                // return response()->json($id,200);
                try {
                    $userPackage=UserPackage::where('order_id',$id)->first();
                    
                    // return response()->json($id,200);
                    return response()->json($userPackage,200);
                } catch (Throwable $e) {
                    report($e);
            
                    return response()->json('Internal Server Error',404);
                }
                
             }   
             
             
            public function getUserPackages($id){
                // return response()->json($id,200);
                try {
                    $userPackage=UserPackage::orderBy('created_at','DESC')->where('user_id',$id)->get();
                    
                    // return response()->json($id,200);
                    return response()->json($userPackage,200);
                } catch (Throwable $e) {
                    report($e);
            
                    return response()->json('Internal Server Error',404);
                }
                
             }   
             
             
}