<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Models\Package;
use App\Models\PackageCustom;
use App\User;
use Illuminate\Http\Request;
use Throwable;

class PackageController extends Controller
{
    /**
     * Retrieve the user for the given ID.
     *
     * @param  int  $id
     * @return Response
     */
     public function getPackages(){

        // dd('hii');

        try {
            $packages=Package::orderBy('created_at', 'DESC')->get();

            // dd($package);

         return response()->json($packages,200);
        } catch (Throwable $e) {
            report($e);

            return response()->json('Internal Server Error',404);
        }

     }

     public function getPackageDetail($id){
        try {
            $package=Package::find($id);

         return response()->json($package,200);
        } catch (Throwable $e) {
            report($e);

            return response()->json('Internal Server Error',404);
        }

     }

      //Create Package
      public function createPackage(Request $request){
        // dd($request->custom);
        // return response()->json($request->hasFile('attachment'),200);
        try {

            // if($request->custom)
            // {
            //     foreach($request->custom as $key=>$value)
            //     {
            //         $data[] = $value;
            //     }

            // }


            /** Following is the code which you will use to handle the file upload */
            if($request->hasFile('attachment')){
              /** Make sure you will properly validate your file before uploading here */

               /** Get the file data here */
               $file = $request->attachment;

               /** Generate random unique_name for file */
               $fileName = time().md5(time()).'.'.$file->getClientOriginalExtension();
            //    dd($fileName);
               $file->move('uploads/packages', $fileName);
            }



            $package =  Package::create([

            'name'=>$request->name ?? '',
            'description'=>$request->description ?? '' ,
            'status'=>$request->status ?? '0' ,
            'package_type'=>$request->package_type ?? '0' ,
            'package_time'=>$request->package_time ?? '0' ,
            'amount'=>$request->amount ?? '0' ,
            'support'=>$request->support ?? '' ,
            'is_default'=>$request->is_default ?? '0' ,
            'is_free_trial'=>$request->is_free_trial ?? '0' ,
            'billing_type'=>$request->billing_type ?? '0' ,
            'currency'=>$request->currency ?? '0' ,
            'users_for_facial_recognition'=>$request->users_for_facial_recognition ?? '0' ,
            'check_ins'=>$request->check_ins ?? '0',
            'touch_point'=>$request->touch_point ?? '0' ,
            'admin_touch_point'=>$request->admin_touch_point ?? '0' ,
            'front_touch_point'=>$request->front_touch_point ?? '0' ,
            'edge_touch_point'=>$request->edge_touch_point ?? '0' ,
            'data_maintained_days'=>$request->data_maintained_days ?? '0' ,
            'backend_integration'=>$request->backend_integration ?? '0' ,
            'sites'=>$request->sites ?? '0' ,
            'rate_limit'=>$request->rate_limit ?? '0',
            'admin_users'=>$request->admin_users ?? '0',
            'custom'=>$request->custom ?? '',
            'image'=>$fileName
            ]);



         return response()->json($package,200);
        } catch (Throwable $e) {
            report($e);

            return response()->json('Internal Server Error',404);
        }

     }


     public function updatePackage(Request $request, $id){
        try {

                  /** Following is the code which you will use to handle the file upload */
                  if($request->hasFile('attachment')){
                    /** Make sure you will properly validate your file before uploading here */

                     /** Get the file data here */
                     $file = $request->attachment;

                     /** Generate random unique_name for file */
                     $fileName = time().md5(time()).'.'.$file->getClientOriginalExtension();
                  //    dd($fileName);
                     $file->move('uploads/packages', $fileName);
                  }

            $package =  Package::find($id);

            $package->name  = $request->name ?? '';
            $package->description = $request->description ?? '';
            $package->status = $request->status ?? '0';
            $package->package_type = $request->package_type ?? '0';
            $package->package_time = $request->package_time ?? '0';
            $package->amount = $request->amount ?? '0';
            $package->support = $request->support ?? '';
            $package->is_default = $request->is_default ?? '0';
            $package->is_free_trial = $request->is_free_trial ?? '0';
            $package->billing_type = $request->type ?? '0';
            $package->currency = $request->currency ?? '0';
            $package->users_for_facial_recognition = $request->users_for_facial_recognition ?? '0';
            $package->check_ins = $request->check_ins ?? '0';
            $package->touch_point = $request->touch_point ?? '0';
            $package->admin_touch_point = $request->admin_touch_point ?? '0';
            $package->front_touch_point = $request->front_touch_point ?? '0';
            $package->edge_touch_point = $request->edge_touch_point ?? '0';
            $package->data_maintained_days = $request->data_maintained_days ?? '0';
            $package->backend_integration = $request->backend_integration ?? '0';
            $package->sites = $request->sites ?? '0';
            $package->rate_limit = $request->rate_limit ?? '0';
            $package->admin_users = $request->admin_users ?? '0';
            $package->custom = $request->custom ?? '';
            $package->image = $fileName ?? '';

            $package->save();

         return response()->json($package,200);
        } catch (Throwable $e) {
            report($e);

            return response()->json('Internal Server Error',404);
        }

     }

     public function destroyPackage($id){
        try {
            $package=Package::find($id);

            $package->delete();

         return response()->json('success',200);
        } catch (Throwable $e) {
            report($e);

            return response()->json('Internal Server Error',404);
        }

     }

     public function filterPackage(Request $request)
     {

        try {
            if($request->status != '' && $request->package_type != ''){
                $packages = Package::where('status',$request->status)->where('package_type',$request->package_type)->get();
                return response()->json($packages,200);
            } else {
                if($request->status != ''){
                    $packages = Package::where('status',$request->status)->get();
                    return response()->json($packages,200);
                }
                if($request->package_type != ''){
                    $packages = Package::where('package_type',$request->package_type)->get();
                    return response()->json($packages,200);
                }
                if($request->status == '' && $request->package_type == '')
                {
                    $packages = Package::all();
                    return response()->json($packages,200);
                }
            }

        } catch (Throwable $e) {
            report($e);

            return response()->json('Internal Server Error',404);
        }
    }

}
