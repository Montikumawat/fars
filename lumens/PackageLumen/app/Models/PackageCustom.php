<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class PackageCustom extends Model{

protected $table='package_customs';
protected $fillable=['package_id','name','category','value'];

}