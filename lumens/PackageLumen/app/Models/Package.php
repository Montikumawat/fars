<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Package extends Model{

protected $table='packages';
protected $fillable=['name','description','image','status','package_type','package_time','amount','support','is_default','billing_type','is_free_trial','type','currency','users_for_facial_recognition','check_ins','touch_point','admin_touch_point','front_touch_point','edge_touch_point','data_maintained_days','backend_integration','sites','rate_limit','admin_users','custom','image'];

}