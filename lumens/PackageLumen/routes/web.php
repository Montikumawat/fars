<?php

/** @var \Laravel\Lumen\Routing\Router $router */

use App\Http\Controllers\Api\PackageController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Traits\CapsuleManagerTrait;
use App\Models\Package;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    $Packages=Package::all();
    // dd($Packages);
    return $router->app->version();
});

$router->get('api/get/packages', [
    'as' => 'packages', 'uses' => 'Api\PackageController@getPackages'
]);

$router->post('api/create/package', [
    'as' => 'packages', 'uses' => 'Api\PackageController@createPackage'
]);

$router->get('api/get/package/detail/{id}', [
    'as' => 'packages', 'uses' => 'Api\PackageController@getPackageDetail'
]);

$router->post('api/update/package/{id}', [
    'as' => 'packages', 'uses' => 'Api\PackageController@updatePackage'
]);

$router->post('api/destroy/package/{id}', [
    'as' => 'packages', 'uses' => 'Api\PackageController@destroyPackage'
]);

$router->get('api/filter/package', [
    'as' => 'packages', 'uses' => 'Api\PackageController@filterPackage'
]);
