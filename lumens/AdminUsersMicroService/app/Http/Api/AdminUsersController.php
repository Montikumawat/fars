<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Models\Tag;
use Illuminate\Http\Request;
use Throwable;

class TagController extends Controller
{
    /**
     * Retrieve the user for the given ID.
     *
     * @param  int  $id
     * @return Response
     */
     public function getUsers(){
        try {
            $users=AdminUser::orderBy('created_at', 'DESC')->get();
        
         return response()->json($users,200);
        } catch (Throwable $e) {
            report($e);
    
            return response()->json('Internal Server Error',404);
        }
        
     }

     public function getUserDetail($id){
        try {
            $user=AdminUser::find($id);
        
         return response()->json($user,200);
        } catch (Throwable $e) {
            report($e);
    
            return response()->json('Internal Server Error',404);
        }
        
     }

      //Create Tag
      public function createTag(Request $request){

        try {

             
            $user =  AdminUser::create([
                'name'=>$request->name ?? '',
                'description'=>$request->description ?? '' ,
                'status'=>$request->status ?? '0' ,
            ]);


            
         return response()->json($user,200);
        } catch (Throwable $e) {
            report($e);
    
            return response()->json('Internal Server Error',404);
        }
        
     }


     public function updateTag(Request $request, $id){
        try {

              
              
            $user =  AdminUser::find($id);

            $user->name  = $request->name ?? '';
            $user->description = $request->description ?? '';
            $user->status = $request->status ?? '0';

            $user->save();
            
         return response()->json($user,200);
        } catch (Throwable $e) {
            report($e);
    
            return response()->json('Internal Server Error',404);
        }
        
     }

     public function destroyTag($id){
        try {
            $user=AdminUser::find($id);

            $user->delete();
        
         return response()->json('success',200);
        } catch (Throwable $e) {
            report($e);
    
            return response()->json('Internal Server Error',404);
        }
        
     }

     public function activate($id){
        try {
            $user=AdminUser::find($id);
            $user->status=1;
            $user->save();
            return response()->json('success',200);
        } catch (Throwable $e) {
            report($e);   
            return response()->json('Internal Server Error',404);
        }
     }

     public function deactivate($id){
        try {
            $user=AdminUser::find($id);
            $user->status=0;
            $user->save();
            return response()->json('success',200);
        } catch (Throwable $e) {
            report($e);   
            return response()->json('Internal Server Error',404);
        }
     }

}