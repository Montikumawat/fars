<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Models\Tag;
use Illuminate\Http\Request;
use Throwable;

class TagController extends Controller
{
    /**
     * Retrieve the user for the given ID.
     *
     * @param  int  $id
     * @return Response
     */
     public function getTags(){
        try {
            $tags=Tag::orderBy('created_at', 'DESC')->get();
        
         return response()->json($tags,200);
        } catch (Throwable $e) {
            report($e);
    
            return response()->json('Internal Server Error',404);
        }
        
     }

     public function getTagDetail($id){
        try {
            $tag=Tag::find($id);
        
         return response()->json($tag,200);
        } catch (Throwable $e) {
            report($e);
    
            return response()->json('Internal Server Error',404);
        }
        
     }

      //Create Tag
      public function createTag(Request $request){

        try {

             
            $tag =  Tag::create([
                'name'=>$request->name ?? '',
                'description'=>$request->description ?? '' ,
                'status'=>$request->status ?? '0' ,
            ]);


            
         return response()->json($tag,200);
        } catch (Throwable $e) {
            report($e);
    
            return response()->json('Internal Server Error',404);
        }
        
     }


     public function updateTag(Request $request, $id){
        try {

              
              
            $tag =  Tag::find($id);

            $tag->name  = $request->name ?? '';
            $tag->description = $request->description ?? '';
            $tag->status = $request->status ?? '0';

            $tag->save();
            
         return response()->json($tag,200);
        } catch (Throwable $e) {
            report($e);
    
            return response()->json('Internal Server Error',404);
        }
        
     }

     public function destroyTag($id){
        try {
            $tag=Tag::find($id);

            $tag->delete();
        
         return response()->json('success',200);
        } catch (Throwable $e) {
            report($e);
    
            return response()->json('Internal Server Error',404);
        }
        
     }

     public function activate($id){
        try {
            $tag=Tag::find($id);
            $tag->status=1;
            $tag->save();
            return response()->json('success',200);
        } catch (Throwable $e) {
            report($e);   
            return response()->json('Internal Server Error',404);
        }
     }

     public function deactivate($id){
        try {
            $tag=Tag::find($id);
            $tag->status=0;
            $tag->save();
            return response()->json('success',200);
        } catch (Throwable $e) {
            report($e);   
            return response()->json('Internal Server Error',404);
        }
     }

}