<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function () use ($router) {

    $router->get('get/tags', [
        'as' => 'tags.get', 'uses' => 'Api\TagController@getTags'
    ]);
    
    $router->post('create/tag', [
        'as' => 'tags.create', 'uses' => 'Api\TagController@createTag'
    ]);
    
    $router->get('get/tag/detail/{id}', [
        'as' => 'tags.detail', 'uses' => 'Api\TagController@getTagDetail'
    ]);
    
    $router->post('update/tag/{id}', [
        'as' => 'tags.update', 'uses' => 'Api\TagController@updateTag'
    ]);
    
    $router->post('destroy/tag/{id}', [
        'as' => 'tags.destroy', 'uses' => 'Api\TagController@destroyTag'
    ]);

    $router->post('tag/activate/{id}', [
        'as' => 'tags.activate', 'uses' => 'Api\TagController@activate'
    ]);

    $router->post('tag/deactivate/{id}', [
        'as' => 'tags.deactivate', 'uses' => 'Api\TagController@deactivate'
    ]);


});