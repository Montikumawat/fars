<?php

/** @var \Laravel\Lumen\Routing\Router $router */
use App\Http\Controllers\Api\CustomerController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Traits\CapsuleManagerTrait;
use App\Models\Customer;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    // $Customer=Customer::all();
    $customer=Customer::all();
    return $router->app->version();
});

$router->get('api/get/customers', [
    'as' => 'customer.get', 'uses' => 'Api\CustomerController@getCustomers'
]);

$router->post('api/create/customer', [
    'as' => 'customer.create', 'uses' => 'Api\CustomerController@createCustomer'
]);

$router->get('api/get/customer/detail/{id}', [
    'as' => 'customer.detail', 'uses' => 'Api\CustomerController@getCustomerDetail'
]);

$router->post('api/update/customer/{id}', [
    'as' => 'customers', 'uses' => 'Api\CustomerController@updateCustomer'
]);

$router->post('api/destroy/customer/{id}', [
    'as' => 'customer', 'uses' => 'Api\CustomerController@destroyCustomer'
]);

$router->post('api/status/block/customer/{id}', [
    'as' => 'customer.status', 'uses' => 'Api\CustomerController@CustomerBlock'
]);

$router->post('api/status/unblock/customer/{id}', [
    'as' => 'customer.status', 'uses' => 'Api\CustomerController@CustomerUnBlock'
]);

$router->get('api/filter/customer', [
    'as' => 'customers', 'uses' => 'Api\CustomerController@filterCustomer'
]);

$router->get('api/get/total/customers', [
    'as' => 'customers', 'uses' => 'Api\CustomerController@totalCustomers'
]);
