<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\CustomerCustom;
use App\User;
// use Illuminate\Support\Facades\Request;
use Illuminate\Http\Request;
// use Laravel\Lumen\Http\Request;
use Throwable;

class CustomerController extends Controller
{
    /**
     * Retrieve the user for the given ID.
     *
     * @param  int  $id
     * @return Response
     */
     public function getCustomers(){
        try {
            $customers=Customer::orderBy('created_at', 'DESC')->get();
            return response()->json($customers,200);
        } catch (Throwable $e) {
            report($e);
            return response()->json('Internal Server Error',404);
        }

     }

     public function getCustomerDetail($id){
        try {
            $customer=Customer::find($id);

         return response()->json($customer,200);
        } catch (Throwable $e) {
            report($e);

            return response()->json('Internal Server Error',404);
        }

     }

     public function createCustomer(Request $request){

    //    dd($request->all());

        // return response()->json($request->email,200);/

        // return response()->json($request->email,200);

        $customer = Customer::where('email',$request->email)->first();

    //    dd($customer);

        if($customer == [])
        {
            try {

                $customer =  Customer::create([
                    'user_unique_id'=>$request->user_unique_id ?? '0' ,
                    // dd('hii'),
                'org_unique_id'=>$request->org_unique_id ?? '0' ,
                'country_id'=>$request->country_id ?? '0' ,
                'name'=>$request->name ?? '' ,
                'phone_number'=>$request->phone_number ?? '' ,
                'email'=>$request->email,
                'company_name'=>$request->company_name ?? '' ,
                'customer_type'=>$request->customer_type ?? '' ,
                'domain_name'=>$request->domain_name ?? '' ,
                'account_number'=>$request->account_number ?? '' ,
                'billing_type'=>$request->billing_type ?? '0' ,
                'payment_method'=>$request->payment_method ?? '0' ,
                'address'=>$request->address ?? '',
                'address_line_2'=>$request->address_line_2 ?? '' ,
                'billing_address'=>$request->billing_address ?? '' ,
                'billing_address_line_2'=>$request->billing_address_line_2 ?? '' ,
                'street_address'=>$request->street_address ?? '' ,
                'street_address_line_2'=>$request->street_address_line_2 ?? '' ,
                'city'=>$request->city ?? '' ,
                'state'=>$request->state ?? '' ,
                'status'=>$request->status ?? '1',
                'package_id'=>$request->package_id ?? '',
                'zip_code'=>$request->zip_code ?? '',
                'pan_card_number'=>$request->pan_card_number ?? '',
                'gst_number'=>$request->gst_number ?? '',
                'password'=>$request->password ?? '',
                'subscription_expire_date'=> $request->subscription_expire_date ?? '',
                    ]);

                    return response()->json($customer,200);
                } catch (Throwable $e) {
                    report($e);

                    return response()->json('Internal Server Error',404);
                }
        } else {
            return response()->json('Email Already Exists',404);
        }



         }

    public function updateCustomer(Request $request, $id){

        //    dd($request->all());
         //   dd($id);

		// return response()->json($request->all(),200);

            try {


                $customer =  Customer::find($id);
                // dd($customer);
                $customer->user_unique_id = $request->user_unique_id ?? '0';
                $customer->org_unique_id = $request->org_unique_id ?? '0';
                $customer->country_id = $request->country_id ?? '0';
                $customer->name=$request->name ??  $customer->name;
                $customer->phone_number=$request->phone_number ?? $customer->phone_number;
                //  $customer->email=$request->email ?? '';
                $customer->company_name=$request->company_name ?? $customer->company_name;
                $customer->customer_type = $request->customer_type ?? $customer->customer_type;
                $customer->domain_name=$request->domain_name ?? $customer->domain_name;

                $customer->account_number=$request->account_number ?? $customer->account_number;
                $customer->billing_type=$request->billing_type ?? $customer->billing_type;
                $customer->payment_method=$request->payment_method ?? $customer->payment_method;

                $customer->address=$request->address ?? $customer->address;
                $customer->address_line_2=$request->address_line_2 ?? $customer->address_line_2;
                $customer->billing_address=$request->billing_address ?? $customer->billing_address;

                $customer->billing_address_line_2=$request->billing_address_line_2 ?? $customer->billing_address_line_2;
                $customer->street_address=$request->street_address ?? $customer->street_address;
                $customer->street_address_line_2=$request->street_address_line_2 ?? $customer->street_address_line_2;

                $customer->city=$request->city ?? $customer->city;
                $customer->state=$request->state ?? $customer->state;
                $customer->status=$request->status ?? '1';

                // $customer->package_id=$request->package_id ?? $customer->package_id;
                $customer->zip_code=$request->zip_code ?? $customer->zip_code;
                $customer->pan_card_number=$request->pan_card_number ?? $customer->pan_card_number;

                $customer->gst_number=$request->gst_number ?? $customer->gst_number;
                // $customer->email_verified_at=$request->email_verified_at ?? '';
                // $customer->subscription_expire_date=$request->subscription_expire_date ?? '';

                $customer->remember_token=$request->remember_token ?? '';
                $customer->api_token=$request->api_token ?? '';

                $customer->token=$request->token ?? '';

                // dd($customer);
                $customer->save();
                return response()->json($customer,200);
               }
               catch (Throwable $e) {
                   report($e);

                   return response()->json('Internal Server Error',404);
               }

            }

            public function destroyCustomer($id){
                try {
                    $customer=Customer::find($id);

                    $customer->delete();

                 return response()->json('success',200);
                } catch (Throwable $e) {
                    report($e);

                    return response()->json('Internal Server Error',400);
                }

             }

             public function CustomerBlock($id){
                // return response()->json($id,200);
                try {
                    $user=Customer::find($id);
                    $user->status=2;
                    $user->save();
                    return response()->json('success',200);
                } catch (Throwable $e) {
                    report($e);
                    return response()->json('Internal Server Error',404);
                }

             }
             public function CustomerUnBlock($id){
                // return response()->json($id,200);
                try {
                    $user=Customer::find($id);
                    $user->status=1;
                    $user->save();
                    return response()->json('success',200);
                } catch (Throwable $e) {
                    report($e);
                    return response()->json('Internal Server Error',404);
                }

             }

             public function filterCustomer(Request $request)
             {

                try {
                    if($request->status != '' && $request->package_id != ''){
                        $users = Customer::where('status',$request->status)->where('package_id',$request->package_id)->get();
                        return response()->json($users,200);
                    } else {
                        if($request->status != ''){
                            $users = Customer::where('status',$request->status)->get();
                            return response()->json($users,200);
                        }
                        if($request->package_id != ''){
                            $users = Customer::where('package_id',$request->package_id)->get();
                            return response()->json($users,200);
                        }
                        if($request->status == '' && $request->package_id == '')
                        {
                            $users = Customer::all();
                            return response()->json($users,200);
                        }
                    }

                } catch (Throwable $e) {
                    report($e);

                    return response()->json('Internal Server Error',404);
                }
            }

            public function totalCustomers(){

                try {
                    $customers=DB::table('users')->count();
                    return response()->json($customers,200);
                } catch (Throwable $e) {
                    report($e);
                    return response()->json('Internal Server Error',404);
                }

             }
    }
