<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{

protected $table = 'users';
// protected $guard=[];
protected $fillable=['user_unique_id','org_unique_id','country_id','name','phone_number','email','company_name','customer_type','domain_name','account_number','billing_type','payment_method','address','address_line_2','billing_address','billing_address_line_2','street_address','street_address_line_2','city','state','zip_code','pan_card_number','gst_number','status','package_id','email_verified_at','subscription_expire_date','password','remember_token','api_token','token'];

}
