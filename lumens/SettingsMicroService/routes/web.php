<?php
use App\Models\SmsSetting;
/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    // dd(SmsSetting::all());
    return $router->app->version();
});

$router->get('api/get/sms/detail', [
    'as' => 'sms', 'uses' => 'Api\SmsController@getSmsDetail'
]);

$router->post('api/update/sms/{id}', [
    'as' => 'sms.update', 'uses' => 'Api\SmsController@updateSms'
]);

$router->post('api/sms/status', [
    'as' => 'sms.status', 'uses' => 'Api\SmsController@smsStatus'
]);


$router->get('api/get/email/detail', [
    'as' => 'email', 'uses' => 'Api\EmailController@getEmailDetail'
]);

$router->post('api/update/email', [
    'as' => 'email.update', 'uses' => 'Api\EmailController@updateEmail'
]);

$router->get('api/get/razorpay/detail', [
    'as' => 'razorpay', 'uses' => 'Api\PaymentController@getRazorpayDetail'
]);

$router->post('api/update/razorpay', [
    'as' => 'razorpay.update', 'uses' => 'Api\PaymentController@updateRazorpay'
]);

$router->get('api/get/stripe/detail', [
    'as' => 'stripe', 'uses' => 'Api\PaymentController@getStripeDetail'
]);

$router->post('api/update/stripe', [
    'as' => 'stripe.update', 'uses' => 'Api\PaymentController@updateStripe'
]);


