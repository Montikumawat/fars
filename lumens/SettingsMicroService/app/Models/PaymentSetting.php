<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class PaymentSetting extends Model{

    protected $table='payment_settings';
    protected $fillable=['payment_provider','status','fields','key','secret','created_at','updated_at'];

}