<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class SmsSetting extends Model{

    protected $table='sms_settings';
    protected $fillable=['api_key','api_secret_key','signature_secret','created_at','updated_at'];

}