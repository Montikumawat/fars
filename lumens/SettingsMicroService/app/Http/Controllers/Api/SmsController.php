<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Models\SmsSetting;
use Illuminate\Http\Request;
use Throwable;

class SmsController extends Controller
{

    public function getSmsDetail(){

        try {
            $sms=SmsSetting::where('type','sms')->get();
                //  dd($sms);
            return response()->json($sms,200);
        } catch (Throwable $e) {
            report($e);

            return response()->json('Internal Server Error',404);
        }

     }



    public function updateSms(Request $request, $id){
        // dd($id);

        $sm = SmsSetting::first();
        // dd($request->all());
        try {

            $sms=SmsSetting::where('id', $id)->first();

            if ($request->status == 'on' && $sm->status == 0) {
                // here you code
                $sms->status = 1;

            }
            else{


                $sms->status = 0;
                $sms->save();
                return response()->json('Internal Server Error',404);
            }



            // dd($sms->status);

            $sms->api_key  = $request->api_key ?? $sms->api_key;
            $sms->api_secret_key = $request->api_secret_key ?? $sms->api_secret_key;
            $sms->signature_secret = $request->signature_secret ?? $sms->signature_secret;

            $sms->save();

                // $status = $response->json();

            // if($sms->status == 1){

            //     return redirect()->back()->with('danger','Something went wrong! Please try again later');
            // }

         return response()->json($sms,200);
        } catch (Throwable $e) {
            report($e);

            return response()->json('Internal Server Error',404);
        }

     }

     public function smsStatus(Request $request)
     {
        // dd($id);
        // dd($request->all());

        try {
                $sms=SmsSetting::where('id', $request->id)->first();

                // dd($sms);

                $sms->status = $request->status ?? '' ;

                $sms->save();

            return response()->json($sms,200);
        } catch (Throwable $e) {
            report($e);

            return response()->json('Internal Server Error',404);
        }

     }




}
