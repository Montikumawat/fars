<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Models\PaymentSetting;
use Illuminate\Http\Request;
use Throwable;

class PaymentController extends Controller
{

    public function getRazorpayDetail(){
        try {
            $payment=PaymentSetting::where('payment_provider','Razorpay')->first();       
            return response()->json($payment,200);
        } catch (Throwable $e) {
            report($e); 
            return response()->json('Internal Server Error',404);
        }
        
     }

     public function getStripeDetail(){
        try {
            $payment=PaymentSetting::where('payment_provider','Stripe')->first();       
            return response()->json($payment,200);
        } catch (Throwable $e) {
            report($e); 
            return response()->json('Internal Server Error',404);
        }
        
     }

    public function updateRazorpay(Request $request){
       
        try {
           
            $payment=PaymentSetting::where('payment_provider','Razorpay')->first();
            
            $payment->key  = $request->key ?? '';
            $payment->secret_key = $request->secret_key ?? '';

            $payment->save();
            
         return response()->json($payment,200);
        } catch (Throwable $e) {
            report($e);
    
            return response()->json('Internal Server Error',404);
        }
        
     }

    public function updateStripe(Request $request){
        try {
            $payment=PaymentSetting::where('payment_provider','Stripe')->first();
            $payment->key  = $request->key ?? '';
            $payment->secret_key = $request->secret_key ?? '';
           
            $payment->save();
            
         return response()->json($payment,200);
        } catch (Throwable $e) {
            report($e);
    
            return response()->json('Internal Server Error',404);
        }
        
     } 

     
}