<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Models\EmailSetting;
use Illuminate\Http\Request;
use Throwable;

class EmailController extends Controller
{

    public function getEmailDetail(){
        try {
            $email=EmailSetting::where('type','smtp')->first();
        
         return response()->json($email,200);
        } catch (Throwable $e) {
            report($e);
    
            return response()->json('Internal Server Error',404);
        }
        
     }
    public function updateEmail(Request $request){
        
        try {
            $email=EmailSetting::where('type','smtp')->first();

            $email->mail_host  = $request->mail_host ?? '';
            $email->mail_port = $request->mail_port ?? '';
            $email->mail_username = $request->mail_username ?? '0';
            $email->mail_password = $request->mail_password ?? '';
            $email->mail_encryption = $request->mail_encryption ?? '';
            $email->mail_from_address = $request->mail_from_address ?? '';
            $email->mail_from_name = $request->mail_from_name ?? '';
           
            $email->save();
            
         return response()->json($email,200);
        } catch (Throwable $e) {
            report($e);
    
            return response()->json('Internal Server Error',404);
        }
        
     }

     
}