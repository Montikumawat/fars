    <!-- BEGIN: Main Menu-->
    <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mr-auto"><a class="navbar-brand" href="../../../html/ltr/vertical-menu-template/index.html"><span class="brand-logo">
                            <svg viewbox="0 0 139 95" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="24">
                                <defs>
                                    <lineargradient id="linearGradient-1" x1="100%" y1="10.5120544%" x2="50%" y2="89.4879456%">
                                        <stop stop-color="#000000" offset="0%"></stop>
                                        <stop stop-color="#FFFFFF" offset="100%"></stop>
                                    </lineargradient>
                                    <lineargradient id="linearGradient-2" x1="64.0437835%" y1="46.3276743%" x2="37.373316%" y2="100%">
                                        <stop stop-color="#EEEEEE" stop-opacity="0" offset="0%"></stop>
                                        <stop stop-color="#FFFFFF" offset="100%"></stop>
                                    </lineargradient>
                                </defs>
                                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g id="Artboard" transform="translate(-400.000000, -178.000000)">
                                        <g id="Group" transform="translate(400.000000, 178.000000)">
                                            <path class="text-primary" id="Path" d="M-5.68434189e-14,2.84217094e-14 L39.1816085,2.84217094e-14 L69.3453773,32.2519224 L101.428699,2.84217094e-14 L138.784583,2.84217094e-14 L138.784199,29.8015838 C137.958931,37.3510206 135.784352,42.5567762 132.260463,45.4188507 C128.736573,48.2809251 112.33867,64.5239941 83.0667527,94.1480575 L56.2750821,94.1480575 L6.71554594,44.4188507 C2.46876683,39.9813776 0.345377275,35.1089553 0.345377275,29.8015838 C0.345377275,24.4942122 0.230251516,14.560351 -5.68434189e-14,2.84217094e-14 Z" style="fill:currentColor"></path>
                                            <path id="Path1" d="M69.3453773,32.2519224 L101.428699,1.42108547e-14 L138.784583,1.42108547e-14 L138.784199,29.8015838 C137.958931,37.3510206 135.784352,42.5567762 132.260463,45.4188507 C128.736573,48.2809251 112.33867,64.5239941 83.0667527,94.1480575 L56.2750821,94.1480575 L32.8435758,70.5039241 L69.3453773,32.2519224 Z" fill="url(#linearGradient-1)" opacity="0.2"></path>
                                            <polygon id="Path-2" fill="#000000" opacity="0.049999997" points="69.3922914 32.4202615 32.8435758 70.5039241 54.0490008 16.1851325"></polygon>
                                            <polygon id="Path-21" fill="#000000" opacity="0.099999994" points="69.3922914 32.4202615 32.8435758 70.5039241 58.3683556 20.7402338"></polygon>
                                            <polygon id="Path-3" fill="url(#linearGradient-2)" opacity="0.099999994" points="101.428699 0 83.0667527 94.1480575 130.378721 47.0740288"></polygon>
                                        </g>
                                    </g>
                                </g>
                            </svg></span>
                        <h2 class="brand-text">FARS</h2>
                    </a></li>
                <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i><i class="d-none d-xl-block collapse-toggle-icon font-medium-4  text-primary" data-feather="disc" data-ticon="disc"></i></a></li>
            </ul>
        </div>
        <div class="shadow-bottom"></div>
        <div class="main-menu-content">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                {{-- <li class=" nav-item"><a class="d-flex align-items-center" href="index.html"><i data-feather="home"></i><span class="menu-title text-truncate" data-i18n="Dashboards">Dashboards</span><span class="badge badge-light-warning badge-pill ml-auto mr-1">2</span></a>
                    <ul class="menu-content">
                        <li><a class="d-flex align-items-center" href="dashboard-analytics.html"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Analytics">Analytics</span></a>
                        </li>
                        <li class="active"><a class="d-flex align-items-center" href="dashboard-ecommerce.html"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="eCommerce">eCommerce</span></a>
                        </li>
                    </ul>
                </li> --}}
                {{-- <li class=" navigation-header"><span data-i18n="Apps &amp; Pages">Apps &amp; Pages</span><i data-feather="more-horizontal"></i>
                </li> --}}
                <li class="{{ Request::segment(2) == 'home' ? ' nav-item active' :  'nav-item' }}"><a class="d-flex align-items-center" href="{{route('home.admin')}}"><i class="fa fa-home"></i><span class="menu-title text-truncate" data-i18n="Home">Dashboard</span></a>
                </li>
                <li class="{{ Request::segment(2) == 'users' ? ' nav-item active' :  'nav-item' }}"><a class="d-flex align-items-center" href="{{route('admin.users.index')}}"><i class="fa fa-users"></i><span class="menu-title text-truncate" data-i18n="Users"> Users Management</span></a>
                </li>
                <li class="{{ Request::segment(2) == 'self' ? ' nav-item active' :  'nav-item' }}"><a class="d-flex align-items-center" href="{{route('admin.users.self.index')}}"><i class="fa fa-users"></i><span class="menu-title text-truncate" data-i18n="Users">Self Registered Users</span></a>
                </li>

                <li class="{{ Request::segment(2) == 'training' ? ' nav-item active' :  'nav-item' }}"><a class="d-flex align-items-center" href="{{route('admin.training.index')}}"><i class="fa fa-bars"></i><span class="menu-title text-truncate" data-i18n="Training">Training</span></a>
                </li>
                <li class="{{ Request::segment(2) == 'staff' ? ' nav-item active' :  'nav-item' }}"><a class="d-flex align-items-center" href="{{route('admin.staff.index')}}"><i class="fas fa-th"></i><span class="menu-title text-truncate" data-i18n="Staff">Staff Managment</span></a>
                </li>
                <li class="{{ Request::segment(2) == 'model' ? ' nav-item active' :  'nav-item' }}"><a class="d-flex align-items-center" href="{{route('admin.model.index')}}"><i class="fas fa-hotel"></i><span class="menu-title text-truncate" data-i18n="Model">Model Management</span></a>
                </li>
                <li class="{{ Request::segment(2) == 'api' ? ' nav-item active' :  'nav-item' }}"><a class="d-flex align-items-center" href="{{route('admin.api.index')}}"><i class="fas fa-globe"></i><span class="menu-title text-truncate" data-i18n="API">API Management</span></a>
                </li>
                <li class="{{ Request::segment(2) == 'billing' ? ' nav-item active' :  'nav-item' }}"><a class="d-flex align-items-center" href="#"><i class="fa fa-credit-card"></i><span class="menu-title text-truncate" data-i18n="Menu Levels">Billing & Invoices</span></a>
                    <ul class="menu-content">
                        <li class="{{ Request::segment(3) == 'subscription' ? 'active' : null }}"><a class="d-flex align-items-center" href="{{route('admin.billing.subscription.index')}}"><i class="far fa-circle nav-icon"></i><span class="menu-item text-truncate" data-i18n="Payment">Subscriptions</span></a>
                        </li>
                        <li class="{{ Request::segment(3) == 'payment' ? 'active' : null }}"><a class="d-flex align-items-center" href="{{ route('admin.billing.payment.index') }}"><i class="far fa-circle nav-icon"></i><span class="menu-item text-truncate" data-i18n="Email">Payment Method</span></a>
                        </li> 
                        <li class="{{ Request::segment(3) == 'invoice' ? 'active' : null }}"><a class="d-flex align-items-center" href="{{ route('admin.billing.invoice.index') }}"><i class="far fa-circle nav-icon"></i><span class="menu-item text-truncate" data-i18n="SMS">Invoices</span></a>
                        </li>
                    </ul>
                </li>
                <li class="{{ Request::segment(2) == 'setting' ? ' nav-item active' :  'nav-item' }}"><a class="d-flex align-items-center" href="#"><i class="fas fa-cog"></i><span class="menu-title text-truncate" data-i18n="Menu Levels">Settings</span></a>
                    <ul class="menu-content">
                        <li class="{{ Request::segment(3) == 'secret' ? 'active' : null }}"><a class="d-flex align-items-center" href="#"><i class="far fa-circle nav-icon"></i><span class="menu-item text-truncate" data-i18n="Payment">Server/Identity Management</span></a>
                        </li>
                        <li class="{{ Request::segment(3) == 'email' ? 'active' : null }}"><a class="d-flex align-items-center" href="#"><i class="far fa-circle nav-icon"></i><span class="menu-item text-truncate" data-i18n="Email">Attendance Management</span></a>
                        </li>
                        <li class="{{ Request::segment(3) == 'sms' ? 'active' : null }}"><a class="d-flex align-items-center" href="#"><i class="far fa-circle nav-icon"></i><span class="menu-item text-truncate" data-i18n="SMS">Hardware Management</span></a>
                        </li>
                        <li class="{{ Request::segment(3) == 'smscdc' ? 'active' : null }}"><a class="d-flex align-items-center" href="#"><i class="far fa-circle nav-icon"></i><span class="menu-item text-truncate" data-i18n="Third Party">Touch Point Management</span></a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <!-- END: Main Menu-->