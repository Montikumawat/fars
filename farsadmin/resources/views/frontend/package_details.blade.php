@extends('frontend.layout.master')

@section('css')
<style>
  .razorpay-payment-button{
    display: none;
  }
</style>
<style type="text/css">
  .panel-title {
  display: inline;
  font-weight: bold;
  }
  .display-table {
      display: table;
  }
  .display-tr {
      display: table-row;
  }
  .display-td {
      display: table-cell;
      vertical-align: middle;
      width: 61%;
  }
  .hide {
    display: none;
  }

</style>
<style>
    label {
    font-size: 16px;
  }
  select {
    font-size: 16px;
  }
  input {
    font-size: 16px;
  }
  .form-control {
    font-size: 2rem;
  }
  button {
    font-size: 2rem;
  }
</style>
@endsection

@section('content')

<main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section class="breadcrumbs">
      <div class="container">

        <ol>
          <li><a href="index.html">Home</a></li>
          <li>Package Details</li>
        </ol>
        <h2>Package Details</h2>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Portfolio Details Section ======= -->
    @php
    $image = config('helper.image_url').$package['image']
    @endphp
    <section id="portfolio-details" class="portfolio-details">
      <div class="container">

        <div class="row gy-4">

          <div class="col-lg-8">
            <div class="portfolio-details-slider swiper-container">
              <div class="swiper-wrapper align-items-center">

                <div class="swiper-slide">
                  {{-- <img src="{{asset('assets/img/portfolio/portfolio-3.jpg')}}" alt=""> --}}
                  @if(isset($package['image']))
                    <img src="{{config('helper.image_url').$package['image']}}" class="" alt="" >
                  @else
                  <img src="{{ asset('default/no_image_available.png') }}" class="" alt="" >  
                  @endif
                </div>
{{-- 
                <div class="swiper-slide">
                  <img src="assets/img/portfolio/portfolio-2.jpg" alt="">
                </div>

                <div class="swiper-slide">
                  <img src="assets/img/portfolio/portfolio-3.jpg" alt="">
                </div> --}}

              </div>
              <div class="swiper-pagination"></div>
            </div>
          </div>

          <div class="col-lg-4">
            <div class="portfolio-info">
              <h3>Package information</h3>
              <ul>
                <li><strong>Name</strong>: {{ $package['name'] ?? '' }}</li>
                <li><strong>Package Amount</strong>: 
                  @if(isset($package['currency']))
                    @if($package['currency'] == 'DOLLAR')
                    $
                    @endif
                    @if($package['currency'] == 'INR')
                    ₹
                    @endif
                    {{ $package->amount ?? '' }}
                    @if($package['package_type'] == '0')
                        / month
                    @else
                        / year
                    @endif
                  @endif
                </li>
                <li><strong>Package Type</strong>: @if($package['package_type'] == '0')
                    Corporate
                    @else
                    Retail
                    @endif
                </li>
                <li><strong>Package Time</strong>: {{ $package['package_time'] ?? '' }} Months</li>
                <li><strong>
                    </strong>: 01 March, 2020</li>
              </ul>
            </div>
            <div class="portfolio-description">
              <h2>Package Description</h2>
              {!! $package['description'] ?? '' !!}
            </div>
          </div>

          <div class="col-lg-8">
            <div class="portfolio-info">
              <h3>Package Details</h3>
              <ul>
                <li><strong>Total Users for Facial Recognition</strong>: {{ $package['users_for_facial_recognition'] ?? '' }} <span style="float: right;"><strong> Total Checkins</strong>: {{ $package->check_ins ?? '' }}</span></li>
                <li><strong>Total Touch Points</strong>: {{ $package['admin_touch_point'] + $package['edge_touch_point'] + $package['front_touch_point'] }} <span style="float: right;"><strong>Total Number of Backend Integrations</strong>: {{ $package['backend_integration'] ?? ''}}</span></li>
                <li><strong>Total Number of Sites</strong>: {{ $package['sites'] ?? '' }} <span style="float: right;"><strong> Total Checkins</strong>: {{ $package['rate_limit'] ?? '' }}</span></li>
                <li><strong>Total Number of Admin Users</strong>: {{ $package['admin_users'] ?? '' }}</li>
              </ul>
              {{-- <div class="col-lg-6">

                  <input type="submit" class="btn btn-primary" value="Pay Now" style="float: right;">
                   
              </div> --}}
            </div>
            {{-- <div class="portfolio-description">
              <h2>This is an example of portfolio detail</h2>
              <p>
                Autem ipsum nam porro corporis rerum. Quis eos dolorem eos itaque inventore commodi labore quia quia. Exercitationem repudiandae officiis neque suscipit non officia eaque itaque enim. Voluptatem officia accusantium nesciunt est omnis tempora consectetur dignissimos. Sequi nulla at esse enim cum deserunt eius.
              </p>
            </div> --}}
          </div>

          <div class="col-lg-8">
            <div class="portfolio-info">
              <h3>Select Payment Method</h3>
                  <label for="" style="margin-bottom: 5px; ">Select Payment Method</label>
                  <select name="payment_method" id="payment_method" class="form-control">
                      <option value="">Select Payment Method</option>
                      <option value="0">Razorpay</option>
                      <option value="1">Stripe</option>
                  </select> 
              <form action="{{ route('razorpay.payment.store') }}" method="POST" style="display: none; margin-top: 26px;" id="razorpayPaymentForm">
                @csrf
                <input type="text" name="package_id" value="{{ $package['id'] ?? '' }}" hidden>
                <input type="text" name="amount" value="{{ $package['amount'] ?? '' }}" hidden>
                <input type="text" name="currency" value="{{ $package['currency'] ?? '' }}" hidden>
                <input type="text" name="package_time" value="{{ $package['package_time'] ?? '' }}" hidden>
                <script src="https://checkout.razorpay.com/v1/checkout.js"
                        data-key="{{ config('payment.razorpay_credentials_key') }}"
                        data-amount="{{ $package['amount']*100 ?? '' }}"
                        data-name="FARS"
                        data-description="{{$package['name'] ?? 'N/A'}}"
                        data-currency=@if($package['currency'] == 'DOLLAR')
                        USD
                        @endif
                        @if($package['currency'] == 'INR')
                        INR
                        @endif
                        data-prefill.name="name"
                        data-prefill.email="email"
                        data-theme.color="#ff7529">
                </script>
                <button class="btn btn-primary btn-block"> Pay @if($package['currency'] == 'DOLLAR')
                  USD
                  @endif
                  @if($package['currency'] == 'INR')
                  INR
                  @endif {{ $package['amount'] ?? '' }}</button>
              </form>

              <form role="form" action="{{ route('stripe.payment.store') }}" method="post" class="row require-validation"
                                                     data-cc-on-file="false"
                                                    data-stripe-publishable-key="{{ config('payment.stripe_credentials_key') }}"
                                                    id="payment-form" style="display: none;">
                        @csrf
                        <input type="text" name="package_id" value="{{ $package['id'] ?? '' }}" hidden>
                        <input type="text" name="amount" value="{{ $package['amount'] ?? '' }}" hidden>
                        <input type="text" name="currency" value="{{ $package['currency'] ?? '' }}" hidden>
                        <input type="text" name="package_time" value="{{ $package['package_time'] ?? '' }}" hidden>
                        <div class='form-group row'>
                            <div class='col-xs-12 form-group required'>
                                <label class='control-label'>Name on Card</label> 
                                <input
                                    class='form-control' size='4' type='text'>
                            </div>
                        </div>

                        <div class='form-group row'>
                          <div class='col-xs-12 form-group required'>
                              <label class='control-label'>Card Number</label> 
                              <input
                                    autocomplete='off' class='form-control card-number' size='20'
                                    type='text'>
                          </div>
                      </div>
  
                        {{-- <div class='form-group row'>
                            <div class='col-xs-12 form-group card required'>
                                <label class='control-label'>Card Number</label> <input
                                    autocomplete='off' class='form-control card-number' size='20'
                                    type='text'>
                            </div>
                        </div> --}}
  
                        <div class='form-row row'>
                            <div class='col-xs-12 col-md-4 form-group cvc required'>
                                <label class='control-label'>CVC</label> <input autocomplete='off'
                                    class='form-control card-cvc' placeholder='ex. 311' size='4'
                                    type='text'>
                            </div>
                            <div class='col-xs-12 col-md-4 form-group expiration required'>
                                <label class='control-label'>Expiration Month</label> <input
                                    class='form-control card-expiry-month' placeholder='MM' size='2'
                                    type='text'>
                            </div>
                            <div class='col-xs-12 col-md-4 form-group expiration required'>
                                <label class='control-label'>Expiration Year</label> <input
                                    class='form-control card-expiry-year' placeholder='YYYY' size='4'
                                    type='text'>
                            </div>
                        </div>
  
                        <div class='form-row row'>
                            <div class='col-md-12 error form-group hide'>
                                <div class='alert-danger alert'>Please correct the errors and try
                                    again.</div>
                            </div>
                        </div>
  
                        <div class="row">
                            <div class="col-xs-12">
                                <button class="btn btn-primary btn-lg btn-block" type="submit" style="    float: right;
                                margin-top: 20px;">Pay Now (@if($package['currency'] == 'DOLLAR')
                                                               $
                                                            @endif
                                                            @if($package['currency'] == 'INR')
                                                                ₹
                                                            @endif
                                                            {{ $package['amount'] ?? '' }})</button>
                            </div>
                        </div>
                          
              </form>
                {{-- <div class="col-lg-6" style="margin-top: 5px;">
                    <input type="submit" class="btn btn-primary" value="Pay Now" style="float: right;">                
                </div> --}}
            </div>
            {{-- <div class="portfolio-description">
              <h2>This is an example of portfolio detail</h2>
              <p>
                Autem ipsum nam porro corporis rerum. Quis eos dolorem eos itaque inventore commodi labore quia quia. Exercitationem repudiandae officiis neque suscipit non officia eaque itaque enim. Voluptatem officia accusantium nesciunt est omnis tempora consectetur dignissimos. Sequi nulla at esse enim cum deserunt eius.
              </p>
            </div> --}}
          </div>

        </div>

      </div>
    </section><!-- End Portfolio Details Section -->

  </main><!-- End #main -->
@endsection

@section('js')
<script>
  $(document).ready(function() {
      $('#payment_method').on('change', function() {
          console.log(this.value);
          var html = '';
          html = ``
          if (this.value == '0')
          //.....................^.......
          {
              $("#razorpayPaymentForm").css('display', 'block')
              $("#payment-form").css('display', 'none')
          } else {
              // $("#divUsers").html('<label for="inputUsers">Number of Users for Facial Recognition</label><select id="inputUsers"  name="users" class="form-control custom-select"><option selected disabled>Select Number of Users</option><option value="10">10k</option><option value="100">100K</option><option value="custom">Custom</option></select>');
              
              $("#payment-form").css('display', 'block')
              $("#razorpayPaymentForm").css('display', 'none')
          }
      });

  
  
  });
</script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
  
<script type="text/javascript">
  $(function() {
      var $form         = $(".require-validation");
    $('form.require-validation').bind('submit', function(e) {
      var $form         = $(".require-validation"),
          inputSelector = ['input[type=email]', 'input[type=password]',
                           'input[type=text]', 'input[type=file]',
                           'textarea'].join(', '),
          $inputs       = $form.find('.required').find(inputSelector),
          $errorMessage = $form.find('div.error'),
          valid         = true;
          $errorMessage.addClass('hide');
   
          $('.has-error').removeClass('has-error');
      $inputs.each(function(i, el) {
        var $input = $(el);
        if ($input.val() === '') {
          $input.parent().addClass('has-error');
          $errorMessage.removeClass('hide');
          e.preventDefault();
        }
      });
    
      if (!$form.data('cc-on-file')) {
        e.preventDefault();
        Stripe.setPublishableKey($form.data('stripe-publishable-key'));
        Stripe.createToken({
          number: $('.card-number').val(),
          cvc: $('.card-cvc').val(),
          exp_month: $('.card-expiry-month').val(),
          exp_year: $('.card-expiry-year').val()
        }, stripeResponseHandler);
      }
    
    });
    
    function stripeResponseHandler(status, response) {
          if (response.error) {
              $('.error')
                  .removeClass('hide')
                  .find('.alert')
                  .text(response.error.message);
          } else {
              // token contains id, last4, and card type
              var token = response['id'];
              // insert the token into the form so it gets submitted to the server
              $form.find('input[type=text]').empty();
              var html = `<input type='hidden' name='stripeToken' value='` + token + `'/>`;
              $form.append(html);
              $form.get(0).submit();
          }
      }
    
  });
  </script>
@endsection