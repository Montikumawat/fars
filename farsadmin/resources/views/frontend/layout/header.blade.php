<header id="header" class="header fixed-top">
    <div class="container-fluid container-xl d-flex align-items-center justify-content-between pt-2 pb-3"
        style="max-width: unset;">

        <a href="#" class="logo d-flex align-items-center">
        <img src="assets/img/logo.png" alt="">
        <span>FARS</span>
        </a>
        <?php
        if(\Request::route()->getName() == 'front'){
            $url = '#hero';
        } else {
            $url =  route('front') ;
        }
        ?>
        <nav id="navbar" class="navbar">
            <ul>
                <li><a class="nav-link scrollto active" href="{{ $url }}">Home</a></li>
                <li><a class="nav-link scrollto" href="#about">About</a></li>
                <li><a class="nav-link scrollto" href="#testimonials">Testimonials</a></li>
                <li><a class="nav-link scrollto" href="#pricing">Pricing</a></li>
                <li><a class="nav-link scrollto" href="#recent-blog-posts">Blog</a></li>
                <li><a class="nav-link scrollto" href="#contact">Demo Request Form</a></li>
                <li class="dropdown"><a href="#"><span>Pages</span> <i class="bi bi-chevron-down"></i></a>
                    <ul>

                        <li><a class="nav-link scrollto" href="#">Api Document</a></li>
                        <li><a class="nav-link scrollto" href="#">Terms & Condition</a></li>
                        <li><a class="nav-link scrollto" href="#">Privacy Policy</a></li>
                        <li><a class="nav-link scrollto" href="#">Refund Policy</a></li>
                    </ul>
                </li>
                @if (Route::has('login'))
                    @if(request()->session()->get('authenticated') == 'true' )
                     
                            <li><a class="getstarted scrollto" href="{{ route('home.admin') }}"
                                    style="background: green;">My Account</a></li>
                    
  
                    @else
                        <li><a class="getstarted scrollto" href="{{ route('login') }}"
                                style="background: green;">Sign In</a></li>
                    @endif
                @endif
                @if (Route::has('register'))
                    @if(request()->session()->get('authenticated') == 'true' )
                        <li><a class="getstarted scrollto" href="{{ route('logout') }}"
                                onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
                        </li>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    @else
                        <li><a class="getstarted scrollto" href="{{ route('register') }}">Get Started</a></li>
                    @endif
                @endif
            </ul>
            <i class="bi bi-list mobile-nav-toggle"></i>
        </nav>
       
        <!-- .navbar -->

    </div>

    {{-- <div class="container">
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <strong>Holy guacamole!</strong> You should check in on some of those fields below.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="float: right;">
              <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div> --}}
    @include('frontend.includes.alert')
</header>