@extends('layout.master')
@section('title','Dashboard')
@section('description','Admin Dashboard of FARS')
@section('keywords','admin panel, fars')
@section('author','FARS')

@section('css')
<link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
 <style>
   #example_filter {
       float: right;
   }
   #example_paginate {
       float: right;
   }

   #example2_filter {
       float: right;
   }
   #example2_paginate {
       float: right;
   }
       /* width */
::-webkit-scrollbar {
width: 10px;
}

/* Track */
::-webkit-scrollbar-track {
background: #f1f1f1;

}

/* Handle */
::-webkit-scrollbar-thumb {
background: #958cf4;
border-radius: 10px;
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
background: #958cf4;
}
 </style>
@endsection

@section('content')

<div class="row">
    <div class="col-lg-4 col-sm-6 col-12">
        <div class="card">
            <div class="card-header align-items-start pb-0">
                <div>
                    <h2 class="font-weight-bolder">659.8k</h2>
                    <p class="card-text">Total Users</p>
                </div>
                <div class="avatar bg-light-success p-50">
                    <div class="avatar-content">
                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user-check font-medium-5"><path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="8.5" cy="7" r="4"></circle><polyline points="17 11 19 13 23 9"></polyline></svg>
                    </div>
                </div>
            </div>
            <div id="line-area-chart-6" style="min-height: 100px;"><div id="apexchartsrzqiluwc" class="apexcharts-canvas apexchartsrzqiluwc apexcharts-theme-light" style="width: 326px; height: 100px;"><svg id="SvgjsSvg1735" width="326" height="100" xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" class="apexcharts-svg" xmlns:data="ApexChartsNS" transform="translate(0, 0)" style="background: transparent;"><g id="SvgjsG1737" class="apexcharts-inner apexcharts-graphical" transform="translate(0, 0)"><defs id="SvgjsDefs1736"><clipPath id="gridRectMaskrzqiluwc"><rect id="SvgjsRect1742" width="335" height="105" x="-4.5" y="-2.5" rx="0" ry="0" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0" fill="#fff"></rect></clipPath><clipPath id="gridRectMarkerMaskrzqiluwc"><rect id="SvgjsRect1743" width="330" height="104" x="-2" y="-2" rx="0" ry="0" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0" fill="#fff"></rect></clipPath><linearGradient id="SvgjsLinearGradient1748" x1="0" y1="1" x2="1" y2="1"><stop id="SvgjsStop1749" stop-opacity="1" stop-color="rgba(85,221,146,1)" offset="0"></stop><stop id="SvgjsStop1750" stop-opacity="1" stop-color="rgba(40,199,111,1)" offset="1"></stop><stop id="SvgjsStop1751" stop-opacity="1" stop-color="rgba(40,199,111,1)" offset="1"></stop><stop id="SvgjsStop1752" stop-opacity="1" stop-color="rgba(85,221,146,1)" offset="1"></stop></linearGradient><filter id="SvgjsFilter1754" filterUnits="userSpaceOnUse" width="200%" height="200%" x="-50%" y="-50%"><feFlood id="SvgjsFeFlood1755" flood-color="#000000" flood-opacity="0.1" result="SvgjsFeFlood1755Out" in="SourceGraphic"></feFlood><feComposite id="SvgjsFeComposite1756" in="SvgjsFeFlood1755Out" in2="SourceAlpha" operator="in" result="SvgjsFeComposite1756Out"></feComposite><feOffset id="SvgjsFeOffset1757" dx="0" dy="5" result="SvgjsFeOffset1757Out" in="SvgjsFeComposite1756Out"></feOffset><feGaussianBlur id="SvgjsFeGaussianBlur1758" stdDeviation="4 " result="SvgjsFeGaussianBlur1758Out" in="SvgjsFeOffset1757Out"></feGaussianBlur><feMerge id="SvgjsFeMerge1759" result="SvgjsFeMerge1759Out" in="SourceGraphic"><feMergeNode id="SvgjsFeMergeNode1760" in="SvgjsFeGaussianBlur1758Out"></feMergeNode><feMergeNode id="SvgjsFeMergeNode1761" in="[object Arguments]"></feMergeNode></feMerge><feBlend id="SvgjsFeBlend1762" in="SourceGraphic" in2="SvgjsFeMerge1759Out" mode="normal" result="SvgjsFeBlend1762Out"></feBlend></filter></defs><line id="SvgjsLine1741" x1="0" y1="0" x2="0" y2="100" stroke="#b6b6b6" stroke-dasharray="3" class="apexcharts-xcrosshairs" x="0" y="0" width="1" height="100" fill="#b1b9c4" filter="none" fill-opacity="0.9" stroke-width="1"></line><g id="SvgjsG1763" class="apexcharts-xaxis" transform="translate(0, 0)"><g id="SvgjsG1764" class="apexcharts-xaxis-texts-g" transform="translate(0, -4)"></g></g><g id="SvgjsG1766" class="apexcharts-grid"><g id="SvgjsG1767" class="apexcharts-gridlines-horizontal" style="display: none;"><line id="SvgjsLine1769" x1="0" y1="0" x2="326" y2="0" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line><line id="SvgjsLine1770" x1="0" y1="16.666666666666668" x2="326" y2="16.666666666666668" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line><line id="SvgjsLine1771" x1="0" y1="33.333333333333336" x2="326" y2="33.333333333333336" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line><line id="SvgjsLine1772" x1="0" y1="50" x2="326" y2="50" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line><line id="SvgjsLine1773" x1="0" y1="66.66666666666667" x2="326" y2="66.66666666666667" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line><line id="SvgjsLine1774" x1="0" y1="83.33333333333334" x2="326" y2="83.33333333333334" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line><line id="SvgjsLine1775" x1="0" y1="100.00000000000001" x2="326" y2="100.00000000000001" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line></g><g id="SvgjsG1768" class="apexcharts-gridlines-vertical" style="display: none;"></g><line id="SvgjsLine1777" x1="0" y1="100" x2="326" y2="100" stroke="transparent" stroke-dasharray="0"></line><line id="SvgjsLine1776" x1="0" y1="1" x2="0" y2="100" stroke="transparent" stroke-dasharray="0"></line></g><g id="SvgjsG1744" class="apexcharts-line-series apexcharts-plot-series"><g id="SvgjsG1745" class="apexcharts-series" seriesName="ActivexUsers" data:longestSeries="true" rel="1" data:realIndex="0"><path id="SvgjsPath1753" d="M 0 91.66666666666669C 19.016666666666662 91.66666666666669 35.31666666666666 50.00000000000003 54.33333333333333 50.00000000000003C 73.35 50.00000000000003 89.64999999999999 66.66666666666669 108.66666666666666 66.66666666666669C 127.68333333333332 66.66666666666669 143.98333333333332 8.333333333333343 163 8.333333333333343C 182.01666666666665 8.333333333333343 198.31666666666666 50.00000000000003 217.33333333333331 50.00000000000003C 236.34999999999997 50.00000000000003 252.64999999999998 16.666666666666686 271.66666666666663 16.666666666666686C 290.6833333333333 16.666666666666686 306.98333333333335 33.33333333333334 326 33.33333333333334" fill="none" fill-opacity="1" stroke="url(#SvgjsLinearGradient1748)" stroke-opacity="1" stroke-linecap="butt" stroke-width="5" stroke-dasharray="0" class="apexcharts-line" index="0" clip-path="url(#gridRectMaskrzqiluwc)" filter="url(#SvgjsFilter1754)" pathTo="M 0 91.66666666666669C 19.016666666666662 91.66666666666669 35.31666666666666 50.00000000000003 54.33333333333333 50.00000000000003C 73.35 50.00000000000003 89.64999999999999 66.66666666666669 108.66666666666666 66.66666666666669C 127.68333333333332 66.66666666666669 143.98333333333332 8.333333333333343 163 8.333333333333343C 182.01666666666665 8.333333333333343 198.31666666666666 50.00000000000003 217.33333333333331 50.00000000000003C 236.34999999999997 50.00000000000003 252.64999999999998 16.666666666666686 271.66666666666663 16.666666666666686C 290.6833333333333 16.666666666666686 306.98333333333335 33.33333333333334 326 33.33333333333334" pathFrom="M -1 216.66666666666669L -1 216.66666666666669L 54.33333333333333 216.66666666666669L 108.66666666666666 216.66666666666669L 163 216.66666666666669L 217.33333333333331 216.66666666666669L 271.66666666666663 216.66666666666669L 326 216.66666666666669"></path><g id="SvgjsG1746" class="apexcharts-series-markers-wrap" data:realIndex="0"><g class="apexcharts-series-markers"><circle id="SvgjsCircle1783" r="0" cx="0" cy="0" class="apexcharts-marker wl6l12pem no-pointer-events" stroke="#ffffff" fill="#28c76f" fill-opacity="1" stroke-width="2" stroke-opacity="0.9" default-marker-size="0"></circle></g></g></g><g id="SvgjsG1747" class="apexcharts-datalabels" data:realIndex="0"></g></g><line id="SvgjsLine1778" x1="0" y1="0" x2="326" y2="0" stroke="#b6b6b6" stroke-dasharray="0" stroke-width="1" class="apexcharts-ycrosshairs"></line><line id="SvgjsLine1779" x1="0" y1="0" x2="326" y2="0" stroke-dasharray="0" stroke-width="0" class="apexcharts-ycrosshairs-hidden"></line><g id="SvgjsG1780" class="apexcharts-yaxis-annotations"></g><g id="SvgjsG1781" class="apexcharts-xaxis-annotations"></g><g id="SvgjsG1782" class="apexcharts-point-annotations"></g></g><rect id="SvgjsRect1740" width="0" height="0" x="0" y="0" rx="0" ry="0" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0" fill="#fefefe"></rect><g id="SvgjsG1765" class="apexcharts-yaxis" rel="0" transform="translate(-18, 0)"></g><g id="SvgjsG1738" class="apexcharts-annotations"></g></svg><div class="apexcharts-legend" style="max-height: 50px;"></div><div class="apexcharts-tooltip apexcharts-theme-light"><div class="apexcharts-tooltip-series-group" style="order: 1;"><span class="apexcharts-tooltip-marker" style="background-color: rgb(40, 199, 111);"></span><div class="apexcharts-tooltip-text" style="font-family: Helvetica, Arial, sans-serif; font-size: 12px;"><div class="apexcharts-tooltip-y-group"><span class="apexcharts-tooltip-text-label"></span><span class="apexcharts-tooltip-text-value"></span></div><div class="apexcharts-tooltip-z-group"><span class="apexcharts-tooltip-text-z-label"></span><span class="apexcharts-tooltip-text-z-value"></span></div></div></div></div><div class="apexcharts-yaxistooltip apexcharts-yaxistooltip-0 apexcharts-yaxistooltip-left apexcharts-theme-light"><div class="apexcharts-yaxistooltip-text"></div></div></div></div>
        <div class="resize-triggers"><div class="expand-trigger"><div style="width: 327px; height: 178px;"></div></div><div class="contract-trigger"></div></div></div>
    </div>

    <div class="col-lg-4 col-sm-6 col-12">
        <div class="card">
            <div class="card-header align-items-start pb-0">
                <div>
                    <h2 class="font-weight-bolder">78.9k</h2>
                    <p class="card-text">Total CheckIns</p>
                </div>
                <div class="avatar bg-light-primary p-50">
                    <div class="avatar-content">
                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-monitor font-medium-5"><rect x="2" y="3" width="20" height="14" rx="2" ry="2"></rect><line x1="8" y1="21" x2="16" y2="21"></line><line x1="12" y1="17" x2="12" y2="21"></line></svg>
                    </div>
                </div>
            </div>
            <div id="line-area-chart-5" style="min-height: 100px;"><div id="apexchartsd1w3avnv" class="apexcharts-canvas apexchartsd1w3avnv apexcharts-theme-light" style="width: 326px; height: 100px;"><svg id="SvgjsSvg1685" width="326" height="100" xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" class="apexcharts-svg" xmlns:data="ApexChartsNS" transform="translate(0, 0)" style="background: transparent;"><g id="SvgjsG1687" class="apexcharts-inner apexcharts-graphical" transform="translate(0, 0)"><defs id="SvgjsDefs1686"><clipPath id="gridRectMaskd1w3avnv"><rect id="SvgjsRect1692" width="335" height="105" x="-4.5" y="-2.5" rx="0" ry="0" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0" fill="#fff"></rect></clipPath><clipPath id="gridRectMarkerMaskd1w3avnv"><rect id="SvgjsRect1693" width="330" height="104" x="-2" y="-2" rx="0" ry="0" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0" fill="#fff"></rect></clipPath><linearGradient id="SvgjsLinearGradient1698" x1="0" y1="1" x2="1" y2="1"><stop id="SvgjsStop1699" stop-opacity="1" stop-color="rgba(169,162,246,1)" offset="0"></stop><stop id="SvgjsStop1700" stop-opacity="1" stop-color="rgba(115,103,240,1)" offset="1"></stop><stop id="SvgjsStop1701" stop-opacity="1" stop-color="rgba(115,103,240,1)" offset="1"></stop><stop id="SvgjsStop1702" stop-opacity="1" stop-color="rgba(169,162,246,1)" offset="1"></stop></linearGradient><filter id="SvgjsFilter1704" filterUnits="userSpaceOnUse" width="200%" height="200%" x="-50%" y="-50%"><feFlood id="SvgjsFeFlood1705" flood-color="#000000" flood-opacity="0.1" result="SvgjsFeFlood1705Out" in="SourceGraphic"></feFlood><feComposite id="SvgjsFeComposite1706" in="SvgjsFeFlood1705Out" in2="SourceAlpha" operator="in" result="SvgjsFeComposite1706Out"></feComposite><feOffset id="SvgjsFeOffset1707" dx="0" dy="5" result="SvgjsFeOffset1707Out" in="SvgjsFeComposite1706Out"></feOffset><feGaussianBlur id="SvgjsFeGaussianBlur1708" stdDeviation="4 " result="SvgjsFeGaussianBlur1708Out" in="SvgjsFeOffset1707Out"></feGaussianBlur><feMerge id="SvgjsFeMerge1709" result="SvgjsFeMerge1709Out" in="SourceGraphic"><feMergeNode id="SvgjsFeMergeNode1710" in="SvgjsFeGaussianBlur1708Out"></feMergeNode><feMergeNode id="SvgjsFeMergeNode1711" in="[object Arguments]"></feMergeNode></feMerge><feBlend id="SvgjsFeBlend1712" in="SourceGraphic" in2="SvgjsFeMerge1709Out" mode="normal" result="SvgjsFeBlend1712Out"></feBlend></filter></defs><line id="SvgjsLine1691" x1="0" y1="0" x2="0" y2="100" stroke="#b6b6b6" stroke-dasharray="3" class="apexcharts-xcrosshairs" x="0" y="0" width="1" height="100" fill="#b1b9c4" filter="none" fill-opacity="0.9" stroke-width="1"></line><g id="SvgjsG1713" class="apexcharts-xaxis" transform="translate(0, 0)"><g id="SvgjsG1714" class="apexcharts-xaxis-texts-g" transform="translate(0, -4)"></g></g><g id="SvgjsG1716" class="apexcharts-grid"><g id="SvgjsG1717" class="apexcharts-gridlines-horizontal" style="display: none;"><line id="SvgjsLine1719" x1="0" y1="0" x2="326" y2="0" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line><line id="SvgjsLine1720" x1="0" y1="16.666666666666668" x2="326" y2="16.666666666666668" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line><line id="SvgjsLine1721" x1="0" y1="33.333333333333336" x2="326" y2="33.333333333333336" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line><line id="SvgjsLine1722" x1="0" y1="50" x2="326" y2="50" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line><line id="SvgjsLine1723" x1="0" y1="66.66666666666667" x2="326" y2="66.66666666666667" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line><line id="SvgjsLine1724" x1="0" y1="83.33333333333334" x2="326" y2="83.33333333333334" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line><line id="SvgjsLine1725" x1="0" y1="100.00000000000001" x2="326" y2="100.00000000000001" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line></g><g id="SvgjsG1718" class="apexcharts-gridlines-vertical" style="display: none;"></g><line id="SvgjsLine1727" x1="0" y1="100" x2="326" y2="100" stroke="transparent" stroke-dasharray="0"></line><line id="SvgjsLine1726" x1="0" y1="1" x2="0" y2="100" stroke="transparent" stroke-dasharray="0"></line></g><g id="SvgjsG1694" class="apexcharts-line-series apexcharts-plot-series"><g id="SvgjsG1695" class="apexcharts-series" seriesName="TrafficxRate" data:longestSeries="true" rel="1" data:realIndex="0"><path id="SvgjsPath1703" d="M 0 66.66666666666667C 22.82 66.66666666666667 42.38 38.888888888888886 65.2 38.888888888888886C 88.02000000000001 38.888888888888886 107.58000000000001 80.55555555555556 130.4 80.55555555555556C 153.22 80.55555555555556 172.78 25 195.6 25C 218.42 25 237.98000000000002 38.888888888888886 260.8 38.888888888888886C 283.62 38.888888888888886 303.18 11.111111111111114 326 11.111111111111114" fill="none" fill-opacity="1" stroke="url(#SvgjsLinearGradient1698)" stroke-opacity="1" stroke-linecap="butt" stroke-width="5" stroke-dasharray="0" class="apexcharts-line" index="0" clip-path="url(#gridRectMaskd1w3avnv)" filter="url(#SvgjsFilter1704)" pathTo="M 0 66.66666666666667C 22.82 66.66666666666667 42.38 38.888888888888886 65.2 38.888888888888886C 88.02000000000001 38.888888888888886 107.58000000000001 80.55555555555556 130.4 80.55555555555556C 153.22 80.55555555555556 172.78 25 195.6 25C 218.42 25 237.98000000000002 38.888888888888886 260.8 38.888888888888886C 283.62 38.888888888888886 303.18 11.111111111111114 326 11.111111111111114" pathFrom="M -1 150L -1 150L 65.2 150L 130.4 150L 195.6 150L 260.8 150L 326 150"></path><g id="SvgjsG1696" class="apexcharts-series-markers-wrap" data:realIndex="0"><g class="apexcharts-series-markers"><circle id="SvgjsCircle1733" r="0" cx="0" cy="0" class="apexcharts-marker wh1mspsrzg no-pointer-events" stroke="#ffffff" fill="#7367f0" fill-opacity="1" stroke-width="2" stroke-opacity="0.9" default-marker-size="0"></circle></g></g></g><g id="SvgjsG1697" class="apexcharts-datalabels" data:realIndex="0"></g></g><line id="SvgjsLine1728" x1="0" y1="0" x2="326" y2="0" stroke="#b6b6b6" stroke-dasharray="0" stroke-width="1" class="apexcharts-ycrosshairs"></line><line id="SvgjsLine1729" x1="0" y1="0" x2="326" y2="0" stroke-dasharray="0" stroke-width="0" class="apexcharts-ycrosshairs-hidden"></line><g id="SvgjsG1730" class="apexcharts-yaxis-annotations"></g><g id="SvgjsG1731" class="apexcharts-xaxis-annotations"></g><g id="SvgjsG1732" class="apexcharts-point-annotations"></g></g><rect id="SvgjsRect1690" width="0" height="0" x="0" y="0" rx="0" ry="0" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0" fill="#fefefe"></rect><g id="SvgjsG1715" class="apexcharts-yaxis" rel="0" transform="translate(-18, 0)"></g><g id="SvgjsG1688" class="apexcharts-annotations"></g></svg><div class="apexcharts-legend" style="max-height: 50px;"></div><div class="apexcharts-tooltip apexcharts-theme-light"><div class="apexcharts-tooltip-series-group" style="order: 1;"><span class="apexcharts-tooltip-marker" style="background-color: rgb(115, 103, 240);"></span><div class="apexcharts-tooltip-text" style="font-family: Helvetica, Arial, sans-serif; font-size: 12px;"><div class="apexcharts-tooltip-y-group"><span class="apexcharts-tooltip-text-label"></span><span class="apexcharts-tooltip-text-value"></span></div><div class="apexcharts-tooltip-z-group"><span class="apexcharts-tooltip-text-z-label"></span><span class="apexcharts-tooltip-text-z-value"></span></div></div></div></div><div class="apexcharts-yaxistooltip apexcharts-yaxistooltip-0 apexcharts-yaxistooltip-left apexcharts-theme-light"><div class="apexcharts-yaxistooltip-text"></div></div></div></div>
        <div class="resize-triggers"><div class="expand-trigger"><div style="width: 327px; height: 178px;"></div></div><div class="contract-trigger"></div></div></div>
    </div>

</div>   

</div>


    <!-- Basic Tables start -->
    <div class="row" id="basic-table">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">API Details</h4>
                    {{-- <a href="{{route('admin.users.add')}}" class="btn btn-secondary" style="float: right;">Add Users</a> --}}
                </div>
               
                       <div class="table-responsive container-fluid">
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>API Name</th>
                                    <th>API Key</th>
                                    <th>Creation Date</th>
                                    
    
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    
                                    <td>API Key 1</td>
                                    <td>AIzaSyA4b2okiLGOiv_zAVL8e1wYszoeiPXwF6c</td>
                                    <td>21/07/2021</td>
                                </tr>
                                    
                                <tr>
                                    <td>API Key 1</td>
                                    <td>AIzaSyA4b2okiLGOiv_zAVL8e1wYszoeiPXwF6c</td>
                                    <td>21/07/2021</td>                                  
                                  </tr>                                            
                            </tbody>
                        </table>
                       </div>    

                </div>
            </div>
        </div>
    </div>
    <!-- Basic Tables end -->



  <!-- Basic Tables start -->
  <div class="row" id="basic-table">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Subscription Details</h4>
                {{-- <a href="{{route('admin.users.add')}}" class="btn btn-secondary" style="float: right;">Add Users</a> --}}
            </div>
           
                   <div class="table-responsive container-fluid">
                    <table id="example2" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th> Name</th>
                                <th>Purchased Date</th>
                                <th>End Date</th>
                                <th>Quota Used</th>
                                <th>Subscription Quota</th>
                                

                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                
                                <td>User 1</td>
                                <td>21/07/2021</td>
                                <td>30/07/2021</td>
                                <td>80000</td>
                                <td>10000</td>
                            </tr>
                                
                            <tr>
                                <td>User 2</td>
                                <td>21/07/2021</td>                                  
                                <td>30/07/2021</td>
                                <td>5000</td>
                                <td>8000</td>
                              </tr>                                            
                        </tbody>
                    </table>
                   </div>    

            </div>
        </div>
    </div>
</div>
<!-- Basic Tables end -->
@endsection


@section('script')
    <!-- BEGIN: Page JS-->
    <script src="{{ asset('app-assets/js/scripts/pages/dashboard-ecommerce.js') }}"></script>
    <!-- END: Page JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/responsive.bootstrap4.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/vfs_fonts.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.rowGroup.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
        <!-- END: Page Vendor JS-->
        <script>


       function EuToUsCurrencyFormat(input) {
	return input.replace(/[,.]/g, function(x) {
		return x == "," ? "." : ",";
	});
}

$(document).ready(function() {
	//Only needed for the filename of export files.
	//Normally set in the title tag of your page.
	// document.title = 'DataTable Excel';
	// DataTable initialisation
	var table = $('#example').DataTable({
		// "dom": '<"dt-buttons"Bf><"clear">lirtp',
		// "paging": true,
		// "autoWidth": true,
		// "buttons": [{
		// 	extend: 'excelHtml5',
		// 	text: 'Excel',
		// 	customize: function(xlsx) {
		// 		var sheet = xlsx.xl.worksheets['sheet1.xml'];
		// 		//All cells
		// 		$('row c', sheet).attr('s', '25');
		// 		//Second column
		// 		$('row c:nth-child(2)', sheet).attr('s', '42');
		// 		//First row
		// 		$('row:first c', sheet).attr('s', '36');
		// 		// One cell
		// 		$('row c[r^="D6"]', sheet).attr('s', '32');
		// 		// Loop over the cells in column `E` the amount column
		// 		$('row c[r^="E"]', sheet).each(function() {
		// 			if (parseFloat(EuToUsCurrencyFormat($('is t', this).text())) > 1500) {
		// 				$(this).attr('s', '17');
		// 			}
		// 		});
		// 		//All cells of row 10
		// 		$('row c[r*="10"]', sheet).attr('s', '49');
		// 		//Search all cells for a specific text
		// 		$('row* c[r]', sheet).each(function() {
		// 			if ($('is t', this).text().match(/(?:^|\b)(cover)(?=\b|$)/gmi)) {
		// 				$(this).attr('s', '20');
		// 			}
		// 		});
		// 	}
		// }]


	});

    var table = $('#example2').DataTable({



	});
  

     

});

    </script>
 
@endsection