@extends('layout.master')
@section('title', 'Staff')
@section('description', 'Staff of FARS')
@section('keywords', 'admin panel, fars')
@section('author', 'FARS')

@section('css')
<link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
   <style>
    #example_filter {
        float: right;
    }
    #example_paginate {
        float: right;
    }
        /* width */
 ::-webkit-scrollbar {
 width: 10px;
 }
 
 /* Track */
 ::-webkit-scrollbar-track {
 background: #f1f1f1;
 
 }
 
 /* Handle */
 ::-webkit-scrollbar-thumb {
 background: #958cf4;
 border-radius: 10px;
 }
 
 /* Handle on hover */
 ::-webkit-scrollbar-thumb:hover {
 background: #958cf4;
 }
  </style>
@endsection
@section('breadcrumb_title', 'Role')
@section('breadcrumb_2', 'Role')

    @section('content')
    <!-- Basic Tables start -->
    <div class="row" id="basic-table">
      <div class="col-12">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title">Roles</h4>
                  
                  <a href="{{route('admin.role.add')}}" style="float: right;" class="btn btn-secondary">Add Roles</a>
              </div>
              <div class="table-responsive  container-fluid">
                {{-- <div class="row">
                    <div class="col-lg-3">
                      <div class="form-group">
                        <select class="form-control">
                          <option value="">Filter By Status</option>
                          <option>option 1</option>
                          <option>option 2</option>
                          <option>option 3</option>
                          <option>option 4</option>
                          <option>option 5</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-lg-3">
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="Search by name">
                      </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                          <button class="btn btn-success" type="button">Apply Filters</button>
                        </div>
                      </div>
                </div> --}}
                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Name</th>
                          <th>Permission</th>
                          <th>Created_at</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                          <tr>
                            <td>Role 1</td>
                            <td>Permission</td>
                            <td>01-07-2021</td>
                              <td>
                                  <div class="dropdown">
                                      <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                                          <i class="fas fa-ellipsis-v"></i>
                                      </button>
                                      <div class="dropdown-menu">
                                       
                                        <a class="dropdown-item" href="{{route('admin.role.detail')}}">
                                            <i data-feather="view" class="mr-50"></i>
                                            <span>View</span>
                                        </a>
                                        <a class="dropdown-item" href="#">
                                            <i data-feather="delete" class="mr-50"></i>
                                            <span>Delete</span>
                                        </a>
                                        <a class="dropdown-item" href="{{route('admin.role.edit')}}">
                                            <i data-feather="edit" class="mr-50"></i>
                                            <span>Edit</span>
                                        </a>
                                        
                                      </div>
                                  </div>
                              </td>
                          </tr>
                          <tr>
                            <td>Role 2</td>
                    <td>Permission</td>
                    <td>02-07-2021</td>
                              <td>
                                  <div class="dropdown">
                                      <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                                          <i class="fas fa-ellipsis-v"></i>
                                      </button>
                                      <div class="dropdown-menu">
                                       
                                        <a class="dropdown-item" href="{{route('admin.staff.detail')}}">
                                            <i data-feather="view" class="mr-50"></i>
                                            <span>View</span>
                                        </a>
                                        <a class="dropdown-item" href="#">
                                            <i data-feather="delete" class="mr-50"></i>
                                            <span>Delete</span>
                                        </a>
                                        <a class="dropdown-item" href="{{route('admin.staff.edit')}}">
                                            <i data-feather="edit" class="mr-50"></i>
                                            <span>Edit</span>
                                        </a>
                                        
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
    </div>
  <!-- Basic Tables end -->
@endsection



@section('script')
    <!-- BEGIN: Page JS-->
    <script src="{{ asset('app-assets/js/scripts/pages/dashboard-ecommerce.js') }}"></script>
    <!-- END: Page JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/responsive.bootstrap4.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/vfs_fonts.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.rowGroup.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
        <!-- END: Page Vendor JS-->
        <script>
       function EuToUsCurrencyFormat(input) {
	return input.replace(/[,.]/g, function(x) {
		return x == "," ? "." : ",";
	});
}

$(document).ready(function() {
	//Only needed for the filename of export files.
	//Normally set in the title tag of your page.
	// document.title = 'DataTable Excel';
	// DataTable initialisation
	$('#example').DataTable({
		// "dom": '<"dt-buttons"Bf><"clear">lirtp',
		// "paging": true,
		// "autoWidth": true,
		// "buttons": [{
		// 	extend: 'excelHtml5',
		// 	text: 'Excel',
		// 	customize: function(xlsx) {
		// 		var sheet = xlsx.xl.worksheets['sheet1.xml'];
		// 		//All cells
		// 		$('row c', sheet).attr('s', '25');
		// 		//Second column
		// 		$('row c:nth-child(2)', sheet).attr('s', '42');
		// 		//First row
		// 		$('row:first c', sheet).attr('s', '36');
		// 		// One cell
		// 		$('row c[r^="D6"]', sheet).attr('s', '32');
		// 		// Loop over the cells in column `E` the amount column
		// 		$('row c[r^="E"]', sheet).each(function() {
		// 			if (parseFloat(EuToUsCurrencyFormat($('is t', this).text())) > 1500) {
		// 				$(this).attr('s', '17');
		// 			}
		// 		});
		// 		//All cells of row 10
		// 		$('row c[r*="10"]', sheet).attr('s', '49');
		// 		//Search all cells for a specific text
		// 		$('row* c[r]', sheet).each(function() {
		// 			if ($('is t', this).text().match(/(?:^|\b)(cover)(?=\b|$)/gmi)) {
		// 				$(this).attr('s', '20');
		// 			}
		// 		});
		// 	}
		// }]
	});
});
    
    </script>
@endsection
