@extends('layout.master')
@section('title', 'Edit Staff')
@section('description', 'Edit Staff')
@section('keywords', 'admin panel, fars')
@section('author', 'FARS')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/select/select2.min.css') }}">
@endsection
@section('breadcrumb_title', 'Edit Staff')
@section('breadcrumb_2', 'Edit Staff')
@section('content')
    <section id="multiple-column-form">
        <form class="row" id="users" action="#" method="POST"
            enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Edit Role</h4>
                        </div>
                        <div class="card-body">
                            <form class="form">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="inputName">Role Name</label>
                                            <input type="text" id="inputName" name="name" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="inputDescription">Role Permission</label>
                                            <input type="text" id="inputName" name="text" class="form-control">
                                        </div>
                                    </div>
                                   
                                 

                                    <div class="col-12">
                                        <button type="reset" class="btn btn-primary mr-1">Submit</button>
                                        <button type="reset" class="btn btn-outline-secondary">Reset</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
 
@endsection


@section('script')

  <!-- BEGIN: Page Vendor JS-->
  <script src="{{ asset('app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
  <!-- END: Page Vendor JS-->

      <!-- BEGIN: Page JS-->
      <script src="{{ asset('app-assets/js/scripts/forms/form-select2.js') }}"></script>
      <!-- END: Page JS-->

@endsection
