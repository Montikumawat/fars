@extends('layout.master')
@section('title', 'Job Add')
@section('description', 'Job of FARS')
@section('keywords', 'admin panel, fars')
@section('author', 'FARS')

@section('css')
    {{-- <!-- BEGIN: Page CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/pickers/form-flat-pickr.css') }}">
<!-- END: Page CSS--> --}}
    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/form-wizard.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/wizard/bs-stepper.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/select/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/form-validation.css') }}">
    <!-- END: Page CSS-->
    {{-- <style>
        .checkbox > input[type="checkbox"] {  display: none; }
        .checkbox > input[type="checkbox"]:checked + label:before {margin-right: 10px; content: "[]";}
        .checkbox > input[type="checkbox"] + label:before {margin-right: 10px; content: "[ ]";}
        </style>

        <style>
            .radios{
  display:none;
}
.color-box {
  width: 50px;
  line-height: 50px;
  margin-top: 0.72917%;
  display: inline-block;
  margin: 7px 0.72917%;
  color: #fff;
  text-align:center;
  text-decoration: none;
  border: 1px solid transparent;
}
.black-box {
  background: #000;
}
.grey-box {
  background: #9E9E9E;
}
.blue-box {
  background: #205DB1;
}
.radios:checked + label{
  text-decoration:underline;
}
#rad1:checked ~ #result{
  background-color:black;
}
#rad2:checked ~ #result{
  background-color:#9E9E9E;
}
#rad3:checked ~ #result{
  background-color:#205DB1;
}
#result{
  width:200px;
  height:200px;
  border:2px orange solid;
}
        </style> --}}
<style>
    @import url('https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&display=swap');

*{
  margin: 0;
  padding: 0;
  box-sizing: border-box;
}
body{
  font-family: 'Open Sans', sans-serif;
  font-size: 15px;
  line-height: 1.5;
  font-weight: 400;
  background: #f0f3f6;
  color: #3a3a3a;
}
hr {
  margin: 20px 0;
  border: none;
  border-bottom: 1px solid #d9d9d9;
}
label, input{
	cursor: pointer;
}
h2,h3,h4,h5{
	font-weight: 600;
	line-height: 1.3;
	color: #1f2949;
}
h2{
	font-size: 24px;
}
h3 {
	font-size: 18px;
}
h4 {
	font-size: 14px;
}
h5 {
	font-size: 12px;
	font-weight: 400;
}
img{
	max-width: 100%;
	display: block;
	vertical-align: middle;
}
.container {
  max-width: 99vw;
  margin: 15px auto;
  padding: 0 15px;
}

.top-text-wrapper {
	margin: 20px 0 30px 0;
}
.top-text-wrapper h4{
	font-size: 24px;
  margin-bottom: 10px;
}
.top-text-wrapper code{
  font-size: .85em;
  background: linear-gradient(90deg,#fce3ec,#ffe8cc);
  color: #ff2200;
  padding: .1rem .3rem .2rem;
  border-radius: .2rem;
}
.tab-section-wrapper{
  padding: 30px 0;
}

.grid-wrapper {
	display: grid;
	grid-gap: 30px;
	place-items: center;
	place-content: center;
}
.grid-col-auto{
  grid-template-columns: repeat(auto-fill, minmax(280px, .1fr));
  grid-template-rows: auto;
}
.grid-col-1{
	grid-template-columns: repeat(1, auto);
	grid-template-rows: repeat(1, auto);
}
.grid-col-2{
	grid-template-columns: repeat(2, auto);
	grid-template-rows: repeat(1, auto);
}
.grid-col-3{
	grid-template-columns: repeat(3, auto);
	grid-template-rows: repeat(1, auto);
}
.grid-col-4{
	grid-template-columns: repeat(4, auto);
	grid-template-rows: repeat(1, auto);
}


/* ******************* Selection Radio Item */

.selected-content{
	text-align: center;
	border-radius: 3px;
  box-shadow: 0 2px 4px 0 rgba(219, 215, 215, 0);
  border: solid 2px transparent;
	background: #fff;
	max-width: 280px;
	/* height: 330px; */
	padding: 15px;
	display: grid;
	grid-gap: 15px;
	place-content: center;
	transition: .3s ease-in-out all;
}

.selected-content img {
    width: 230px;
		margin: 0 auto;
}
.selected-content h4 {
	font-size: 16px;
  letter-spacing: -0.24px;
  text-align: center;
  color: #1f2949;
}
.selected-content h5 {
	font-size: 14px;
  line-height: 1.4;
  text-align: center;
  color: #686d73;
}

.selected-label{
	position: relative;
}
.selected-label input{
	display: none;
}
.selected-label .icon{
	width: 20px;
  height: 20px;
  border: solid 2px #e3e3e3;
	border-radius: 50%;
	position: absolute;
	top: 15px;
	left: 15px;
	transition: .3s ease-in-out all;
	transform: scale(1);
	z-index: 1;
}
.selected-label .icon:before{
	content: "\f00c";
	position: absolute;
	width: 100%;
	height: 100%;
	font-family: "Font Awesome 5 Free";
	font-weight: 900;
	font-size: 12px;
	color: #000;
	text-align: center;
	opacity: 0;
	transition: .2s ease-in-out all;
	transform: scale(2);
}
.selected-label input:checked + .icon{
	background: #3057d5;
	border-color: #3057d5;
	transform: scale(1.2);
}
.selected-label input:checked + .icon:before{
	color: #fff;
	opacity: 1;
	transform: scale(.8);
}
.selected-label input:checked ~ .selected-content{
  box-shadow: 0 2px 4px 0 rgba(219, 215, 215, 0.5);
  border: solid 2px #3057d5;
}
</style>        
@endsection
@section('breadcrumb_title', 'Job')
@section('breadcrumb_2', 'Job')

@section('content')
    {{-- <!-- Horizontal Wizard -->
    <div class="checkbox">
        <div class="card">
            <div class="card-body">
                <input type="checkbox" id="test">
                <label for="test">HELLO</label>
            </div>
        </div>
    </div>
      <div>
        <input type="radio" id="rad1" name="rad" class="radios">
        <label for="rad1" class="color-box black-box">Black</label>
      
        <input type="radio" id="rad2" name="rad" class="radios">
        <label for="rad2" class="color-box grey-box">Grey</label>
      
        <input type="radio" id="rad3" name="rad" class="radios">
        <label for="rad3" class="color-box blue-box">Blue</label>
      
        <hr>
        <div id="result"></div>
      </div> --}}
     
                <section class="horizontal-wizard">
                    <div class="bs-stepper horizontal-wizard-example">
                        <div class="bs-stepper-header">
                            <div class="step" data-target="#account-details">
                                <button type="button" class="step-trigger">
                                    <span class="bs-stepper-box">1</span>
                                    <span class="bs-stepper-label">
                                        <span class="bs-stepper-title">Add Job</span>
                                        {{-- <span class="bs-stepper-subtitle">Setup Account Details</span> --}}
                                    </span>
                                </button>
                            </div>
                            <div class="line">
                                <i data-feather="chevron-right" class="font-medium-2"></i>
                            </div>
                            <div class="step" data-target="#personal-info">
                                <button type="button" class="step-trigger">
                                    <span class="bs-stepper-box">2</span>
                                    <span class="bs-stepper-label">
                                        <span class="bs-stepper-title">Job</span>
                                        {{-- <span class="bs-stepper-subtitle">Add Personal Info</span> --}}
                                    </span>
                                </button>
                            </div>
                            {{-- <div class="line">
                                <i data-feather="chevron-right" class="font-medium-2"></i>
                            </div> --}}
                            {{-- <div class="step" data-target="#address-step">
                                <button type="button" class="step-trigger">
                                    <span class="bs-stepper-box">3</span>
                                    <span class="bs-stepper-label">
                                        <span class="bs-stepper-title">Address</span>
                                        <span class="bs-stepper-subtitle">Add Address</span>
                                    </span>
                                </button>
                            </div> --}}
                            {{-- <div class="line">
                                <i data-feather="chevron-right" class="font-medium-2"></i>
                            </div> --}}
                            {{-- <div class="step" data-target="#social-links">
                                <button type="button" class="step-trigger">
                                    <span class="bs-stepper-box">4</span>
                                    <span class="bs-stepper-label">
                                        <span class="bs-stepper-title">Social Links</span>
                                        <span class="bs-stepper-subtitle">Add Social Links</span>
                                    </span>
                                </button>
                            </div> --}}
                        </div>
                        <div class="bs-stepper-content">
                            <div id="account-details" class="content">
                                <div class="content-header">
                                    <h5 class="mb-0">Add Job</h5>
                                </div>
                                <form>
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label for="inputName">Job Title</label>
                                            <input type="text" id="inputName" name="name" class="form-control" required>
                                        </div>
                                        <div class="form-group col-md-6 float-right">
                                            
                                            <label for="">Job Time</label>
                                            <div class="demo-inline-spacing">
                                                <div class="custom-control custom-radio">
                                                    <input type="radio" id="customRadio1" name="schedule"  class="custom-control-input" />
                                                    <label class="custom-control-label" for="customRadio1">Schedule</label>
                                                </div>
                                                <div class="custom-control custom-radio">
                                                    <input type="radio" id="customRadio2" name="schedule" class="custom-control-input" />
                                                    <label class="custom-control-label" for="customRadio2">Run Now</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6" id="scheduleDiv">
                  
                                        </div>
                                    </div>
                                    <div class="row">
                                        {{-- <div class="form-group form-password-toggle col-md-6">
                                            <label for="inputStatus">Status</label>
                                            <select id="inputStatus"  name="status" class="form-control custom-select" required>
                                              <option selected disabled>Select Status</option>
                                              <option value="1">Done</option>
                                              <option value="0">Pending</option>
                                              
                                            </select>
                                        </div> --}}
                                        <div class="form-group form-password-toggle col-md-6">
                                            <label>Select Model</label>
                                            <select id="inputModel"  name="model" class="form-control custom-select">
                                                <option value="0"></option>
                                                <option value="0">Model 1 (V 1.1)</option>
                                                <option value="0">Model 2 (V 2.3)</option>
                            
                                            </select>
                                        </div>

                                    </div>
                                </form>
                                <div class="d-flex justify-content-between">
                                    <button class="btn btn-outline-secondary btn-prev" disabled>
                                        <i data-feather="arrow-left" class="align-middle mr-sm-25 mr-0"></i>
                                        <span class="align-middle d-sm-inline-block d-none">Previous</span>
                                    </button>
                                    <button class="btn btn-primary btn-next">
                                        <span class="align-middle d-sm-inline-block d-none">Next</span>
                                        <i data-feather="arrow-right" class="align-middle ml-sm-25 ml-0"></i>
                                    </button>
                                </div>
                            </div>
                            <div id="personal-info" class="content">
                                <div class="content-header">
                                    <h5 class="mb-0">Job</h5>
                                    {{-- <small>Enter Your Personal Info.</small> --}}
                                </div>
                                <form>
                                    <div class="row">
                                        <div class="col-xl-12 col-md-12 col-12 mb-1">
                                            <label>Already Trained Users (This section will display when user has chosen Base Model)</label>
                                          <select class="select2" multiple="multiple" data-placeholder="Select Users"
                                            style="width: 100%;">
                                            <option value="0" selected>User1</option>
                                            <option value="0" selected>User2</option>
                                            <option value="0" selected>User3</option>
                                            <option value="0" selected>User4</option>
                                          </select>
                                        </div>
                                        {{-- <div class="col-xl-12 col-md-12 col-12 mb-1">
                                            <label>Select Users</label>
                                            <select class="select2" multiple="multiple" data-placeholder="Select Users">
                                                <option value="0">User1</option>
                                                <option value="0">User2</option>
                                                <option value="0">User3</option>
                                                <option value="0">User4</option>
                                            </select>

                                        </div> --}}
                                        <div class="container">
                                            <div class="top-text-wrapper">
                                                <h4>Add Users</h4>
                                                {{-- <p>Altering the default styles of <code>Checkbox</code>, <code>Radio</code> and other custom <code>Input</code> types and so forth.</p> --}}
                                                <p>Select Users for this job</p>
                                                <hr>
                                            </div>
                                            <div class="row">
                                            <div class="col-lg-12 col-md-6">
                                                <div class="grid-wrapper grid-col-4">
                                                    <div class="selection-wrapper">
                                                        <label for="selected-item-1" class="selected-label">
                                                            <input type="checkbox" name="selected-item" id="selected-item-1">
                                                            <span class="icon"></span>
                                                            <div class="selected-content">
                                                                <img src="https://image.freepik.com/free-vector/group-friends-giving-high-five_23-2148363170.jpg" alt="">
                                                                <h4>User 1</h4>
                                                                {{-- <h5>Lorem ipsum dolor sit amet, consectetur.</h5> --}}
                                                            </div>
                                                        </label>
                                                    </div>
                                                    <div class="selection-wrapper">
                                                        <label for="selected-item-2" class="selected-label">
                                                            <input type="checkbox" checked name="selected-item" id="selected-item-2">
                                                            <span class="icon"></span>
                                                            <div class="selected-content">
                                                                <img src="https://image.freepik.com/free-vector/people-putting-puzzle-pieces-together_52683-28610.jpg" alt="">
                                                                <h4>User 2</h4>
                                                                {{-- <h5>Lorem ipsum dolor sit amet, consectetur.</h5> --}}
                                                            </div>
                                                        </label>
                                                    </div>
                                                    <div class="selection-wrapper">
                                                      <label for="selected-item-3" class="selected-label">
                                                          <input type="checkbox" checked name="selected-item" id="selected-item-3">
                                                          <span class="icon"></span>
                                                          <div class="selected-content">
                                                              <img src="https://image.freepik.com/free-vector/people-putting-puzzle-pieces-together_52683-28610.jpg" alt="">
                                                              <h4>User 3</h4>
                                                              {{-- <h5>Lorem ipsum dolor sit amet, consectetur.</h5> --}}
                                                          </div>
                                                      </label>
                                                  </div>
                                                  <div class="selection-wrapper">
                                                    <label for="selected-item-4" class="selected-label">
                                                        <input type="checkbox" checked name="selected-item" id="selected-item-4">
                                                        <span class="icon"></span>
                                                        <div class="selected-content">
                                                            <img src="https://image.freepik.com/free-vector/people-putting-puzzle-pieces-together_52683-28610.jpg" alt="">
                                                            <h4>User 4</h4>
                                                            {{-- <h5>Lorem ipsum dolor sit amet, consectetur.</h5> --}}
                                                        </div>
                                                    </label>
                                                </div>
                                                <div class="selection-wrapper">
                                                  <label for="selected-item-5" class="selected-label">
                                                      <input type="checkbox" checked name="selected-item" id="selected-item-5">
                                                      <span class="icon"></span>
                                                      <div class="selected-content">
                                                          <img src="https://image.freepik.com/free-vector/people-putting-puzzle-pieces-together_52683-28610.jpg" alt="">
                                                          <h4>User 5</h4>
                                                          {{-- <h5>Lorem ipsum dolor sit amet, consectetur.</h5> --}}
                                                      </div>
                                                  </label>
                                              </div>
                                              <div class="selection-wrapper">
                                                <label for="selected-item-6" class="selected-label">
                                                    <input type="checkbox" checked name="selected-item" id="selected-item-6">
                                                    <span class="icon"></span>
                                                    <div class="selected-content">
                                                        <img src="https://image.freepik.com/free-vector/people-putting-puzzle-pieces-together_52683-28610.jpg" alt="">
                                                        <h4>User 6</h4>
                                                        {{-- <h5>Lorem ipsum dolor sit amet, consectetur.</h5> --}}
                                                    </div>
                                                </label>
                                              </div>
                                            <div class="selection-wrapper">
                                              <label for="selected-item-7" class="selected-label">
                                                  <input type="checkbox" checked name="selected-item" id="selected-item-7">
                                                  <span class="icon"></span>
                                                  <div class="selected-content">
                                                      <img src="https://image.freepik.com/free-vector/people-putting-puzzle-pieces-together_52683-28610.jpg" alt="">
                                                      <h4>User 7</h4>
                                                      {{-- <h5>Lorem ipsum dolor sit amet, consectetur.</h5> --}}
                                                  </div>
                                              </label>
                                            </div>
                                            <div class="selection-wrapper">
                                                <label for="selected-item-8" class="selected-label">
                                                <input type="checkbox" checked name="selected-item" id="selected-item-8">
                                                <span class="icon"></span>
                                                <div class="selected-content">
                                                    <img src="https://image.freepik.com/free-vector/people-putting-puzzle-pieces-together_52683-28610.jpg" alt="">
                                                    <h4>User 8</h4>
                                                    {{-- <h5>Lorem ipsum dolor sit amet, consectetur.</h5> --}}
                                                </div>
                                                  </label>
                                            </div>
                                                </div>
                                            </div>
                                            </div>
                                          </div>
                                    </div>
                                </form>
                                <div class="d-flex justify-content-between">
                                    <button class="btn btn-primary btn-prev">
                                        <i data-feather="arrow-left" class="align-middle mr-sm-25 mr-0"></i>
                                        <span class="align-middle d-sm-inline-block d-none">Previous</span>
                                    </button>
                                    <button class="btn btn-success btn-submit">Submit</button>
                                </div>
                            </div>
                           
                        </div>
                    </div>
                </section>
                <!-- /Horizontal Wizard -->
@endsection



@section('script')


  <!-- BEGIN: Page Vendor JS-->
  <script src="{{ asset('app-assets/vendors/js/forms/wizard/bs-stepper.min.js') }}"></script>
  <script src="{{ asset('app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
  <script src="{{ asset('app-assets/vendors/js/forms/validation/jquery.validate.min.js') }}"></script>
  <!-- END: Page Vendor JS-->

      <!-- BEGIN: Page JS-->
      <script src="{{ asset('app-assets/js/scripts/forms/form-wizard.js') }}"></script>
      <script src="{{ asset('app-assets/js/scripts/forms/form-select2.js') }}"></script>
      <!-- END: Page JS-->

      <script>
        $(document).ready(function(){
        $('#scheduleButton').on('click', function() {
          $("#scheduleDiv").html('<label for="inputName">Schedule</label><input type="datetime-local" id="inputImage" name="image" class="form-control">')
      
        });  
      });
      </script>

     <script>
         $("input[name='schedule']").click(function()
{
  var schedule = $(this).attr('checked');
  var name = $(this).attr('name');
  
   // do something


console.log(schedule)
if ($("#customRadio1").is(":checked")) {
  
    $("#scheduleDiv").html('<label for="inputName">Schedule</label><input type="datetime-local" id="inputImage" name="image" class="form-control">')
  }
  else
  {
    $("#scheduleDiv").html('')
  }
});
     </script>

@endsection
