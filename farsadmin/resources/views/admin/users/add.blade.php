@extends('layout.master')
@section('title', 'Users')
@section('description', 'Users of FARS')
@section('keywords', 'admin panel, fars')
@section('author', 'FARS')

@section('css')
    <!-- BEGIN: Vendor CSS-->

    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/file-uploaders/dropzone.min.css') }}">
    <!-- END: Vendor CSS-->
    
    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/form-file-uploader.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/select/select2.min.css') }}">
    <!-- END: Page CSS-->
   <style>
       .whatever{
    background-color: #FFFFFF;
    display: inline-block;
    width: 100px;
    height: 34px;
    border-radius: 18px;
    /* border-style: inset; */
}

#checkboxes input[type=checkbox]{
    display: none;
}

#checkboxes input[type=checkbox]:checked + .whatever{
    background-color: #8e84f3;
}
   </style>
@endsection
@section('breadcrumb_title', 'Users')
@section('breadcrumb_2', 'Users')
@section('content')
    <section id="multiple-column-form">
        <form class="row" id="users" action="{{ route('admin.users.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">User Add</h4>
                        </div>
                        <div class="card-body">
                            <form class="form">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="first-name-column">User Name</label>
                                            <input type="text" id="name" class="form-control"
                                                placeholder="User Name" name="name" />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="last-name-column">User Email</label>
                                            <input type="email" id="email" class="form-control"
                                                placeholder="User Email" name="email" />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                      <div class="form-group">
                                        <label for="inputStatus">Status</label>
                                        <select id="inputStatus" name="status" class="form-control custom-select">
                                            <option selected disabled>Status</option>
                                            <option value="0">In Active</option>
                                            <option value="1">Active</option>
                                        </select>
                                      </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                      <div class="form-group">
                                        <label for="inputEstimatedBudget">Password</label>
                                        <input type="password" name="password" id="inputEstimatedBudget" class="form-control">
                                      </div>
                                    </div>

                                    <div class="col-12">
                                        <button type="submit" class="btn btn-primary mr-1">Submit</button>
                                        <button type="reset" class="btn btn-outline-secondary">Reset</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
           
        </form>
        
        <!-- remove all thumbnails file upload starts -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Upload User Images</h4>
                    </div>

                    <p id="demo"></p>
                    <div class="card-body">
                        <p class="card-text">
                        {{-- This example allows user to create a button that will remove all files from a dropzone. Hear for the
                            button's click event and then call <code>removeAllFiles</code> method to remove all the files from the
                            dropzone. --}}
                        </p>
                        <button id="clear-dropzone" class="btn btn-outline-primary mb-1">
                            <i data-feather="trash" class="mr-25"></i>
                            <span class="align-middle">Clear Drop</span>
                        </button>
                        
                        <form action="3" class="dropzone dropzone-area" id="dpz-remove-all-thumb">
                            <div class="dz-message">Drop files here or click to upload.</div>

                            <div class="container"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- remove all thumbnails file upload ends -->
    </section>

 
    <!-- Modal -->
    <div class="modal fade text-left" id="default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" id="myModalLabel1">Select Tags</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div id="checkboxes">
                                                    
                                                                @for($i=1;$i<=12;$i++)
                                                                <input type="checkbox" class="form-group" onclick="myFunction('{{$i}}')" myTag{{$i}}="456" name="rGroup" value="" id="r{{$i}}"/>
                                                                <label class="whatever btn2" for="r{{$i}}"  style="text-align: center; padding-top: 9px;"> Tag {{$i}} </label>
                                                                @endfor
                                                                
                                                            </div>
                                                            
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-primary" id="javed" data-dismiss="modal">Submit</button>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
    </div>
@endsection
@section('style')
<style>
    .tagify--outside{
    border: 0;
}

.tagify--outside .tagify__input{
  order: -1;
  flex: 100%;
  border: 1px solid var(--tags-border-color);
  margin-bottom: 1em;
  transition: .1s;
}

.tagify--outside .tagify__input:hover{ border-color:var(--tags-hover-border-color); }
.tagify--outside.tagify--focus .tagify__input{
  transition:0s;
  border-color: var(--tags-focus-border-color);
}
</style>

@endsection
@section('script')

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('app-assets/vendors/js/extensions/dropzone.min.js') }}"></script>
    <!-- END: Page Vendor JS-->
    <!-- BEGIN: Page JS-->
    
    <script src="{{ asset('app-assets/js/scripts/forms/form-select2.js') }}"></script>
    <!-- END: Page JS-->
    <script>
// $(function() {
    var javed= [];
   
    function myFunction(id) {
        // console.log(id);
        var checkBox = document.getElementById("r"+id);
        if (checkBox.checked == true){
        javed.push(id);
     
            console.log(id)
            
        }else{
            const index = javed.indexOf(id);
            
            javed.splice(index, 1);
           
        }
       
        

// console.log(javed);

document.getElementById("demo").innerHTML = javed;

}
    // function tag(id) {
    //     console.log(id);

    //         var element = $(this);
    //         var myTag = element.attr("mytag1");
    //         console.log(myTag);
    //         var option = $('option:selected', this).attr('mytag');

    //         $('#setMyTag').val(myTag);
            
            
    //     });
       
    // });
    </script>
    <!-- <script>
// // $( "input .form-group" )
// $( "select" )
//   .change(function() {
//     var str = "";
//     $( "select option:selected" ).each(function() {
//       str += $( this ).text() + " ";
//     });
//     $( "div" ).text( str );
//   })
//   .trigger( "change" );
// </script> -->


    <script>
        var input = document.querySelector('input[name=rGroup]')
// init Tagify script on the above inputs
var tagify = new Tagify(input, {
  whitelist: ["foo", "bar", "baz"],
  dropdown: {
    position: "input",
    enabled : 0 // always opens dropdown when input gets focus
  }
});
    </script>

@endsection
