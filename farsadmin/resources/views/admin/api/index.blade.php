@extends('layout.master')
@section('title', 'API Management')
@section('description', 'API Management')
@section('keywords', 'admin panel, fars')
@section('author', 'FARS')

@section('css')
<link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<style>
    #example_filter {
        float: right;
    }
    #example_paginate {
        float: right;
    }
        /* width */
 ::-webkit-scrollbar {
 width: 10px;
 }
 
 /* Track */
 ::-webkit-scrollbar-track {
 background: #f1f1f1;
 
 }
 
 /* Handle */
 ::-webkit-scrollbar-thumb {
 background: #958cf4;
 border-radius: 10px;
 }
 
 /* Handle on hover */
 ::-webkit-scrollbar-thumb:hover {
 background: #958cf4;
 }
  </style>
@endsection
@section('breadcrumb_title', 'API Management')
@section('breadcrumb_2', 'API Management')

@section('content')
    {{-- <section id="multiple-column-form">
        <form class="row" id="users" action="#" method="POST"
            enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">API Management</h4>
                        </div>
                        <div class="card-body">
                            <form class="form">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="inputName">Android and IOS WebView URL for staff All Conversation</label>
                                            <input type="text" id="inputName" name="name" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="inputName">App ID</label>
                                <input type="text" name="name" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="inputName">Secret Key (Use for calling the Rest API)</label>
                                <input type="text" name="name" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="inputName">App Name</label>
                                <input type="text" name="name" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="inputName">App URL</label>
                                <input type="text" name="name" class="form-control">
                                        </div>
                                    </div>
                                    

                                    <div class="col-12">
                                        <button type="reset" class="btn btn-primary mr-1">Submit</button>
                                        <button type="reset" class="btn btn-outline-secondary">Reset</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Server</h4>
                        </div>
                        <div class="card-body">
                            <form class="form">
                                <div class="row">
                                   
                                    <div class="col-md-12 col-12">
                                        <div class="form-group">
                                            <label for="inputName">PCM Server Key</label>
                                            <input type="text" name="name" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <button type="reset" class="btn btn-primary mr-1">Submit</button>
                                        <button type="reset" class="btn btn-outline-secondary">Reset</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section> --}}
    <div class="row" id="basic-table">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">API Management</h4>
                    <a  style="float: right;" class="btn btn-secondary" data-toggle="modal" data-target="#default">Generate API</a>
                </div>
                <div class="table-responsive  container-fluid">
                  {{-- <div class="row">
                      <div class="col-lg-3">
                        <div class="form-group">
                          <select class="form-control">
                            <option value="">Filter By Status</option>
                            <option>option 1</option>
                            <option>option 2</option>
                            <option>option 3</option>
                            <option>option 4</option>
                            <option>option 5</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-lg-3">
                        <div class="form-group">
                          <input type="text" class="form-control" placeholder="Search by name">
                        </div>
                      </div>
                      <div class="col-lg-3">
                          <div class="form-group">
                            <button class="btn btn-success" type="button">Apply Filters</button>
                          </div>
                        </div>
                  </div> --}}
                  <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                          <tr>
                            <th>Name</th>
                            <th>Creation Date</th>
                            <th>Restrictions</th>
                            <th>Key</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                            <tr>
                              <td>API Key 1</td>
                              <td>02/7/2021</td>
                              <td>None</td>
                              <td>AIzaSyA4b2okiLGOiv_zAVL8e1wYszoeiPXwF6c</td>
                                <td>
                                    <div class="dropdown">
                                        <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                                            <i class="fas fa-ellipsis-v"></i>
                                        </button>
                                        <div class="dropdown-menu">
                                         
                                            <a class="dropdown-item" href="{{route('admin.api.edit')}}">
                                                {{-- <i data-feather="edit" class="mr-50"></i> --}}
                                                <i class="fa fa-edit"></i>
                                                <span>Edit</span>
                                            </a>

                                            <a class="dropdown-item" href="#">
                                              {{-- <i data-feather="delete" class="mr-50"></i> --}}
                                              <i class="fa fa-trash"></i>
                                              <span>Delete</span>
                                            </a>
                                         
                                          
                                        </div>
                                    </div>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
     <!-- Modal -->
     <div class="modal fade text-left" id="default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">Created API</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="inputPackage">Your API Key</label>
                        <input type="text" class="form-control" name="role" value="AIzaSyA4b2okiLGOiv_zAVL8e1wYszoeiPXwF6c" disabled>
                    </div>
                
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    <a href="{{route('admin.api.edit')}}" class="btn btn-success" >Restrict Key</a>
                </div>
            </div>
        </div>
    </div>    
@endsection



@section('script')
 <!-- BEGIN: Page JS-->
 <script src="{{ asset('app-assets/js/scripts/pages/dashboard-ecommerce.js') }}"></script>
 <!-- END: Page JS-->

     <!-- BEGIN: Page Vendor JS-->
     <script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
     <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
     <script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
     <script src="{{ asset('app-assets/vendors/js/tables/datatable/responsive.bootstrap4.js') }}"></script>
     <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js') }}"></script>
     <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
     <script src="{{ asset('app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
     <script src="{{ asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
     <script src="{{ asset('app-assets/vendors/js/tables/datatable/vfs_fonts.js') }}"></script>
     <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
     <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
     <script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.rowGroup.min.js') }}"></script>
     <script src="{{ asset('app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
     <!-- END: Page Vendor JS-->
     <script>
    function EuToUsCurrencyFormat(input) {
 return input.replace(/[,.]/g, function(x) {
     return x == "," ? "." : ",";
 });
  }

  $(document).ready(function() {
 // DataTable initialisation
 $('#example').DataTable({
 
 });
 });
 
 </script>
@endsection
