@extends('layout.master')
@section('title', 'Restrict/Edit API Key')
@section('description', 'Restrict/Edit API Key')
@section('keywords', 'admin panel, fars')
@section('author', 'FARS')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/select/select2.min.css') }}">
@endsection
@section('breadcrumb_title', 'Restrict/Edit API Key')
@section('breadcrumb_2', 'Restrict/Edit API Key')
@section('content')
    <section id="multiple-column-form">
        <form class="row" id="users" action="#" method="POST"
            enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Restrict/Edit API Key</h4>
                        </div>
                        <div class="card-body">
                            <form class="row" id="users" action="#" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4 class="card-title">Restrict/Edit User</h4>
                                            </div>
                                            <div class="card-body">
                                                <form class="form">
                                                    <div class="row">
                                                        <div class="col-md-6 col-12">
                                                            <div class="form-group">
                                                                <label for="inputName">API Name</label>
                                                                <input type="text" id="inputName" name="name" value="API 1" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-12">
                                                            <div class="form-group">
                                                                <label for="inputDescription">API Key</label>
                                                                <input type="text" id="inputName" name="api_key" value="AIzaSyA4b2okiLGOiv_zAVL8e1wYszoeiPXwF6c" class="form-control" disabled>
                                                            </div>
                                                        </div>
                                                        <div class="col-12">
                                                            <button type="reset" class="btn btn-primary mr-1">Submit</button>
                                                            <button type="reset" class="btn btn-outline-secondary">Reset</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Restrict/Edit API Key</h4>
                        </div>
                        <div class="card-body">
                            <form class="row" id="users" action="#" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4 class="card-title">Restrictions are yet to be design</h4>
                                            </div>
                                           
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection


@section('script')

  <!-- BEGIN: Page Vendor JS-->
  <script src="{{ asset('app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
  <!-- END: Page Vendor JS-->

      <!-- BEGIN: Page JS-->
      <script src="{{ asset('app-assets/js/scripts/forms/form-select2.js') }}"></script>
      <!-- END: Page JS-->

@endsection
