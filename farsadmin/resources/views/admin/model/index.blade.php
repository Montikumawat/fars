@extends('layout.master')
@section('title', 'Model')
@section('description', 'Model of FARS')
@section('keywords', 'admin panel, fars')
@section('author', 'FARS')

@section('css')
<link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<style>
    #example1_filter {
        float: right;
    }
    #example1_paginate {
        float: right;
    }
    #example2_filter {
        float: right;
    }
    #example2_paginate {
        float: right;
    }
    #example3_filter {
        float: right;
    }
    #example3_paginate {
        float: right;
    }
    #example4_filter {
        float: right;
    }
    #example4_paginate {
        float: right;
    }
        /* width */
 ::-webkit-scrollbar {
 width: 10px;
 }
 
 /* Track */
 ::-webkit-scrollbar-track {
 background: #f1f1f1;
 
 }
 
 /* Handle */
 ::-webkit-scrollbar-thumb {
 background: #958cf4;
 border-radius: 10px;
 }
 
 /* Handle on hover */
 ::-webkit-scrollbar-thumb:hover {
 background: #958cf4;
 }
  </style>
@endsection
@section('breadcrumb_title', 'Model')
@section('breadcrumb_2', 'Model')

    @section('content')


                  <!-- Accordion with shadow start -->
                  <section id="accordion-with-shadow">
                    <div class="row">
                        <div class="col-sm-12">
                            <div id="accordionWrapa10" role="tablist" aria-multiselectable="true">
                                <div class="card collapse-icon">
                                    <div class="card-header">
                                        <h4 class="card-title">Model Management</h4>
                                    </div>
                                    <div class="card-body">
                                        <p class="card-text">
                                            {{-- Use class <code>.collapse-shadow</code> as a wrapper of your accordion/collapse <code>card</code> for
                                            shadow. --}}
                                        </p>
                                        <div class="collapse-shadow">
                                            <div class="card">
                                                <div id="heading11" class="card-header" data-toggle="collapse" role="button" data-target="#accordion10" aria-expanded="false" aria-controls="accordion10">
                                                    <span class="lead collapse-title"> Model 1 (Latest Version 1.4) </span>
                                                </div>
                                                <div id="accordion10" role="tabpanel" data-parent="#accordionWrapa10" aria-labelledby="heading11" class="collapse">
                                                    <div class="card-body">
                                                        <div class="table-responsive container-fluid">
                                                            <table id="example1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            
                                                                  <thead>
                                                                    <tr>
                                                                      <th>Model 1</th>
                                                                      <th>Created At</th>
                                                                      <th>Deploy Status</th>
                                                                      <th>Running Status</th>
                                                                      <th>Action</th>
                                                                    </tr>
                                                                  </thead>
                                                                  <tbody>
                                                                      <tr>
                                                                        <td>Model 1.1</td>
                                                                        <td>18:18 26-05-2021</td>
                                                                        <td>Deployed</td>
                                                                        <td>Running</td>
                                                                          <td>
                                                                              <div class="dropdown">
                                                                                  <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                                                                                      <i class="fas fa-ellipsis-v"></i>
                                                                                  </button>
                                                                                  <div class="dropdown-menu">
                                                                                    <a class="dropdown-item" href="{{route('admin.model.detail')}}">
                                                                                        <i data-feather="detail" class="mr-50"></i>
                                                                                        <span>Detail</span>
                                                                                    </a>
                                                                                    <a class="dropdown-item" href="#">
                                                                                      <i data-feather="undeloy" class="mr-50"></i>
                                                                                      <span>Undeploy</span>
                                                                                  </a>
                                                                                    
                                                                                  </div>
                                                                              </div>
                                                                          </td>
                                                                      </tr>
                                                                      <tr>
                                                                        <td>Model 1.2</td>
                                                                        <td>18:18 26-05-2021</td>
                                                                        <td>Not Deployed</td>
                                                                        <td>Failed</td>
                                                                          <td>
                                                                              <div class="dropdown">
                                                                                  <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                                                                                      <i class="fas fa-ellipsis-v"></i>
                                                                                  </button>
                                                                                  <div class="dropdown-menu">
                                                                                    <a class="dropdown-item" href="{{route('admin.model.detail')}}">
                                                                                        <i data-feather="detail" class="mr-50"></i>
                                                                                        <span>Detail</span>
                                                                                    </a>
                                                                                    <a class="dropdown-item" href="#">
                                                                                      <i data-feather="undeloy" class="mr-50"></i>
                                                                                      <span>Undeploy</span>
                                                                                  </a>
                                                                                    
                                                                                  </div>
                                                                              </div>
                                                                          </td>
                                                                      </tr>
                                                                      <tr>
                                                                        <td>Model 1.3</td>
                                                                        <td>18:18 26-05-2021</td>
                                                                        <td>Not Deployed</td>
                                                                        <td>Error</td>
                                                                          <td>
                                                                              <div class="dropdown">
                                                                                  <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                                                                                      <i class="fas fa-ellipsis-v"></i>
                                                                                  </button>
                                                                                  <div class="dropdown-menu">
                                                                                    <a class="dropdown-item" href="{{route('admin.model.detail')}}">
                                                                                        <i data-feather="detail" class="mr-50"></i>
                                                                                        <span>Detail</span>
                                                                                    </a>
                                                                                    <a class="dropdown-item" href="#">
                                                                                      <i data-feather="undeloy" class="mr-50"></i>
                                                                                      <span>Undeploy</span>
                                                                                  </a>
                                                                                    
                                                                                  </div>
                                                                              </div>
                                                                          </td>
                                                                      </tr>
                                                                      <tr>
                                                                        <td>Model 1.4</td>
                                                                        <td>18:18 26-05-2021</td>
                                                                        <td>Not Deployed</td>
                                                                        <td>Not Running</td>
                                                                          <td>
                                                                              <div class="dropdown">
                                                                                  <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                                                                                      <i class="fas fa-ellipsis-v"></i>
                                                                                  </button>
                                                                                  <div class="dropdown-menu">
                                                                                    <a class="dropdown-item" href="{{route('admin.model.detail')}}">
                                                                                        <i data-feather="detail" class="mr-50"></i>
                                                                                        <span>Detail</span>
                                                                                    </a>
                                                                                    <a class="dropdown-item" href="#">
                                                                                      <i data-feather="undeloy" class="mr-50"></i>
                                                                                      <span>Undeploy</span>
                                                                                  </a>
                                                                                    
                                                                                  </div>
                                                                              </div>
                                                                          </td>
                                                                      </tr>
                                                                  </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div id="heading20" class="card-header" data-toggle="collapse" role="button" data-target="#accordion20" aria-expanded="false" aria-controls="accordion20">
                                                    <span class="lead collapse-title"> Model 2 (Latest Version 2.4) </span>
                                                </div>
                                                <div id="accordion20" role="tabpanel" data-parent="#accordionWrapa10" aria-labelledby="heading20" class="collapse" aria-expanded="false">
                                                    <div class="card-body">
                                                        <div class="table-responsive container-fluid">
                                                            <table id="example2" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            
                                                                <thead>
                                                                  <tr>
                                                                    <th>Model 2</th>
                                                                    <th>Created At</th>
                                                                    <th>Deploy Status</th>
                                                                    <th>Running Status</th>
                                                                    <th>Action</th>
                                                                  </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                      <td>Model 2.1</td>
                                                                      <td>18:18 26-05-2021</td>
                                                                      <td>Deployed</td>
                                                                      <td>Running</td>
                                                                        <td>
                                                                            <div class="dropdown">
                                                                                <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                                                                                    <i class="fas fa-ellipsis-v"></i>
                                                                                </button>
                                                                                <div class="dropdown-menu">
                                                                                  <a class="dropdown-item" href="{{route('admin.model.detail')}}">
                                                                                      <i data-feather="detail" class="mr-50"></i>
                                                                                      <span>Detail</span>
                                                                                  </a>
                                                                                  <a class="dropdown-item" href="#">
                                                                                    <i data-feather="undeloy" class="mr-50"></i>
                                                                                    <span>Undeploy</span>
                                                                                </a>
                                                                                  
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                      <td>Model 2.2</td>
                                                                      <td>18:18 26-05-2021</td>
                                                                      <td>Not Deployed</td>
                                                                      <td>Failed</td>
                                                                        <td>
                                                                            <div class="dropdown">
                                                                                <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                                                                                    <i class="fas fa-ellipsis-v"></i>
                                                                                </button>
                                                                                <div class="dropdown-menu">
                                                                                  <a class="dropdown-item" href="{{route('admin.model.detail')}}">
                                                                                      <i data-feather="detail" class="mr-50"></i>
                                                                                      <span>Detail</span>
                                                                                  </a>
                                                                                  <a class="dropdown-item" href="#">
                                                                                    <i data-feather="undeloy" class="mr-50"></i>
                                                                                    <span>Undeploy</span>
                                                                                </a>
                                                                                  
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                      <td>Model 2.3</td>
                                                                      <td>18:18 26-05-2021</td>
                                                                      <td>Not Deployed</td>
                                                                      <td>Error</td>
                                                                        <td>
                                                                            <div class="dropdown">
                                                                                <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                                                                                    <i class="fas fa-ellipsis-v"></i>
                                                                                </button>
                                                                                <div class="dropdown-menu">
                                                                                  <a class="dropdown-item" href="{{route('admin.model.detail')}}">
                                                                                      <i data-feather="detail" class="mr-50"></i>
                                                                                      <span>Detail</span>
                                                                                  </a>
                                                                                  <a class="dropdown-item" href="#">
                                                                                    <i data-feather="undeloy" class="mr-50"></i>
                                                                                    <span>Undeploy</span>
                                                                                </a>
                                                                                  
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                      <td>Model 2.4</td>
                                                                      <td>18:18 26-05-2021</td>
                                                                      <td>Not Deployed</td>
                                                                      <td>Not Running</td>
                                                                        <td>
                                                                            <div class="dropdown">
                                                                                <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                                                                                    <i class="fas fa-ellipsis-v"></i>
                                                                                </button>
                                                                                <div class="dropdown-menu">
                                                                                  <a class="dropdown-item" href="{{route('admin.model.detail')}}">
                                                                                      <i data-feather="detail" class="mr-50"></i>
                                                                                      <span>Detail</span>
                                                                                  </a>
                                                                                  <a class="dropdown-item" href="#">
                                                                                    <i data-feather="undeloy" class="mr-50"></i>
                                                                                    <span>Undeploy</span>
                                                                                </a>
                                                                                  
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div id="heading30" class="card-header" data-toggle="collapse" role="button" data-target="#accordion30" aria-expanded="false" aria-controls="accordion30">
                                                    <span class="lead collapse-title"> Model 3 (Latest Version 3.3))</span>
                                                </div>
                                                <div id="accordion30" role="tabpanel" data-parent="#accordionWrapa10" aria-labelledby="heading30" class="collapse" aria-expanded="false">
                                                    <div class="card-body">
                                                        <div class="table-responsive container-fluid">
                                                            <table id="example3" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                                                  <thead>
                                                                    <tr>
                                                                      <th>Model 3</th>
                                                                      <th>Created At</th>
                                                                      <th>Deploy Status</th>
                                                                      <th>Running Status</th>
                                                                      <th>Action</th>
                                                                    </tr>
                                                                  </thead>
                                                                  <tbody>
                                                                      <tr>
                                                                        <td>Model 3.1</td>
                                                                        <td>18:18 26-05-2021</td>
                                                                        <td>Deployed</td>
                                                                        <td>Running</td>
                                                                          <td>
                                                                              <div class="dropdown">
                                                                                  <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                                                                                      <i class="fas fa-ellipsis-v"></i>
                                                                                  </button>
                                                                                  <div class="dropdown-menu">
                                                                                    <a class="dropdown-item" href="{{route('admin.model.detail')}}">
                                                                                        <i data-feather="detail" class="mr-50"></i>
                                                                                        <span>Detail</span>
                                                                                    </a>
                                                                                    <a class="dropdown-item" href="#">
                                                                                      <i data-feather="undeloy" class="mr-50"></i>
                                                                                      <span>Undeploy</span>
                                                                                  </a>
                                                                                    
                                                                                  </div>
                                                                              </div>
                                                                          </td>
                                                                      </tr>
                                                                      <tr>
                                                                        <td>Model 3.2</td>
                                                                        <td>18:18 26-05-2021</td>
                                                                        <td>Not Deployed</td>
                                                                        <td>Failed</td>
                                                                          <td>
                                                                              <div class="dropdown">
                                                                                  <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                                                                                      <i class="fas fa-ellipsis-v"></i>
                                                                                  </button>
                                                                                  <div class="dropdown-menu">
                                                                                    <a class="dropdown-item" href="{{route('admin.model.detail')}}">
                                                                                        <i data-feather="detail" class="mr-50"></i>
                                                                                        <span>Detail</span>
                                                                                    </a>
                                                                                    <a class="dropdown-item" href="#">
                                                                                      <i data-feather="undeloy" class="mr-50"></i>
                                                                                      <span>Undeploy</span>
                                                                                  </a>
                                                                                    
                                                                                  </div>
                                                                              </div>
                                                                          </td>
                                                                      </tr>
                                                                      <tr>
                                                                        <td>Model 3.3</td>
                                                                        <td>18:18 26-05-2021</td>
                                                                        <td>Not Deployed</td>
                                                                        <td>Error</td>
                                                                          <td>
                                                                              <div class="dropdown">
                                                                                  <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                                                                                      <i class="fas fa-ellipsis-v"></i>
                                                                                  </button>
                                                                                  <div class="dropdown-menu">
                                                                                    <a class="dropdown-item" href="{{route('admin.model.detail')}}">
                                                                                        <i data-feather="detail" class="mr-50"></i>
                                                                                        <span>Detail</span>
                                                                                    </a>
                                                                                    <a class="dropdown-item" href="#">
                                                                                      <i data-feather="undeloy" class="mr-50"></i>
                                                                                      <span>Undeploy</span>
                                                                                  </a>
                                                                                    
                                                                                  </div>
                                                                              </div>
                                                                          </td>
                                                                      </tr>
                                                                     
                                                                  </tbody>
                                                              </table>
                                                          </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div id="heading40" class="card-header" data-toggle="collapse" role="button" data-target="#accordion40" aria-expanded="false" aria-controls="accordion40">
                                                    <span class="lead collapse-title"> Model 4 (Latest Version 4.4) </span>
                                                </div>
                                                <div id="accordion40" role="tabpanel" data-parent="#accordionWrapa10" aria-labelledby="heading40" class="collapse" aria-expanded="false">
                                                    <div class="card-body">
                                                        <div class="table-responsive container-fluid">
                                                            <table id="example4" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                                                  <thead>
                                                                    <tr>
                                                                      <th>Model 4</th>
                                                                      <th>Created At</th>
                                                                      <th>Deploy Status</th>
                                                                      <th>Running Status</th>
                                                                      <th>Action</th>
                                                                    </tr>
                                                                  </thead>
                                                                  <tbody>
                                                                      <tr>
                                                                        <td>Model 4.1</td>
                                                                        <td>18:18 26-05-2021</td>
                                                                        <td>Deployed</td>
                                                                        <td>Running</td>
                                                                          <td>
                                                                              <div class="dropdown">
                                                                                  <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                                                                                      <i class="fas fa-ellipsis-v"></i>
                                                                                  </button>
                                                                                  <div class="dropdown-menu">
                                                                                    <a class="dropdown-item" href="{{route('admin.model.detail')}}">
                                                                                        <i data-feather="detail" class="mr-50"></i>
                                                                                        <span>Detail</span>
                                                                                    </a>
                                                                                    <a class="dropdown-item" href="#">
                                                                                      <i data-feather="undeloy" class="mr-50"></i>
                                                                                      <span>Undeploy</span>
                                                                                  </a>
                                                                                    
                                                                                  </div>
                                                                              </div>
                                                                          </td>
                                                                      </tr>
                                                                      <tr>
                                                                        <td>Model 4.2</td>
                                                                        <td>18:18 26-05-2021</td>
                                                                        <td>Not Deployed</td>
                                                                        <td>Failed</td>
                                                                          <td>
                                                                              <div class="dropdown">
                                                                                  <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                                                                                      <i class="fas fa-ellipsis-v"></i>
                                                                                  </button>
                                                                                  <div class="dropdown-menu">
                                                                                    <a class="dropdown-item" href="{{route('admin.model.detail')}}">
                                                                                        <i data-feather="detail" class="mr-50"></i>
                                                                                        <span>Detail</span>
                                                                                    </a>
                                                                                    <a class="dropdown-item" href="#">
                                                                                      <i data-feather="undeloy" class="mr-50"></i>
                                                                                      <span>Undeploy</span>
                                                                                  </a>
                                                                                    
                                                                                  </div>
                                                                              </div>
                                                                          </td>
                                                                      </tr>
                                                                      <tr>
                                                                        <td>Model 4.3</td>
                                                                        <td>18:18 26-05-2021</td>
                                                                        <td>Not Deployed</td>
                                                                        <td>Error</td>
                                                                          <td>
                                                                              <div class="dropdown">
                                                                                  <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                                                                                      <i class="fas fa-ellipsis-v"></i>
                                                                                  </button>
                                                                                  <div class="dropdown-menu">
                                                                                    <a class="dropdown-item" href="{{route('admin.model.detail')}}">
                                                                                        <i data-feather="detail" class="mr-50"></i>
                                                                                        <span>Detail</span>
                                                                                    </a>
                                                                                    <a class="dropdown-item" href="#">
                                                                                      <i data-feather="undeloy" class="mr-50"></i>
                                                                                      <span>Undeploy</span>
                                                                                  </a>
                                                                                    
                                                                                  </div>
                                                                              </div>
                                                                          </td>
                                                                      </tr>
                                                                      <tr>
                                                                        <td>Model 4.4</td>
                                                                        <td>18:18 26-05-2021</td>
                                                                        <td>Not Deployed</td>
                                                                        <td>Not Running</td>
                                                                          <td>
                                                                              <div class="dropdown">
                                                                                  <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                                                                                      <i class="fas fa-ellipsis-v"></i>
                                                                                  </button>
                                                                                  <div class="dropdown-menu">
                                                                                    <a class="dropdown-item" href="{{route('admin.model.detail')}}">
                                                                                        <i data-feather="detail" class="mr-50"></i>
                                                                                        <span>Detail</span>
                                                                                    </a>
                                                                                    <a class="dropdown-item" href="#">
                                                                                      <i data-feather="undeloy" class="mr-50"></i>
                                                                                      <span>Undeploy</span>
                                                                                  </a>
                                                                                    
                                                                                  </div>
                                                                              </div>
                                                                          </td>
                                                                      </tr>
                                                                  </tbody>
                                                              </table>
                                                          </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- Accordion with Shadow end -->
@endsection



@section('script')
    <!-- BEGIN: Page JS-->
    <script src="{{ asset('app-assets/js/scripts/pages/dashboard-ecommerce.js') }}"></script>
    <!-- END: Page JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/responsive.bootstrap4.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/vfs_fonts.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.rowGroup.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
        <!-- END: Page Vendor JS-->
        <script>
       function EuToUsCurrencyFormat(input) {
	return input.replace(/[,.]/g, function(x) {
		return x == "," ? "." : ",";
	});
     }

     $(document).ready(function() {
	//Only needed for the filename of export files.
	//Normally set in the title tag of your page.
	// document.title = 'DataTable Excel';
	// DataTable initialisation
	$('#example1').DataTable({
		// "dom": '<"dt-buttons"Bf><"clear">lirtp',
		// "paging": true,
		// "autoWidth": true,
		// "buttons": [{
		// 	extend: 'excelHtml5',
		// 	text: 'Excel',
		// 	customize: function(xlsx) {
		// 		var sheet = xlsx.xl.worksheets['sheet1.xml'];
		// 		//All cells
		// 		$('row c', sheet).attr('s', '25');
		// 		//Second column
		// 		$('row c:nth-child(2)', sheet).attr('s', '42');
		// 		//First row
		// 		$('row:first c', sheet).attr('s', '36');
		// 		// One cell
		// 		$('row c[r^="D6"]', sheet).attr('s', '32');
		// 		// Loop over the cells in column `E` the amount column
		// 		$('row c[r^="E"]', sheet).each(function() {
		// 			if (parseFloat(EuToUsCurrencyFormat($('is t', this).text())) > 1500) {
		// 				$(this).attr('s', '17');
		// 			}
		// 		});
		// 		//All cells of row 10
		// 		$('row c[r*="10"]', sheet).attr('s', '49');
		// 		//Search all cells for a specific text
		// 		$('row* c[r]', sheet).each(function() {
		// 			if ($('is t', this).text().match(/(?:^|\b)(cover)(?=\b|$)/gmi)) {
		// 				$(this).attr('s', '20');
		// 			}
		// 		});
		// 	}
		// }]
	});

    $('#example2').DataTable({
	});

    $('#example3').DataTable({
	});

    $('#example4').DataTable({
	});
    });
    
</script>


<script src="{{ asset('app-assets/js/scripts/components/components-collapse.js') }}"></script>

@endsection
