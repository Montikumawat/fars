@extends('layout.master')
@section('title', 'Model')
@section('description', 'Model of FARS')
@section('keywords', 'admin panel, fars')
@section('author', 'FARS')

@section('css')
    {{-- <!-- BEGIN: Page CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/pickers/form-flat-pickr.css') }}">
<!-- END: Page CSS--> --}}

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/tables/datatable/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/tables/datatable/rowGroup.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">
    <!-- END: Vendor CSS-->
    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
    <!-- END: Page CSS-->
@endsection
@section('breadcrumb_title', 'Model')
@section('breadcrumb_2', 'Model')
@section('content')
    <section id="multiple-column-form">
        <form id="users" action="#" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Model</h4>
                        </div>
                        <div class="card-body">
                          <div class="card-body">
                            <div class="table-responsive" >
                                <table class="table table-hover table-striped">
                
                                  <tbody>
                                  <tr>
                                      <th scope="row">Model ID</th>
                                      <td>545545465</td>
                                  </tr>
                  
                                  <tr>
                                      <th scope="row">Model Name</th>
                                      <td>Model 1</td>
                                  </tr>
                                  <tr>
                                      <th scope="row">Validation State</th>
                                      <td>Validated</td>
                                  </tr>
                                  <tr>
                                      <th scope="row">Accuracy</th>
                                      <td>88%</td>
                                  </tr>
                                  <tr>
                                    <th scope="row">Recoil</th>
                                    <td>44</td>
                                    </tr>
                                  </tbody>
                
                              </table>
                            </div>  
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </form> 
    </section>
    <div class="row" id="basic-table">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">List Of User</h4>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                          <tr>
                              <th>Id</th>
                              <th>Thumbnail</th>                             
                              <th>Name</th>
                              {{-- <th>Status</th>
                              <th>FARS Training Status</th> --}}
                          </tr>
                        </thead>
                        <tbody>
                            <tr>
                              
                              <td>1</td>
                              <td>
                                <img src="{{asset('app-assets/images/portrait/small/avatar-s-7.jpg')}}" alt="Avatar" height="50px" width="50px" />
                              </td>
                              <td>Test</td>
                              {{-- <td>

                              </td>
                              <td>

                              </td> --}}
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('script')


<!-- BEGIN: Page Vendor JS-->
<script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/responsive.bootstrap4.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/vfs_fonts.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.rowGroup.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
<!-- END: Page Vendor JS-->

@endsection
