@extends('layout.master')
@section('title', 'Users')
@section('description', 'Self Registered Users of FARS')
@section('keywords', 'admin panel, fars')
@section('author', 'FARS')

@section('css')
<link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<style>
    #example_filter {
        float: right;
    }
    #example_paginate {
        float: right;
    }
        /* width */
 ::-webkit-scrollbar {
 width: 10px;
 }
 
 /* Track */
 ::-webkit-scrollbar-track {
 background: #f1f1f1;
 
 }
 
 /* Handle */
 ::-webkit-scrollbar-thumb {
 background: #958cf4;
 border-radius: 10px;
 }
 
 /* Handle on hover */
 ::-webkit-scrollbar-thumb:hover {
 background: #958cf4;
 }
  </style>

@endsection
@section('breadcrumb_title', 'Self Registered Users')
@section('breadcrumb_2', 'Self Registered Users')


@section('content')
    <!-- Basic Tables start -->
    <div class="row" id="basic-table">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Self Registered Users Management</h4>
                    {{-- <a href="{{ route('admin.users.add') }}" class="btn btn-secondary" style="float: right;">Add Users</a> --}}
                    <div id="qft"></div>
                    
                </div>
                <div class="table-responsive container-fluid">
                    <div class="row">
                        <div class="col-lg-3">
                          <div class="form-group">
                            <select class="form-control">
                              <option value="">Filter By Status</option>
                              <option>option 1</option>
                              <option>option 2</option>
                              <option>option 3</option>
                              <option>option 4</option>
                              <option>option 5</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-lg-3">
                          <div class="form-group">
                            <input type="text" class="form-control" placeholder="Search by name">
                          </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                              <button class="btn btn-success" type="button">Apply Filters</button>
                            </div>
                          </div>
                    </div>
                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <!-- <th>
                                    {{-- <label><input type="checkbox" name="sample" class="selectall"/> Select all</label> --}}
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" name="sample" class="custom-control-input selectall" id="customCheck0">
                                        <label class="custom-control-label" for="customCheck0"> All</label>
                                    </div>
                                </th><th> -->
                              <th>  <label><input style="width:18px; height:18px;" type="checkbox" id="ckbCheckAll" class="selectall">&nbsp;All </label></th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            @if (count($users) > 0)
                                @foreach ($users as $user)
                                    <tr>
                                        <!-- {{-- <td> <label><input type="checkbox" name="sample[]"/></label></td> --}} -->
                                        <!-- <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" name="sample[]" class="custom-control-input selectone" id="customCheck{{$user->id}}">
                                            <label class="custom-control-label" for="customCheck{{$user->id}}"></label>
                                        </div>
                                        </td> -->
                                        <td><input style="width:18px; height:18px; left:0;" type="checkbox" class="checkBoxClass selectall" id="Checkbox1" />
                                <input type="checkbox" name="sample[]" class="custom-control-input selectone"
                                    id="customCheck{{$user->id}}">
                            </td>
                                        <td>{{ $user->name ?? '' }}</td>
                                        <td>{{ $user->email ?? '' }}</td>
                                            <td>
                                                Active
                                            </td>
                                        <td>
                                            <div class="dropdown">
                                                <button type="button" class="btn btn-sm dropdown-toggle hide-arrow"
                                                    data-toggle="dropdown">
                                                    <i class="fas fa-ellipsis-v"></i>
                                                </button>
                                                <div class="dropdown-menu">
                                                    <a class="dropdown-item" href="{{ route('admin.users.self.show') }}">
                                                        {{-- <i data-feather="show" class="mr-50"></i> --}}
                                                        <i class="fa fa-eye"></i>
                                                        <span>Show</span>
                                                    </a>
                                                    <a class="dropdown-item"
                                                        href="{{ route('admin.users.self.edit', $user->id) }}">
                                                        {{-- <i data-feather="edit-2" class="mr-50"></i> --}}
                                                        <i class="fa fa-edit"></i>
                                                        <span>Edit</span>
                                                    </a>
                                                    <a class="dropdown-item"
                                                        href="{{ route('admin.users.self.destroy', $user->id) }}">
                                                        {{-- <i data-feather="trash" class="mr-50"></i> --}}
                                                        <i class="fa fa-trash"></i>
                                                        <span>Delete</span>
                                                    </a>
                                                    <a class="dropdown-item"
                                                        href="#">
                                                        {{-- <i data-feather="train" class="mr-50"></i> --}}
                                                        <i class="fa fa-check"></i>
                                                        <span>Approve</span>
                                                    </a>

                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- Basic Tables end -->
@endsection


@section('script')
    <!-- BEGIN: Page JS-->
    <script src="{{ asset('app-assets/js/scripts/pages/dashboard-ecommerce.js') }}"></script>
    <!-- END: Page JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/responsive.bootstrap4.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/vfs_fonts.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.rowGroup.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
        <!-- END: Page Vendor JS-->
        <script>
       function EuToUsCurrencyFormat(input) {
	return input.replace(/[,.]/g, function(x) {
		return x == "," ? "." : ",";
	});
     }

     $(document).ready(function() {
	// DataTable initialisation
	$('#example').DataTable({
	
	});
    });
    
    </script>


<script>
    $(document).ready(function() {
        var table = $('#example').DataTable();

        $("#example .xy th").each(function(i) {
            if (i != 3) {
                var select = $('<select class="form-control form-control-sm"><option value="">Filter</option></select>')
                    .appendTo($(this).empty())
                    .on('change', function() {
                        table.column(i)
                            .search($(this).val())
                            .draw();
                    });

                table.column(i).data().unique().sort().each(function(d, j) {
                    console.log(d)
                    select.append('<option value="' + d + '">' + d + '</option>')
                });

            }
        });

    });

</script>
<script>
    setTimeout(() => {
            // $(".create-new").hide(); 
            $(".action").hide();
            })
        }, 10);
</script>
<!-- <script>
    $('.selectall').click(function() {
    if ($(this).is(':checked')) {
      if  $('selectone').is('checked', true);
        $('#qft').html('<button type="button" class="btn btn-success">Approve</button>');
    } else {
        $('div input').attr('checked', false);
        $('#qft').html('');
    }
});
$('.selectone').click(function() {
    if ($(this).is(':checked')) {
        $('#qft').html('<button type="button" class="btn btn-success">Approve</button>');
    } else {
        $('.selectall').attr('checked', false);
    }
});
</script> -->

<script>

$('.selectall').click(function() {
    // console.log('hii')
    if ($(this).is(':checked')) {
        if ($('.selectone').is(':checked')) {

            console.log('checked')
            $('.selectone').attr('checked', false);
        } else {
            $('div input').attr('checked', true);
            $('#qft').html('<button type="button" class="btn btn-success">Approve</button>');
        }
        
    } else {
        $('div input').attr('checked', false);
        $('#qft').html('');
    }
   
});
$('.selectone').click(function() {
    console.log('sfsf');
    if ($(this).is(':checked')) {
        $('#qft').html('<button type="button" class="btn btn-success">Approve</button>');
    } else {
        $(this).attr('checked', false);
        $('#qft').html('');
    }

});

</script>


<script>
$(document).ready(function() {
    $("#ckbCheckAll").click(function() {

        $(".checkBoxClass").prop('checked', $(this).prop('checked'));
       
    });
  
    $(".checkBoxClass").click(function() {
if ($(this).prop('checked') == false)
{
    console.log($(this).prop('checked'))
    $("#ckbCheckAll").prop('checked', false);
}
});
$('.checkBoxClass').on('click',function(){
        if($('.checkBoxClass:checked').length == $('.checkBoxClass').length){
            $('#ckbCheckAll').prop('checked',true);
        }else{
            $('#ckbCheckAll').prop('checked',false);
        }
    });
});
</script>

@endsection
