@extends('layout.master')
@section('title', 'Users')
@section('description', 'Users of FARS')
@section('keywords', 'admin panel, fars')
@section('author', 'FARS')

@section('css')
 <!-- BEGIN: Vendor CSS-->

<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/file-uploaders/dropzone.min.css') }}">
<!-- END: Vendor CSS-->
 <!-- BEGIN: Vendor CSS-->
 
 <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/swiper.min.css') }}">
 <!-- END: Vendor CSS-->
<!-- BEGIN: Page CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/form-file-uploader.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/select/select2.min.css') }}">
<!-- END: Page CSS-->
   <!-- BEGIN: Page CSS-->
   <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-swiper.css') }}">
   <!-- END: Page CSS-->

   

@endsection
@section('breadcrumb_title', 'Users')
@section('breadcrumb_2', 'Users')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <button class="btn btn-info" style="float: right; margin-bottom: 4px;">Approve</button>
        <button class="btn btn-success" style="float: right; margin-bottom: 4px; margin-right: 4px;">Queue for Training</button>
        <button class="btn btn-primary" style="float: right; margin-bottom: 4px; margin-right: 4px;">Edit</button>
    </div>
</div>
    <section id="multiple-column-form">
        <form id="users" action="{{ route('admin.users.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">User Detail</h4>
                        </div>
                        <div class="card-body">
                          <div class="card-body">
                            <div class="table-responsive" >
                                <table class="table table-hover table-striped">
                
                                  <tbody>
                                  <tr>
                                      <th scope="row">User Name</th>
                                      <td>Test </td>
                                  </tr>
                  
                                  <tr>
                                      <th scope="row">User Email</th>
                                      <td>test@gmail.com</td>
                                  </tr>
                                  <tr>
                                      <th scope="row">Status</th>
                                      <td>Status </td>
                                  </tr>
                                  <tr>
                                      <th scope="row">Model</th>
                                      <td>Model 1 </td>
                                  </tr>
                                  </tbody>
                
                              </table>
                            </div>  
                          </div>
                        </div>
                        {{-- <div class="col-md-6 col-12">
                          <div class="form-group">
                            <label for="inputEstimatedBudget">Password</label>
                            <input type="password" name="password" id="inputEstimatedBudget" class="form-control">
                          </div>
                        </div>
                        <div class="col-md-6 col-12">
                          <div class="form-group">
                            <label for="inputEstimatedBudget">Image</label>
                            <input type="file" name="image" id="inputEstimatedBudget" class="form-control">
                          </div>
                        </div>
                        <div class="col-12 my-3">
                            <button type="reset" class="btn btn-primary mr-1">Submit</button>
                            <button type="reset" class="btn btn-outline-secondary">Reset</button>
                        </div> --}}
                    </div>
                </div>
            </div>
        </form> 
    </section>
    {{-- <section id="component-swiper-multiple">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">User Images</h4>
            </div>
            <div class="card-body">
                <div class="swiper-multiple swiper-container swiper-container-initialized swiper-container-horizontal">
                    <div class="swiper-wrapper" id="swiper-wrapper-510fbd61cea7421cb" aria-live="polite" style="transform: translate3d(0px, 0px, 0px); transition: all 0ms ease 0s;">
                        <div class="swiper-slide swiper-slide-active" role="group" aria-label="1 / 5" style="width: 310.333px; margin-right: 30px;">
                            <img class="img-fluid" src="{{ asset('app-assets/images/banner/banner-31.jpg') }}" alt="banner">
                            <span style="font-size: 18px;">Tag 1, Tag 2, Tag 3, Tag 11, Tag 12</span>
                        </div>
                        <div class="swiper-slide swiper-slide-next" role="group" aria-label="2 / 5" style="width: 310.333px; margin-right: 30px;">
                            <img class="img-fluid" src="{{ asset('app-assets/images/banner/banner-32.jpg') }}" alt="banner">
                            <span style="font-size: 18px;"> Tag 10, Tag 12</span>
                        </div>
                        <div class="swiper-slide" role="group" aria-label="3 / 5" style="width: 310.333px; margin-right: 30px;">
                            <img class="img-fluid" src="{{ asset('app-assets/images/banner/banner-33.jpg') }}" alt="banner">
                            <span style="font-size: 18px;">Tag 4, Tag 5, Tag 6</span>
                        </div>
                        <div class="swiper-slide" role="group" aria-label="4 / 5" style="width: 310.333px; margin-right: 30px;">
                            <img class="img-fluid" src="{{ asset('app-assets/images/banner/banner-34.jpg') }}" alt="banner">
                            <span style="font-size: 18px;">Tag 1, Tag 2, Tag 3</span>
                        </div>
                        <div class="swiper-slide" role="group" aria-label="5 / 5" style="width: 310.333px; margin-right: 30px;">
                            <img class="img-fluid" src="{{ asset('app-assets/images/banner/banner-35.jpg') }}" alt="banner">
                            <span style="font-size: 18px;">Tag 1, Tag 2, Tag 8</span>
                        </div>
                    </div>
                    <!-- Add Pagination -->
                    <div class="swiper-pagination swiper-pagination-clickable swiper-pagination-bullets"><span class="swiper-pagination-bullet swiper-pagination-bullet-active" tabindex="0" role="button" aria-label="Go to slide 1"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 2"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 3"></span></div>
                <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div>
            </div>
        </div>
    </section> --}}

    <section id="component-swiper-multiple">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">User Images</h4>
            </div>
            <div class="card-body">
                <div class="swiper-multiple swiper-container swiper-container-initialized swiper-container-horizontal">
                    <div class="swiper-wrapper" id="swiper-wrapper-510fbd61cea7421cb" aria-live="polite" style="transform: translate3d(0px, 0px, 0px); transition: all 0ms ease 0s;">
                        <div class="swiper-slide swiper-slide-active" role="group" aria-label="1 / 5" style="width: 310.333px; margin-right: 30px;">
                            <img class="img-fluid" src="{{ asset('app-assets/images/banner/banner-31.jpg') }}" alt="banner">
                            <span style="font-size: 18px;">Tag 1, Tag 2, Tag 3, Tag 11, Tag 12</span>
                        </div>
                        <div class="swiper-slide swiper-slide-next" role="group" aria-label="2 / 5" style="width: 310.333px; margin-right: 30px;">
                            <img class="img-fluid" src="{{ asset('app-assets/images/banner/banner-32.jpg') }}" alt="banner">
                            <span style="font-size: 18px;"> Tag 10, Tag 12</span>
                        </div>
                        <div class="swiper-slide" role="group" aria-label="3 / 5" style="width: 310.333px; margin-right: 30px;">
                            <img class="img-fluid" src="{{ asset('app-assets/images/banner/banner-33.jpg') }}" alt="banner">
                            <span style="font-size: 18px;">Tag 4, Tag 5, Tag 6</span>
                        </div>
                        <div class="swiper-slide" role="group" aria-label="4 / 5" style="width: 310.333px; margin-right: 30px;">
                            <img class="img-fluid" src="{{ asset('app-assets/images/banner/banner-34.jpg') }}" alt="banner">
                            <span style="font-size: 18px;">Tag 1, Tag 2, Tag 3</span>
                        </div>
                        <div class="swiper-slide" role="group" aria-label="5 / 5" style="width: 310.333px; margin-right: 30px;">
                            <img class="img-fluid" src="{{ asset('app-assets/images/banner/banner-35.jpg') }}" alt="banner">
                            <span style="font-size: 18px;">Tag 1, Tag 2, Tag 8</span>
                        </div>
                    </div>
                    <!-- Add Pagination -->
                    <div class="swiper-pagination swiper-pagination-clickable swiper-pagination-bullets"><span class="swiper-pagination-bullet swiper-pagination-bullet-active" tabindex="0" role="button" aria-label="Go to slide 1"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 2"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 3"></span></div>
                <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div>
            </div>
        </div>
    </section>
@endsection



@section('script')

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('app-assets/vendors/js/extensions/dropzone.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/extensions/swiper.min.js') }}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Page JS-->  
    <script src="{{ asset('app-assets/js/scripts/forms/form-select2.js') }}"></script>
    <script src="{{ asset('app-assets/js/scripts/extensions/ext-component-swiper.js') }}"></script>
    <!-- END: Page JS-->

    
@endsection
