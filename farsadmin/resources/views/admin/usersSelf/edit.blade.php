@extends('layout.master')
@section('title', 'Users')
@section('description', 'Users of FARS')
@section('keywords', 'admin panel, fars')
@section('author', 'FARS')

@section('css')
    <!-- BEGIN: Vendor CSS-->

    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/file-uploaders/dropzone.min.css') }}">
    <!-- END: Vendor CSS-->
    
    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/form-file-uploader.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/select/select2.min.css') }}">
    <!-- END: Page CSS-->
@endsection
@section('breadcrumb_title', 'Users')
@section('breadcrumb_2', 'Users')
@section('content')

    <section id="multiple-column-form">
        <form class="row" id="users" action="{{ route('admin.users.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Edit User</h4>
                        </div>
                        <div class="card-body">
                            <form class="form">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="first-name-column">User Name</label>
                                            <input type="text" id="first-name-column" class="form-control"
                                                placeholder="User Name" name="name" />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="last-name-column">User Email</label>
                                            <input type="email" id="last-name-column" class="form-control"
                                                placeholder="User Email" name="email" />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                      <div class="form-group">
                                        <label for="inputStatus">Status</label>
                                        <select id="inputStatus" name="status" class="form-control custom-select">
                                            <option selected disabled>Status</option>
                                            <option value="1">Active</option>
                                            <option value="0">In Active</option>

                                        </select>
                                      </div>
                                    </div>
                                    {{-- <div class="col-md-6 col-12">
                                    <div class="form-group">
                                      <label for="inputModel">Model</label>
                                      <select id="inputModel"  name="model" class="form-control custom-select">
                                        <option selected disabled>Select Model</option>
                                        <option value="1">Model 1</option>
                                        <option value="2" >Model 2</option>
                                        <option value="3" >Model 3</option>
                                      </select>
                                    </div>
                                    </div> --}}
                                    <div class="col-md-6 col-12">
                                      <div class="form-group">
                                        <label for="inputEstimatedBudget">Password</label>
                                        <input type="password" name="password" id="inputEstimatedBudget" class="form-control">
                                      </div>
                                    </div>
                                    {{-- <div class="col-md-6 col-12">
                                      <div class="form-group">
                                        <label for="inputEstimatedBudget">Image</label>
                                        <input type="file" name="image" id="inputImage" class="form-control">
                                      </div>
                                    </div> --}}
                                    <div class="col-12">
                                        <button type="reset" class="btn btn-primary mr-1">Submit</button>
                                        <button type="reset" class="btn btn-outline-secondary">Reset</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        
        <!-- remove all thumbnails file upload starts -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Upload User Images</h4>
                    </div>
                    <div class="card-body">
                        <p class="card-text">
                        {{-- This example allows user to create a button that will remove all files from a dropzone. Hear for the
                            button's click event and then call <code>removeAllFiles</code> method to remove all the files from the
                            dropzone. --}}
                        </p>
                        <button id="clear-dropzone" class="btn btn-outline-primary mb-1">
                            <i data-feather="trash" class="mr-25"></i>
                            <span class="align-middle">Clear Drop</span>
                        </button>
                        <form action="3" class="dropzone dropzone-area" id="dpz-remove-all-thumb">
                            <div class="dz-message">Drop files here or click to upload.</div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- remove all thumbnails file upload ends -->
    </section>

     

    <!-- Modal -->
    <div class="modal fade text-left" id="default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" id="myModalLabel1">Select Tags</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            {{-- <h5>Check First Paragraph</h5> --}}
                                                            <div class="col-xl-12 col-md-12 col-12 mb-1">
                                                                <label>Select Tags</label>
                                                                <select class="form-control custom-select" data-placeholder="Select Tags">
                                                                    <option value="0">Tag1</option>
                                                                    <option value="0">Tag2</option>
                                                                    <option value="0">Tag3</option>
                                                                    <option value="0">Tag4</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-primary" data-dismiss="modal">Submit</button>
                                                        </div>
                                                    </div>
                                                </div>
    </div>
@endsection

@section('script')

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('app-assets/vendors/js/extensions/dropzone.min.js') }}"></script>
    <!-- END: Page Vendor JS-->
    <!-- BEGIN: Page JS-->
    
    <script src="{{ asset('app-assets/js/scripts/forms/form-select2.js') }}"></script>
    <!-- END: Page JS-->

@endsection
