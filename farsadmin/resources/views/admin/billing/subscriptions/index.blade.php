@extends('layout.master')
@section('title', 'Subscription')
@section('description', 'Subscription of FARS')
@section('keywords', 'admin panel, fars')
@section('author', 'FARS')

@section('css')
<link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
   <style>
    #example_filter {
        float: right;
    }
    #example_paginate {
        float: right;
    }
        /* width */
 ::-webkit-scrollbar {
 width: 10px;
 }
 
 /* Track */
 ::-webkit-scrollbar-track {
 background: #f1f1f1;
 
 }
 
 /* Handle */
 ::-webkit-scrollbar-thumb {
 background: #958cf4;
 border-radius: 10px;
 }
 
 /* Handle on hover */
 ::-webkit-scrollbar-thumb:hover {
 background: #958cf4;
 }
  </style>
@endsection
@section('breadcrumb_title', 'Subscription')
@section('breadcrumb_2', 'Subscription')

    @section('content')

     <!-- Basic Tables start -->
     <div class="row" id="basic-table">
      <div class="col-12">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title">Current Subscription</h4>
                  <a href="#" style="float: right;" class="btn btn-secondary">Upgrade</a>
              </div>
              <div class="table-responsive  container-fluid">
                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Name</th>
                          <th>Type</th>
                          <th>Billing</th>
                          <th>Status</th>
                          <th>Price</th>
                          <th>Purchased Date</th>
                          <th>End Date</th>
                          <th>Quota Used</th>
                          <th>Total Quota</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                          <tr>
                            <td>{{ $current_package['package']['name'] ?? '' }}</td>
                            <td>{{ config('helper.package_type')[$current_package['package']['package_type']] ?? '' }}</td>
                            <td>{{ config('helper.billing_type')[$current_package['package']['billing_type']] ?? '' }}</td>
                            <td>{{ config('helper.status')[$current_package['package']['status']] ?? '' }}</td>
                            <td>{{ config('helper.currency')[$current_package['package']['currency']] ?? '' }}{{ $current_package['package']['amount'] ?? '' }}</td>
                            <td>{{ $current_package['start_date'] ?? '' }}</td>
                            <td>{{ $current_package['end_date'] ?? '' }}</td>
                            <td>10000</td>
                            <td>{{ $current_package['package']['users_for_facial_recognition'] ?? '' }}</td>
                            
                            <td>
                                  <div class="dropdown">
                                      <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                                          <i class="fas fa-ellipsis-v"></i>
                                      </button>
                                      <div class="dropdown-menu">
                                       
                                        <a class="dropdown-item" href="#">
                                            {{-- <i data-feather="view" class="mr-50"></i> --}}
                                            <i class="fa fa-upgrade"></i>
                                            <span>Upgrade</span>
                                        </a>
                                       
                                        
                                      </div>
                                  </div>
                              </td>
                          </tr>
                        
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
    </div>
  <!-- Basic Tables end -->


    <!-- Basic Tables start -->
    <div class="row" id="basic-table">
      <div class="col-12">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title">Past Subscription</h4>
                  <a href="#" style="float: right;" class="btn btn-secondary">Upgrade</a>
              </div>
              <div class="table-responsive  container-fluid">
                {{-- <div class="row">
                    <div class="col-lg-3">
                      <div class="form-group">
                        <select class="form-control">
                          <option value="">Filter By Status</option>
                          <option>option 1</option>
                          <option>option 2</option>
                          <option>option 3</option>
                          <option>option 4</option>
                          <option>option 5</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-lg-3">
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="Search by name">
                      </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                          <button class="btn btn-success" type="button">Apply Filters</button>
                        </div>
                      </div>
                </div> --}}
                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Name</th>
                          <th>Type</th>
                          <th>Billing</th>
                          <th>Status</th>
                          <th>Price</th>
                          <th>Purchased Date</th>
                          <th>End Date</th>
                          {{-- <th>Action</th> --}}
                        </tr>
                      </thead>
                      <tbody>
                         @if($userpurchasedpackages != '' && count($userpurchasedpackages) > 0)
                            @foreach($userpurchasedpackages as $userpurchasedpackage)

                            @if(isset($userpurchasedpackage['package_id']))
                            <tr>
                              <td>{{ $packages[$userpurchasedpackage['package_id']]['name'] ?? '' }}</td>
                              <td>{{ $packages[$userpurchasedpackage['package_id']]['package_type'] ?? '' }}</td>
                              <td>{{ $packages[$userpurchasedpackage['package_id']]['billing_type'] ?? '' }}</td>
                              <td>{{ $packages[$userpurchasedpackage['package_id']]['status'] ?? '' }}</td>
                              <td>{{ $packages[$userpurchasedpackage['package_id']]['currency'] ?? '' }} {{ $packages[$userpurchasedpackage['package_id']]['amount'] ?? '' }}</td>
                              <td>{{ $userpurchasedpackage['start_date'] ?? '' }}</td>
                              <td>{{ $userpurchasedpackage['end_date'] ?? '' }}</td>
                            </tr>
                            @endif

                            @endforeach
                         @endif
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
    </div>
  <!-- Basic Tables end -->
@endsection



@section('script')
    <!-- BEGIN: Page JS-->
    <script src="{{ asset('app-assets/js/scripts/pages/dashboard-ecommerce.js') }}"></script>
    <!-- END: Page JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/responsive.bootstrap4.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/vfs_fonts.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.rowGroup.min.js') }}"></script>
        <script src="{{ asset('app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
        <!-- END: Page Vendor JS-->
        <script>
       function EuToUsCurrencyFormat(input) {
	return input.replace(/[,.]/g, function(x) {
		return x == "," ? "." : ",";
	});
}

$(document).ready(function() {
	//Only needed for the filename of export files.
	//Normally set in the title tag of your page.
	// document.title = 'DataTable Excel';
	// DataTable initialisation
	$('#example').DataTable({
		// "dom": '<"dt-buttons"Bf><"clear">lirtp',
		// "paging": true,
		// "autoWidth": true,
		// "buttons": [{
		// 	extend: 'excelHtml5',
		// 	text: 'Excel',
		// 	customize: function(xlsx) {
		// 		var sheet = xlsx.xl.worksheets['sheet1.xml'];
		// 		//All cells
		// 		$('row c', sheet).attr('s', '25');
		// 		//Second column
		// 		$('row c:nth-child(2)', sheet).attr('s', '42');
		// 		//First row
		// 		$('row:first c', sheet).attr('s', '36');
		// 		// One cell
		// 		$('row c[r^="D6"]', sheet).attr('s', '32');
		// 		// Loop over the cells in column `E` the amount column
		// 		$('row c[r^="E"]', sheet).each(function() {
		// 			if (parseFloat(EuToUsCurrencyFormat($('is t', this).text())) > 1500) {
		// 				$(this).attr('s', '17');
		// 			}
		// 		});
		// 		//All cells of row 10
		// 		$('row c[r*="10"]', sheet).attr('s', '49');
		// 		//Search all cells for a specific text
		// 		$('row* c[r]', sheet).each(function() {
		// 			if ($('is t', this).text().match(/(?:^|\b)(cover)(?=\b|$)/gmi)) {
		// 				$(this).attr('s', '20');
		// 			}
		// 		});
		// 	}
		// }]
	});
});
    
    </script>
@endsection
