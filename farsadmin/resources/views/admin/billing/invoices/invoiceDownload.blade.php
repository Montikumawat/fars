<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>FARS Invoice of {{ $order['user']['user']['name'] ?? '' }} for {{ $order['package']['package']['name'] ?? '' }}</title>

		<style>
			.invoice-box {
				max-width: 800px;
				margin: auto;
				padding: 30px;
				border: 1px solid #eee;
				box-shadow: 0 0 10px rgba(0, 0, 0, 0.15);
				font-size: 16px;
				line-height: 24px;
				font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
				color: #555;
			}

			.invoice-box table {
				width: 100%;
				line-height: inherit;
				text-align: left;
			}

			.invoice-box table td {
				padding: 5px;
				vertical-align: top;
			}

			.invoice-box table tr td:nth-child(2) {
				text-align: right;
			}

			.invoice-box table tr.top table td {
				padding-bottom: 20px;
			}

			.invoice-box table tr.top table td.title {
				font-size: 45px;
				line-height: 45px;
				color: #333;
			}

			.invoice-box table tr.information table td {
				padding-bottom: 40px;
			}

			.invoice-box table tr.heading td {
				background: #eee;
				border-bottom: 1px solid #ddd;
				font-weight: bold;
			}

			.invoice-box table tr.details td {
				padding-bottom: 20px;
			}

			.invoice-box table tr.item td {
				border-bottom: 1px solid #eee;
			}

			.invoice-box table tr.item.last td {
				border-bottom: none;
			}

			.invoice-box table tr.total td:nth-child(2) {
				border-top: 2px solid #eee;
				font-weight: bold;
			}

			@media only screen and (max-width: 600px) {
				.invoice-box table tr.top table td {
					width: 100%;
					display: block;
					text-align: center;
				}

				.invoice-box table tr.information table td {
					width: 100%;
					display: block;
					text-align: center;
				}
			}

			/** RTL **/
			.invoice-box.rtl {
				direction: rtl;
				font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
			}

			.invoice-box.rtl table {
				text-align: right;
			}

			.invoice-box.rtl table tr td:nth-child(2) {
				text-align: left;
			}
		</style>
	</head>

	<body>
		<div class="invoice-box">
            <table cellpadding="0" cellspacing="0">
                <tr class="top">
                    <td colspan="2">
                        <table>
                            <tr>
                                <td class="title">
                                    {{-- <img src="https://www.sparksuite.com/images/logo.png" style="width: 100%; max-width: 300px" /> --}}
                                    <h1>Logo Here</h1>
                                </td>
                                {{-- <a href={{route('superadmin.userAccount.invoice.download',['id' => 2])}} class="btn btn-outline-secondary" title="Download Invoice" style="float: right;">  <i class="fa fa-download"></i> </a> --}}
                                <td>
                                    Invoice #: {{ $order['id'] ?? '' }}<br />
                                    Created: {{ $order['transaction_date'] ?? '' }}<br />
                                    Due: {{ $order['userpurchasedpackage']['userpurchasedpackage']['end_date'] ?? '' }}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
    
                <tr class="information">
                    <td colspan="2">
                        <table>
                            <tr>
                                <td>
                                    FARS<br />
                                    Address<br />
                                    State, Country Pin Code
                                </td>
    
                                <td>
                                    {{-- Acme Corp.<br /> --}}
                                    {{ $order['user']['user']['name'] ?? '' }}<br />
                                    {{ $order['user']['user']['email'] ?? '' }}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
    
                <tr class="heading">
                    <td>Payment Method</td>
                    <td>Transaction ID</td>

                </tr>
    
                <tr class="details">
                    <td>{{ $order['payment_type'] ?? '' }}</td>

                    <td>{{ $order['transaction_id'] ?? '' }}</td>
    
                </tr>
    
                <tr class="heading">
                    <td>Item</td>
    
                    <td>Price</td>
                </tr>
    
                <tr class="item">
                    <td>{{ $order['package']['package']['name'] ?? '' }}</td>

                    <td>@if(isset($order['package']['package']) && $order['package']['package'] != []) @if($order['package']['package']['currency'] == 'INR') <span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> @endif  @if($order['package']['package']['currency'] == 'USD') <span style="font-family: DejaVu Sans; sans-serif;">&#36;</span> @endif @endif{{ $order['package']['package']['amount'] ?? '' }}</td>
                </tr>
    
    
                <tr class="total">
                    <td></td>
    
                    <td>Total:  @if($order['currency'] == 'INR') <span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> @endif @if($order['currency'] == 'USD') <span style="font-family: DejaVu Sans; sans-serif;">&#36;</span> @endif {{ $order['amount'] ?? '' }}</td>
                </tr>
            </table>
        </div>
	</body>
</html>