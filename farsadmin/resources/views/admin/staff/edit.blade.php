@extends('layout.master')
@section('title', 'Edit Staff')
@section('description', 'Edit Staff')
@section('keywords', 'admin panel, fars')
@section('author', 'FARS')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/select/select2.min.css') }}">
@endsection
@section('breadcrumb_title', 'Edit Staff')
@section('breadcrumb_2', 'Edit Staff')
@section('content')
    <section id="multiple-column-form">
        <form class="row" id="users" action="#" method="POST"
            enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Edit Staff</h4>
                        </div>
                        <div class="card-body">
                            <form class="form">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="inputName">Staff Name</label>
                                            <input type="text" id="inputName" name="name" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="inputDescription">Staff Email</label>
                                            <input type="email" id="inputName" name="email" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="inputPasswordReset">Staff Password Reset</label>
                                            <input type="password" id="inputPasswordReset" name="passwordReset"
                                                class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="inputStatus">Status</label>
                                            <select id="inputStatus" name="status" class="form-control custom-select">
                                                <option selected disabled>Status</option>
                                                <option value="1">Active</option>
                                                <option value="0">In Active</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="inputModel">Role</label>
                                            <select id="inputModel" name="role" class="form-control custom-select">
                                                <option selected disabled>Select Role</option>
                                                <option value="1">Staff</option>
                                                <option value="2">SubAdmin</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-1">
                                      <label>Role Permission</label>
                                      <select class="select2 form-control select2-hidden-accessible" multiple="" data-select2-id="7" tabindex="-1" aria-hidden="true">
                                        <optgroup label="Select Role Permission">
                                          <option value="0">User 1</option>
                                          <option value="1">User 2</option>
                                          <option value="2">User 3</option>
                                          <option value="3">User 4</option>
                                      </optgroup>
                                      </select>
                                    </div>

                                    <div class="col-md-6 mb-1" data-select2-id="52">
                                        <label>Multiple</label>
                                        <div class="position-relative" data-select2-id="51">
                                        
                                    </div>

                                    <div class="col-12">
                                        <button type="reset" class="btn btn-primary mr-1">Submit</button>
                                        <button type="reset" class="btn btn-outline-secondary">Reset</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection


@section('script')

  <!-- BEGIN: Page Vendor JS-->
  <script src="{{ asset('app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
  <!-- END: Page Vendor JS-->

      <!-- BEGIN: Page JS-->
      <script src="{{ asset('app-assets/js/scripts/forms/form-select2.js') }}"></script>
      <!-- END: Page JS-->

@endsection
