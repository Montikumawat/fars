@extends('layout.master')
@section('title', 'Staff')
@section('description', 'Staff of FARS')
@section('keywords', 'admin panel, fars')
@section('author', 'FARS')

@section('css')

@endsection
@section('breadcrumb_title', 'Staff')
@section('breadcrumb_2', 'Staff')

@section('content')
    <section id="multiple-column-form">
        <form id="users" action="{{ route('admin.users.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Staff</h4>
                        </div>
                        <div class="card-body">
                          <div class="card-body">
                            <div class="table-responsive" >
                                <table class="table table-hover table-striped">
                
                                  <tbody>
                                  <tr>
                                      <th scope="row">Staff Name</th>
                                      <td>Staff 1 </td>
                                  </tr>
                  
                                  <tr>
                                      <th scope="row">Staff Email</th>
                                      <td>staff@gmail.com</td>
                                  </tr>
                                  <tr>
                                      <th scope="row">Status</th>
                                      <td>Status </td>
                                  </tr>
                                  <tr>
                                      <th scope="row">Role</th>
                                      <td>Admin</td>
                                  </tr>
                                  </tbody>
                
                              </table>
                            </div>  
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </form> 
    </section>
@endsection

@section('script')

@endsection
