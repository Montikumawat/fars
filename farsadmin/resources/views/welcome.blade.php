@extends('frontend.layout.master')
@section('content')
<!-- ======= Hero Section ======= -->

<section id="hero" class="hero d-flex align-items-center">

    <div class="container">
        <div class="row">
            <div class="col-lg-6 d-flex flex-column justify-content-center">
                <h1 data-aos="fade-up">We offer modern solutions for growing your business</h1>
                <h2 data-aos="fade-up" data-aos-delay="400">We are team of talented designers making websites with
                    Bootstrap</h2>
                 @if(request()->session()->get('authenticated') != 'true' )
                <div data-aos="fade-up" data-aos-delay="600">
                    <div class="text-center text-lg-start">
                        <a href="{{ route('package.select') }}"
                            class="btn-get-started scrollto d-inline-flex align-items-center justify-content-center align-self-center">
                            <span>Get Started</span>
                            <i class="bi bi-arrow-right"></i>
                        </a>
                    </div>
                </div>

                @endif
            </div>
            <div class="col-lg-6 hero-img" data-aos="zoom-out" data-aos-delay="200">
                <img src="{{ asset('assets/img/hero-img.png') }}" class="img-fluid" alt="">
            </div>
        </div>
    </div>

</section>
<!-- End Hero -->

<main id="main">
    <!-- ======= About Section ======= -->
    <section id="about" class="about">

        <div class="container" data-aos="fade-up">
            <div class="row gx-0">

                <div class="col-lg-6 d-flex flex-column justify-content-center" data-aos="fade-up" data-aos-delay="200">
                    <div class="content">
                        <h3>Who We Are</h3>
                        <h2>Expedita voluptas omnis cupiditate totam eveniet nobis sint iste. Dolores est repellat
                            corrupti reprehenderit.</h2>
                        <p>
                            Quisquam vel ut sint cum eos hic dolores aperiam. Sed deserunt et. Inventore et et dolor
                            consequatur itaque ut voluptate sed et. Magnam nam ipsum tenetur suscipit voluptatum nam
                            et est corrupti.
                        </p>
                        <div class="text-center text-lg-start">
                            <a href="#"
                                class="btn-read-more d-inline-flex align-items-center justify-content-center align-self-center">
                                <span>Read More</span>
                                <i class="bi bi-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 d-flex align-items-center" data-aos="zoom-out" data-aos-delay="200">
                    <img src="{{ asset('assets/img/about.jpg') }}" class="img-fluid" alt="">
                </div>

            </div>
        </div>

    </section>
    <!-- End About Section -->

    <!-- ======= Pricing Section ======= -->
    <!-- {{-- <section id="pricing" class="pricing">

        <div class="container" data-aos="fade-up">

            <header class="section-header">
                <h2>Pricing</h2>
                <p>Check our Pricing</p>
            </header>

            <div class="row gy-4" data-aos="fade-left">

                <div class="col-lg-3 col-md-6" data-aos="zoom-in" data-aos-delay="100">
                    <div class="box">
                        <h3 style="color: #07d5c0;">Free Plan</h3>
                        <div class="price"><sup>$</sup>0<span> / mo</span></div>
                        <img src="assets/img/pricing-free.png" class="img-fluid" alt="">
                        <ul>
                            <li>Aida dere</li>
                            <li>Nec feugiat nisl</li>
                            <li>Nulla at volutpat dola</li>
                            <li class="na">Pharetra massa</li>
                            <li class="na">Massa ultricies mi</li>
                        </ul>
                        <a href="#" class="btn-buy">Buy Now</a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6" data-aos="zoom-in" data-aos-delay="200">
                    <div class="box">
                        <span class="featured">Featured</span>
                        <h3 style="color: #65c600;">Starter Plan</h3>
                        <div class="price"><sup>$</sup>19<span> / mo</span></div>
                        <img src="assets/img/pricing-starter.png" class="img-fluid" alt="">
                        <ul>
                            <li>Aida dere</li>
                            <li>Nec feugiat nisl</li>
                            <li>Nulla at volutpat dola</li>
                            <li>Pharetra massa</li>
                            <li class="na">Massa ultricies mi</li>
                        </ul>
                        <a href="#" class="btn-buy">Buy Now</a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6" data-aos="zoom-in" data-aos-delay="300">
                    <div class="box">
                        <h3 style="color: #ff901c;">Business Plan</h3>
                        <div class="price"><sup>$</sup>29<span> / mo</span></div>
                        <img src="assets/img/pricing-business.png" class="img-fluid" alt="">
                        <ul>
                            <li>Aida dere</li>
                            <li>Nec feugiat nisl</li>
                            <li>Nulla at volutpat dola</li>
                            <li>Pharetra massa</li>
                            <li>Massa ultricies mi</li>
                        </ul>
                        <a href="#" class="btn-buy">Buy Now</a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6" data-aos="zoom-in" data-aos-delay="400">
                    <div class="box">
                        <h3 style="color: #ff0071;">Ultimate Plan</h3>
                        <div class="price"><sup>$</sup>49<span> / mo</span></div>
                        <img src="assets/img/pricing-ultimate.png" class="img-fluid" alt="">
                        <ul>
                            <li>Aida dere</li>
                            <li>Nec feugiat nisl</li>
                            <li>Nulla at volutpat dola</li>
                            <li>Pharetra massa</li>
                            <li>Massa ultricies mi</li>
                        </ul>
                        <a href="#" class="btn-buy">Buy Now</a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6" data-aos="zoom-in" data-aos-delay="100">
                    <div class="box">
                        <h3 style="color: #07d5c0;">Free Plan</h3>
                        <div class="price"><sup>$</sup>0<span> / mo</span></div>
                        <img src="assets/img/pricing-free.png" class="img-fluid" alt="">
                        <ul>
                            <li>Aida dere</li>
                            <li>Nec feugiat nisl</li>
                            <li>Nulla at volutpat dola</li>
                            <li class="na">Pharetra massa</li>
                            <li class="na">Massa ultricies mi</li>
                        </ul>
                        <a href="#" class="btn-buy">Buy Now</a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6" data-aos="zoom-in" data-aos-delay="200">
                    <div class="box">
                        <span class="featured">Featured</span>
                        <h3 style="color: #65c600;">Starter Plan</h3>
                        <div class="price"><sup>$</sup>19<span> / mo</span></div>
                        <img src="assets/img/pricing-starter.png" class="img-fluid" alt="">
                        <ul>
                            <li>Aida dere</li>
                            <li>Nec feugiat nisl</li>
                            <li>Nulla at volutpat dola</li>
                            <li>Pharetra massa</li>
                            <li class="na">Massa ultricies mi</li>
                        </ul>
                        <a href="#" class="btn-buy">Buy Now</a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6" data-aos="zoom-in" data-aos-delay="300">
                    <div class="box">
                        <h3 style="color: #ff901c;">Business Plan</h3>
                        <div class="price"><sup>$</sup>29<span> / mo</span></div>
                        <img src="assets/img/pricing-business.png" class="img-fluid" alt="">
                        <ul>
                            <li>Aida dere</li>
                            <li>Nec feugiat nisl</li>
                            <li>Nulla at volutpat dola</li>
                            <li>Pharetra massa</li>
                            <li>Massa ultricies mi</li>
                        </ul>
                        <a href="#" class="btn-buy">Buy Now</a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6" data-aos="zoom-in" data-aos-delay="400">
                    <div class="box">
                        <h3 style="color: #ff0071;">Ultimate Plan</h3>
                        <div class="price"><sup>$</sup>49<span> / mo</span></div>
                        <img src="assets/img/pricing-ultimate.png" class="img-fluid" alt="">
                        <ul>
                            <li>Aida dere</li>
                            <li>Nec feugiat nisl</li>
                            <li>Nulla at volutpat dola</li>
                            <li>Pharetra massa</li>
                            <li>Massa ultricies mi</li>
                        </ul>
                        <a href="#" class="btn-buy">Buy Now</a>
                    </div>
                </div>

            </div>

        </div>

    </section> --}} -->



    <section id="testimonials" class="testimonials">
        <div class="container aos-init aos-animate" data-aos="fade-up">

            <header class="section-header">
                <h2>Pricing Demo 1 (with Carousel)</h2>
                <p>Check our Pricing</p>
            </header>

            <div class="testimonials-slider swiper-container " data-aos="fade-up" data-aos-delay="200">

                <div class="swiper-wrapper ">
                    
                 @if($packages != "" && count($packages)>0 )
                 @forelse ($packages as $key => $package)
                 @if($package['status'] == '1')
                    <div class="swiper-slide">
                        <div class="testimonial-item "  style="border-radius: 13px; ">
                            <div class="row gy-4 aos-init aos-animate" data-aos="fade-left">

                                <div class="col-lg-12 col-md-6 aos-init aos-animate" data-aos="zoom-in"
                                    data-aos-delay="100">
                                    @if($package['is_free_trial'] == '0')
                                    <div class="ribbon ribbon-top-right"><span>Featured</span></div>
                                    @endif
                                    <div class="box ">
                                    
                                         
                                        <h2 style="color: #07d5c0; "> {{-- {{ Config::get('packagetype')[ $package->package_type] }} --}}
                                            {{ strtoupper($package['name']) ?? '' }}</h2>
                                            
                                        <div class="price">
                                        <h4 class="card-price text-center">
                                            @if($package['currency'] == 'DOLLAR')
                                            $
                                            @endif
                                            @if($package['currency'] == 'INR')
                                            ₹
                                            @endif
                                            {{ $package['amount'] ?? '' }}
                                            <span class="period">
                                                @if($package['package_type'] == '0')
                                                / month
                                                @else
                                                / year
                                                @endif
                                            </span>
                                        </h4>
                                        <hr>
                                        </div>
                                        @if(isset($package['image']))
                                        <img src="{{config('helper.image_url').$package['image']}}" onerror="javascript:this.src='{{asset('default/no_image_available.png')}}'" class="" alt=""  width="200px;">
                                        @else
                                        <img src="{{asset('default/no_image_available.png')}}" class="" alt="" width="200px;" height="130px;">
                                        @endif
                                        <h4 class=".ex1" style=" text-align:center; margin-top:20px; margin-left: 40px;">
                                            <ul class="fa-ul" style="list-style:none; text-align: left;  ">
                                                <li> Package Type: 
                                            @if($package['package_type'] == '0')
                                            Corporate
                                            @else
                                            Retail
                                            @endif</li>
                                                <li> @if($package['is_free_trial'] == '0')
                                            Do not contains a free trial
                                            @else
                                            Contains a free trial of &nbsp {{$package['data_maintained_days'] ?? ''}} days
                                            @endif</li>
                                                <li> Total facial recognition: &nbsp {{ $package['users_for_facial_recognition'] ?? ''}}</li>
                                                <li class="na"> No. of Checkins:&nbsp {{ $package['check_ins'] ?? '' }}</li>
                                                <li class="na"> No. of Backend Integration: &nbsp  {{ $package['backend_integrations'] ?? '' }}</li>
                                                <li class="na">  No. of Sites: &nbsp  {{ $package['sites'] ?? '' }}</li>
                                                <li class="na"> Rate Limit:  &nbsp {{ $package['rate_limit'] ?? '' }}</li>
                                                <li class="na">  Admin Users: &nbsp {{ $package['admin_users'] ?? '' }}</li>
                                                <li class="na">   Package Time: &nbsp {{ $package['package_time'] ?? '' }} Months</li>
                                            </ul>
                                        </h4>
                                      
                                             @if(request()->session()->get('authenticated') == 'true' )
                                            <?php $mytime = Carbon\Carbon::now()->format('Y-m-d'); ?>

                                           
                                              
                                                <a href="{{ route('frontend.package.details',$package['id']) }}" class="btn-buy btn btn-outline-primary"
                                                style="border-radius: 25px 25px 25px 25px; margin-top:10px; padding: 10px 20px; hover: ">Buy Now</a>
                                               
                                            
                                        @else
                                        <a href="{{ route('login') }}" class="btn-buy btn btn-outline-primary"
                                        style="border-radius: 25px 25px 25px 25px; margin-top:10px; padding: 10px 20px; hover: ">Buy Now</a>
                                        @endif
                                    
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                   

                @endif
                @empty
              @endforelse
              @endif
                </div>


                <div class="swiper-pagination"></div>
            </div>
        </div>



    </section>


    <section id="pricing" class="pricing">

        <div class="container aos-init aos-animate" data-aos="fade-up">
  
          <header class="section-header">
            <h2>Pricing Demo 2</h2>
            <p>Check our Pricing</p>
          </header>
  
          <div class="row gy-4 aos-init aos-animate" data-aos="fade-left">
  
            <div class="col-lg-3 col-md-6 aos-init aos-animate" data-aos="zoom-in" data-aos-delay="100">
              <div class="box">
                <h3 style="color: #07d5c0;">Free Plan</h3>
                <div class="price"><sup>$</sup>0<span> / mo</span></div>
                <img src="assets/img/pricing-free.png" class="img-fluid" alt="">
                <ul>
                  <li>Aida dere</li>
                  <li>Nec feugiat nisl</li>
                  <li>Nulla at volutpat dola</li>
                  <li class="na">Pharetra massa</li>
                  <li class="na">Massa ultricies mi</li>
                </ul>
                <a href="#" class="btn-buy">Buy Now</a>
              </div>
            </div>
  
            <div class="col-lg-3 col-md-6 aos-init aos-animate" data-aos="zoom-in" data-aos-delay="200">
              <div class="box">
                <span class="featured">Featured</span>
                <h3 style="color: #65c600;">Starter Plan</h3>
                <div class="price"><sup>$</sup>19<span> / mo</span></div>
                <img src="assets/img/pricing-starter.png" class="img-fluid" alt="">
                <ul>
                  <li>Aida dere</li>
                  <li>Nec feugiat nisl</li>
                  <li>Nulla at volutpat dola</li>
                  <li>Pharetra massa</li>
                  <li class="na">Massa ultricies mi</li>
                </ul>
                <a href="#" class="btn-buy">Buy Now</a>
              </div>
            </div>
  
            <div class="col-lg-3 col-md-6 aos-init aos-animate" data-aos="zoom-in" data-aos-delay="300">
              <div class="box">
                <h3 style="color: #ff901c;">Business Plan</h3>
                <div class="price"><sup>$</sup>29<span> / mo</span></div>
                <img src="assets/img/pricing-business.png" class="img-fluid" alt="">
                <ul>
                  <li>Aida dere</li>
                  <li>Nec feugiat nisl</li>
                  <li>Nulla at volutpat dola</li>
                  <li>Pharetra massa</li>
                  <li>Massa ultricies mi</li>
                </ul>
                <a href="#" class="btn-buy">Buy Now</a>
              </div>
            </div>
  
            <div class="col-lg-3 col-md-6 aos-init aos-animate" data-aos="zoom-in" data-aos-delay="400">
              <div class="box">
                <h3 style="color: #ff0071;">Ultimate Plan</h3>
                <div class="price"><sup>$</sup>49<span> / mo</span></div>
                <img src="assets/img/pricing-ultimate.png" class="img-fluid" alt="">
                <ul>
                  <li>Aida dere</li>
                  <li>Nec feugiat nisl</li>
                  <li>Nulla at volutpat dola</li>
                  <li>Pharetra massa</li>
                  <li>Massa ultricies mi</li>
                </ul>
                <a href="#" class="btn-buy">Buy Now</a>
              </div>
            </div>

            <div class="row gy-4 aos-init aos-animate" data-aos="fade-left">
  
                <div class="col-lg-3 col-md-6 aos-init aos-animate" data-aos="zoom-in" data-aos-delay="100">
                  <div class="box">
                    <h3 style="color: #07d5c0;">Free Plan</h3>
                    <div class="price"><sup>$</sup>0<span> / mo</span></div>
                    <img src="assets/img/pricing-free.png" class="img-fluid" alt="">
                    <ul>
                      <li>Aida dere</li>
                      <li>Nec feugiat nisl</li>
                      <li>Nulla at volutpat dola</li>
                      <li class="na">Pharetra massa</li>
                      <li class="na">Massa ultricies mi</li>
                    </ul>
                    <a href="#" class="btn-buy">Buy Now</a>
                  </div>
                </div>
      
                <div class="col-lg-3 col-md-6 aos-init aos-animate" data-aos="zoom-in" data-aos-delay="200">
                  <div class="box">
                    <span class="featured">Featured</span>
                    <h3 style="color: #65c600;">Starter Plan</h3>
                    <div class="price"><sup>$</sup>19<span> / mo</span></div>
                    <img src="assets/img/pricing-starter.png" class="img-fluid" alt="">
                    <ul>
                      <li>Aida dere</li>
                      <li>Nec feugiat nisl</li>
                      <li>Nulla at volutpat dola</li>
                      <li>Pharetra massa</li>
                      <li class="na">Massa ultricies mi</li>
                    </ul>
                    <a href="#" class="btn-buy">Buy Now</a>
                  </div>
                </div>
      
                <div class="col-lg-3 col-md-6 aos-init aos-animate" data-aos="zoom-in" data-aos-delay="300">
                  <div class="box">
                    <h3 style="color: #ff901c;">Business Plan</h3>
                    <div class="price"><sup>$</sup>29<span> / mo</span></div>
                    <img src="assets/img/pricing-business.png" class="img-fluid" alt="">
                    <ul>
                      <li>Aida dere</li>
                      <li>Nec feugiat nisl</li>
                      <li>Nulla at volutpat dola</li>
                      <li>Pharetra massa</li>
                      <li>Massa ultricies mi</li>
                    </ul>
                    <a href="#" class="btn-buy">Buy Now</a>
                  </div>
                </div>
      
                <div class="col-lg-3 col-md-6 aos-init aos-animate" data-aos="zoom-in" data-aos-delay="400">
                  <div class="box">
                    <h3 style="color: #ff0071;">Ultimate Plan</h3>
                    <div class="price"><sup>$</sup>49<span> / mo</span></div>
                    <img src="assets/img/pricing-ultimate.png" class="img-fluid" alt="">
                    <ul>
                      <li>Aida dere</li>
                      <li>Nec feugiat nisl</li>
                      <li>Nulla at volutpat dola</li>
                      <li>Pharetra massa</li>
                      <li>Massa ultricies mi</li>
                    </ul>
                    <a href="#" class="btn-buy">Buy Now</a>
                  </div>
                </div>
      
  
          </div>
  
        </div>
  
    </section>

    <section id="services" class="services">

        <div class="container aos-init aos-animate" data-aos="fade-up">
  
          <header class="section-header">
            <h2>Pricing Demo 3</h2>
            <p>Check our Pricing</p>
          </header>
  
          <div class="row gy-4">
  
            <div class="col-lg-4 col-md-6 aos-init aos-animate" data-aos="fade-up" data-aos-delay="200">
              <div class="service-box blue">
                <i class="ri-discuss-line icon"></i>
                <h3>Package 1</h3>
                <p>Provident nihil minus qui consequatur non omnis maiores. Eos accusantium minus dolores iure perferendis tempore et consequatur.</p>
                <a href="#" class="read-more"><span>Read More</span> <i class="bi bi-arrow-right"></i></a>
              </div>
            </div>
  
            <div class="col-lg-4 col-md-6 aos-init aos-animate" data-aos="fade-up" data-aos-delay="300">
              <div class="service-box orange">
                <i class="ri-discuss-line icon"></i>
                <h3>Package 2</h3>
                <p>Ut autem aut autem non a. Sint sint sit facilis nam iusto sint. Libero corrupti neque eum hic non ut nesciunt dolorem.</p>
                <a href="#" class="read-more"><span>Read More</span> <i class="bi bi-arrow-right"></i></a>
              </div>
            </div>
  
            <div class="col-lg-4 col-md-6 aos-init aos-animate" data-aos="fade-up" data-aos-delay="400">
              <div class="service-box green">
                <i class="ri-discuss-line icon"></i>
                <h3>Package 3</h3>
                <p>Ut excepturi voluptatem nisi sed. Quidem fuga consequatur. Minus ea aut. Vel qui id voluptas adipisci eos earum corrupti.</p>
                <a href="#" class="read-more"><span>Read More</span> <i class="bi bi-arrow-right"></i></a>
              </div>
            </div>
  
            <div class="col-lg-4 col-md-6 aos-init aos-animate" data-aos="fade-up" data-aos-delay="500">
              <div class="service-box red">
                <i class="ri-discuss-line icon"></i>
                <h3>Package 4</h3>
                <p>Non et temporibus minus omnis sed dolor esse consequatur. Cupiditate sed error ea fuga sit provident adipisci neque.</p>
                <a href="#" class="read-more"><span>Read More</span> <i class="bi bi-arrow-right"></i></a>
              </div>
            </div>
  
            <div class="col-lg-4 col-md-6 aos-init aos-animate" data-aos="fade-up" data-aos-delay="600">
              <div class="service-box purple">
                <i class="ri-discuss-line icon"></i>
                <h3>Package 5</h3>
                <p>Cumque et suscipit saepe. Est maiores autem enim facilis ut aut ipsam corporis aut. Sed animi at autem alias eius labore.</p>
                <a href="#" class="read-more"><span>Read More</span> <i class="bi bi-arrow-right"></i></a>
              </div>
            </div>
  
            <div class="col-lg-4 col-md-6 aos-init aos-animate" data-aos="fade-up" data-aos-delay="700">
              <div class="service-box pink">
                <i class="ri-discuss-line icon"></i>
                <h3>Package 6</h3>
                <p>Hic molestias ea quibusdam eos. Fugiat enim doloremque aut neque non et debitis iure. Corrupti recusandae ducimus enim.</p>
                <a href="#" class="read-more"><span>Read More</span> <i class="bi bi-arrow-right"></i></a>
              </div>
            </div>
  
          </div>
  
        </div>
  
    </section>




    <!-- End Pricing Section -->
    <!-- ======= Testimonials Section ======= -->

    <section id="testimonials" class="testimonials">

        <div class="container" data-aos="fade-up">

            <header class="section-header">
                <h2>Testimonials</h2>
                <p>What they are saying about us</p>
            </header>

            <div class="testimonials-slider swiper-container  " data-aos="fade-up" data-aos-delay="200">
                <div class="swiper-wrapper ">

                    <div class="swiper-slide w3-container w3-center w3-animate-zoom">
                        <div class="testimonial-item ">
                            <div class="stars">
                                <i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i
                                    class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i
                                    class="bi bi-star-fill"></i>
                            </div>
                            <p>
                                Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit
                                rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam,
                                risus at semper.
                            </p>
                            <div class="profile mt-auto">
                                <img src="assets/img/testimonials/testimonials-1.jpg" class="testimonial-img" alt="">
                                <h3>Saul Goodman</h3>
                                <h4>Ceo &amp; Founder</h4>
                            </div>
                        </div>
                    </div>
                    <!-- End testimonial item -->

                    <div class="swiper-slide">
                        <div class="testimonial-item">
                            <div class="stars">
                                <i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i
                                    class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i
                                    class="bi bi-star-fill"></i>
                            </div>
                            <p>
                                Export tempor illum tamen malis malis eram quae irure esse labore quem cillum quid
                                cillum eram malis quorum velit fore eram velit sunt aliqua noster fugiat irure amet
                                legam anim culpa.
                            </p>
                            <div class="profile mt-auto">
                                <img src="assets/img/testimonials/testimonials-2.jpg" class="testimonial-img" alt="">
                                <h3>Sara Wilsson</h3>
                                <h4>Designer</h4>
                            </div>
                        </div>
                    </div>
                    <!-- End testimonial item -->

                    <div class="swiper-slide">
                        <div class="testimonial-item">
                            <div class="stars">
                                <i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i
                                    class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i
                                    class="bi bi-star-fill"></i>
                            </div>
                            <p>
                                Enim nisi quem export duis labore cillum quae magna enim sint quorum nulla quem
                                veniam duis minim tempor labore quem eram duis noster aute amet eram fore quis sint
                                minim.
                            </p>
                            <div class="profile mt-auto">
                                <img src="assets/img/testimonials/testimonials-3.jpg" class="testimonial-img" alt="">
                                <h3>Jena Karlis</h3>
                                <h4>Store Owner</h4>
                            </div>
                        </div>
                    </div>
                    <!-- End testimonial item -->

                    <div class="swiper-slide">
                        <div class="testimonial-item">
                            <div class="stars">
                                <i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i
                                    class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i
                                    class="bi bi-star-fill"></i>
                            </div>
                            <p>
                                Fugiat enim eram quae cillum dolore dolor amet nulla culpa multos export minim
                                fugiat minim velit minim dolor enim duis veniam ipsum anim magna sunt elit fore quem
                                dolore labore illum veniam.
                            </p>
                            <div class="profile mt-auto">
                                <img src="assets/img/testimonials/testimonials-4.jpg" class="testimonial-img" alt="">
                                <h3>Matt Brandon</h3>
                                <h4>Freelancer</h4>
                            </div>
                        </div>
                    </div>
                    <!-- End testimonial item -->

                    <div class="swiper-slide">
                        <div class="testimonial-item">
                            <div class="stars">
                                <i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i
                                    class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i
                                    class="bi bi-star-fill"></i>
                            </div>
                            <p>
                                Quis quorum aliqua sint quem legam fore sunt eram irure aliqua veniam tempor noster
                                veniam enim culpa labore duis sunt culpa nulla illum cillum fugiat legam esse veniam
                                culpa fore nisi cillum quid.
                            </p>
                            <div class="profile mt-auto">
                                <img src="assets/img/testimonials/testimonials-5.jpg" class="testimonial-img" alt="">
                                <h3>John Larson</h3>
                                <h4>Entrepreneur</h4>
                            </div>
                        </div>
                    </div>
                    <!-- End testimonial item -->

                </div>
                <div class="swiper-pagination"></div>
            </div>

        </div>

    </section>
    <!-- End Testimonials Section -->

    <!-- ======= Recent Blog Posts Section ======= -->
    {{-- <section id="recent-blog-posts" class="recent-blog-posts">

        <div class="container" data-aos="fade-up">

            <header class="section-header">
                <h2>Blog</h2>
                <p>Recent posts form our Blog</p>
            </header>

            <div class="row">

                <div class="col-lg-4">
                    <div class="post-box">
                        <div class="post-img"><img src="{{ asset('assets/img/blog/blog-1.jpg') }}"
    class="img-fluid" alt=""></div>
    <span class="post-date">Tue, September 15</span>
    <h3 class="post-title">Eum ad dolor et. Autem aut fugiat debitis voluptatem consequuntur sit
    </h3>
    <a href="blog-singe.html" class="readmore stretched-link mt-auto"><span>Read More</span><i
            class="bi bi-arrow-right"></i></a>
    </div>
    </div>

    <div class="col-lg-4">
        <div class="post-box">
            <div class="post-img"><img src="{{ asset('assets/img/blog/blog-2.jpg') }}" class="img-fluid" alt=""></div>
            <span class="post-date">Fri, August 28</span>
            <h3 class="post-title">Et repellendus molestiae qui est sed omnis voluptates magnam</h3>
            <a href="blog-singe.html" class="readmore stretched-link mt-auto"><span>Read More</span><i
                    class="bi bi-arrow-right"></i></a>
        </div>
    </div>

    <div class="col-lg-4">
        <div class="post-box">
            <div class="post-img"><img src="{{ asset('assets/img/blog/blog-3.jpg') }}" class="img-fluid" alt=""></div>
            <span class="post-date">Mon, July 11</span>
            <h3 class="post-title">Quia assumenda est et veritatis aut quae</h3>
            <a href="blog-singe.html" class="readmore stretched-link mt-auto"><span>Read More</span><i
                    class="bi bi-arrow-right"></i></a>
        </div>
    </div>

    </div>

    </div>

    </section> --}}
    <!-- End Recent Blog Posts Section -->

    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">

        <div class="container" data-aos="fade-up">

            <header class="section-header">
                <p>Request Form</p>
            </header>

            <div class="row gy-4">

                <div class="col-lg-6">

                    <div class="row gy-4">
                        <div class="col-md-6">
                            <div class="info-box">
                                <i class="bi bi-geo-alt"></i>
                                <h3>Address</h3>
                                <p>A108 Adam Street,<br>New York, NY 535022</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="info-box">
                                <i class="bi bi-telephone"></i>
                                <h3>Call Us</h3>
                                <p>+1 5589 55488 55<br>+1 6678 254445 41</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="info-box">
                                <i class="bi bi-envelope"></i>
                                <h3>Email Us</h3>
                                <p>info@example.com<br>contact@example.com</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="info-box">
                                <i class="bi bi-clock"></i>
                                <h3>Open Hours</h3>
                                <p>Monday - Friday<br>9:00AM - 05:00PM</p>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-lg-6">
                    <form action="forms/contact.php" method="post" class="php-email-form">
                        <div class="row gy-4">

                            <div class="col-md-6">
                                <input type="text" name="name" class="form-control" placeholder="Your Name" required>
                            </div>

                            <div class="col-md-6 ">
                                <input type="email" class="form-control" name="email" placeholder="Your Email" required>
                            </div>

                            <div class="col-md-12">
                                <input type="text" class="form-control" name="subject" placeholder="Subject" required>
                            </div>

                            <div class="col-md-12">
                                <textarea class="form-control" name="message" rows="6" placeholder="Message"
                                    required></textarea>
                            </div>

                            <div class="col-md-12 text-center">
                                <div class="loading">Loading</div>
                                <div class="error-message"></div>
                                <div class="sent-message">Your message has been sent. Thank you!</div>

                                <button type="submit">Send Message</button>
                            </div>

                        </div>
                    </form>

                </div>

            </div>

        </div>

    </section>
    <!-- End Contact Section -->

</main>
@endsection

@section('css')
<style>

/* common */
.ribbon {
  width: 150px;
  height: 150px;
  overflow: hidden;
  position: absolute;
}
.ribbon::before,
.ribbon::after {
  position: absolute;
  z-index: -1;
  content: '';
  display: block;
  border: 5px solid #2980b9;
}
.ribbon span {
  position: absolute;
  display: block;
  width: 225px;
  padding: 15px 0;
  background-color: #3498db;
  box-shadow: 0 5px 10px rgba(0,0,0,.1);
  color: #fff;
  font: 700 18px/1 'Lato', sans-serif;
  text-shadow: 0 1px 1px rgba(0,0,0,.2);
  text-transform: uppercase;
  text-align: center;
}
/* top right*/
.ribbon-top-right {
  top: -29px;
  right: -22px;
}
.ribbon-top-right::before,
.ribbon-top-right::after {
  border-top-color: transparent;
  border-right-color: transparent;
}
.ribbon-top-right::before {
  top: 0;
  left: 0;
}
.ribbon-top-right::after {
  bottom: 0;
  right: 0;
}
.ribbon-top-right span {
  left: -25px;
  top: 30px;
  transform: rotate(45deg);
}

</style>
@endsection



@section('js')
<script>
const totalCards = $('.card-slider_card').length;
const cardWidth = $('.card-slider_card:not(.active)').width();
const activeCardWidth = $('.card-slider_card.active').width();

goToCard($('.card-slider_card.active'));

$('.card-slider_card').click((e) => {
  let $currentCard = $(e.target).closest('.card-slider_card');
  goToCard($currentCard); 
});

$('.previous').click(goToPreviousCard);
$('.next').click(goToNextCard);

function goToCard($card) {
  
  if (!$card.hasClass('active')) {
      $('.active-out, .active-in').removeClass('active-out active-in');
    $('.card-slider_card.active').addClass('active-out').removeClass('active');
    $card.closest('.card-slider_card').addClass('active active-in');
  }
  
  let currentPos = $('.card-slider_card').index($card);
  let scrollPos = cardWidth * currentPos;

  $('.card-slider_list').css({
      transform: 'translateX(-'+scrollPos+'px)'
  }, 200);
}

function goToNextCard() {
  let $nextCard = $('.card-slider_card.active').next('.card-slider_card');
  if ($nextCard.length) {
    goToCard($nextCard);
  }
}
function goToPreviousCard() {
  let $previousCard = $('.card-slider_card.active').prev('.card-slider_card');
  if ($previousCard.length) {
    goToCard($previousCard);
  }
}

</script>
@endsection