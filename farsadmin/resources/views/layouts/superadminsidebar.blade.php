  
                <!-- Main Sidebar Container -->
                <aside class="main-sidebar sidebar-dark-primary elevation-4">
                  <!-- Brand Logo -->
                  <a href="{{ route('home.superadmin') }}" class="brand-link">
                    <img src="{{asset('dist/img/AdminLTELogo.png')}}" alt="Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
                    <span class="brand-text font-weight-light">SuperAdmin</span>
                  </a>
              
                  <!-- Sidebar -->
                  <div class="sidebar">
                    <!-- Sidebar user panel (optional) -->
                    {{-- <div class="user-panel mt-3 pb-3 mb-3 d-flex" >
                      <div class="image">
                      <img src="{{asset('dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
                        
                      </div>
                     
                    </div> --}}
                    <a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="false">
                      <img src="{{asset('dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2 mr-3" alt="User Image" style="height: 34px; width: 34px;">
                        {{auth()->user()->name}}
                    </a>
                     <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                      <a href="/" class="dropdown-item">
                        <!-- Message Start -->
                        <div class="media">
                          <img src="{{asset('dist/img/user2-160x160.jpg')}}" alt="User Avatar" class="img-size-50 mr-3 img-circle">
                          <div class="media-body">
                            <h3 class="dropdown-item-title">
                              My Account
                              <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                            </h3>
                            <h3>
                              <p class="text-sm">See your Account</p>
                            </h3>
                            {{-- <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p> --}}
                          </div>
                        </div>
                        <!-- Message End -->
                      </a>
                      <div class="dropdown-divider"></div>
                      <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <!-- Message Start -->
                        <div class="media">
                          <img src="{{asset('dist/img/user2-160x160.jpg')}}" alt="User Avatar" class="img-size-50 img-circle mr-3">
                          {{-- <i class="fa fa-users"></i> --}}
                          <div class="media-body">
                            <h3 class="dropdown-item-title">
                              Logout
                              <span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>
                            </h3>
                            <p class="text-sm">Logout of the application</p>
                            {{-- <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p> --}}
                          </div>
                        </div>
                        <!-- Message End -->
                      </a>
                      <div class="dropdown-divider"></div>
                      <a href="#" class="dropdown-item dropdown-footer">Go To Dashboard</a>
                     </div>
                    <hr style="background: #c2c7d0;">
                    <!-- SidebarSearch Form -->
                    <div class="form-inline">
                      <div class="input-group" data-widget="sidebar-search">
                        <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
                        <div class="input-group-append">
                          <button class="btn btn-sidebar">
                            <i class="fas fa-search fa-fw"></i>
                          </button>
                        </div>
                      </div>
                    </div>
              
                    <!-- Sidebar Menu -->
                    <nav class="mt-2">
                      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class
                             with font-awesome or any other icon font library -->
                        <li class="nav-item">
                          <a href="{{route('home.superadmin')}}" class="nav-link {{ Request::segment(2) === 'home' ? 'active' : null }}">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>
                              Dashboard
                            </p>
                          </a>
                      
                        </li>
                        {{-- <li class="nav-item">
                          <a href="{{route('admin.users.index')}}" class="nav-link {{ Request::segment(2) === 'users' ? 'active' : null }}">
                            <i class="nav-icon fas fa-users"></i>
                            <p>
                              Users Managment
                              <span class="right badge badge-danger">New</span>
                            </p>
                          </a>
                        </li>
 
                        <li class="nav-item">
                          <a href="{{route('admin.training.index')}}" class="nav-link {{ Request::segment(2) === 'training' ? 'active' : null }}">
                            <i class="nav-icon fas fa-book"></i>
                            <p>
                              Training
                              <span class="right badge badge-danger">New</span>
                            </p>
                          </a>
                        </li> --}}
                        <li class="nav-item">
                          <a href="{{route('superadmin.package.index')}}" class="nav-link {{ Request::segment(2) === 'package' ? 'active' : null }}">
                            <i class="nav-icon fas fa-th"></i>
                            <p>
                              Package Manager
                              {{-- <span class="right badge badge-danger">New</span> --}}
                            </p>
                          </a>
                        </li>

                        <li class="nav-item">
                          <a href="{{route('superadmin.userAccount.index')}}" class="nav-link {{ Request::segment(2) === 'userAccount' ? 'active' : null }}">
                            <i class="nav-icon fas fa-users"></i>
                            <p>
                              Manage Customer A/C
                              {{-- <span class="right badge badge-danger">New</span> --}}
                            </p>
                          </a>
                        </li>

                        {{-- <li class="nav-item">
                          <a href="{{route('superadmin.user.package')}}" class="nav-link {{ Request::segment(2) === 'user' ? 'active' : null }}">
                            <i class="nav-icon fas fa-bars"></i>
                            <p>
                              User Package
                              <span class="right badge badge-danger">New</span>
                            </p>
                          </a>
                        </li> --}}

                        <li class="nav-item has-treeview {{ Request::segment(2) == 'setting' ? 'menu-open' : null }}">
                          <a href="#" class="nav-link" {{ Request::segment(2) == 'setting' ? 'active' : null }}>
                            <i class="nav-icon fas fa-chart-pie"></i>
                            <p>
                              Setting
                              <i class="right fas fa-angle-left"></i>
                            </p>
                          </a>
                          <ul class="nav nav-treeview">
                            <li class="nav-item">
                              <a href="{{ route('superadmin.paymentSetting') }}" class="nav-link {{ Request::segment(3) == 'payment' ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Payment Settings</p>
                              </a>
                            </li>
                            <li class="nav-item">
                              <a href="{{ route('superadmin.emailSetting') }}" class="nav-link {{ Request::segment(3) == 'email' ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Email Settings</p>
                              </a>
                            </li>
                            <li class="nav-item">
                              <a href="{{ route('superadmin.smsSetting') }}" class="nav-link {{ Request::segment(3) == 'sms' ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>SMS Setting</p>
                              </a>
                            </li>
                            <li class="nav-item ">
                              <a href="{{ route('superadmin.smsSetting') }}" class="nav-link {{ Request::segment(3) == 'smscdc' ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Third Party Integrations</p>
                              </a>
                            </li>
                          </ul>
                        </li>

                      
                      </ul>
                    </nav>
                    <!-- /.sidebar-menu -->
                  </div>
                  <!-- /.sidebar -->
                </aside>