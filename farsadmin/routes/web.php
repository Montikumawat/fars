<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'FrontendController@index')->name('front');
// Authentication Routes...
Route::get('login', 'Auth\CustomAuthController@showLoginForm')->name('loginForm');
Route::post('login', 'Auth\CustomAuthController@authenticate')->name('login');
Route::post('logout', 'Auth\CustomAuthController@logout')->name('logout');
// Registration Routes...
Route::get('register', 'Auth\CustomRegistrationController@showRegistrationForm');
Route::post('register', 'Auth\CustomRegistrationController@register')->name('register');
// Password Reset Routes...
Route::get('password/reset/{token?}', 'Auth\CustomPasswordController@showResetForm')->name('password.request');
Route::post('password/email', 'Auth\CustomPasswordController@sendResetLinkEmail');
Route::post('password/reset', 'Auth\CustomPasswordController@reset');
// Auth::routes();
//package routes
Route::get('/package/details/{id}', 'FrontendController@packageDetails')->name('frontend.package.details');
//user routes
Route::group(['middleware' => ['role:user'],'prefix'=>'user'], function () {
    
    Route::get('/home', 'HomeController@index')->name('home.user');
    
});
//admin routes ,'Package'
Route::group(['middleware' => ['package'],'prefix'=>'admin'], function () {
    


    Route::get('/home', 'HomeController@index')->name('home.admin');
      //User Routes
    Route::get('/users','Admin\UsersController@index')->name('admin.users.index');
    Route::get('/users/add','Admin\UsersController@add')->name('admin.users.add');
    Route::post('/users/store','Admin\UsersController@store')->name('admin.users.store');
    Route::get('/users/edit/{id}','Admin\UsersController@edit')->name('admin.users.edit');
    Route::post('/users/update/{id}','Admin\UsersController@update')->name('admin.users.update');
    Route::get('/users/destroy/{id}','Admin\UsersController@destroy')->name('admin.users.destroy');
    // Route::get('/users/show/{id}','Admin\UsersController@show')->name('admin.users.show');
    Route::get('/users/show/', function () {
      return view('admin.users.show');
    })->name('admin.users.show');

    //Training

    Route::get('/training','Admin\TraningController@index')->name('admin.training.index');
    Route::get('/training/view','Admin\TraningController@view')->name('admin.training.view');
    Route::get('/training/add','Admin\TraningController@add')->name('admin.training.add');


    Route::get('/staff/listing', function () {
      return view('admin.staff.index');
  })->name('admin.staff.index');
  Route::get('/staff/add', function () {
    return view('admin.staff.add');
})->name('admin.staff.add');  
  Route::get('/staff/edit', function () {
      return view('admin.staff.edit');
  })->name('admin.staff.edit');  
  Route::get('/staff/detail', function () {
      return view('admin.staff.detail');
  })->name('admin.staff.detail');   

  Route::get('/model/listing', function () {
    return view('admin.model.index');
})->name('admin.model.index');
Route::get('/model/add', function () {
  return view('admin.model.add');
})->name('admin.model.add');  
Route::get('/model/edit', function () {
    return view('admin.model.edit');
})->name('admin.model.edit');  
Route::get('/model/detail', function () {
    return view('admin.model.detail');
})->name('admin.model.detail');   

Route::get('/api', function () {
  return view('admin.api.index');
})->name('admin.api.index');
   
Route::get('/api/generate', function () {
  // return view('admin.api.index');
})->name('admin.api.generate');

Route::get('/api/edit', function () {
  return view('admin.api.edit');
})->name('admin.api.edit');
// Self Register routes

Route::get('/self/users','Admin\UsersController@indexSelf')->name('admin.users.self.index');
Route::get('/self/users/add','Admin\UsersController@addSelf')->name('admin.users.self.add');
Route::post('/self/users/store','Admin\UsersController@storeSelf')->name('admin.users.self.store');
Route::get('/self/users/edit/{id}','Admin\UsersController@editSelf')->name('admin.users.self.edit');
Route::post('/self/users/update/{id}','Admin\UsersController@updateSelf')->name('admin.users.self.update');
Route::get('/self/users/destroy/{id}','Admin\UsersController@destroySelf')->name('admin.users.self.destroy');
// Route::get('/users/show/{id}','Admin\UsersController@show')->name('admin.users.show');
Route::get('/self/users/show/', function () {
return view('admin.usersSelf.show');
})->name('admin.users.self.show');
    

Route::get('role',function(){
  return view('admin.role.index');
})->name('admin.role.index');
Route::get('role/add',function(){
  return view('admin.role.add');
})->name('admin.role.add');
Route::get('role/edit',function(){
  return view('admin.role.edit');
})->name('admin.role.edit');
Route::get('role/detail',function(){
  return view('admin.role.detail');
})->name('admin.role.detail');

//Billing
//Subscription
Route::get('/billing/subscription','Admin\BillingController@subscriptionIndex')->name('admin.billing.subscription.index');

//Payment
Route::get('billing/payment',function(){
  return view('admin.billing.payments.index');
})->name('admin.billing.payment.index');
Route::get('billing/payment/add',function(){
  return view('admin.billing.payments.add');
})->name('admin.billing.payment.add');
//Invoices

Route::get('/billing/invoice','Admin\BillingController@invoiceIndex')->name('admin.billing.invoice.index');
Route::get('/userAccount/invoice/{id}','Admin\BillingController@invoiceDetail')->name('admin.invoice.detail');
Route::get('/userAccount/invoice/{id}/download','Admin\BillingController@invoiceDownload')->name('admin.invoice.download');
Route::get('/billing/invoice/filter/payment','Admin\BillingController@filterPayment')->name('admin.payment.filter');
});
Route::post('/user/package/subscribe', 'SubscriptionController@subscribe')->name('user.subscription');
Route::get('/package/select', 'Auth\RegisterController@packageSelect')->name('package.select');
Route::post('/razorpay/payment/store', 'FrontendController@razorpay')->name('razorpay.payment.store');
Route::post('/stripe/payment/store', 'FrontendController@stripe')->name('stripe.payment.store');


