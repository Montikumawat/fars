-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 19, 2021 at 07:53 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `farsadmin`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_users`
--

CREATE TABLE `admin_users` (
  `id` bigint(11) NOT NULL,
  `admin_id` bigint(11) DEFAULT NULL,
  `user_id` bigint(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin_users`
--

INSERT INTO `admin_users` (`id`, `admin_id`, `user_id`, `created_at`, `updated_at`) VALUES
(2, 3, 7, '2021-04-22 00:32:01', '2021-04-22 00:32:01'),
(10, 3, 20, '2021-04-22 00:56:20', '2021-04-22 00:56:20'),
(23, 3, 38, '2021-05-04 00:26:52', '2021-05-04 00:26:52'),
(24, 3, 39, '2021-05-04 00:39:16', '2021-05-04 00:39:16'),
(25, 3, 41, '2021-05-04 00:45:21', '2021-05-04 00:45:21'),
(26, 3, 42, '2021-05-04 00:52:35', '2021-05-04 00:52:35'),
(27, 3, 43, '2021-05-04 01:07:51', '2021-05-04 01:07:51'),
(28, 3, 45, '2021-05-04 01:19:11', '2021-05-04 01:19:11'),
(29, 3, 46, '2021-05-04 01:27:21', '2021-05-04 01:27:21'),
(30, 3, 47, '2021-05-04 01:33:10', '2021-05-04 01:33:10'),
(31, 3, 48, '2021-05-04 01:34:51', '2021-05-04 01:34:51'),
(32, 3, 49, '2021-05-04 01:37:16', '2021-05-04 01:37:16'),
(34, 3, 52, '2021-05-04 01:46:47', '2021-05-04 01:46:47'),
(37, 3, 55, '2021-05-04 03:30:17', '2021-05-04 03:30:17'),
(38, 3, 87, '2021-05-06 02:08:58', '2021-05-06 02:08:58'),
(39, 3, 88, '2021-05-25 00:47:53', '2021-05-25 00:47:53');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\User', 6),
(2, 'App\\User', 3),
(3, 'App\\User', 2),
(3, 'App\\User', 4),
(3, 'App\\User', 7),
(3, 'App\\User', 20),
(3, 'App\\User', 35),
(3, 'App\\User', 36),
(3, 'App\\User', 38),
(3, 'App\\User', 39),
(3, 'App\\User', 41),
(3, 'App\\User', 42),
(3, 'App\\User', 43),
(3, 'App\\User', 45),
(3, 'App\\User', 46),
(3, 'App\\User', 47),
(3, 'App\\User', 48),
(3, 'App\\User', 49),
(3, 'App\\User', 50),
(3, 'App\\User', 52),
(3, 'App\\User', 55),
(3, 'App\\User', 87),
(3, 'App\\User', 88);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('05a16e888b859f901fe798e8653e200ce2cbe04c82729daa3def62ea8c060350e79870173036102f', 56, 1, 'MyApp', '[]', 0, '2021-03-15 12:30:43', '2021-03-15 12:30:43', '2022-03-15 12:30:43'),
('08d6431550628fb7a5edfe892157896b25739aa8247e6cf9a5703017cff0566dc72b9a768179c34e', 72, 1, NULL, '[]', 0, '2021-05-05 04:33:01', '2021-05-05 04:33:01', '2022-05-05 10:03:01'),
('0a98c63365a1f2fc02dbd3d40aa70110cd136b3c4d61e7be1ae868bb8cbd7e9be808f89c430c2ad5', 48, 1, 'MyApp', '[]', 0, '2021-03-12 06:36:01', '2021-03-12 06:36:01', '2022-03-12 06:36:01'),
('0c456167ff86eab169c31dadc7976d2cd3630543d30e987b5001f183cab1fa8e535a788dd6e24256', 34, 1, 'MyApp', '[]', 0, '2021-02-24 06:05:47', '2021-02-24 06:05:47', '2022-02-24 11:35:47'),
('0cd43fab7d536a5d60bfe5342cb77087bdf5f2cfeac0b4117b49bea94b36e3b49e2d7bdc4dd94eab', 56, 1, 'MyApp', '[]', 0, '2021-03-15 11:06:51', '2021-03-15 11:06:51', '2022-03-15 11:06:51'),
('0dab965578314c3dbc5d4f1199163c80703f0e47440e6adc4b1c47ab6eaf0c7a2d785289acdbb6ab', 48, 1, 'MyApp', '[]', 0, '2021-03-18 09:46:12', '2021-03-18 09:46:12', '2022-03-18 09:46:12'),
('0e52cdc6aa75a34e9cb640d30c65da420c0a34ba23a952fa2eeeb690b56b2f2f9c33d854ebd9a081', 24, 1, NULL, '[]', 0, '2021-02-24 05:18:47', '2021-02-24 05:18:47', '2022-02-24 10:48:47'),
('10c0daf847aeb078ddb0850505d17167bc21e65ecdea19d61d2d0eca13b720c57041ad6e21b5fcfe', 81, 1, 'MyApp', '[]', 0, '2021-05-05 06:11:31', '2021-05-05 06:11:31', '2022-05-05 11:41:31'),
('128436db466a10865e0252da0d253ad162acdfcd92b99e38ffe5252562afec13bd181dfaf237321e', 48, 1, 'MyApp', '[]', 0, '2021-03-17 12:08:05', '2021-03-17 12:08:05', '2022-03-17 12:08:05'),
('1706c8168515783ac21950295cd5cb9a2eef8b73a44cd00da94eae20400169a7e409e86d6d4b8a24', 48, 1, 'MyApp', '[]', 0, '2021-03-15 12:26:20', '2021-03-15 12:26:20', '2022-03-15 12:26:20'),
('183fc0bc4eb002c9f6f8f6a48140f6f2ad9fe828a18405fab3698616515fae2c797a8a02062c589f', 56, 1, 'MyApp', '[]', 0, '2021-03-15 10:47:55', '2021-03-15 10:47:55', '2022-03-15 10:47:55'),
('1a22f84c313c5f5d6b9ea52f9b0d7073a697226aa832ca165f98d34ce5c5898218e6d418fcb2216c', 49, 1, 'MyApp', '[]', 0, '2021-03-12 10:14:36', '2021-03-12 10:14:36', '2022-03-12 10:14:36'),
('1b7dcbc31235d2a866051124ac57dac6cab3263e0aec6c43dedc53d597a0f86f49bc8bec8b65df43', 51, 1, 'MyApp', '[]', 0, '2021-03-13 04:47:58', '2021-03-13 04:47:58', '2022-03-13 04:47:58'),
('21d1808d18620c5c58bfa027afe3ff5d6a35840d1d933e4dba48b5e3ab163d9997814b5c237d51c0', 63, 1, 'MyApp', '[]', 0, '2021-03-16 10:40:34', '2021-03-16 10:40:34', '2022-03-16 10:40:34'),
('227f16e44c94020d4eca83668f6c964e3ebfe4df8e8e40e3ed3a041eee99aaa462ef5ad21f0abf19', 45, 1, 'MyApp', '[]', 0, '2021-03-01 04:09:11', '2021-03-01 04:09:11', '2022-03-01 09:39:11'),
('238ee0c42e4666e4e0aa684f617c0247dd6b3869b7990367438cbcdc3af3ba18de3d129e21124f72', 47, 1, 'MyApp', '[]', 0, '2021-03-12 06:35:06', '2021-03-12 06:35:06', '2022-03-12 06:35:06'),
('252d961dcd476cccc2a75a33e1258c255e920dd69de3fb572a73fdcba48d34a0b083e97f7a97b003', 48, 1, 'MyApp', '[]', 0, '2021-03-16 08:58:04', '2021-03-16 08:58:04', '2022-03-16 08:58:04'),
('287a557ccffa8ca0fe620a3a71157215fe3dc892e0349c34bc9d571c318e39a070c08c25143ce5cb', 30, 1, NULL, '[]', 0, '2021-02-24 05:30:32', '2021-02-24 05:30:32', '2022-02-24 11:00:32'),
('2cc65f149cc71ee01cd1f30c40a96807724336dc5d903ccc5d2a62d8ed623f7115ae0f680b786da4', 48, 1, 'MyApp', '[]', 0, '2021-03-15 11:36:54', '2021-03-15 11:36:54', '2022-03-15 11:36:54'),
('2d33915dba92ca0721fe157a4e61f4bac1491e5b30bbfb7c79f53d3911a98c232a266bdf1c445b44', 47, 1, 'MyApp', '[]', 0, '2021-03-12 07:20:51', '2021-03-12 07:20:51', '2022-03-12 07:20:51'),
('2ecbf4b3072439ab1370fadb3e563c2823be9bb04934651e431299954b4b8229f480ef71e613f98f', 49, 1, 'MyApp', '[]', 0, '2021-03-17 12:07:27', '2021-03-17 12:07:27', '2022-03-17 12:07:27'),
('2fcf7e3b5aef89f71414d1c3d9bf39033c7eff6952a7a3ef67c81646c7696ff177ac061b34d17014', 84, 3, 'MyApp', '[]', 0, '2021-05-05 06:53:53', '2021-05-05 06:53:53', '2022-05-05 12:23:53'),
('32e20082c7778b2f89451d8f4fd203903ed90b77069d8aef78af5268a1a3ea6acffbbff14ca981b3', 75, 1, NULL, '[]', 0, '2021-05-05 05:09:38', '2021-05-05 05:09:38', '2022-05-05 10:39:38'),
('34c42e27ca7783646c6b3a7ba8c81efe0ca38d78255703c11fccb2d50c3f5b4d00833a388e51d0f7', 80, 1, 'MyApp', '[]', 0, '2021-05-05 05:53:27', '2021-05-05 05:53:27', '2022-05-05 11:23:27'),
('356dd26ed95ef023322a3501b035f8e1343b412daeda4c21586cf2eb790fe79540c8a4f456c73efd', 45, 1, 'MyApp', '[]', 0, '2021-03-02 02:40:20', '2021-03-02 02:40:20', '2022-03-02 08:10:20'),
('3824adccee657174baa62ef5ecb1b1885416ef2c57a1afdd64465156720b659c4bd67aa16f07c8e5', 38, 1, 'MyApp', '[]', 0, '2021-02-25 02:33:02', '2021-02-25 02:33:02', '2022-02-25 08:03:02'),
('3c60468841d15a1ec7f51be86a05987bdfdab2a3be32f2f631b3fc4471113156616bdb81059ccf4e', 36, 1, 'MyApp', '[]', 0, '2021-02-24 23:38:21', '2021-02-24 23:38:21', '2022-02-25 05:08:21'),
('3f445aa106b3451129ae20163419b0c9dc0fa30078679e9fed70e6c9357ab3badeecad7fc2f0d9f8', 47, 1, 'MyApp', '[]', 0, '2021-03-12 10:27:06', '2021-03-12 10:27:06', '2022-03-12 10:27:06'),
('3f58fc45526ddec3d09f981b4a699f62522702e0ee74f50002db4d6af4b41527028f80cd7ace37fc', 22, 1, 'MyApp', '[]', 0, '2021-03-18 06:52:19', '2021-03-18 06:52:19', '2022-03-18 06:52:19'),
('40de6d33ca381d8f1c27fce74d174f49d67e582eee99daae532fd55d2cd9e6ae1d030327b34fe3ef', 40, 1, 'MyApp', '[]', 0, '2021-02-25 03:25:53', '2021-02-25 03:25:53', '2022-02-25 08:55:53'),
('41b2eb46b23285fb18800920808568a7870dd815010cf9d4ec73e98e45ed8c590cb2450bab6ccedb', 63, 1, 'MyApp', '[]', 0, '2021-03-17 08:45:32', '2021-03-17 08:45:32', '2022-03-17 08:45:32'),
('438ca56e06f254965b20c0aeed951b02cf3479dac95018c80ce5f3d70fcac1ec34ac8a1b23fb66d1', 28, 1, NULL, '[]', 0, '2021-02-24 05:26:27', '2021-02-24 05:26:27', '2022-02-24 10:56:27'),
('451941858d74e3fea284510a8a389c915539fa33d3c1da303aac7c5e69c0a9bfd5fec40342b2b0db', 51, 1, 'MyApp', '[]', 0, '2021-03-13 04:47:35', '2021-03-13 04:47:35', '2022-03-13 04:47:35'),
('470786698b831baee9dd72f431f2eb920a384ea1c3ad9ab5fcf9b065d65c509bfa0b2e0ba2f60a24', 46, 1, 'MyApp', '[]', 0, '2021-03-17 07:35:33', '2021-03-17 07:35:33', '2022-03-17 07:35:33'),
('47c207cbc5d67a7e2e44ad52586cd52f0da8d3204aa4b65f651345ec993ad41f3f7745aed29f0c61', 48, 1, 'MyApp', '[]', 0, '2021-03-15 13:51:52', '2021-03-15 13:51:52', '2022-03-15 13:51:52'),
('4e109b261482bec40b299bb45e87a1858da656db92b9034f412f03aaa2b1fd2ab69be89e162c2175', 84, 3, 'MyApp', '[]', 0, '2021-05-05 07:23:41', '2021-05-05 07:23:41', '2022-05-05 12:53:41'),
('4e2f3316ced9e6a7aead8a3e598254fa23d66ca5c5c1dad753bd5a5325ee93fb68af42641722afa4', 45, 1, 'MyApp', '[]', 0, '2021-03-02 02:40:45', '2021-03-02 02:40:45', '2022-03-02 08:10:45'),
('4e63c765352e30ddeaebf54f4c94337343739a64931e30d180cdb6bc56e5e82cf3fd5201a62eeb85', 83, 3, 'MyApp', '[]', 0, '2021-05-05 06:27:25', '2021-05-05 06:27:25', '2022-05-05 11:57:25'),
('4fb04724c7a108ad4f10dc7f0b7a74a2b83509b25b734f5a35a8d9258f040216a243e02f1712cb5c', 36, 1, 'MyApp', '[]', 0, '2021-02-24 23:39:26', '2021-02-24 23:39:26', '2022-02-25 05:09:26'),
('5036d84ad1c52ef6d466a109d916825450e6187b21ad02f2b47dc8e13e04c73867ccaaa1e73020f5', 78, 1, 'MyApp', '[]', 0, '2021-05-05 05:20:35', '2021-05-05 05:20:35', '2022-05-05 10:50:35'),
('527ae0f9fbc6a2dabbc4ed417b1212132757181eeb483558be4fb499420b8f1e529a4dff6b23a312', 47, 1, 'MyApp', '[]', 0, '2021-03-12 10:11:47', '2021-03-12 10:11:47', '2022-03-12 10:11:47'),
('55957130e223a217769227d74f6ef1dc05eea636773277cfe9e4d76810457052f75494b4bacef3bf', 31, 1, NULL, '[]', 0, '2021-02-24 05:33:17', '2021-02-24 05:33:17', '2022-02-24 11:03:17'),
('591aa9f7a70a95bd0e8ae418a965938439a35d1861eec8b7567cba49de8b27b3a8784ce3db432845', 67, 1, 'MyApp', '[]', 0, '2021-03-18 07:16:20', '2021-03-18 07:16:20', '2022-03-18 07:16:20'),
('59aec336c5ef472056885a4ecd34133bcc97eb8a2c2c07268ccc5400e3cc8144636d21b1333cfa9e', 48, 1, 'MyApp', '[]', 0, '2021-03-15 06:14:54', '2021-03-15 06:14:54', '2022-03-15 06:14:54'),
('5b12b3b4026f0a51579d88f2300f47794a43d603e1b8c5bfac93ddbafaca7360144124ad7403076f', 85, 3, 'MyApp', '[]', 0, '2021-05-05 22:10:39', '2021-05-05 22:10:39', '2022-05-06 03:40:39'),
('5f30cf586b0a1975aec2e9871837e5d1ee411d3646d07152e9c2b06f8d7b0af7162435b50baffb5d', 36, 1, 'MyApp', '[]', 0, '2021-02-24 23:40:19', '2021-02-24 23:40:19', '2022-02-25 05:10:19'),
('663e1f3bb541519eac5bfd3bc6628b8bb00cca6ff226cdc40eaaf564c30db212ea208e15889a4cf9', 76, 1, NULL, '[]', 0, '2021-05-05 05:10:41', '2021-05-05 05:10:41', '2022-05-05 10:40:41'),
('66df5c9ad3622efecfc85e6c7e6dc1560b295cee2b082fe75e7ddcc67753c4bb7a10f14fafa3bb49', 48, 1, 'MyApp', '[]', 0, '2021-03-13 05:30:10', '2021-03-13 05:30:10', '2022-03-13 05:30:10'),
('67c2abfec69e5619e4f4858aa917b483eaae5abb3c99289f3b4e16a7f14cb46af6284d4a604cc460', 49, 1, 'MyApp', '[]', 0, '2021-03-12 10:24:50', '2021-03-12 10:24:50', '2022-03-12 10:24:50'),
('6e2003f7cfe5ab861804b7652faf7ee221db331b4d546a72596ebdc4a3d17518ed81f4099271c9c2', 61, 1, 'MyApp', '[]', 0, '2021-03-16 10:07:26', '2021-03-16 10:07:26', '2022-03-16 10:07:26'),
('71ea7c46acfb7ce6b9a80634277742d26480bd87446c55bde6905a1a429fad39a7f6a58b9a0ec735', 61, 1, 'MyApp', '[]', 0, '2021-03-16 10:16:22', '2021-03-16 10:16:22', '2022-03-16 10:16:22'),
('72095717379b7fa75da9af48907ca632d88ad4f8ab5b5779b8de7c6a593fa1cf6a21a16e6c79d5e2', 22, 1, 'MyApp', '[]', 0, '2021-02-27 12:56:45', '2021-02-27 12:56:45', '2022-02-27 18:26:45'),
('7cf39c1755d0541b606e46e98d14c6013342c7f9ba113a1289d8835d4951a3a1f907d255eaf89fd7', 22, 1, 'MyApp', '[]', 0, '2021-03-17 12:26:47', '2021-03-17 12:26:47', '2022-03-17 12:26:47'),
('7e9cb54eeae8ed97f9adf896012c7bb91e66e316864ac48d86931c19cf49e391350bd77bd6103c1e', 36, 1, 'MyApp', '[]', 0, '2021-02-24 23:39:05', '2021-02-24 23:39:05', '2022-02-25 05:09:05'),
('7fffbf27f8186d6fc9177009293935ae774a1272db621a5bf56e8a8f7be1edcf9a9ba4fe212b3e68', 48, 1, 'MyApp', '[]', 0, '2021-03-16 05:15:19', '2021-03-16 05:15:19', '2022-03-16 05:15:19'),
('834721cf15e55323978691489a6c15e509f654d7bc053237140b99d08eafe0ad9228bb405d79f4b6', 38, 1, 'MyApp', '[]', 0, '2021-02-25 02:26:41', '2021-02-25 02:26:41', '2022-02-25 07:56:41'),
('8411dbb6b22facaa76ca4befb1c69faae0ff2fef018c5f3ca58a4b4e3444a5814976a891b95763c8', 48, 1, 'MyApp', '[]', 0, '2021-03-12 10:07:58', '2021-03-12 10:07:58', '2022-03-12 10:07:58'),
('87d66c88005191d65871391654253e20d05660c17c9470cb15a4fae2e823848b29e3ded0c79a9c04', 44, 1, 'MyApp', '[]', 0, '2021-02-26 05:39:19', '2021-02-26 05:39:19', '2022-02-26 11:09:19'),
('87f747493d84ea6dd07ed7f1f24efaf9251c6a1052cb1f179f114733b09bd40b9512d0011d1d59c3', 36, 1, 'MyApp', '[]', 0, '2021-02-24 23:36:10', '2021-02-24 23:36:10', '2022-02-25 05:06:10'),
('88cb976aaf5f45b94c936e3d5e406b2f207434814eed0770e1876c33bc15e2c2a9dbd64948d14842', 71, 1, NULL, '[]', 0, '2021-05-05 04:23:46', '2021-05-05 04:23:46', '2022-05-05 09:53:46'),
('8b10d538478371d9e51f34ee09ee51470d16ea0a0e2f2638a146cbc3617ce190ec990aaccf559423', 73, 1, NULL, '[]', 0, '2021-05-05 04:46:23', '2021-05-05 04:46:23', '2022-05-05 10:16:23'),
('8c4382927bdb96884a637eadbeab007c34247124c33c0b809be9efdee11cfaad2fe2f4a74dd49ec8', 25, 1, NULL, '[]', 0, '2021-02-24 05:19:41', '2021-02-24 05:19:41', '2022-02-24 10:49:41'),
('8e0a71f57ba60e86d82b84da09f790370100a22f3e43b352e6a0f6639933b368ac347d8dc72681b3', 48, 1, 'MyApp', '[]', 0, '2021-03-15 07:12:50', '2021-03-15 07:12:50', '2022-03-15 07:12:50'),
('922ea4bb712cb42ef47858cbcee11a0d6f416b9ad1edf50d515c66b19e44c1bded8cd49cff0796c7', 86, 3, 'MyApp', '[]', 0, '2021-05-06 01:04:44', '2021-05-06 01:04:44', '2022-05-06 06:34:44'),
('9264cfe0abe207b66441ead4f8deaf923e4f3a63032a030d2dab5faee26cd437b0d61774165e3e45', 48, 1, 'MyApp', '[]', 0, '2021-03-12 07:21:57', '2021-03-12 07:21:57', '2022-03-12 07:21:57'),
('95bcbda56c71f8a6fed58da9f20d2ad16ff78713674165d63428c540cb9533211bef62142217abbf', 50, 1, 'MyApp', '[]', 0, '2021-03-12 13:46:43', '2021-03-12 13:46:43', '2022-03-12 13:46:43'),
('983e185e692a415e1f92b55231e4325ea00ce8d0f4544670fdc9ccb605c9a1532ffd67cd178e555c', 48, 1, 'MyApp', '[]', 0, '2021-03-12 06:35:47', '2021-03-12 06:35:47', '2022-03-12 06:35:47'),
('9a69a399f4e74468e8eeb9544731304b174f7dbab0837f8675e8f67b06586ea72ab1237e9fd4b751', 39, 1, 'MyApp', '[]', 0, '2021-02-25 02:42:20', '2021-02-25 02:42:20', '2022-02-25 08:12:20'),
('9ac21b4d96c3ff45555f53730f8661464e0ffac95856a799e55873da8a29fba48b0081aa78ce54fa', 48, 1, 'MyApp', '[]', 0, '2021-03-15 13:32:24', '2021-03-15 13:32:24', '2022-03-15 13:32:24'),
('9f2eb59758106966b78d51f7bfa3b67cc5d80d08eb3346a3f9a5fb30858717da83b5746713e7597d', 56, 1, 'MyApp', '[]', 0, '2021-03-15 11:04:33', '2021-03-15 11:04:33', '2022-03-15 11:04:33'),
('a08360859ec18e27ef33c0dccd21269bb569f04e21d1d63eb7b44b4c075ce6f71b67fc444a47d2ca', 56, 1, 'MyApp', '[]', 0, '2021-03-15 12:43:35', '2021-03-15 12:43:35', '2022-03-15 12:43:35'),
('a099772b58c54299ec0146e0bbc3a1db063368711382d85f07cff8affe930b12c3b2fd2ee2a388b9', 50, 1, 'MyApp', '[]', 0, '2021-03-12 13:47:10', '2021-03-12 13:47:10', '2022-03-12 13:47:10'),
('a26ef6b77e42e44c48cc8f9a48c21b1595d2d18241e96500877a2c7ca7ddce6c40a131ef9cce3414', 42, 1, 'MyApp', '[]', 0, '2021-02-25 03:33:23', '2021-02-25 03:33:23', '2022-02-25 09:03:23'),
('a5e76e277b686147f66e89e33878cc028d42014220b55969658133c585096177811db8c24b803e7a', 38, 1, 'MyApp', '[]', 0, '2021-02-25 02:33:03', '2021-02-25 02:33:03', '2022-02-25 08:03:03'),
('a615464cefb303626f6cc7f3c07d02263b96b9a30c4dab677189969409fa37947f5bd133f6badfb1', 22, 1, 'MyApp', '[]', 0, '2021-03-18 06:53:03', '2021-03-18 06:53:03', '2022-03-18 06:53:03'),
('a649da0ff90c676605950dca2aa7187fe42f878900bdda59f5718762a5cc2b5807eb3f08c9bfbf72', 48, 1, 'MyApp', '[]', 0, '2021-03-15 11:40:41', '2021-03-15 11:40:41', '2022-03-15 11:40:41'),
('a65a87262d08244f27f2a61592325aa4d73418df5391fa640655a9abef7b9a6f712807d488f59233', 48, 1, 'MyApp', '[]', 0, '2021-03-12 07:23:17', '2021-03-12 07:23:17', '2022-03-12 07:23:17'),
('a73ea5a887e4bad4df2515f0b35109e05807e5da8221bae8d8fc6ab70e0d72fb8fc3d3e15db3b49e', 56, 1, 'MyApp', '[]', 0, '2021-03-15 12:55:50', '2021-03-15 12:55:50', '2022-03-15 12:55:50'),
('a745f29f616b420116ee8c0e3c30bd51f594518025367e7e1df9c436dd482d8488afd16d52faa23a', 33, 1, NULL, '[]', 0, '2021-02-24 05:59:35', '2021-02-24 05:59:35', '2022-02-24 11:29:35'),
('a94b855f014864538ce15a31c74847b7bc32ddb27c1e9ccf67144abcdf7b64ee7b3db9edafbf5d98', 86, 3, 'MyApp', '[]', 0, '2021-05-06 01:04:43', '2021-05-06 01:04:43', '2022-05-06 06:34:43'),
('a954119faab0c7b14d6673eb9673f9fc9f51b334530f70f17f7e0825824e95d969198c8d764102b7', 49, 1, 'MyApp', '[]', 0, '2021-03-16 15:59:51', '2021-03-16 15:59:51', '2022-03-16 15:59:51'),
('af5659acec6cb5401b9ea8b919911ccfe908c16b9fe223ace518e9958d8366deadd3d5ac3afdf07d', 77, 1, 'MyApp', '[]', 0, '2021-05-05 05:14:24', '2021-05-05 05:14:24', '2022-05-05 10:44:24'),
('afd187817370eee36c8dfdf7249c20843e351826e14cd9b7bfdc0683c882333154740599e5104160', 27, 1, NULL, '[]', 0, '2021-02-24 05:24:14', '2021-02-24 05:24:14', '2022-02-24 10:54:14'),
('b28a25d8bfe9765b740b99504d12e5e1e0c18b8aa893a4892db3ac962565fb429b3cc6072d79217f', 48, 1, 'MyApp', '[]', 0, '2021-03-16 09:24:52', '2021-03-16 09:24:52', '2022-03-16 09:24:52'),
('b28eeef29a27bc545b64769d534fc1b605c8856743c1f2256ac96fc28dd801e9ff7e1b52a20066bf', 36, 1, 'MyApp', '[]', 0, '2021-02-24 23:35:25', '2021-02-24 23:35:25', '2022-02-25 05:05:25'),
('b3915cfca21b4902c836c15464b137f00f621095c661b51497a96813ce0d1a719ca125b781e101d1', 47, 1, 'MyApp', '[]', 0, '2021-03-12 06:36:18', '2021-03-12 06:36:18', '2022-03-12 06:36:18'),
('b561b4ab1d1d1c4147c7874b6869f2f7cba241ad348758c3bfe3a2334c950909eef059f1df9cce81', 48, 1, 'MyApp', '[]', 0, '2021-03-16 05:38:06', '2021-03-16 05:38:06', '2022-03-16 05:38:06'),
('b58b3a857caff6dc3d11472af4a672f6629023720df01b8a855554b4bbcc7bb2c9dbcd094f0d3839', 26, 1, NULL, '[]', 0, '2021-02-24 05:22:32', '2021-02-24 05:22:32', '2022-02-24 10:52:32'),
('b6aa39a44bda90bc26b2e8ac48da8a6a398b0c17ac0e83ba058e3aef75465b93d8daa7e85d6d3d0e', 63, 1, 'MyApp', '[]', 0, '2021-03-17 14:33:08', '2021-03-17 14:33:08', '2022-03-17 14:33:08'),
('b79f773d8d10b4313645f962a134d6fed87b808023bce81fce9761f86f0af94f859b1b5f5aa78c29', 48, 1, 'MyApp', '[]', 0, '2021-03-17 07:20:24', '2021-03-17 07:20:24', '2022-03-17 07:20:24'),
('b7fb9ffdce9c1a0825b4127d0254eaedb54471ddbb8e7ba21adb5d5cf2bd18d974c5cedb2d212356', 56, 1, 'MyApp', '[]', 0, '2021-03-15 11:10:48', '2021-03-15 11:10:48', '2022-03-15 11:10:48'),
('ba974124f4c567f7c347a9129f54cb128e71b4d1ef54af5a8cae7b266495bb2c1d6ac7714eadd751', 63, 1, 'MyApp', '[]', 0, '2021-03-17 09:31:12', '2021-03-17 09:31:12', '2022-03-17 09:31:12'),
('bf908f27e8092ce00e6ae7597c49bd59c19380d8a98ae93d0d66cc5213ef1a9120139b05526eeb26', 49, 1, 'MyApp', '[]', 0, '2021-03-17 04:10:01', '2021-03-17 04:10:01', '2022-03-17 04:10:01'),
('c238c750ee5340806a1a5222b083d0c86d51ce7f6dfdbd9fae9a18b3b2cb32531bc60af78929c225', 56, 1, 'MyApp', '[]', 0, '2021-03-15 10:47:22', '2021-03-15 10:47:22', '2022-03-15 10:47:22'),
('c389c0e2b8cf84c00583de8bac23e7d4e7fc01e948a1487e3c5be71fc3c85bb9cb0568476a42cabb', 42, 1, 'MyApp', '[]', 0, '2021-02-25 03:34:51', '2021-02-25 03:34:51', '2022-02-25 09:04:51'),
('c45675f714c3608009192b74f05568bba9d52dce9487d1b0d4cb691a879011b317adf6e26755c9e1', 49, 1, 'MyApp', '[]', 0, '2021-03-12 10:14:13', '2021-03-12 10:14:13', '2022-03-12 10:14:13'),
('c548637cbc0387a108db69000049ed96363c1f0f944ad987131d10583b4f2a0b207ed355f030de64', 42, 1, 'MyApp', '[]', 0, '2021-02-25 06:16:51', '2021-02-25 06:16:51', '2022-02-25 11:46:51'),
('c8d2994b66945caca5bec4c667d9c3be0df3867cf6dbb271f2b1f00f4c9ddd9bef0dcc61f8563f89', 46, 1, 'MyApp', '[]', 0, '2021-03-12 06:33:25', '2021-03-12 06:33:25', '2022-03-12 06:33:25'),
('c905ccdd723d3864a38eb1da4b91850de9d0b05a13645a216c440b61ec659be187c530c32b4f4f69', 68, 1, 'MyApp', '[]', 0, '2021-03-18 07:20:07', '2021-03-18 07:20:07', '2022-03-18 07:20:07'),
('ca910bacf136f71b6301fd46b1334f854c2959a58239c0db2c11d2b2bd14be2fae846dd247c55f80', 3, 1, 'MyApp', '[]', 0, '2021-03-02 23:47:04', '2021-03-02 23:47:04', '2022-03-03 05:17:04'),
('caeee9d472f44eda8cd11caeee562adba0931d65212dc7c340dc3f884a72ffd1c141039cad7553e5', 63, 1, 'MyApp', '[]', 0, '2021-03-16 10:39:59', '2021-03-16 10:39:59', '2022-03-16 10:39:59'),
('cc630d37bb56bf40df98fcbc34c8a6c59bde317b3f2affb60efc7141614524c9cff459b3d0fda94b', 41, 1, 'MyApp', '[]', 0, '2021-02-25 03:30:19', '2021-02-25 03:30:19', '2022-02-25 09:00:19'),
('cdb93f695f3e2a4193ad3c15d92b7c0f892916ef9a1f5a07fc8f4b60ec423ece2cd0f5784e70b0aa', 56, 1, 'MyApp', '[]', 0, '2021-03-15 12:36:52', '2021-03-15 12:36:52', '2022-03-15 12:36:52'),
('ce1b1dbdbe77513b3bd8aa9e4e9f82af162f8082a5709d658b90c394a81c6905b9090fbcbd80c968', 56, 1, 'MyApp', '[]', 0, '2021-03-15 11:07:46', '2021-03-15 11:07:46', '2022-03-15 11:07:46'),
('d4a6039f571ee7daf14f435f3301642fa9da8fef7e94315458ee245b3ab0e25962afe83b3017f05a', 43, 1, 'MyApp', '[]', 0, '2021-02-26 01:29:06', '2021-02-26 01:29:06', '2022-02-26 06:59:06'),
('d72791779f60038a740902a21912a6d35992de43f82165193b666351f41c92fc103630103156bf8b', 48, 1, 'MyApp', '[]', 0, '2021-03-18 09:41:15', '2021-03-18 09:41:15', '2022-03-18 09:41:15'),
('d86bdcae8d70657509b2221f4a8af61deab85b8ccbe1d94499b4fefeada056f2f45ac1666ec39e7e', 67, 1, 'MyApp', '[]', 0, '2021-03-18 07:16:52', '2021-03-18 07:16:52', '2022-03-18 07:16:52'),
('dde53821d1442034cbb3fd65b449c0c40d0742ddf709164a2f7f4faf9f333d1c1fc456c7de57ff47', 57, 1, 'MyApp', '[]', 0, '2021-03-16 02:32:30', '2021-03-16 02:32:30', '2022-03-16 02:32:30'),
('dfa4a5ce4cd4cfa4477ea333de9d4a8b220dc1abea41df5aee639305a42027dcefe971c7dce04e66', 48, 1, 'MyApp', '[]', 0, '2021-03-17 11:45:30', '2021-03-17 11:45:30', '2022-03-17 11:45:30'),
('e2dd5caa5971f2642db380040ad677ba6bee6bd2c38d9bcf41d1ac20e0b4c74779731add28c3c3c8', 68, 1, 'MyApp', '[]', 0, '2021-03-18 07:19:50', '2021-03-18 07:19:50', '2022-03-18 07:19:50'),
('e53c19508ee534036575e04a08c8d99af1ec845e9c287bf867647994bbfeb1f9758ba8a0103c8e7e', 34, 1, 'MyApp', '[]', 0, '2021-02-24 06:38:51', '2021-02-24 06:38:51', '2022-02-24 12:08:51'),
('e5c05eae119e56cfee3bef8125111f4d2dbbb6e4fff8986801259ed675cd88b9a4d8091320d60eae', 50, 1, 'MyApp', '[]', 0, '2021-03-16 15:58:50', '2021-03-16 15:58:50', '2022-03-16 15:58:50'),
('e8d1a08f01f8cad447b6ecc4ac5fb2b8ddcae7e96c626f5d8d2676d3d63a80e360ec6d914239b9cc', 62, 1, 'MyApp', '[]', 0, '2021-03-16 10:12:01', '2021-03-16 10:12:01', '2022-03-16 10:12:01'),
('e9cc6fc21e4b38d6fd2b811f67aa40f18267d2877350a261bbfb60d35e4014d0c1245d7488e5a9d4', 37, 1, 'MyApp', '[]', 0, '2021-02-25 00:52:43', '2021-02-25 00:52:43', '2022-02-25 06:22:43'),
('eb8ebd2c61a5715b719602b14f1a559e92aa1738c5262a266ac63f21b8aedd9d5d8e74dcc71db464', 48, 1, 'MyApp', '[]', 0, '2021-03-16 09:49:17', '2021-03-16 09:49:17', '2022-03-16 09:49:17'),
('ee1b014debeb444837225f866d2c6786d6aa08ce8833d4637837b801bc39be934cfcd78c494add06', 85, 3, 'MyApp', '[]', 0, '2021-05-05 22:11:27', '2021-05-05 22:11:27', '2022-05-06 03:41:27'),
('ee5f84ba440467c161b92bc1b18fc13269c198349462554ecde3c6b5cac1a79ca23c815b5cde4309', 22, 1, 'MyApp', '[]', 0, '2021-03-17 10:44:52', '2021-03-17 10:44:52', '2022-03-17 10:44:52'),
('eee645f17a5b742505e042d9f20207ea8ff3ab9d5f01ca0de5cbd159e8c1aa5d7d3d14a9c5091834', 47, 1, 'MyApp', '[]', 0, '2021-03-12 07:12:41', '2021-03-12 07:12:41', '2022-03-12 07:12:41'),
('f3686e54115e1d8a7d6d3dc04f0caec424706bd55bc73a1d617a7460dfd0793a53a5af57d3055fe3', 47, 1, 'MyApp', '[]', 0, '2021-03-12 06:34:29', '2021-03-12 06:34:29', '2022-03-12 06:34:29'),
('f68e5dac685c7bf6c85b1b339f6ae8a0d336d6aa915099ffb5a85d910aa535affb41758e9bbba13e', 46, 1, 'MyApp', '[]', 0, '2021-03-16 09:00:46', '2021-03-16 09:00:46', '2022-03-16 09:00:46'),
('f79251f93beebe05e99c2514559e4c437fe8f718012073a2fcf506a499d886a9625a35c013c7fe95', 47, 1, 'MyApp', '[]', 0, '2021-03-13 06:29:39', '2021-03-13 06:29:39', '2022-03-13 06:29:39'),
('fa164a1d4ad94eedc50a5c070d215952931a9e6d75ba1e6e90f9ca537a22f0fb566b0efc2603700e', 48, 1, 'MyApp', '[]', 0, '2021-03-15 11:32:57', '2021-03-15 11:32:57', '2022-03-15 11:32:57'),
('fa333cfff9c8ae6ec8defc11393cd77e1390f3ae9eff546f042c758c6b693d5994433a1cc2e73108', 61, 1, 'MyApp', '[]', 0, '2021-03-16 10:07:00', '2021-03-16 10:07:00', '2022-03-16 10:07:00'),
('fda547f8857aceb1339a91fe3fa7c239d0ec1bb3090495a491a50eea0834d596fbc0161e8c211217', 63, 1, 'MyApp', '[]', 0, '2021-03-16 15:14:58', '2021-03-16 15:14:58', '2022-03-16 15:14:58');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'DezzXzKGFHMqBH7SlhhTESkZJy9ivdrLnQLaNDlD', 'http://localhost', 1, 0, 0, '2021-02-24 05:17:59', '2021-02-24 05:17:59'),
(2, NULL, 'Laravel Password Grant Client', 'At1WKkrjmZYbV20imakMfr4MIYvY9ODaFoivHpk6', 'http://localhost', 0, 1, 0, '2021-02-24 05:17:59', '2021-02-24 05:17:59'),
(3, NULL, 'Laravel Personal Access Client', 'HGHwCCYxyEPZwrrSEmFXwkiyw6c1rFe2fCDrSgEq', 'http://localhost', 1, 0, 0, '2021-05-05 06:26:05', '2021-05-05 06:26:05'),
(4, NULL, 'Laravel Password Grant Client', 'GfL3XfKeubucP01JvLYPKITwr3lQw8DUhWegHlii', 'http://localhost', 0, 1, 0, '2021-05-05 06:26:05', '2021-05-05 06:26:05');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2021-02-24 05:17:59', '2021-02-24 05:17:59'),
(2, 3, '2021-05-05 06:26:05', '2021-05-05 06:26:05');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `package_id` bigint(20) DEFAULT NULL,
  `payment_type` varchar(50) DEFAULT NULL,
  `currency` varchar(50) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `transaction_id` varchar(255) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `package_id`, `payment_type`, `currency`, `amount`, `transaction_id`, `status`, `updated_at`, `created_at`) VALUES
(1, 3, 12, NULL, NULL, '456.00', 'pay_HZqM3EjhdLhY0D', 1, '2021-07-16 21:37:02', '2021-07-16 21:37:02'),
(2, 3, 12, NULL, NULL, '456.00', 'pay_HZqOYSbiPlOmgl', 1, '2021-07-16 21:38:18', '2021-07-16 21:38:18'),
(3, 3, 12, 'Stripe', NULL, '456.00', 'ch_1JE7a3SGI0QnWGvHApG4KLJt', 1, '2021-07-17 01:35:00', '2021-07-17 01:35:00'),
(4, 3, 12, 'Razorpay', NULL, '456.00', 'pay_HZuYMhBKhmcSNS', 1, '2021-07-17 01:42:23', '2021-07-17 01:42:23'),
(5, 3, 12, 'Razorpay', NULL, '456.00', 'pay_HZuYMhBKhmcSNS', 1, '2021-07-17 01:42:44', '2021-07-17 01:42:44'),
(6, 3, 12, 'Razorpay', NULL, '456.00', 'pay_HZufc1wMhhXgLD', 1, '2021-07-17 01:49:13', '2021-07-17 01:49:13'),
(7, 3, 12, 'Razorpay', NULL, '456.00', 'pay_HZuiRzGvBX9FPK', 1, '2021-07-17 01:52:57', '2021-07-17 01:52:57');

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `Item_title` varchar(255) DEFAULT NULL COMMENT 'Packages.description',
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `order_id`, `Item_title`, `updated_at`, `created_at`) VALUES
(1, 1, NULL, '2021-04-17 01:11:38', '2021-04-17 01:11:38'),
(2, 2, NULL, '2021-04-17 01:49:59', '2021-04-17 01:49:59'),
(3, 3, NULL, '2021-04-17 01:58:59', '2021-04-17 01:58:59'),
(4, 4, NULL, '2021-04-17 02:06:00', '2021-04-17 02:06:00'),
(5, 5, NULL, '2021-04-17 02:22:50', '2021-04-17 02:22:50'),
(6, 1, NULL, '2021-07-16 21:37:02', '2021-07-16 21:37:02'),
(7, 2, NULL, '2021-07-16 21:38:18', '2021-07-16 21:38:18'),
(8, 3, NULL, '2021-07-17 01:35:00', '2021-07-17 01:35:00'),
(9, 4, NULL, '2021-07-17 01:42:23', '2021-07-17 01:42:23'),
(10, 5, NULL, '2021-07-17 01:42:45', '2021-07-17 01:42:45'),
(11, 6, NULL, '2021-07-17 01:49:13', '2021-07-17 01:49:13'),
(12, 7, NULL, '2021-07-17 01:52:57', '2021-07-17 01:52:57');

-- --------------------------------------------------------

--
-- Table structure for table `organizations`
--

CREATE TABLE `organizations` (
  `id` int(11) NOT NULL,
  `user_id` bigint(11) DEFAULT NULL,
  `org_unique_id` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `organization_users`
--

CREATE TABLE `organization_users` (
  `id` int(11) NOT NULL,
  `organization_id` bigint(11) DEFAULT NULL,
  `user_id` bigint(11) DEFAULT NULL,
  `user_unique_id` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `status` int(4) DEFAULT 0 COMMENT '0=>in_active,1=>active',
  `package_type` int(11) DEFAULT 0 COMMENT '0=>monthly,1=>annually',
  `package_time` varchar(50) DEFAULT NULL,
  `amount` int(50) DEFAULT NULL,
  `support` text DEFAULT NULL,
  `is_default` tinyint(4) DEFAULT 0 COMMENT '0=>not_default,1=>default',
  `is_free_trial` tinyint(4) DEFAULT 0 COMMENT '0=>not_free_trial,1=>free_trial',
  `type` tinyint(11) DEFAULT NULL,
  `currency` varchar(11) DEFAULT NULL,
  `users_for_facial_recognition` int(11) DEFAULT NULL,
  `check_ins` int(55) DEFAULT NULL,
  `touch_point` int(11) DEFAULT NULL,
  `admin_touch_point` int(11) DEFAULT NULL,
  `front_touch_point` int(11) DEFAULT NULL,
  `edge_touch_point` int(11) DEFAULT NULL,
  `data_maintained_days` int(11) DEFAULT NULL,
  `backend_integration` int(11) DEFAULT NULL,
  `sites` int(11) DEFAULT NULL,
  `rate_limit` int(11) DEFAULT NULL,
  `admin_users` int(11) DEFAULT NULL,
  `custom` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `name`, `description`, `image`, `status`, `package_type`, `package_time`, `amount`, `support`, `is_default`, `is_free_trial`, `type`, `currency`, `users_for_facial_recognition`, `check_ins`, `touch_point`, `admin_touch_point`, `front_touch_point`, `edge_touch_point`, `data_maintained_days`, `backend_integration`, `sites`, `rate_limit`, `admin_users`, `custom`, `created_at`, `updated_at`) VALUES
(7, 'admin', '<p>jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj</p>', NULL, 0, 0, '8', 55655555, NULL, 1, 0, 0, 'DOLLAR', 100, NULL, 1, NULL, NULL, NULL, 8, 8, 8, 8, 8, NULL, '2021-07-15 08:17:49', '2021-07-15 08:17:49'),
(8, 'admin', '<p>jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj</p>', NULL, 0, 0, '8', 55655555, NULL, 1, 0, 0, 'DOLLAR', 100, NULL, 1, NULL, NULL, NULL, 8, 8, 8, 8, 8, NULL, '2021-07-15 08:18:00', '2021-07-15 08:18:00'),
(9, 'munna', '<p>bggbgbgbgbgbgbgbgbgbgbgbgbgbggbb</p>', NULL, 1, 0, '5', 2555555, NULL, 1, 0, 0, 'DOLLAR', 100, 6, 1, 2, NULL, NULL, 8, 2, 5, 4, 5, NULL, '2021-07-15 08:25:54', '2021-07-15 08:25:54'),
(10, 'javed tanwar', '<p>nvbnmhmjhjmjhmhjmhj</p>', NULL, 1, 0, '2', NULL, NULL, 1, 1, 0, 'DOLLAR', NULL, 2, NULL, NULL, NULL, NULL, 2, 2, 5, 2, 2, NULL, '2021-07-15 08:34:08', '2021-07-15 08:34:08'),
(11, 'javed tanwar', '<p>gjhgkvkj</p>', 'C:\\xampp\\tmp\\php9346.tmp', 1, 1, '1', 1, NULL, 1, 1, 0, 'INR', 10, 1, NULL, NULL, NULL, NULL, 2, 1, 1, 1, 1, NULL, '2021-07-15 08:41:33', '2021-07-15 08:41:33'),
(12, 'javed tanwar', '<p>fjhgkjfjhj</p>', 'C:\\xampp\\tmp\\php707E.tmp', 1, 0, '46', 456, NULL, 1, 1, 0, 'INR', 10, 46, NULL, NULL, NULL, NULL, 46, 646, 46, 64, 46, NULL, '2021-07-15 08:55:36', '2021-07-15 08:55:36'),
(13, 'hghgh', '<p>hhhhh</p>', 'uploads/post/1626359962pexels-prathsnap-4117257.jpg', 1, 0, '5', 56555, NULL, 1, 1, 0, 'DOLLAR', 100, 5, 2, NULL, NULL, NULL, 5, 5, 5, 5, 5, NULL, '2021-07-15 09:09:22', '2021-07-15 09:09:22'),
(14, 'tanwar khan', '<p>dddddddddddddddddddd</p>', 'uploads/package/1626410500pexels-jess-bailey-designs-1764436.jpg', 1, 0, '1', 4444444, NULL, 1, 1, 0, 'DOLLAR', 100, 45, 2, 200, 200, 200, 21, 200, 200, 20, 200, NULL, '2021-07-15 23:11:40', '2021-07-15 23:11:40'),
(15, 'javed tanwar', '<p>wertyu</p>', 'uploads/package/1626413394Resume_Javed tanwar_Format6 (2)_page-0001.jpg', 1, 0, '23', 222222, NULL, 1, 1, 0, 'INR', 10, 3, 2, 23, 32, 23, 23, 23, 23, 32, 32, '[{\"name\":\"we\",\"category\":\"w\",\"value\":\"<p>weg<\\/p>\"},{\"name\":\"ew\",\"category\":\"rwer\",\"value\":\"<p>eerttet<\\/p>\"}]', '2021-07-15 23:59:54', '2021-07-15 23:59:54');

-- --------------------------------------------------------

--
-- Table structure for table `packages_old`
--

CREATE TABLE `packages_old` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `package_type` int(20) DEFAULT NULL COMMENT '0=subscription, 1=premium',
  `title` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `package_time` int(11) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `advertisement_type` tinyint(4) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `packages_old`
--

INSERT INTO `packages_old` (`id`, `package_type`, `title`, `description`, `package_time`, `amount`, `status`, `advertisement_type`, `updated_at`, `created_at`) VALUES
(1, 0, 'Latest', 'First Package This New', 1, '240.00', 1, 0, '2021-04-16 05:07:30', '2021-04-16 07:23:26'),
(2, 1, 'saif', 'bhhgb', 9, '500.00', 1, NULL, '2021-04-16 05:11:21', '2021-04-16 02:40:26'),
(5, 0, 'MOHAMMED SAIF QURESHI', 'IO', 3, '300.00', 1, NULL, '2021-04-16 23:22:39', '2021-04-16 04:52:34');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'superadmin', 'web', '2021-04-15 04:33:54', '2021-04-15 04:33:54'),
(2, 'admin', 'web', '2021-04-15 04:35:25', '2021-04-15 04:35:25'),
(3, 'user', 'web', '2021-04-15 04:35:46', '2021-04-15 04:35:46');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tranings`
--

CREATE TABLE `tranings` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(255) DEFAULT NULL,
  `org_unique_id` varchar(255) DEFAULT NULL,
  `org_user_id` bigint(11) DEFAULT NULL,
  `user_unique_id` varchar(255) DEFAULT NULL,
  `user_id` bigint(11) DEFAULT NULL,
  `training_type` tinyint(4) DEFAULT NULL COMMENT '0=user_tranings,\r\n1=user_organization',
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0=pending,\r\n1=completed',
  `scheduled_time` datetime DEFAULT NULL,
  `logs` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tranings`
--

INSERT INTO `tranings` (`id`, `unique_id`, `org_unique_id`, `org_user_id`, `user_unique_id`, `user_id`, `training_type`, `status`, `scheduled_time`, `logs`, `created_at`, `updated_at`) VALUES
(1, '1', '1', 1, '1', 55, 1, 0, '2021-05-26 11:17:51', 'dgdgdfdf', '2021-05-25 11:17:51', '2021-05-26 11:17:51');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_unique_id` bigint(11) DEFAULT NULL,
  `org_unique_id` bigint(11) DEFAULT NULL,
  `country_id` bigint(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `domain_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_method` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_line_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_address_line_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street_address_line_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pan_card_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gst_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `subscription_expire_date` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `api_token` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `token` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_unique_id`, `org_unique_id`, `country_id`, `name`, `phone_number`, `email`, `company_name`, `domain_name`, `account_number`, `billing_type`, `payment_method`, `address`, `address_line_2`, `billing_address`, `billing_address_line_2`, `street_address`, `street_address_line_2`, `city`, `state`, `zip_code`, `pan_card_number`, `gst_number`, `status`, `email_verified_at`, `subscription_expire_date`, `password`, `remember_token`, `api_token`, `token`, `created_at`, `updated_at`) VALUES
(2, NULL, NULL, NULL, 'user', NULL, 'user@fars.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '$2a$10$JxlW7fajVZ4YpLy3Mrc/ferjagzcVirepQF.lf/MR9t/Pvcwye4em', NULL, NULL, NULL, '2021-04-16 04:08:33', '2021-04-17 01:11:38'),
(3, NULL, NULL, NULL, 'Si', NULL, 'admin@fars.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '2025-05-17 01:52:57', '$2a$10$JxlW7fajVZ4YpLy3Mrc/ferjagzcVirepQF.lf/MR9t/Pvcwye4em', NULL, NULL, NULL, NULL, '2021-07-17 01:52:58');

-- --------------------------------------------------------

--
-- Table structure for table `users_original`
--

CREATE TABLE `users_original` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_unique_id` bigint(11) DEFAULT NULL,
  `org_unique_id` bigint(11) DEFAULT NULL,
  `country_id` bigint(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street_address_line_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pan_card_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gst_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `subscription_expire_date` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `api_token` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `token` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users_original`
--

INSERT INTO `users_original` (`id`, `user_unique_id`, `org_unique_id`, `country_id`, `name`, `phone_number`, `email`, `company_name`, `address`, `billing_address`, `street_address`, `street_address_line_2`, `city`, `state`, `zip_code`, `pan_card_number`, `gst_number`, `status`, `email_verified_at`, `subscription_expire_date`, `password`, `remember_token`, `api_token`, `token`, `created_at`, `updated_at`) VALUES
(2, NULL, NULL, NULL, 'user', NULL, 'user@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '$2a$10$JxlW7fajVZ4YpLy3Mrc/ferjagzcVirepQF.lf/MR9t/Pvcwye4em', NULL, NULL, NULL, '2021-04-16 04:08:33', '2021-04-17 01:11:38'),
(3, NULL, NULL, NULL, 'Si', NULL, 'si@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '2021-05-17 02:22:50', '$2a$10$JxlW7fajVZ4YpLy3Mrc/ferjagzcVirepQF.lf/MR9t/Pvcwye4em', NULL, NULL, NULL, NULL, '2021-04-17 02:22:50'),
(4, NULL, NULL, NULL, 'MOHAMMED SAIF QURESHI', NULL, 'ali000000@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '$2y$10$Md086uE.OeXCOoBaCp1KIueP0V7Hz90VW3L6jIvt2Grc5YtVsqMXm', NULL, NULL, NULL, '2021-04-17 04:41:53', '2021-04-17 04:41:53'),
(6, NULL, NULL, NULL, 'superadmin', NULL, 'superadmin@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '$2a$10$JxlW7fajVZ4YpLy3Mrc/ferjagzcVirepQF.lf/MR9t/Pvcwye4em', 'A9qLW4ZXu6Ei78OoPG8aZysVGCPCRg0YE5AhwVzfqx7yUg4bzO6EvnvL0Dpd', NULL, NULL, NULL, NULL),
(7, NULL, NULL, NULL, 'saif', NULL, 'saif@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '$2y$10$wO1Sy.88OT0voGDHBWgVFu2maNtjf5ShdZaYeJDhSWtLxDgbh4osG', NULL, NULL, NULL, '2021-04-22 00:32:01', '2021-04-22 00:32:01'),
(20, NULL, NULL, NULL, 'sufiyan', NULL, 'sufiyan@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '$2y$10$642mHP09wdPSe96gTHh9W.4hazj1Ahqj9O2VoUsEpBf8VDKOMwQWm', NULL, NULL, NULL, '2021-04-22 00:56:19', '2021-04-22 00:56:19'),
(35, NULL, NULL, NULL, 'aa@a.com', NULL, 'sohaib', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '$2y$10$T35r0K4NJHtzIUJAy188yOA6SowDh5GAkNvJZLvv/.v8DvnUVw1QC', NULL, NULL, NULL, '2021-05-03 23:47:30', '2021-05-03 23:47:30'),
(36, NULL, NULL, NULL, 'am', NULL, 'am@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '$2y$10$z1O/th9r.6yNEA7NKonDM.H3WB7yu5wAgDy3kvMuG5ZjIFzBMfSbG', NULL, NULL, NULL, '2021-05-03 23:56:13', '2021-05-03 23:56:13'),
(38, NULL, NULL, NULL, 'admin', NULL, 'admin@admin.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '$2a$10$JxlW7fajVZ4YpLy3Mrc/ferjagzcVirepQF.lf/MR9t/Pvcwye4em', NULL, NULL, NULL, '2021-05-04 00:26:50', '2021-05-04 00:26:50'),
(39, NULL, NULL, NULL, 'hiba', NULL, 'hiba@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '$2y$10$2qxYZoJOd.5ZHjbKy4q4x.UDiglFtk3cAg3RSbgbQJ3hVTjYo.i0W', NULL, NULL, NULL, '2021-05-04 00:39:15', '2021-05-04 00:39:15'),
(41, NULL, NULL, NULL, 'fars', NULL, 'fff@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '$2y$10$pEMuD/ff9X4dkKIqHRGvL.fpbt2lbftMr5OOcW2s8ZNWXvuLeNbvy', NULL, NULL, NULL, '2021-05-04 00:45:19', '2021-05-04 00:45:19'),
(42, NULL, NULL, NULL, 'hi', NULL, 'hi@hi.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '$2y$10$/kapEOjs8TkNcRnyV4GCD.hy94Hw9uVjbjaN1ilQ7fVW9FNnlpYJ2', NULL, NULL, NULL, '2021-05-04 00:52:35', '2021-05-04 00:52:35'),
(43, NULL, NULL, NULL, 'test10', NULL, 'test10@.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '$2y$10$YUDJyF9NVR.RM5NMITK8tu8yCr3R55ky9mXPiY7vGHIWkN6bid9x2', NULL, NULL, NULL, '2021-05-04 01:07:51', '2021-05-04 01:07:51'),
(45, NULL, NULL, NULL, 'test11', NULL, 'test11@.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '$2y$10$jb9dNyynHX17KxDMAspRkuOnnWiRM8Kzj9SSyIZc.gRlFaRk46k4O', NULL, NULL, NULL, '2021-05-04 01:19:09', '2021-05-04 01:19:09'),
(46, NULL, NULL, NULL, 'saifc', NULL, 'chohan@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '$2y$10$DAf0YHVarOdhZ1.SPaunS.C8H2coYa9xJGNCHQvsPlh2ALC5tPEni', NULL, NULL, NULL, '2021-05-04 01:27:20', '2021-05-04 01:27:20'),
(47, NULL, NULL, NULL, 'sufi', NULL, 'sufi@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '$2y$10$7.qWea9KNMES0vN2eLxECO6QCDBCgqPjeK3aw6VWwY9lFajEK5ljy', NULL, NULL, NULL, '2021-05-04 01:33:10', '2021-05-04 01:33:10'),
(48, NULL, NULL, NULL, 'test13', NULL, 'test13@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '$2y$10$9B9nzcKfvDpCSbjwcEf7yOnHG/Dpk94hO.Wfs94/LgN80wPbM8Rpu', NULL, NULL, NULL, '2021-05-04 01:34:50', '2021-05-04 02:46:43'),
(49, NULL, NULL, NULL, 'rest', NULL, 'sdsD@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '$2y$10$20r3oRF2zVch9JZXtpSqKOcJFwhlFTnDuUHmcSnBLJDI2Y96DsqA2', NULL, NULL, NULL, '2021-05-04 01:37:16', '2021-05-04 01:37:16'),
(50, NULL, NULL, NULL, 'xcv', NULL, 'xvxv@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '$2y$10$mOM1lGg8K1iSxrcjmzDoP.8mAoA/BI54QU7NlLImr5rD.pS9sM/2i', NULL, NULL, NULL, '2021-05-04 01:39:56', '2021-05-04 01:39:56'),
(52, NULL, NULL, NULL, 'queen', NULL, 'queen@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '$2y$10$mEhb7PA8P5DZWCklh3C3ve12T6uuECsnBr2rd85nwig6OupG2urN2', NULL, NULL, NULL, '2021-05-04 01:46:47', '2021-05-04 02:44:01'),
(55, NULL, NULL, NULL, 'xyz', NULL, 'xyz@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '$2y$10$qUrTzWTYaPDTA.f9CU2R7.bkGXcgaD4Z63Vg4z.8CYNdh14aZtoN2', NULL, NULL, NULL, '2021-05-04 03:30:17', '2021-05-04 03:30:17'),
(56, NULL, NULL, NULL, 'sohaib', NULL, 'sohai4btanwadsdsdr275@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '$2y$10$ob5UynX2y0tzFLl8oMDIq.WNB5qYXieImdpnifHBLh7KjoJXx5ixa', NULL, NULL, NULL, '2021-05-05 02:40:56', '2021-05-05 02:40:56'),
(72, NULL, NULL, NULL, 'b', NULL, 'b@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '$2y$10$mbCOFEvNLmOAwH/2P6iKG.YSJb.fVDkGFgd9lUp1yU.mhc.CyMMoq', NULL, NULL, NULL, '2021-05-05 04:33:01', '2021-05-05 04:33:01'),
(73, NULL, NULL, NULL, 'a', NULL, 'a@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '$2y$10$3g48xOr53z/8eXO3wUzQ1eFlHWBY6z33M0LUH6WUthKybewwg.iqW', NULL, NULL, NULL, '2021-05-05 04:46:21', '2021-05-05 04:46:21'),
(75, NULL, NULL, NULL, 'v', NULL, 'va@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '$2y$10$FZUAr44foSGUWjbjJXTUJekt3C6HQr8PCMUsqjvfWsBylrgXXlJJG', NULL, NULL, NULL, '2021-05-05 05:09:37', '2021-05-05 05:09:37'),
(76, NULL, NULL, NULL, 'cv', NULL, 'vca@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '$2y$10$jCkfD4sYhzETFjOx177u8eEBjQsjNSgx06S6e0VHDmgzz31t1iJUa', NULL, NULL, NULL, '2021-05-05 05:10:40', '2021-05-05 05:10:40'),
(77, NULL, NULL, NULL, 'cvv', NULL, 'vvv@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '$2y$10$N61iaz/VsBhG.41GTe636.H6qFA4e8BjAL/p45HgdBCnocAV4kdhu', NULL, NULL, NULL, '2021-05-05 05:14:23', '2021-05-05 05:14:23'),
(78, NULL, NULL, NULL, 'sddssd', NULL, 'dfvfvdv@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '$2y$10$SzeVsPSPND.zNYodMrVMwOQd7iIkccQiR8QLwcZ7P9OMVcVChazoK', NULL, NULL, NULL, '2021-05-05 05:20:35', '2021-05-05 05:20:35'),
(80, NULL, NULL, NULL, 'saifc', NULL, 'saifc@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '$2y$10$GZ4yyunySnN3somU1jbIMuKbky8pwFpgt9gTF96LZMvu3FbvLmxmy', NULL, NULL, NULL, '2021-05-05 05:53:27', '2021-05-05 05:53:27'),
(81, NULL, NULL, NULL, 'saifch', NULL, 'saifch@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '$2y$10$YZthUQhxYoYGHxFGlpoxRuNt9TcosZNN/cBHOih1VoiWlAbaq9vRS', NULL, NULL, NULL, '2021-05-05 06:11:31', '2021-05-05 06:11:31'),
(83, NULL, NULL, NULL, 'xyzf', NULL, 'xyzj@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '$2y$10$mPatWpIRwfDt9GlTZtUYWOnqz3SWcvXrsa.VhW2KvkU6E45PxJGl6', NULL, NULL, NULL, '2021-05-05 06:27:25', '2021-05-05 06:27:25'),
(84, NULL, NULL, NULL, 'kaif', NULL, 'kaif@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '$2y$10$k2yeOQfDGtzExJH6mm7J/eHHQEkTGLQ.bdkIf139GOEHOgomB.kXa', NULL, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjRlMTA5YjI2MTQ4MmJlYzQwYjI5OWJiNDVlODdhMTg1OGRhNjU2ZGI5MmI5MDM0ZjQxMmYwM2FhYTJiMWZkMmFiNjliZTg5ZTE2MmMyMTc1In0.eyJhdWQiOiIzIiwianRpIjoiNGUxMDliMjYxNDgyYmVjNDBiMjk5YmI0NWU4N2ExODU4ZGE2NTZkYjkyYjkwMzRmNDEyZjAzYWFhMmIxZmQyYWI2OWJlODllMTYyYzIxNzUiLCJpYXQiOjE2MjAyMTkyMjMsIm5iZiI6MTYyMDIxOTIyMywiZXhwIjoxNjUxNzU1MjIxLCJzdWIiOiI4NCIsInNjb3BlcyI6W119.E7Zq5r27AbvlIBwnQc99N09M5Ya7Ly-rZXo2z2_61gS6l2710FC4a_XBi016hXyfpgjkp5GVzJNBaC3ZVg9nZY7VCQLn5s0a0rk49N976KaHiaWaIR6uGU55d8wq1ysixFSLjdbGJdmPjoD-6xrhlzTlJTUnCh9_OLWPYgPv4Hrwozo73KacNxrDNayJ8MctAhOW8OL4B1mxZuHj4sD9K6ezw8TeznKvYqHd9WL_i2iJ91bPdnEKzI5MYzOhSD8zUfCAPQmypZWtSFkSBq_6su42ktITubbPjbvd7BcApIy31l3VrAsgqxcJmrQoQ6lUJMKvgq5_xTkcvFRXnIQ_VeHiRsqz9s_joWpdW5Qe_aPWO9PeY5AFxqbIBkVO1RIrYhE0uSarYhUCFz7tkAaWDIVxvYPLLaO8h1FKBCInb2DWWIdDVYeHwh1Pb9Q2LUuBIc9PMdFhErvzaZE5SJ1rZQW3BZ7jyWcQNxTCRb_qTeJD_CAZSYZ9hOUsTImm4Mt6_tDd4pjue5YoRd6YVOOAud3aBjESzo7Y_che9Wm38ROhoTm2PL6mp-9SzFoqFiBeysDYEIzJv6w5A6MDTDCTTUWudMC1zGOOCSvi-4EHhyeWD4PD4xlHwWcS8PQX6zJzAn_BtcWlI7SdhzbriMG_GUnGqHaypXSsAF3FZO6IYgA', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjRlMTA5YjI2MTQ4MmJlYzQwYjI5OWJiNDVlODdhMTg1OGRhNjU2ZGI5MmI5MDM0ZjQxMmYwM2FhYTJiMWZkMmFiNjliZTg5ZTE2MmMyMTc1In0.eyJhdWQiOiIzIiwianRpIjoiNGUxMDliMjYxNDgyYmVjNDBiMjk5YmI0NWU4N2ExODU4ZGE2NTZkYjkyYjkwMzRmNDEyZjAzYWFhMmIxZmQyYWI2OWJlODllMTYyYzIxNzUiLCJpYXQiOjE2MjAyMTkyMjMsIm5iZiI6MTYyMDIxOTIyMywiZXhwIjoxNjUxNzU1MjIxLCJzdWIiOiI4NCIsInNjb3BlcyI6W119.E7Zq5r27AbvlIBwnQc99N09M5Ya7Ly-rZXo2z2_61gS6l2710FC4a_XBi016hXyfpgjkp5GVzJNBaC3ZVg9nZY7VCQLn5s0a0rk49N976KaHiaWaIR6uGU55d8wq1ysixFSLjdbGJdmPjoD-6xrhlzTlJTUnCh9_OLWPYgPv4Hrwozo73KacNxrDNayJ8MctAhOW8OL4B1mxZuHj4sD9K6ezw8TeznKvYqHd9WL_i2iJ91bPdnEKzI5MYzOhSD8zUfCAPQmypZWtSFkSBq_6su42ktITubbPjbvd7BcApIy31l3VrAsgqxcJmrQoQ6lUJMKvgq5_xTkcvFRXnIQ_VeHiRsqz9s_joWpdW5Qe_aPWO9PeY5AFxqbIBkVO1RIrYhE0uSarYhUCFz7tkAaWDIVxvYPLLaO8h1FKBCInb2DWWIdDVYeHwh1Pb9Q2LUuBIc9PMdFhErvzaZE5SJ1rZQW3BZ7jyWcQNxTCRb_qTeJD_CAZSYZ9hOUsTImm4Mt6_tDd4pjue5YoRd6YVOOAud3aBjESzo7Y_che9Wm38ROhoTm2PL6mp-9SzFoqFiBeysDYEIzJv6w5A6MDTDCTTUWudMC1zGOOCSvi-4EHhyeWD4PD4xlHwWcS8PQX6zJzAn_BtcWlI7SdhzbriMG_GUnGqHaypXSsAF3FZO6IYgA', '2021-05-05 06:53:53', '2021-05-05 07:23:43'),
(85, NULL, NULL, NULL, 'avii', NULL, 'avii@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '$2y$10$JPrKWB4NMHemIEL.iWEz..yphjNVqbNkYkDVo3YnJuA0uK.lCys5y', NULL, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImVlMWIwMTRkZWJlYjQ0NDgzNzIyNWY4NjZkMmM2Nzg2ZDZhYTA4Y2U4ODMzZDQ2Mzc4MzdiODAxYmMzOWJlOTM0Y2ZjZDc4YzQ5NGFkZDA2In0.eyJhdWQiOiIzIiwianRpIjoiZWUxYjAxNGRlYmViNDQ0ODM3MjI1Zjg2NmQyYzY3ODZkNmFhMDhjZTg4MzNkNDYzNzgzN2I4MDFiYzM5YmU5MzRjZmNkNzhjNDk0YWRkMDYiLCJpYXQiOjE2MjAyNzI0ODcsIm5iZiI6MTYyMDI3MjQ4NywiZXhwIjoxNjUxODA4NDg3LCJzdWIiOiI4NSIsInNjb3BlcyI6W119.QHCbNLoNfp-lianSrOwkPrSP8NF9fdWm7b7i92tt8-NfXnedM8I04MgPLdXqhNhjAJgmEn0CLVQ41P8f02zOFPcBFdrSBdKLfGUziEOGj2OY42ujipkWFe-8rAp4Yyc-qdpj0AGoaCUDGxuVj73juSLP_qK2AvVrgn-9KjRmPk7QFFyIns4p-cuNbqRJ2lw2MhjUeffqr29nI4LnFCzjKwFcPIrb4HCBFeQt5Z_1H82YK8RYoKWqw88jf59E_KrzClhGXsxa9zzMAFL95KoiWrsEZ7c6LumdG529RT0tF7K6zdj0NuqwnDnvZqHvcKlHtkguN8zjwTGFOTdYDeXC305SbrjG5pG411EidD7Ztbk6bh8NuRxe75_KWM1JaSHuyPUFbVR-IyGBabsgc7w2xBIOwNTsy5k_wqrjiwGsISe2fwnuoEU4BKsH4DPGGSuDEYNs1LSJEa39Kl0RzPPZmd79V2iNRnii-cjs-pWF_YMbGwVi6qTOLnaRS2-LFeNfyeJqrZyZubDeN1rhKFC25d-FyqbINL37D5-H0oG7BfsgwSI4Kuj0a9Y2ybUi3sEAB5RB33M9VYOiKHD5AFT6S3UIuqd1ACGJufXW6DfKQvrYNb6PsrUXRjFdcmpv9QZtIZP77QYwkiFInPQsScLx8vHTlwBNoR0UdNHymngPkYY', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImVlMWIwMTRkZWJlYjQ0NDgzNzIyNWY4NjZkMmM2Nzg2ZDZhYTA4Y2U4ODMzZDQ2Mzc4MzdiODAxYmMzOWJlOTM0Y2ZjZDc4YzQ5NGFkZDA2In0.eyJhdWQiOiIzIiwianRpIjoiZWUxYjAxNGRlYmViNDQ0ODM3MjI1Zjg2NmQyYzY3ODZkNmFhMDhjZTg4MzNkNDYzNzgzN2I4MDFiYzM5YmU5MzRjZmNkNzhjNDk0YWRkMDYiLCJpYXQiOjE2MjAyNzI0ODcsIm5iZiI6MTYyMDI3MjQ4NywiZXhwIjoxNjUxODA4NDg3LCJzdWIiOiI4NSIsInNjb3BlcyI6W119.QHCbNLoNfp-lianSrOwkPrSP8NF9fdWm7b7i92tt8-NfXnedM8I04MgPLdXqhNhjAJgmEn0CLVQ41P8f02zOFPcBFdrSBdKLfGUziEOGj2OY42ujipkWFe-8rAp4Yyc-qdpj0AGoaCUDGxuVj73juSLP_qK2AvVrgn-9KjRmPk7QFFyIns4p-cuNbqRJ2lw2MhjUeffqr29nI4LnFCzjKwFcPIrb4HCBFeQt5Z_1H82YK8RYoKWqw88jf59E_KrzClhGXsxa9zzMAFL95KoiWrsEZ7c6LumdG529RT0tF7K6zdj0NuqwnDnvZqHvcKlHtkguN8zjwTGFOTdYDeXC305SbrjG5pG411EidD7Ztbk6bh8NuRxe75_KWM1JaSHuyPUFbVR-IyGBabsgc7w2xBIOwNTsy5k_wqrjiwGsISe2fwnuoEU4BKsH4DPGGSuDEYNs1LSJEa39Kl0RzPPZmd79V2iNRnii-cjs-pWF_YMbGwVi6qTOLnaRS2-LFeNfyeJqrZyZubDeN1rhKFC25d-FyqbINL37D5-H0oG7BfsgwSI4Kuj0a9Y2ybUi3sEAB5RB33M9VYOiKHD5AFT6S3UIuqd1ACGJufXW6DfKQvrYNb6PsrUXRjFdcmpv9QZtIZP77QYwkiFInPQsScLx8vHTlwBNoR0UdNHymngPkYY', '2021-05-05 22:10:38', '2021-05-05 22:11:27'),
(86, NULL, NULL, NULL, 'jatu', NULL, 'jatu@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '$2y$10$2uoyEOzAazeeVAc9.cPFxOlYaUnndoQMRWx1wpHGOWjqVJzI1jgoe', NULL, NULL, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjkyMmVhNGJiNzEyY2I0MmVmNDc4NThjYmNlZTExYTBkNmY0MTZiOWFkMWVkZjUwZDUxNWM2NmIxOWU0NGMxYmRlZDhjZDQ5Y2ZmMDc5NmM3In0.eyJhdWQiOiIzIiwianRpIjoiOTIyZWE0YmI3MTJjYjQyZWY0Nzg1OGNiY2VlMTFhMGQ2ZjQxNmI5YWQxZWRmNTBkNTE1YzY2YjE5ZTQ0YzFiZGVkOGNkNDljZmYwNzk2YzciLCJpYXQiOjE2MjAyODI4ODQsIm5iZiI6MTYyMDI4Mjg4NCwiZXhwIjoxNjUxODE4ODg0LCJzdWIiOiI4NiIsInNjb3BlcyI6W119.nZ93qDrRONOD02JXJ2wMr_R05drPZ9-xJwgiOjdNzUpBbA7Ky7s9OEn-Ur0ywUzO_9QarRuMncJ7AjYH85g6VzQtZT5OqEdWQmHgolUkqaVKrehUo30LZdlNrzlL5lrJaRJ6NzL6VBZnVgkkcAt_CS6Y3R7Jwc_COhUxMvxSEaRkX_SzQu2rVDaQEYyqpBhtj1RsHfuZMvtDk1oQLav2QWSbLc4MGiwtnqM1pSVtHrn8KVsSg-ztPCEtTYqRKgLfFYnLedVqbPBgVbfaLyd3se71yCObE2kqTosImxPNM388S-Ew4NVRQFgD1BDZpwhgPFEq08Awu3L3SuGze7GRtH6KcyJkfemDlWdq-ypqlDJnhKvEFjwb4VP9OjWasGzfYklBs7EqE0Z63ubhC8CnTEmz0btF7NT1GIbVy62k6p5pIGbgHcgqvX91xDcpTWY11osr8gbUGkI8BDiez7gzkogX0QsRoxbRGOM5tYid_5viaaZDDBNA7qeM_EiDnY1hkypqlFUKWu9zlNe2tiQ025B4fNCXjkFA0dFKTk-5rfAyuc5TvovUySQI5lWHyN3pkSCZFQPYVzOyYcaHdGGgzAXLZ-ty1lvMW4UXFEFt5rOtSzrcwWNtTT8T8AEtKFMvgEv0IV6wXRhHCRPN3iQ23TsylOEQBLefxj0UyE9iclc', '2021-05-06 01:04:43', '2021-05-06 01:04:44'),
(87, NULL, NULL, NULL, 'sohai', NULL, 'sdsdds@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '$2y$10$EOf9aflLAOs3L.00dBcA/O4nDhSoialRsRvXFMUV43logLKiXh6qO', NULL, NULL, NULL, '2021-05-06 02:08:58', '2021-05-06 02:08:58'),
(88, NULL, NULL, NULL, 'Sufiyan Qureshi', NULL, 'admin@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '$2y$10$8GaT1vJmPtbAgS8L2zQQLOoy4r0ycw/Tm8VMjDq7x1HMW1xFeLXRa', 'qzj6LY98eEJ8pD6LS1zHWbt9Dq20nO0J94ed3ugYqPZ9zpvSB6bbjxcPw98k', NULL, NULL, '2021-05-25 00:47:53', '2021-05-25 00:47:53');

-- --------------------------------------------------------

--
-- Table structure for table `user_images`
--

CREATE TABLE `user_images` (
  `id` bigint(11) NOT NULL,
  `user_id` bigint(11) DEFAULT NULL,
  `images` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_images`
--

INSERT INTO `user_images` (`id`, `user_id`, `images`, `created_at`, `updated_at`) VALUES
(11, 32, 'assets/uploads\\1619086719daniel-olah-2lMK4dgqwFM-unsplash.jpg', '2021-04-22 04:48:39', '2021-04-22 04:48:39'),
(15, 37, '', '2021-05-04 00:00:22', '2021-05-04 00:00:22'),
(16, 37, '', '2021-05-04 00:00:22', '2021-05-04 00:00:22'),
(17, 37, '', '2021-05-04 00:00:22', '2021-05-04 00:00:22'),
(18, 37, '', '2021-05-04 00:00:22', '2021-05-04 00:00:22'),
(19, 42, '', '2021-05-04 00:52:35', '2021-05-04 00:52:35'),
(20, 45, ',6051fbef5c6f6 - Copy - Copy.png,image/png,0,', '2021-05-04 01:19:11', '2021-05-04 01:19:11'),
(21, 45, ',6051fbef5c6f6 - Copy.png,image/png,0,', '2021-05-04 01:19:11', '2021-05-04 01:19:11'),
(22, 45, ',6051fbef5c6f6.png,image/png,0,', '2021-05-04 01:19:11', '2021-05-04 01:19:11'),
(23, 45, ',default - Copy - Copy.png,image/png,0,', '2021-05-04 01:19:11', '2021-05-04 01:19:11'),
(24, 47, '6051fbef5c6f6 - Copy - Copy - Copy.png', '2021-05-04 01:33:10', '2021-05-04 01:33:10'),
(25, 47, '6051fbef5c6f6 - Copy - Copy - Copy.png,6051fbef5c6f6 - Copy - Copy (2).png', '2021-05-04 01:33:10', '2021-05-04 01:33:10'),
(26, 47, '6051fbef5c6f6 - Copy - Copy - Copy.png,6051fbef5c6f6 - Copy - Copy (2).png,6051fbef5c6f6 - Copy - Copy.png', '2021-05-04 01:33:10', '2021-05-04 01:33:10'),
(27, 47, '6051fbef5c6f6 - Copy - Copy - Copy.png,6051fbef5c6f6 - Copy - Copy (2).png,6051fbef5c6f6 - Copy - Copy.png,6051fbef5c6f6 - Copy (2).png', '2021-05-04 01:33:10', '2021-05-04 01:33:10'),
(28, 48, ',slider.jpg,image/jpeg,0,', '2021-05-04 01:34:51', '2021-05-04 02:46:43'),
(29, 48, 'wp2762074-wallpaper-masjid-hd.jpg,daniel-olah-2lMK4dgqwFM-unsplash.jpg', '2021-05-04 01:34:51', '2021-05-04 01:34:51'),
(30, 48, 'wp2762074-wallpaper-masjid-hd.jpg,daniel-olah-2lMK4dgqwFM-unsplash.jpg,6051fbef5c6f6.png', '2021-05-04 01:34:51', '2021-05-04 01:34:51'),
(31, 48, 'wp2762074-wallpaper-masjid-hd.jpg,daniel-olah-2lMK4dgqwFM-unsplash.jpg,6051fbef5c6f6.png,default.png', '2021-05-04 01:34:51', '2021-05-04 01:34:51'),
(32, 48, 'wp2762074-wallpaper-masjid-hd.jpg,daniel-olah-2lMK4dgqwFM-unsplash.jpg,6051fbef5c6f6.png,default.png,arrow.png', '2021-05-04 01:34:51', '2021-05-04 01:34:51'),
(33, 52, ',1570169134.jpg,image/jpeg,0,', '2021-05-04 01:46:47', '2021-05-04 02:44:02'),
(34, 52, 'loader.gif', '2021-05-04 02:39:08', '2021-05-04 02:39:08'),
(35, 52, 'noimage.jpg', '2021-05-04 02:39:09', '2021-05-04 02:39:09'),
(36, 52, 'no-image.png', '2021-05-04 02:39:09', '2021-05-04 02:39:09'),
(39, 55, '1576857025.jpg,1576857211.jpg,1576857342.jpg,1576857447.jpg,1576857498.jpg,1577036162.jpg', '2021-05-04 03:30:17', '2021-05-04 03:30:17'),
(40, 87, '6051fbef5c6f6.png,default.png,arrow.png,logo.png,IMG-20210209-WA0001.jpg,iconfinder_Apple_Settings_2697651.png', '2021-05-06 02:08:58', '2021-05-06 02:08:58'),
(41, 88, NULL, '2021-05-25 00:47:53', '2021-05-25 00:47:53');

-- --------------------------------------------------------

--
-- Table structure for table `user_purchased_packages`
--

CREATE TABLE `user_purchased_packages` (
  `id` bigint(20) NOT NULL,
  `order_id` int(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `package_id` bigint(20) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_purchased_packages`
--

INSERT INTO `user_purchased_packages` (`id`, `order_id`, `user_id`, `package_id`, `start_date`, `end_date`, `updated_at`, `created_at`) VALUES
(1, 1, NULL, NULL, '2021-04-17', '2022-01-17', '2021-04-17 01:11:38', '2021-04-17 01:11:38'),
(2, 2, NULL, NULL, '2021-04-17', '2022-01-17', '2021-04-17 01:49:59', '2021-04-17 01:49:59'),
(3, 3, NULL, NULL, '2021-04-17', '2022-01-17', '2021-04-17 01:58:59', '2021-04-17 01:58:59'),
(4, 4, NULL, NULL, '2021-04-17', '2021-07-17', '2021-04-17 02:06:00', '2021-04-17 02:06:00'),
(5, 5, NULL, NULL, '2021-04-17', '2021-05-17', '2021-04-17 02:22:50', '2021-04-17 02:22:50'),
(6, 2, 3, 12, '2021-04-17', '2021-04-17', '2021-07-16 21:38:18', '2021-07-16 21:38:18'),
(7, 3, 3, 12, '2021-04-17', '2021-04-17', '2021-07-17 01:35:00', '2021-07-17 01:35:00'),
(8, 4, 3, 12, '2021-07-17', '2021-07-17', '2021-07-17 01:42:23', '2021-07-17 01:42:23'),
(9, 5, 3, 12, '2021-07-16', '2021-07-16', '2021-07-17 01:42:45', '2021-07-17 01:42:45'),
(10, 6, 3, 12, '2021-07-17', '2025-05-17', '2021-07-17 01:49:13', '2021-07-17 01:49:13'),
(11, 7, 3, 12, '2021-07-17', '2025-05-17', '2021-07-17 01:52:57', '2021-07-17 01:52:57');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_users`
--
ALTER TABLE `admin_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organizations`
--
ALTER TABLE `organizations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organization_users`
--
ALTER TABLE `organization_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packages_old`
--
ALTER TABLE `packages_old`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `tranings`
--
ALTER TABLE `tranings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `users_original`
--
ALTER TABLE `users_original`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_images`
--
ALTER TABLE `user_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_purchased_packages`
--
ALTER TABLE `user_purchased_packages`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_users`
--
ALTER TABLE `admin_users`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `organizations`
--
ALTER TABLE `organizations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `organization_users`
--
ALTER TABLE `organization_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `packages_old`
--
ALTER TABLE `packages_old`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tranings`
--
ALTER TABLE `tranings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT for table `users_original`
--
ALTER TABLE `users_original`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `user_images`
--
ALTER TABLE `user_images`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `user_purchased_packages`
--
ALTER TABLE `user_purchased_packages`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
