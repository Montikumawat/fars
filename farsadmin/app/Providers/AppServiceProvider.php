<?php

namespace App\Providers;

use Illuminate\Support\Facades\Config;

use Illuminate\Support\ServiceProvider;

use Illuminate\Support\Facades\Http;

use Illuminate\Contracts\Support\DeferrableProvider;

class AppServiceProvider extends ServiceProvider implements DeferrableProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $response = Http::get(config('helper.setting_api_url').'/get/stripe/detail');
        $stripe= $response->json() ?? '';

        $razor_response = Http::get(config('helper.setting_api_url').'/get/razorpay/detail');
        $razorpay = $razor_response->json() ?? '';
        Config::set('payment', [
            'stripe_credentials_key' => $stripe['key'] ?? '',
            'stripe_credentials_secret_key' => $stripe['secret_key'] ?? '',
            'razorpay_credentials_key' => $razorpay['key'] ?? '',
            'razorpay_credentials_secret_key' => $razorpay['secret_key'] ?? '',
        ]);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    public function provides()
    {
        // dd('hii');
        return [Config::set('key', [])];
    }
}
