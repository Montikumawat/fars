<?php

// namespace App\Http\Middleware;

// use Illuminate\Auth\Middleware\Authenticate as Middleware;

// class Authenticate extends Middleware
// {
//     /**
//      * Get the path the user should be redirected to when they are not authenticated.
//      *
//      * @param  \Illuminate\Http\Request  $request
//      * @return string
//      */
//     protected function redirectTo($request)
//     {
//         if (! $request->expectsJson()) {
//             return route('login');
//         }
//     }
// }

namespace App\Http\Middleware;
use Closure;
use Illuminate\Support\Facades\Auth;
class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        
       // if the session does not have 'authenticated' forget the user and redirect to login
       if ($request->session()->get('authenticated',false) === true)
        {
            // dd($request->session()->get('authenticated',false));
            return $next($request);
        }
        $request->session()->forget('authenticated');
        $request->session()->forget('user');
        // return redirect()->action("CustomAuthController@showLoginForm")->with('error', 'Your session has expired.');
        return redirect()->route('loginForm')->with('error', 'Your session has expired.');
    }
}
