<?php

namespace App\Http\Controllers;

use App\Order;
use App\Package;
use App\Services\OrderService;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SubscriptionController extends Controller
{
    private $orderServices;

    public function __construct()
    {
        $this->orderServices = new OrderService();
    }

    public function subscribe(Request $request){
      // dd($request->package);
    $package=Package::find($request->package);
    // dd($package);
    $data=$this->orderServices->createOrder($package);
      return redirect()->back();
    }
}
