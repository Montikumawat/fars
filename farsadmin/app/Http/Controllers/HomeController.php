<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $id = request()->session()->get('user')->package_id;
        $response = Http::get(config('helper.package_api_url').'/get/package/detail/'.$id);
        $package = $response->json() ?? '';

        return view('admin.dashboard',compact('package'));
    }

    public function home()
    {
        return redirect()->route('home.admin');
    }
}
