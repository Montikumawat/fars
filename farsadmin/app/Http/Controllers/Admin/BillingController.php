<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use PDF;



class BillingController extends Controller
{
    public function invoiceIndex(){

     $id = request()->session()->get('user')->id;
     $response = Http::get(config('helper.customer_api_url').'/get/customer/detail/'.$id);
     $customer = $response->json() ?? '';
    //  $countries = Country::select(['id','name'])->get();

     $responses = Http::get(config('helper.package_api_url').'/get/packages');
     // dd($response->json());
     $packages = $responses->json() ?? '';

     $response_order = Http::get(config('helper.login_api_url').'/get/order/detail/user/'.$id);
     $orders = $response_order->json() ?? '';

     return view('admin.billing.invoices.index',compact('customer','packages','orders'));


    }

    public function filterPayment(Request $request)
    {
        $response =  Http::get(config('helper.login_api_url').'/filter/userAccount/payment', [
            'payment_type'=>$request->payment_type ?? '' ,
            'amount'=>$request->amount ?? '' ,
            'user_id'=>$request->user_id ?? ''
        ]);
        $payment_type = $request->payment_type ?? '';
        $amount = $request->amount ?? '';
        $id = $request->user_id ?? '';

        $responsec = Http::get(config('helper.customer_api_url').'/get/customer/detail/'.$id);
        $customer = $responsec->json() ?? '';
        // $countries = Country::select(['id','name'])->get();

        $responses = Http::get(config('helper.package_api_url').'/get/packages');
        // dd($response->json());
        $packages = $responses->json() ?? '';
        $orders = $response->json() ?? '';

        $payment_type = $request->payment_type ?? '' ;
        $amount = $request->amount ?? '';

        // dd($users);
        if(isset($request->page) && $request->page=='1')
        {
            $user = $responsec->json() ?? '';
            return view('admin.billing.invoices.index',compact('user','packages','orders','payment_type','amount'));
        } else {
            return view('admin.billing.invoices.index',compact('customer','packages','orders','payment_type','amount'));
        }

    }

    public function invoiceDetail($id){
        $responseo = Http::get(config('helper.login_api_url').'/get/order/detail/'.$id);
        $order = $responseo->json() ?? '';

        $responseu = Http::get(config('helper.customer_api_url').'/get/customer/detail/'.$order['user_id']);
        $user = $responseu->json() ?? '';
        $order['user'] = ['user'=>$user];

        $responsep = Http::get(config('helper.package_api_url').'/get/package/detail/'.$order['package_id']);
        $package = $responsep->json() ?? '';
        $order['package'] = ['package'=>$package];

        $responseup = Http::get(config('helper.login_api_url').'/get/user/package/detail/'.$order['id']);
        $userpurchasedpackage = $responseup->json() ?? '';
        $order['userpurchasedpackage'] = ['userpurchasedpackage'=>$userpurchasedpackage];
        // dd($order);
        return view('admin.billing.invoices.detail',compact('order'));
    }

    public function invoiceDownload($id){

        $responseo = Http::get(config('helper.login_api_url').'/get/order/detail/'.$id);
        $order = $responseo->json() ?? '';

        $responseu = Http::get(config('helper.customer_api_url').'/get/customer/detail/'.$order['user_id']);
        $user = $responseu->json() ?? '';
        $order['user'] = ['user'=>$user];

        $responsep = Http::get(config('helper.package_api_url').'/get/package/detail/'.$order['package_id']);
        $package = $responsep->json() ?? '';
        $order['package'] = ['package'=>$package];

        $responseup = Http::get(config('helper.login_api_url').'/get/user/package/detail/'.$order['id']);
        $userpurchasedpackage = $responseup->json() ?? '';
        $order['userpurchasedpackage'] = ['userpurchasedpackage'=>$userpurchasedpackage];
            view()->share('order',$order);
            $pdf = PDF::loadView('admin.billing.invoices.invoiceDownload');
            $pdf_name = $order['package']['package']['name'].'_invoice.pdf' ?? 'invoice.pdf';
            return $pdf->download($pdf_name);

        return view('admin.billing.invoices.index');
    }

    public function subscriptionIndex(){

        $id = request()->session()->get('user')->id;
        $response = Http::get(config('helper.customer_api_url').'/get/customer/detail/'.$id);
        $customer = $response->json() ?? '';
       //  $countries = Country::select(['id','name'])->get();
       $responseups = Http::get(config('helper.login_api_url').'/get/user/packages/'.$id);
       $userpurchasedpackages = $responseups->json() ?? '';
       foreach($userpurchasedpackages as $userpurchasedpackage)
       {
           $current_package = $userpurchasedpackage;
           break;
       }
       $old_packages = array_shift($userpurchasedpackages);

       $responsep = Http::get(config('helper.package_api_url').'/get/package/detail/'.$current_package['package_id']);
       $package = $responsep->json() ?? '';
       $current_package['package'] = $package;

       $pack = Http::get(config('helper.package_api_url').'/get/packages');
       // dd($response->json());
       $packs = $pack->json() ?? '';

       foreach($packs as $pac)
       {
        //    if(isset($pac) && isset($pac['billing_type'] )){
            $packages[$pac['id']] =['name'=>$pac['name'] ?? '',
            'billing_type'=>config('helper.billing_type')[$pac['billing_type']] ?? '',
            'package_type'=>config('helper.package_type')[$pac['package_type']] ?? '',
            'status'=>config('helper.status')[$pac['status']] ?? '',
            'currency'=>config('helper.currency')[$pac['currency']] ?? '',
            'amount'=>$pac['amount'] ?? '',
            'total_quota'=>$pac['users_for_facial_recognition'] ?? ''
            ] ;
        //    }

       }

       $userpurchasedpackages['packages'] = $packages ?? '';
    //    dd($current_package,$userpurchasedpackages,$packages['44']['billing_type']);




        return view('admin.billing.subscriptions.index',compact('userpurchasedpackages','current_package','packages'));
    }
}
