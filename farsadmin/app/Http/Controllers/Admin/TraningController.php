<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Tranings;

class TraningController extends Controller
{
    public function index()
    {
        $training = Tranings::all();
        return view('admin.traning.index',compact('training'));

    }
    public function view()
    {
        return view('admin.traning.view');
    }
    public function add()
    {
        return view('admin.traning.add');
    }
}
