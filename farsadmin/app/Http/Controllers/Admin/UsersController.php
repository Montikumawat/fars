<?php

namespace App\Http\Controllers\Admin;

use App\AdminUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\AdminUsersService;
use App\User;
use App\UserImage;
use Illuminate\Hashing\BcryptHasher;

class UsersController extends Controller
{
    private $userCreateServices;

    public function __construct()
    {
        $this->userCreateServices = new AdminUsersService();
    }
    public function index()
    {
        // $users=auth()->user()->myUsers;
        $users=User::where('token', '!=', null)->get();
        // $users=auth('api')->user();
         return view('admin.users.index',compact('users'));
     }
     public function add(){
         
          return view('admin.users.add');
      }
      public function store(Request $request){
        //  $imgData = [];
        $user = $this->userCreateServices->addUser($request);

        // dd($user->id);
// dd($request->file('images'));
//         foreach($request->file('images') as $file)
//         {
            
//          $name = $file->getClientOriginalName();
//             $file->move('assets/uploads', $name);
//              $imgData[] = $name;
// // dd($imgData);
// }
        
           $limage = $request->l_image;
            $limage_new_name = time().$limage->getClientOriginalName();
           $st1= $limage->move('assets/uploads', $limage_new_name);
        
           $rimage = $request->r_image;
            $rimage_new_name = time().$rimage->getClientOriginalName();
           $st2= $rimage->move('assets/uploads', $rimage_new_name);
        
           $upimage = $request->up_image;
            $upimage_new_name = time().$upimage->getClientOriginalName();
           $st3= $upimage->move('assets/uploads', $upimage_new_name);
        
           $doimage = $request->do_image;
            $doimage_new_name = time().$doimage->getClientOriginalName();
           $st4= $doimage->move('assets/uploads', $doimage_new_name);
        
         //   $wsimage = $request->ws_image;
         //    $wsimage_new_name = time().$wsimage->getClientOriginalName();
         //   $st5= $wsimage->move('assets/uploads', $wsimage_new_name);
        
         //   $whimage = $request->wh_image;
         //    $whimage_new_name = time().$whimage->getClientOriginalName();
         //   $st6= $whimage->move('assets/uploads', $whimage_new_name);
        
         //   $wbimage = $request->wb_image;
         //    $wbimage_new_name = time().$wbimage->getClientOriginalName();
         //   $st7= $wbimage->move('assets/uploads', $wbimage_new_name);
        
         //   $bimage = $request->auto_b_image;
         //    $bimage_new_name = time().$bimage->getClientOriginalName();
         //   $st8= $bimage->move('assets/uploads', $bimage_new_name);

           
         // array_push($imgData)
        
        UserImage::create([
         'user_id'=>$user->id,
        //  'images'=>collect($imgData)->implode(','),
         'l_image'=>$st1,
         'r_image'=>$st2,
         'up_image'=>$st3,
         'do_image'=>$st4,
        //  'ws_image'=>$st5,
        //  'wh_image'=>$st6,
        //  'wb_image'=>$st7,
        //  'auto_b_image'=>$st8,


     ]);
         return redirect()->route('admin.users.index');
     }
     public function edit($id){
        $user = User::find($id);
        $image = UserImage::where('user_id', '=', $id)->first();
        // dd($user->all());
         return view('admin.users.edit',compact('user','image'));
         
     } 
     public function update(Request $request,User $id){
    //   $imgData = [];
    //     $image=UserImage::where('user_id',$id->id)->first();
    // foreach ($request->file('images') as $photo) {

    //       $filename = $photo->getClientOriginalName();
    //       $photo->move('assets/uploads', $filename);
    //       $imgData[] = $filename;

    //       // $ProjectPhoto = new UserImage;
    //       // $ProjectPhoto->user_id = $id->id;
    //       // $ProjectPhoto->images   = $filename;
    //       // $ProjectPhoto->save();

    //   }
        if($request->hasFile('images')){
        $image = $request->images;
            $image_new_name = time().$image->getClientOriginalName();
           $st= $image->move('assets/uploads', $image_new_name);
           $img->images = $st;
        }

        $image->images = collect($imgData)->implode(',');

        if($request->hasFile('l_image')){
           $limage = $request->l_image;
            $limage_new_name = time().$limage->getClientOriginalName();
           $st1= $limage->move('assets/uploads', $limage_new_name);
           $image->l_image = $st1;
        }
        if($request->hasFile('r_image')){
           $rimage = $request->r_image;
            $rimage_new_name = time().$rimage->getClientOriginalName();
           $st2= $rimage->move('assets/uploads', $rimage_new_name);
           $image->r_image = $st2;

        }
        if($request->hasFile('up_image')){
           $upimage = $request->up_image;
            $upimage_new_name = time().$upimage->getClientOriginalName();
           $st3= $upimage->move('assets/uploads', $upimage_new_name);
           $image->up_image = $st3;
        }
        if($request->hasFile('do_image')){
           $doimage = $request->do_image;
            $doimage_new_name = time().$doimage->getClientOriginalName();
           $st4= $doimage->move('assets/uploads', $doimage_new_name);
           $image->do_image = $st4;
        }
      //   if($request->hasFile('ws_image')){
      //      $wsimage = $request->ws_image;
      //       $wsimage_new_name = time().$wsimage->getClientOriginalName();
      //      $st5= $wsimage->move('assets/uploads', $wsimage_new_name);
      //      $image->ws_image = $st5;
      //   }
      //   if($request->hasFile('wh_image')){
      //      $whimage = $request->wh_image;
      //       $whimage_new_name = time().$whimage->getClientOriginalName();
      //      $st6= $whimage->move('assets/uploads', $whimage_new_name);
      //      $image->wh_image = $st6;
      //   }
      //   if($request->hasFile('wb_image')){
      //      $wbimage = $request->wb_image;
      //       $wbimage_new_name = time().$wbimage->getClientOriginalName();
      //      $st7= $wbimage->move('assets/uploads', $wbimage_new_name);
      //      $image->wb_image = $st7;
      //   }
      //   if($request->hasFile('auto_b_image')){
      //      $bimage = $request->auto_b_image;
      //       $bimage_new_name = time().$bimage->getClientOriginalName();
      //      $st8= $bimage->move('assets/uploads', $bimage_new_name);
      //      $image->auto_b_image = $st8;
      //   }

         
        //  dd($id->id);
        //  dd($image->f_image);
        //  dd($image->all());
         $id->update($request->all());
         $image->save();
         return redirect()->route('admin.users.index');
     }
     public function destroy(User $id){
        $adminuser= AdminUser::where('user_id',$id->id)->first();
        $image= UserImage::where('user_id',$id->id)->first();
        $adminuser->delete();
        $image->delete();
         $id->delete();
         return redirect()->route('admin.users.index');
      } 

      public function indexSelf()
      {
                // $users=auth()->user()->myUsers;
        $users=User::where('token', '!=', null)->get();
         return view('admin.usersSelf.index',compact('users'));
      }
      public function addSelf()
      {
          
           return view('admin.usersSelf.add');
       }
       public function storeSelf(Request $request)
      {
  
      }

      public function showSelf()
      {
         return view('admin.usersSelf.show');
      }

      public function editSelf($id)
      {
         $user = User::find($id);
         return view('admin.usersSelf.edit',compact('user'));
          
      } 
      public function updateSelf(Request $request,User $id)
      {

      }
      public function destroySelf(User $id)
      {

      } 
}
