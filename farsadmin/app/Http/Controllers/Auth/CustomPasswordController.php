<?php
namespace App\Http\Controllers\Auth;
use App\Helpers\HttpHelper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
class CustomPasswordController extends Controller
{
    private $httpHelper;
    /**
     * CustomRegistrationController constructor.
     */
    // public function __construct() {
    //     //initialize HttpHelper
    //     $this->httpHelper = new HttpHelper();
    // }
    /**
     * Show password reset form
     * @return type
     */
    public function showResetForm() {
        return view('auth.password-reset');
    }
    /**
     * Send reset password email
     * @return type
     */
    public function sendResetLinkEmail(Request $request) {
        //get password reset token from api
       try {   
            $result = Http::post("reset-password-token", [
              //insert required password reset fields
            ]); 
        } catch(\GuzzleHttp\Exception\ClientException $e) {
          //return back with errors
        }
        //send password reset email with token from api
        $data = array( 
            'email' => $request->email,
            'token' => $result->token
        );
        Mail::send('auth/emails.password',$data, function($message) use ($data) {
            $message->from('support@website.com');
            $message->to($data['email']);
            $message->subject('Password Reset');
        });
        $request->session()->forget('authenticated');
        $request->session()->forget('user');
        //return success message
        return redirect()->back()->with('success', 'Please check your email to continue');
    }
    public function reset(Request $request) {
        try {   
            $result = Http::post("reset-password", [
              'token' => $request->token,
              'passowrd' => $request->password
            ]); 
        } catch(\GuzzleHttp\Exception\ClientException $e) {
          //return back with errors
        }
        //redirect to login with success message
        return redirect('/login')->with('success', 'Your password has been reset.');
    }
    
}