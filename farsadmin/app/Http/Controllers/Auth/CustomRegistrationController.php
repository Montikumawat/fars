<?php
namespace App\Http\Controllers\Auth;
use App\Helpers\HttpHelper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
class CustomRegistrationController extends Controller
{
    
    /**
     * CustomRegistrationController constructor.
     */
    
    /**
     * Show registration form
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showRegistrationForm() {
        return view("auth.register");
    }
    //
    public function register(Request $request) {
       try {   
            $result = Http::post("register", [
              //insert required registration fields
            ]);
           
        } catch(\GuzzleHttp\Exception\ClientException $e) {
          //return back with errors
        }
        //return to login page after registration
        return redirect('/login');
    }
    
}