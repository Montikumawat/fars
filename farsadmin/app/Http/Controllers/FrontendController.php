<?php

namespace App\Http\Controllers;

use App\Package;
use App\User;
use App\Order;
use App\OrderItem;
use App\UserPurchasedPackage;
use Illuminate\Support\Facades\Config;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Razorpay\Api\Api;
use Session;
use Stripe;
use Exception;

class FrontendController extends Controller
{
    public function index()
    {
        // return Http::dd()->get('http://localhost:8000/api/get/packages');
  
        
        $response = Http::get(config('helper.package_api_url').'/get/packages');
        $packages = $response->json() ?? '';
        
        return view('welcome',compact('packages'));
    }

    public function packageDetails($id)
    {
        $this->payment();

        $response = Http::get(config('helper.package_api_url').'/get/package/detail/'.$id);
        $package = $response->json() ?? '';
        return view('frontend.package_details',compact('package'));
    }

    public function razorpay(Request $request)
    {
        $input = $request->all();
  
        $this->payment();

        // dd(config('payment.razorpay_credentials_key'));

        $api = new Api(config('payment.razorpay_credentials_key'), config('payment.razorpay_credentials_secret_key'));
  
        $payment = $api->payment->fetch($input['razorpay_payment_id']);
  
        // dd($input,$payment);
       
        // dd($input['currency']);

        
        if(count($input)  && !empty($input['razorpay_payment_id'])) {
            try {
                $order =  Http::post(config('helper.login_api_url').'/create/order', [
                  'user_id' => request()->session()->get('user')->id,
                  'package_id' => $input['package_id'] ?? '',
                  'currency' => $input['currency'] ?? '',
                  'amount' => $input['amount'] ?? '0',
                  'payment_type' => 'Razorpay',
                  'transaction_id' => $input['razorpay_payment_id'],
                  'transaction_date' => Carbon::now()->format('Y-m-d'),
                  'status' => 1
                    
                ]);


                $orderItem = Http::post(config('helper.login_api_url').'/create/order/item', [
                    'order_id' => $order['id'],                      
                ]);


                $start = Carbon::now()->format('Y-m-d');
                $end = $request->package_time ?? '';

                $endDate = Carbon::now()->addMonths($end)->format('Y-m-d');

                

                $userPurchasedPackage = Http::post(config('helper.login_api_url').'/create/user/package', [
                    'order_id' => $order['id'],
                    'user_id' => request()->session()->get('user')->id,
                    'package_id' => $input['package_id'],
                    'start_date' => $start,
                    'end_date' => $endDate
                      
                ]);

               

                $user = Http::post(config('helper.login_api_url').'/update/user/package/expire_date', [
                    'user_id' => request()->session()->get('user')->id,
                    'subscription_expire_date' => $endDate,
                    'package_id' =>  $input['package_id'] ?? '',                  
                ]);

                // dd($order->json(),$orderItem->json(),$userPurchasedPackage->json(),$user->json());
                
            } catch (Exception $e) {
                $error =  $e->getMessage();
                // Session::put('error',$e->getMessage());
                return redirect()->back()->with('error',$error);
            }
        }
          
        // Session::put('success', 'Payment successful');
        return redirect()->route('home.admin')->with('success','Payment successful');
    }

    public function stripe (Request $request)
    {
        // dd($request->all());

        $this->payment();
   
        Stripe\Stripe::setApiKey(config('payment.stripe_credentials_secret_key'));
        $customer = Stripe\Customer::create(array(
            'name' => request()->session()->get('user')->name,
            'description' => 'FARS Payment',
            'email' => request()->session()->get('user')->email,
            'source' => $request->stripeToken,
            "address" => ["city" => "Sikar", "country" => "India", "line1" => "adsafd werew", "postal_code" => "500090", "state" => "telangana"],
            "shipping[name]"=>"test",
            "shipping[address][line1]"=>"adsafd werew", 
            "shipping[address][postal_code]"=>"500090", 
            "shipping[address][city]"=>"hyd", 
            "shipping[address][state]"=>"telangana", 
            "shipping[address][country]"=>"india",
        ));
        
        if($request->currency == 'DOLLAR'){
            $currency = 'usd';
        } 
        if($request->currency == 'INR'){
            $currency = 'inr';
        }
       $stripe = Stripe\Charge::create ([
                'customer' => $customer->id, 
                "amount" => $request->amount * 100,
                "currency" => $currency,
                "description" => "Test payment from fars.com.",
                "shipping[name]"=>"test USA",
                "shipping[address][line1]"=>"510 Townsend St", 
                "shipping[address][postal_code]"=>98140, 
                "shipping[address][city]"=>"San Francisco", 
                "shipping[address][state]"=>"CA", 
                "shipping[address][country]"=>"US",

        ]);

        if($stripe) {
            try {
                $order =  Http::post(config('helper.login_api_url').'/create/order', [
                  'user_id' => request()->session()->get('user')->id,
                  'package_id' => $request->package_id ?? '',
                  'currency' => $request->currency ?? '',
                  'amount' => $request->amount ?? '',
                  'payment_type' => 'Stripe',
                  'transaction_id' => $stripe->id ?? '',
                  'transaction_date' => Carbon::now()->format('Y-m-d'),
                  'status' => 1
                      
                  ]);
                

                $orderItem = Http::post(config('helper.login_api_url').'/create/order/item', [
                    'order_id' => $order['id'],                      
                ]);

                $start = Carbon::now()->format('Y-m-d');
                $end = $request->package_time ?? '';

                $endDate = Carbon::now()->addMonths($end)->format('Y-m-d');

                

                $userPurchasedPackage = Http::post(config('helper.login_api_url').'/create/user/package', [
                    'order_id' => $order['id'],
                    'user_id' => request()->session()->get('user')->id,
                    'package_id' => $input['package_id'],
                    'start_date' => $start,
                    'end_date' => $endDate
                      
                ]);

               

                $user = Http::post(config('helper.login_api_url').'/update/user/package/expire_date', [
                    'user_id' => request()->session()->get('user')->id,
                    'subscription_expire_date' => $endDate,
                    'package_id' =>  $input['package_id'] ?? '',                  
                ]);
                
            } catch (Exception $e) {
                $error =  $e->getMessage();
                // Session::put('error',$e->getMessage());
                return redirect()->back()->with('error',$error);
            }
        }
  
        Session::flash('success', 'Payment successful!');
          
        return back();
    }

    function payment()
    {
        $response = Http::get(config('helper.setting_api_url').'/get/stripe/detail');
        $stripe= $response->json() ?? '';

        $razor_response = Http::get(config('helper.setting_api_url').'/get/razorpay/detail');
        $razorpay = $razor_response->json() ?? '';
        Config::set('payment', [
            'stripe_credentials_key' => $stripe['key'] ?? '',
            'stripe_credentials_secret_key' => $stripe['secret_key'] ?? '',
            'razorpay_credentials_key' => $razorpay['key'] ?? '',
            'razorpay_credentials_secret_key' => $razorpay['secret_key'] ?? '',
        ]);
    }
}
