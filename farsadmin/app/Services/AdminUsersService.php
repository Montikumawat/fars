<?php

namespace App\Services;

use App\AdminUser;
use App\User;

class  AdminUsersService{
public function addUser($request){
    $user=new User();
    $user->name=$request->name;
    $user->email=$request->email;
    $user->password=bcrypt($request->password);
    $user->status=$request->status;
    $user->save();
    $user->assignRole('user');
    AdminUser::create([
    'admin_id'=>auth()->user()->id,
    'user_id'=>$user->id
    ]);
    return $user;
}
}
