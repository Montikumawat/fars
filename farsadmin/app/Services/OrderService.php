<?php

namespace App\Services;

use App\Order;
use App\OrderItem;
use App\UserPurchasedPlan;
use Carbon\Carbon;

class OrderService {
    public function createOrder($package){
      // dd($package);
      $order= Order::create([
            'user_id'=>auth()->user()->id,
             'payment_type'=>$package->package_type,
             'amount'=>$package->amount,
             'transaction_id'=>'6636363',
             'status'=>1
            ]);
            
          $item = $this->createOrderItems($order->id,$package->description);
          $userpurchased=$this->createUserPurchasedPlans($order->id,$package->package_time);
          $user=auth()->user();
          $user->subscription_expire_date= $newDateTime = Carbon::now()->addMonth($package->package_time);
          $user->save();
         $data=['order'=>$order,'order_item'=>$item]; 
         return $data;
    }

  public function createOrderItems($order,$description){
   $item= OrderItem::create([
    'order_id'=>$order,
    'Item_title'=>$description
    ]);
    return $item;
    }

    public function createUserPurchasedPlans($order,$time){
      $start_date= Carbon::now();
      $end_date= Carbon::now()->addMonth($time);

      UserPurchasedPlan::create([
      'order_id'=>$order,
      'start_date'=>$start_date,
      'end_date'=>$end_date
      ]);
    }

}