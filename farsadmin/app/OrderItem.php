<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $fillable=['order_id','package_id','item_title'];

    public function order(){
        return $this->belongsTo(Order::class);
    }
}
