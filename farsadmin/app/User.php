<?php

namespace App;
use App\Method\RoleMethod;
use App\Method\UserMethod;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens;
    use Notifiable, HasRoles,UserMethod;

    // const NOT_ACTIVE = 'not_active';
    // const ACTIVE = 'active';
    

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','status','address_line_2','company_name','domain_name','user_unique_id','org_unique_id','country_id','phone_number','account_number','billing_type','payment_method','address','billing_address','billing_address_line_2','street_address','street_address_line_2','city','state','zip_code','pan_card_number','gst_number','email_verified_at','subscription_expire_date','remember_token','api_token','token','created_at','updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function myUsers(){
        return $this->hasMany(AdminUser::class,'admin_id');
    }

    public function user(){
        return $this->hasMany(UserImage::class,'user_id');
    }
    
    public function training()
    {
        return $this->hasMany(Tranings::class,'user_id');
    }

    public function order()
    {
        return $this->hasMany(Order::class,'user_id');
    }
}
