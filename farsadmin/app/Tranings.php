<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tranings extends Model
{
    protected $table = 'tranings';

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
    
}
