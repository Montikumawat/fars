<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserImage extends Model
{
    protected $table = 'user_images';
    protected $fillable = ['user_id','images']; 

    public function image()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    
}
