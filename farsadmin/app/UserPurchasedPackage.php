<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPurchasedPackage extends Model
{
    protected $fillable=['order_id','user_id','package_id','start_date','end_date'];
    public function order(){
        return $this->belongsTo(Order::class);
    }

    public function purshase(){
        return $this->belongsTo(Order::class,'order_id');
    }
}
